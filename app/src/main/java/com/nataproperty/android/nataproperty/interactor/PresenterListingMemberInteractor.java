package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListingMemberInteractor {
    void getListingMember(String agencyCompanyRef,String memberRef,String pageNo,String locationRef);
    void rxUnSubscribe();

}
