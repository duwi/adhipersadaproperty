package com.nataproperty.android.nataproperty.view.ilustration.booking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by User on 5/21/2016.
 */
public class BookingFinishActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String STATUS_PAYMENT = "statusPayment";

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;

    ImageView imgProject;
    TextView txtProjectName, txtCostumerName, txtCostumerMobile, txtCostumerPhone, txtCostumerAddress, txtCostumerEmail, txtMemberEmail;
    TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount, txtTotal;

    String memberCostumerRef, costumerName, costumerMobile, costumerPhone, costumerEmail, costumerAddress, projectName;
    String email;
    String bookingRef, projectRef, dbMasterRef, clusterRef, productRef, unitRef, termRef, termNo;
    String propertys, category, product, unit, area, priceInc;
    String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    String statusPayment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_finish);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Booking Berhasil");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        email = sharedPreferences.getString("isEmail", null);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        Log.d("cek email agency", " " + email);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);

        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);

        statusPayment = intent.getStringExtra(STATUS_PAYMENT);
        Log.d("payment", "" + statusPayment);

        Log.d("cek memberCostumer", "" + memberCostumerRef);

        //imgProject = (ImageView) findViewById(R.id.image_project);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * costumer info
         */
        txtCostumerName = (TextView) findViewById(R.id.txt_customer_name);
        txtCostumerMobile = (TextView) findViewById(R.id.txt_costumer_mobile);
        txtCostumerPhone = (TextView) findViewById(R.id.txt_costumer_phone);
        txtCostumerEmail = (TextView) findViewById(R.id.txt_costumer_email);
        txtCostumerAddress = (TextView) findViewById(R.id.txt_costumer_address);

        txtMemberEmail = (TextView) findViewById(R.id.member_email);

        requestMemberInfo();

    }

    @Override
    protected void onResume() {
        super.onResume();

        requestPayment();

        txtPropertyName.setText(propertys);
        txtCategoryType.setText(category);
        txtProduct.setText(product);
        txtUnitNo.setText(unit);
        txtArea.setText(Html.fromHtml(area));
        txtEstimate.setText(priceInc);

        txtPaymentTerms.setText(paymentTerm);
        txtPriceInc.setText(priceIncVat);
        txtDisconutPersen.setText(discPercent);
        txtdiscount.setText(discAmt);
        txtNetPrice.setText(netPrice);

    }

    private void requestPayment() {
        String url = WebService.getPayment();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&clusterRef=" + clusterRef.toString() +
                "&productRef=" + productRef.toString() +
                "&unitRef=" + unitRef.toString() +
                "&termRef=" + termRef.toString() +
                "&termNo=" + termNo.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("cek param payment", " " + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unitRef + "-" +
                termRef + "-" + termNo);
        try {
            JSONObject jo = new JSONObject(result);
            int status = jo.getInt("status");
            String message = jo.getString("message");
            Log.d("Cek payment info", result);
            // Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();

            if (status == 200) {
                paymentTerm = jo.getJSONObject("dataPrice").getString("paymentTerm");
                priceIncVat = jo.getJSONObject("dataPrice").getString("priceInc");
                discPercent = jo.getJSONObject("dataPrice").getString("discPercent");
                discAmt = jo.getJSONObject("dataPrice").getString("discAmt");
                netPrice = jo.getJSONObject("dataPrice").getString("netPrice");
                termCondition = jo.getJSONObject("dataPrice").getString("termCondition");

                propertys = jo.getJSONObject("propertyInfo").getString("propertys");
                category = jo.getJSONObject("propertyInfo").getString("category");
                product = jo.getJSONObject("propertyInfo").getString("product");
                unit = jo.getJSONObject("propertyInfo").getString("unit");
                area = jo.getJSONObject("propertyInfo").getString("area");
                priceInc = jo.getJSONObject("propertyInfo").getString("priceInc");

            } else {

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {

        }
    }

    private void requestMemberInfo() {
        String url = WebService.getMemberInfo();
        String urlPostParameter = "&memberRef=" + memberCostumerRef.toString();
        ;
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result nup review", result);
            int status = jo.getInt("status");
            String message = jo.getString("message");

            if (status == 200) {
                costumerName = jo.getJSONObject("data").getString("name");
                costumerMobile = jo.getJSONObject("data").getString("hP1");
                costumerPhone = jo.getJSONObject("data").getString("phone1");
                costumerEmail = jo.getJSONObject("data").getString("email1");
                costumerAddress = jo.getJSONObject("data").getString("idAddr");

                txtCostumerName.setText(costumerName);
                txtCostumerMobile.setText(costumerMobile);
                txtCostumerPhone.setText(costumerPhone);
                txtCostumerEmail.setText(costumerEmail);
                txtCostumerAddress.setText(costumerAddress);

                //dari bank tranfer
                if (statusPayment.equals(WebService.updatePaymentTypeBankTransfer)) {
                    String thankYou = txtMemberEmail.getText().toString();
                    txtMemberEmail.setText(thankYou + " " + costumerEmail + " dan " + email);
                } else {
                    String thankYou = txtMemberEmail.getText().toString();
                    txtMemberEmail.setText(thankYou + " " + getResources().getString(R.string.booking_bank_tranfer));
                }

                SharedPreferences sharedPreferences = BookingFinishActivity.this.
                        getSharedPreferences(PREF_NAME, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", "");
                editor.commit();

            } else {
                Log.d("error", " " + message);

            }

        } catch (JSONException e) {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_top_right:
                Intent intent = new Intent(BookingFinishActivity.this, ProjectMenuActivity.class);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intent.putExtra(PROJECT_NAME, projectName);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(BookingFinishActivity.this, ProjectMenuActivity.class);
        intent.putExtra(PROJECT_REF, projectRef);
        intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
        intent.putExtra(PROJECT_NAME, projectName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
