package com.nataproperty.android.nataproperty.view.project;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.CategoryAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyGridView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.CategoryModel;
import com.nataproperty.android.nataproperty.model.project.CategoryModelDao;
//import com.nataproperty.android.nataproperty.model.project.DaoSession;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.nup.NupTermActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/3/2016.
 */
public class CategoryActivity extends AppCompatActivity {
    public static final String TAG = "CategoryActivity";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String IS_NUP = "isNUP";
    public static final String NUP_AMT = "nupAmt";

    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";

    public static final String IMAGE_LOGO = "imageLogo";

    private List<CategoryModel> listCategory = new ArrayList<>();
    private CategoryAdapter adapter;
    MyGridView listView;
    Display display;

    Button btnNUP;
    ImageView imgLogo;
    TextView txtProjectName;

    long categoryRef, dbMasterRef;
    String projectRef, clusterRef, projectName;
    String countCluster;
    String isNUP, nupAmt, isBooking;
    String isShowAvailableUnit, imageLogo;
    private double latitude, longitude;
    private RelativeLayout snackBarBuatan;
    private TextView retry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Category");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = getIntent().getLongExtra(ProjectMenuActivity.DBMASTER_REF, 0);
        projectRef = getIntent().getStringExtra(ProjectMenuActivity.PROJECT_REF);
        projectName = getIntent().getStringExtra(ProjectMenuActivity.PROJECT_NAME);

        latitude = getIntent().getDoubleExtra(LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(LONGITUDE, 0);

        isNUP = getIntent().getStringExtra(ProjectMenuActivity.IS_NUP);
        isBooking = getIntent().getStringExtra(ProjectMenuActivity.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(NUP_AMT);
        isShowAvailableUnit = getIntent().getStringExtra(ProjectMenuActivity.IS_SHOW_AVAILABLE_UNIT);
        imageLogo = getIntent().getStringExtra(ProjectMenuActivity.IMAGE_LOGO);

        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        btnNUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNup = new Intent(CategoryActivity.this, NupTermActivity.class);
                intentNup.putExtra(PROJECT_REF, projectRef);
                intentNup.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentNup.putExtra(NUP_AMT, nupAmt);
                intentNup.putExtra("imageLogo", imageLogo);
                startActivity(intentNup);
            }
        });

        Log.d("Cek Category", dbMasterRef + " " + projectRef + " " + categoryRef);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_category);
        /*adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countCluster = listCategory.get(position).getCountCluster();
                categoryRef = listCategory.get(position).getCategoryRef();
                clusterRef = listCategory.get(position).getClusterRef();
                isShowAvailableUnit = listCategory.get(position).getIsShowAvailableUnit();

                if (countCluster.equals("1")) {
                    Intent intentProduct = new Intent(CategoryActivity.this, ProductActivity.class);
                    intentProduct.putExtra(PROJECT_REF, projectRef);
                    intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                    intentProduct.putExtra(CLUSTER_REF, clusterRef);
                    intentProduct.putExtra(PROJECT_NAME, projectName);
                    intentProduct.putExtra(CATEGORY_REF, String.valueOf(categoryRef));

                    intentProduct.putExtra(LATITUDE, latitude);
                    intentProduct.putExtra(LONGITUDE, longitude);

                    intentProduct.putExtra(IS_NUP, isNUP);
                    intentProduct.putExtra("isBooking", isBooking);
                    intentProduct.putExtra(NUP_AMT, nupAmt);

                    intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentProduct.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentProduct);
                } else {
                    Intent intentCluster = new Intent(CategoryActivity.this, ClusterActivity.class);
                    intentCluster.putExtra(PROJECT_REF, projectRef);
                    intentCluster.putExtra(DBMASTER_REF, dbMasterRef);
                    intentCluster.putExtra(CATEGORY_REF, categoryRef);
                    intentCluster.putExtra(PROJECT_NAME, projectName);

                    intentCluster.putExtra(LATITUDE, latitude);
                    intentCluster.putExtra(LONGITUDE, longitude);

                    intentCluster.putExtra(IS_NUP, isNUP);
                    intentCluster.putExtra("isBooking", isBooking);
                    intentCluster.putExtra(NUP_AMT, nupAmt);
                    intentCluster.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentCluster);
                }
            }
        });

        listCategory.clear();
        requestCategory();
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);

        if (MainMenuActivity.OFF_LINE_MODE) {
            snackBarBuatan.setVisibility(View.VISIBLE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CategoryActivity.this, LaunchActivity.class));
                    finish();
                }
            });
        }
        else {
            snackBarBuatan.setVisibility(View.GONE);
        }
    }

    public void requestCategory() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCategory(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MainMenuActivity.OFF_LINE_MODE=false;
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCategory(jsonArray);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        Log.d(TAG, "" + jsonArray.length());
                        JSONObject data = jsonArray.getJSONObject(i);
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        CategoryModel categoryModel = gson.fromJson("" + data, CategoryModel.class);
                        categoryModel.setCategoryRef(data.optLong("categoryRef"));
                        categoryModel.setDbMasterRef(data.optString("dbMasterRef"));
                        categoryModel.setProjectRef(data.optString("projectRef"));
                        categoryModel.setProjectCategoryType(data.optString("projectCategoryType"));
                        categoryModel.setCategoryDescription(data.optString("categoryDescription"));
                        categoryModel.setCountCluster(data.optString("countCluster"));
                        categoryModel.setClusterRef(data.optString("clusterRef"));
                        categoryModel.setIsShowAvailableUnit(data.optString("isShowAvailableUnit"));
                        categoryModel.setImage(data.optString("image"));
                        CategoryModelDao categoryModelDao = daoSession.getCategoryModelDao();
                        categoryModelDao.insertOrReplace(categoryModel);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainMenuActivity.OFF_LINE_MODE=true;
//                        BaseApplication.getInstance().stopLoader();
//                        NetworkResponse networkResponse = error.networkResponse;
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        listCategory.clear();
                        CategoryModelDao categoryModelDao = daoSession.getCategoryModelDao();
                        List<CategoryModel> categoryModels = categoryModelDao.queryBuilder()
                                .where(CategoryModelDao.Properties.DbMasterRef.eq(dbMasterRef)).list();
                        int numData = categoryModels.size();
                        Log.d(TAG, "" + numData);
                        Log.d("getProjectLocalDB", "" + categoryModels.toString());
                        if (numData > 0) {
                            for (int i = 0; i < numData; i++) {
                                CategoryModel categoryModel = categoryModels.get(i);
                                listCategory.add(categoryModel);

                            }
                            initListView();

                        } else {
                            finish();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "category");

    }

    private void generateListCategory(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CategoryModel category = new CategoryModel();
                category.setDbMasterRef(jo.getString("dbMasterRef"));
                category.setProjectRef(jo.getString("projectRef"));
                category.setCategoryRef(jo.getLong("categoryRef"));
                category.setProjectCategoryType(jo.getString("projectCategoryType"));
                category.setCategoryDescription(jo.getString("categoryDescription"));
                category.setCountCluster(jo.getString("countCluster"));
                category.setClusterRef(jo.getString("clusterRef"));
                category.setIsShowAvailableUnit(jo.getString("isShowAvailableUnit"));
                category.setImage(jo.getString("image"));
                listCategory.add(category);
                initListView();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CategoryActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
