package com.nataproperty.android.nataproperty.model.profile;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 4/25/2016.
 */
@Entity
public class ProvinceModel {
    @Id
    long id;
    String countryCode;
    String provinceCode;
    String provinceName;

    @Generated(hash = 1276647184)
    public ProvinceModel(long id, String countryCode, String provinceCode,
            String provinceName) {
        this.id = id;
        this.countryCode = countryCode;
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    @Generated(hash = 1642997720)
    public ProvinceModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
}
