package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListContactChatInteractor {
    void getContactChat(String agencyCompanyRef);
    void rxUnSubscribe();

}
