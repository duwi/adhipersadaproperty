package com.nataproperty.android.nataproperty.view.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.profile.CountryAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.profile.CountryModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 4/22/2016.
 */
public class EditProfileActivity extends AppCompatActivity {
    public static final String TAG = "EditProfileActivity";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static final String PREF_NAME = "pref";
    public static final String KEY_MEMBERREF = "memberRef";
    public static final String KEY_FULLNAME = "fullname";
    public static final String KEY_KTPID = "ktpid";
    public static final String KEY_BIRTHPLACE = "birthPlace";
    public static final String KEY_MOBILE1 = "hp1";
    public static final String KEY_MOBILE2 = "hp2";
    public static final String KEY_EMAIL1 = "email1";
    public static final String KEY_EMAIL2 = "email2";
    public static final String KEY_BIRTHDATE = "birthDate";
    public static final int CHOOSE_FILE = 2;

    ProgressDialog progressDialog;
    String message;

    private EditText editFullname, editKtpId, editBirthPlace, editMobile1, editMobile2,
            editEmail1, editEmail2, editBirthdate, editNpwp;

    private TextView btnDeleteKtp, btnDeleteNpwp;
    private ImageView imgProfile, imgKtp, imgNpwp;
    private Button btnSave;
    Context context;

    private List<CountryModel> listCounty = new ArrayList<CountryModel>(); //model
    private CountryAdapter adapter; //set adapter

    private Spinner spinnerCountry, spinnerProvince, spinnerCity;

    SharedPreferences sharedPreferences;
    String memberRef, email;
    String fullname, ktpid, birthPlace, mobile1, mobile2, email1, email2, birthdate, npwp, ktpRef, npwpRef;
    String type;

    Uri file;

    static EditProfileActivity editProfileActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_profile));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        editProfileActivity = this;

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editKtpId = (EditText) findViewById(R.id.edit_ktpid);
        editBirthPlace = (EditText) findViewById(R.id.edit_birthplace);
        editMobile1 = (EditText) findViewById(R.id.edit_mobile1);
        editMobile2 = (EditText) findViewById(R.id.edit_mobile2);
        editEmail1 = (EditText) findViewById(R.id.edit_email1);
        editEmail2 = (EditText) findViewById(R.id.edit_email2);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
        editNpwp = (EditText) findViewById(R.id.edit_npwp);

        imgProfile = (ImageView) findViewById(R.id.profileImage);
        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDeleteKtp = (TextView) findViewById(R.id.btn_delete_ktp);
        btnDeleteNpwp = (TextView) findViewById(R.id.btn_delete_npwp);

        //brithdate
        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "Date Picker");
            }
        });
        editBirthdate.setFocusable(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        boolean state = sharedPreferences.getBoolean("isLogin", false);
        email = sharedPreferences.getString("isEmail", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Glide.with(EditProfileActivity.this).load(WebService.getProfile() + memberRef).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(EditProfileActivity.this.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });

        /**
         * upload image
         */
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "1";
                selectImage();

            }
        });

        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                selectImage();
            }
        });

        imgNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                selectImage();
            }
        });

        btnDeleteKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder =
                        new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this);
                alertDialogBuilder.setMessage(R.string.deleteKtp);
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteKtp();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        btnDeleteNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder =
                        new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this);
                alertDialogBuilder.setMessage(R.string.deleteNpwp);
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteNpwp();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        /**
         * update profile
         */
        btnSave.setTypeface(font);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String birthdate = editBirthdate.getText().toString();
                String fullname = editFullname.getText().toString();
                String email1 = editEmail1.getText().toString();

                if (!birthdate.isEmpty() && !fullname.isEmpty() && !email1.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                    updateProfile();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } else {
                    if (birthdate.isEmpty()) {
                        editBirthdate.setError("enter a birthdate");
                        editBirthdate.setFocusableInTouchMode(true);
                        editBirthdate.requestFocus();
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } else {
                        editBirthdate.setError(null);
                    }

                    if (fullname.isEmpty()) {
                        editFullname.setError("entar a fullname");
                        editFullname.setFocusableInTouchMode(true);
                        editFullname.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editFullname, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        editFullname.setError(null);
                    }

                    if (email1.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail1.setError("enter a valid email");
                        editEmail1.setFocusableInTouchMode(true);
                        editEmail1.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editEmail1, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        editEmail1.setError(null);
                    }

                    /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editBirthdate, InputMethodManager.SHOW_IMPLICIT);*/
                }
            }
        });

    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            TextView tv = (TextView) getActivity().findViewById(R.id.edit_birthdate);
            tv.setText(day + "/" + (month + 1) + "/" + year);
        }
    }

    public void updateProfile() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.profileReuired(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result", response);
                    int status = jo.getInt("status");
                    if (status == 200) {
                        Intent intent = new Intent(EditProfileActivity.this, SectionProfileActivity.class);
                        SectionProfileActivity.getInstance().finish();
                        startActivity(intent);
                        finish();
                    } else if (status == 201) {

                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("fullname", editFullname.getText().toString());
                params.put("KTPID", editKtpId.getText().toString().trim());
                params.put("birthPlace", editBirthPlace.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());
                params.put("mobile1", editMobile1.getText().toString().trim());
                params.put("mobile2", editMobile2.getText().toString().trim());
                params.put("email1", editEmail1.getText().toString().trim());
                params.put("email2", editEmail2.getText().toString().trim());
                params.put("NPWP", editNpwp.getText().toString().trim());
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "profileReuired");

    }

    /*private void updateProfile(final String memberRef) {
        String url = WebService.profileReuired();
        String urlPostParameter = "&memberRef=" + memberRef.toString() +
                "&fullname=" + editFullname.getText().toString() +
                "&KTPID=" + editKtpId.getText().toString() +
                "&birthPlace=" + editBirthPlace.getText().toString() +
                "&birthDate=" + editBirthdate.getText().toString() +
                "&mobile1=" + editMobile1.getText().toString() +
                "&mobile2=" + editMobile2.getText().toString() +
                "&email1=" + editEmail1.getText().toString() +
                "&email2=" + editEmail2.getText().toString() +
                "&NPWP=" + editNpwp.getText().toString();

        Log.d("memberRef", memberRef.toString());

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(EditProfileActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            try {
                JSONObject jo = new JSONObject(result);
                Log.d("result", result);
                int status = jo.getInt("status");
//                message = jo.getString("message");
                if (status == 200) {

                    Intent intent = new Intent(EditProfileActivity.this, SectionProfileActivity.class);
                    SectionProfileActivity.getInstance().finish();
                    startActivity(intent);
                    finish();

                } else if (status == 201) {
//                    message = jo.getString("message");

                } else {

                }

            } catch (JSONException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
//            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        boolean state = sharedPreferences.getBoolean("isLogin", false);
        email = sharedPreferences.getString("isEmail", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        requestPsInfo(memberRef);

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(EditProfileActivity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        file = Uri.fromFile(getOutputMediaFile());
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (items[item].equals("Use Existing Foto")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    intent.putExtra("type", type);
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, SELECT_FILE);

                }
            }
        });
        builder.show();
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                    //Toast.makeText(EditProfileActivity.this, "18", Toast.LENGTH_SHORT).show();
                } else {
                    handleGalleryResult19(data);
                    //Toast.makeText(EditProfileActivity.this, "19", Toast.LENGTH_SHORT).show();
                }

            else if (requestCode == REQUEST_CAMERA) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, EditKtpImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, EditNpwpImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }
            //onCaptureImageResult(data);

        }
    }

    private void onCaptureImageResult(Intent data) {
        Log.d("Type Camera", " " + type);

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        File destination = new File(Environment.getExternalStorageDirectory(), "/nataproperty/" +
                System.currentTimeMillis() + ".jpg");

        Log.d("pathImage", destination.toString());

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (type != null && type.equals("1")) {
            Intent i = new Intent(this, EditProfileImageActivity.class);
            i.putExtra("pathImage", destination.toString());
            //i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("2")) {
            Intent i = new Intent(this, EditKtpImageActivity.class);
            i.putExtra("pathImage", destination.toString());
            //i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("3")) {
            Intent i = new Intent(this, EditNpwpImageActivity.class);
            i.putExtra("pathImage", destination.toString());
            //i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Log.d("Type Galery", " " + type);

        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 300;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        if (type != null && type.equals("1")) {
            Intent i = new Intent(this, EditProfileImageActivity.class);
            i.putExtra("Image", byteArray);
            i.putExtra("pathImage", selectedImagePath);
            startActivity(i);
        } else if (type != null && type.equals("2")) {
            Intent i = new Intent(this, EditKtpImageActivity.class);
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("3")) {
            Intent i = new Intent(this, EditNpwpImageActivity.class);
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    private void handleGalleryResult19(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        mTmpGalleryPicturePath = getRealPathFromURI(EditProfileActivity.this,selectedImage);
        if (mTmpGalleryPicturePath==null){
            mTmpGalleryPicturePath = getPath(selectedImage);
        }

        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, EditKtpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, EditNpwpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("1")) {
                        Intent i = new Intent(this, EditProfileImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("2")) {
                        Intent i = new Intent(this, EditKtpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("3")) {
                        Intent i = new Intent(this, EditNpwpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else {
                        Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,   proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, EditKtpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, EditNpwpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("1")) {
                        Intent i = new Intent(this, EditProfileImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("2")) {
                        Intent i = new Intent(this, EditKtpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("3")) {
                        Intent i = new Intent(this, EditNpwpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else {
                        Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    /**
     * request info member
     */
    private void requestPsInfo(final String memberRef) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST, WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        //reuired
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        birthPlace = jo.getJSONObject("data").getString("birthPlace");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        mobile2 = jo.getJSONObject("data").getString("hP2");
                        email1 = jo.getJSONObject("data").getString("email1");
                        email2 = jo.getJSONObject("data").getString("email2");
                        birthdate = jo.getJSONObject("data").getString("birthDate");
                        npwp = jo.getJSONObject("data").getString("npwp");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        //Log.d(TAG, ktpid + " " + birthPlace + " " + birthPlace + " " + mobile1 + " " + mobile2
                        //+ " " + email1 + " " + email2 + " " + birthdate);

                        SharedPreferences sharedPreferences = EditProfileActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isName", fullname);
                        editor.commit();

                        editFullname.setText(fullname);
                        editKtpId.setText(ktpid);
                        editBirthPlace.setText(birthPlace);
                        editMobile1.setText(mobile1);
                        editMobile2.setText(mobile2);
                        editEmail1.setText(email1);
                        editEmail2.setText(email2);
                        editBirthdate.setText(birthdate);
                        editNpwp.setText(npwp);

//                        Glide.with(context).load(WebService.getProfile()+memberRef).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(imgProfile) {
//                            @Override
//                            protected void setResource(Bitmap resource) {
//                                RoundedBitmapDrawable circularBitmapDrawable =
//                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
//                                circularBitmapDrawable.setCircular(true);
//                                imgProfile.setImageDrawable(circularBitmapDrawable);
//                            }
//                        });

//                        Glide.with(EditProfileActivity.this).load(WebService.getProfile() + memberRef).diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .skipMemoryCache(true).into(imgProfile);

                        Glide.with(EditProfileActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgKtp);

                        Glide.with(EditProfileActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgNpwp);

                        if (ktpRef.equals("")) {
                            btnDeleteKtp.setVisibility(View.GONE);
                        } else {
                            btnDeleteKtp.setVisibility(View.VISIBLE);
                        }

                        if (npwpRef.equals("")) {
                            btnDeleteNpwp.setVisibility(View.GONE);
                        } else {
                            btnDeleteNpwp.setVisibility(View.VISIBLE);
                        }

                        Log.d("KTP/NPWP", " " + WebService.getKtp() + ktpRef + " " + WebService.getNpwp() + npwpRef);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(getIntent());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(EditProfileActivity.this, "error connection", Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MEMBERREF, memberRef);
                Log.d("TAG Member Ref", memberRef);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    public void deleteKtp() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteKtp(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ktpRef", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {

                        finish();
                        startActivity(getIntent());

                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("ktpRef", ktpRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteKtp");

    }

    public void deleteNpwp() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteNpwp(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ktpRef", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {

                        finish();
                        startActivity(getIntent());

                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("npwpRef", npwpRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteNpwp");

    }

    public static EditProfileActivity getInstance() {
        return editProfileActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EditProfileActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
