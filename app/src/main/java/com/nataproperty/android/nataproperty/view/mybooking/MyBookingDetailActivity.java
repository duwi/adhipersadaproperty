package com.nataproperty.android.nataproperty.view.mybooking;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.ilustration.IlustrationPaymentCalculatorAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.PaymentModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyBookingDetailActivity extends AppCompatActivity {
    public static final String TAG = "MyBookingDetailActivity";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private ArrayList<PaymentModel> listPayment = new ArrayList<PaymentModel>();
    private IlustrationPaymentCalculatorAdapter adapter;

    String dbMasterRef, projectRef, bookingRef, total,projectBookingRef;

    TextView txtBookDate, txtSalesReferral, txtSalesEvent, txtPurpose, txtSalesLocation,
            txtCategory, txtDetail, txtCluster, txtBlock, txtProduct, txtUnitName;

    TextView txtTotal;
    MyListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);

        Log.d(TAG,dbMasterRef+" "+projectRef+" "+projectBookingRef);

        txtBookDate = (TextView) findViewById(R.id.txt_booking_date);
        txtSalesReferral = (TextView) findViewById(R.id.txt_sales_referral);
        txtSalesEvent = (TextView) findViewById(R.id.txt_sales_event);
        txtPurpose = (TextView) findViewById(R.id.txt_purpose);
        txtSalesLocation = (TextView) findViewById(R.id.txt_sales_location);

        listView = (MyListView) findViewById(R.id.list_payment_schedule);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        adapter = new IlustrationPaymentCalculatorAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        requestMyBookingDetail();
        requestPayment();

    }

    public void rrequestMyBookingDetail() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getDetailBooking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("requestList", "" + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jo.getInt("status");
                    if (status == 200) {
                        String bookDate = jo.getJSONObject("bookingInfo").getString("bookDate");
                        String salesReferral = jo.getJSONObject("bookingInfo").getString("salesReferral");
                        String salesEvent = jo.getJSONObject("bookingInfo").getString("salesEvent");
                        String purpose = jo.getJSONObject("bookingInfo").getString("purpose");
                        String salesLocation = jo.getJSONObject("bookingInfo").getString("salesLocation");
                        String total = jo.getJSONObject("unitInfo").getString("total");

                        txtBookDate.setText(bookDate);
                        txtSalesReferral.setText(salesReferral);
                        txtSalesEvent.setText(salesEvent);
                        txtPurpose.setText(purpose);
                        txtSalesLocation.setText(salesLocation);

                        txtTotal.setText(total);

                    } else {
                        Log.d(TAG, "get error");
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(MyBookingDetailActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(MyBookingDetailActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(dbMasterRef, dbMasterRef.toString());
                params.put(projectRef, projectRef.toString());
                params.put(bookingRef, projectBookingRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestMyBookingDetail");

    }

    private void requestMyBookingDetail() {
        String url = WebService.getDetailBooking();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&bookingRef=" + projectBookingRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d(TAG, result);
            int status = jo.getInt("status");
            if (status == 200) {
                String bookDate = jo.getJSONObject("bookingInfo").getString("bookDate");
                String salesReferral = jo.getJSONObject("bookingInfo").getString("salesReferral");
                String salesEvent = jo.getJSONObject("bookingInfo").getString("salesEvent");
                String purpose = jo.getJSONObject("bookingInfo").getString("purpose");
                String salesLocation = jo.getJSONObject("bookingInfo").getString("salesLocation");
                String total = jo.getJSONObject("unitInfo").getString("total");

                txtBookDate.setText(bookDate);
                txtSalesReferral.setText(salesReferral);
                txtSalesEvent.setText(salesEvent);
                txtPurpose.setText(purpose);
                txtSalesLocation.setText(salesLocation);

                txtTotal.setText(total);

            } else {
                Log.d(TAG, "get error");
            }

        } catch (JSONException e) {

        }
    }

    public void rrequestPayment() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListPaymentSchedule(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("requestList", "" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(TAG, response);
                    generatePayment(jsonArray);


                } catch (JSONException e) {

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(MyBookingDetailActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(MyBookingDetailActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(dbMasterRef, dbMasterRef.toString());
                params.put(projectRef, projectRef.toString());
                params.put(bookingRef, projectBookingRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestPayment");

    }

    private void requestPayment() {
        String url = WebService.getListPaymentSchedule();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&bookingRef=" + projectBookingRef.toString();

        Log.d(TAG, dbMasterRef + " " + projectRef + " " + bookingRef);

        String result = Network.PostHttp(url, urlPostParameter);

        Log.d(TAG, result);

        try {
          JSONArray jsonArray = new JSONArray(result);
            Log.d(TAG, result);
            generatePayment(jsonArray);


        } catch (JSONException e) {

        }
    }

    private void generatePayment(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                PaymentModel payment = new PaymentModel();
                payment.setSchedule(joArray.getString("type"));
                payment.setDueDate(joArray.getString("dueDate"));
                payment.setAmount(joArray.getString("amount"));

                listPayment.add(payment);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingDetailActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
