package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterIlustrationPaymentInteractor;
import com.nataproperty.android.nataproperty.model.kpr.PaymentCCStatusModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationPaymentActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationPaymentPresenter implements PresenterIlustrationPaymentInteractor {
    private IlustrationPaymentActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public IlustrationPaymentPresenter(IlustrationPaymentActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }
    @Override
    public void getPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentCCStatusModel> call = service.getAPI().getPaymentCC(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                view.showPaymentResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                view.showPaymentFailure(t);

            }


        });
    }

    @Override
    public void getPaymentKpr(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo, String KPRYear) {
        Call<PaymentCCStatusModel> call = service.getAPI().getIlusPaymentKpr(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo,KPRYear);
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                view.showPaymentKprResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                view.showPaymentKprFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
