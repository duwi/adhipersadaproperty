package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface DiagramMaticInflateInteractor {
    void getDiagramColor(String memberRef);
    void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef);
    void rxUnSubscribe();

}
