package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterChattingRoomInteractor;
import com.nataproperty.android.nataproperty.model.project.ChatRoomModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.chat.ChattingRoomActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ChattingRoomPresenter implements PresenterChattingRoomInteractor {
    private ChattingRoomActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ChattingRoomPresenter(ChattingRoomActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }






    @Override
    public void getSendMsg(String memberRefSender, String memberRefReceiver, String messaging) {
        Call<List<ChatRoomModel>> call = service.getAPI().postMessaging(memberRefSender,memberRefReceiver,messaging);
        call.enqueue(new Callback<List<ChatRoomModel>>() {
            @Override
            public void onResponse(Call<List<ChatRoomModel>> call, Response<List<ChatRoomModel>> response) {
                view.showChatResults(response);
            }

            @Override
            public void onFailure(Call<List<ChatRoomModel>> call, Throwable t) {
                view.showChatFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
