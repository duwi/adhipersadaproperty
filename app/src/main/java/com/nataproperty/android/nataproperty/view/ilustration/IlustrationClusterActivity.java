package com.nataproperty.android.nataproperty.view.ilustration;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.ClusterAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyGridView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.ClusterModel;
import com.nataproperty.android.nataproperty.model.project.ClusterModelDao;
//import com.nataproperty.android.nataproperty.model.project.DaoSession;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/4/2016.
 */
public class IlustrationClusterActivity extends AppCompatActivity {

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String CLUSTER_DESCRIPTION = "clusterDescription";
    public static final String PROJECT_NAME = "projectName";
    public static final String PRODUCT_REF = "productRef";

    public static final String IS_BOOKING = "isBooking";

    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";

    public static final String IMAGE_LOGO = "imageLogo";

    private List<ClusterModel> listCluster = new ArrayList<ClusterModel>();
    private ClusterAdapter adapter;

    ImageView imgLogo;
    Button btnNUP;
    TextView txtProjectName;

    long dbMasterRef;
    String projectRef, categoryRef, clusterRef, clusterDescription, projectName, isBooking;
    String isShowAvailableUnit, imageLogo;

    MyGridView listView;
    Display display;
    private RelativeLayout snackBarBuatan;
    private TextView retry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cluster);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Cluster");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = String.valueOf(intent.getLongExtra(CATEGORY_REF,0));
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);

        imageLogo = intent.getStringExtra(IMAGE_LOGO);

        Log.d("Cek Cluster", dbMasterRef + " " + projectRef + " " + categoryRef);

        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        btnNUP.setVisibility(View.GONE);
        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_cluster);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clusterRef = String.valueOf(listCluster.get(position).getClusterRef());
                clusterDescription = listCluster.get(position).getClusterDescription();
                isShowAvailableUnit = listCluster.get(position).getIsShowAvailableUnit();

                Intent intentProduct = new Intent(IlustrationClusterActivity.this, IlustrationProductActivity.class);
                intentProduct.putExtra(PROJECT_REF, projectRef);
                intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                intentProduct.putExtra(CLUSTER_REF, clusterRef);
                intentProduct.putExtra(CATEGORY_REF, categoryRef);
                intentProduct.putExtra(CLUSTER_DESCRIPTION, clusterDescription);
                intentProduct.putExtra(PROJECT_NAME, projectName);
                intentProduct.putExtra(PRODUCT_REF, "");
                intentProduct.putExtra(IS_BOOKING, isBooking);
                intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentProduct.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentProduct);
            }
        });

        requestCluster();

        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);

        if (MainMenuActivity.OFF_LINE_MODE) {
            snackBarBuatan.setVisibility(View.VISIBLE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(IlustrationClusterActivity.this, LaunchActivity.class));
                    finish();
                }
            });
        }
        else {
            snackBarBuatan.setVisibility(View.GONE);
        }
    }

    public void requestCluster() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCluster(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MainMenuActivity.OFF_LINE_MODE=false;
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCluster(jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ClusterModel clusterModel = gson.fromJson("" + data, ClusterModel.class);
                        clusterModel.setDbMasterRef(data.optString("dbMasterRef"));
                        clusterModel.setProjectRef(data.optString("projectRef"));
                        clusterModel.setCategoryRef(data.optString("categoryRef"));
                        clusterModel.setClusterRef(data.optLong("clusterRef"));
                        clusterModel.setClusterDescription(data.optString("clusterDescription"));
                        clusterModel.setIsShowAvailableUnit(data.optString("isShowAvailableUnit"));
                        clusterModel.setImage(data.optString("image"));
                        ClusterModelDao clusterModelDao = daoSession.getClusterModelDao();
                        clusterModelDao.insertOrReplace(clusterModel);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        MainMenuActivity.OFF_LINE_MODE=true;

                        /*NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(IlustrationClusterActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(IlustrationClusterActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }*/
                        ClusterModelDao clusterModelDao = daoSession.getClusterModelDao();
                        List<ClusterModel> clusterModels = clusterModelDao.queryBuilder()
                                .where(ClusterModelDao.Properties.DbMasterRef.eq(dbMasterRef),
                                        ClusterModelDao.Properties.CategoryRef.eq(categoryRef)).list();
                        int numData = clusterModels.size();
                        if (numData > 0) {
                            for (int i = 0; i < numData; i++) {
                                ClusterModel categoryModel = clusterModels.get(i);
                                listCluster.add(categoryModel);
                            }

                            initListView();
                        } else {
                            finish();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("categoryRef", categoryRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "cluster");

    }

    private void generateListCluster(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ClusterModel cluster = new ClusterModel();
                cluster.setDbMasterRef(jo.getString("dbMasterRef"));
                cluster.setProjectRef(jo.getString("projectRef"));
                cluster.setCategoryRef(jo.getString("categoryRef"));
                cluster.setClusterRef(jo.getLong("clusterRef"));
                cluster.setClusterDescription(jo.getString("clusterDescription"));
                cluster.setIsShowAvailableUnit(jo.getString("isShowAvailableUnit"));

                listCluster.add(cluster);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        initListView();
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new ClusterAdapter(this, listCluster, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationClusterActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
