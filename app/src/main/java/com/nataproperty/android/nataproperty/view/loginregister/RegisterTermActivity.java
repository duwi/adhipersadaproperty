package com.nataproperty.android.nataproperty.view.loginregister;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

public class RegisterTermActivity extends AppCompatActivity {

    private Typeface font;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_term);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Syarat & Ketentuan");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebView webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl("http://www.nataproperty.com/term-and-condition");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
