package com.nataproperty.android.nataproperty.view.listing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ListingPropertyMapsFragment extends Fragment {
    public static final String TAG = "ListingPropertyMaps";

    public static final String LINK_DETAIL = "linkDetail";
    public static final String PROPERTY_REF = "propertyRef";

    public static final String PREF_NAME = "pref";

    String linkDetail, propertyRef;

    double longitude, latitude;
    private GoogleMap googleMap;

    public ListingPropertyMapsFragment() {
        // Required empty public constructor
    }

    private static View view;

    String listingRef, agencyCompanyRef, imageLogo, memberRefOwner, memberRef;

    @Bind(R.id.txt_no_maps)
    TextView txtNoMaps;
    @Bind(R.id.btn_edit)
    Button btnEdit;

    @Bind(R.id.maps)
    FrameLayout frameLayoutMaps;

    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        listingRef = intent.getStringExtra("listingRef");

        requestPropertyInfo(listingRef);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPreferences = getActivity().getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            view = inflater.inflate(R.layout.fragment_listing_property_maps, container, false);
            ButterKnife.bind(this, view);
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //requestPropertyInfo(propertyRef);

    }

    public void requestPropertyInfo(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingPropertyInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    latitude = jsonObject.getDouble("latitude");
                    longitude = jsonObject.getDouble("longitude");
                    memberRefOwner = jsonObject.getString("memberRefOwner");

                    Log.d(TAG, "latitude " + latitude + " " + longitude);

                    /**
                     *maps
                     */
                    try {
                        // Loading map
                        initilizeMap();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    btnEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(WebService.linkeditMaps + listingRef));
                            startActivity(i);
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "galleryPropertyInfo");

    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.maps_property)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }

            if (latitude == 0.0 && longitude == 0.0) {
                txtNoMaps.setText("*Untuk menambahkan info lokasi, silakan edit via web ");
                frameLayoutMaps.setVisibility(View.GONE);
                if (!memberRefOwner.equals(memberRef)){
                    txtNoMaps.setText("*Lokasi tidak tersedia");
                    frameLayoutMaps.setVisibility(View.GONE);
                    btnEdit.setVisibility(View.GONE);
                }
            }




        }
    }

}
