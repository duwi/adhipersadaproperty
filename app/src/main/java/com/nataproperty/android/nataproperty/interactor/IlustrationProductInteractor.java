package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface IlustrationProductInteractor {
    void getIlustration(String dbMasterRef,String projectRef,String clusterRef,String edtSearch);
    void getClusterInfo(String dbMasterRef,String projectRef,String clusterRef);
    void rxUnSubscribe();

}
