package com.nataproperty.android.nataproperty.model.nup;

/**
 * Created by User on 11/3/2016.
 */
public class UnitTypeModel {
    String surveyUnitTypeRef,surveyUnitTypeName;

    public String getSurveyUnitTypeRef() {
        return surveyUnitTypeRef;
    }

    public void setSurveyUnitTypeRef(String surveyUnitTypeRef) {
        this.surveyUnitTypeRef = surveyUnitTypeRef;
    }

    public String getSurveyUnitTypeName() {
        return surveyUnitTypeName;
    }

    public void setSurveyUnitTypeName(String surveyUnitTypeName) {
        this.surveyUnitTypeName = surveyUnitTypeName;
    }
}
