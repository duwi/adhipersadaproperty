package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by herlambang-nata on 11/2/2016.
 */
@Entity
public class MenuProjectModel2 {
    @Id
    long dbMasterRef;
    String projectDescription;
    String countCategory;
    String countCluster;
    String categoryRef;
    String clusterRef;
    String salesStatus;
    String isBooking;
    String isNUP;
    String isShowAvailableUnit;
    Double latitude;
    Double longitude;
    String nupAmt;
    String linkProjectDetail;
    String urlVideo;
    String imageLogo;
    String bookingContact;

    @Generated(hash = 1309838230)
    public MenuProjectModel2(long dbMasterRef, String projectDescription,
            String countCategory, String countCluster, String categoryRef,
            String clusterRef, String salesStatus, String isBooking, String isNUP,
            String isShowAvailableUnit, Double latitude, Double longitude,
            String nupAmt, String linkProjectDetail, String urlVideo,
            String imageLogo, String bookingContact) {
        this.dbMasterRef = dbMasterRef;
        this.projectDescription = projectDescription;
        this.countCategory = countCategory;
        this.countCluster = countCluster;
        this.categoryRef = categoryRef;
        this.clusterRef = clusterRef;
        this.salesStatus = salesStatus;
        this.isBooking = isBooking;
        this.isNUP = isNUP;
        this.isShowAvailableUnit = isShowAvailableUnit;
        this.latitude = latitude;
        this.longitude = longitude;
        this.nupAmt = nupAmt;
        this.linkProjectDetail = linkProjectDetail;
        this.urlVideo = urlVideo;
        this.imageLogo = imageLogo;
        this.bookingContact = bookingContact;
    }

    @Generated(hash = 1266377443)
    public MenuProjectModel2() {
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getCountCategory() {
        return countCategory;
    }

    public void setCountCategory(String countCategory) {
        this.countCategory = countCategory;
    }

    public String getCountCluster() {
        return countCluster;
    }

    public void setCountCluster(String countCluster) {
        this.countCluster = countCluster;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getSalesStatus() {
        return salesStatus;
    }

    public void setSalesStatus(String salesStatus) {
        this.salesStatus = salesStatus;
    }

    public String getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(String isBooking) {
        this.isBooking = isBooking;
    }

    public String getIsNUP() {
        return isNUP;
    }

    public void setIsNUP(String isNUP) {
        this.isNUP = isNUP;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getNupAmt() {
        return nupAmt;
    }

    public void setNupAmt(String nupAmt) {
        this.nupAmt = nupAmt;
    }

    public String getLinkProjectDetail() {
        return linkProjectDetail;
    }

    public void setLinkProjectDetail(String linkProjectDetail) {
        this.linkProjectDetail = linkProjectDetail;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }

    public String getBookingContact() {
        return bookingContact;
    }

    public void setBookingContact(String bookingContact) {
        this.bookingContact = bookingContact;
    }
}
