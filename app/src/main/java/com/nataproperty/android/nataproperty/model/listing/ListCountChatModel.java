package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by nata on 11/29/2016.
 */

public class ListCountChatModel {
    String status;
    int message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }
}
