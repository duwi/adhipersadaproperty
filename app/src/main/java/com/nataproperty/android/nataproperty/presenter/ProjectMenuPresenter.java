package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterProjectMenuInteractor;
import com.nataproperty.android.nataproperty.model.project.CommisionStatusModel;
import com.nataproperty.android.nataproperty.model.project.DetailStatusModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ProjectMenuPresenter implements PresenterProjectMenuInteractor {
    private ProjectMenuActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ProjectMenuPresenter(ProjectMenuActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getProjectDtl(String dbMasterRef, String projectRef) {
        Call<DetailStatusModel> call = service.getAPI().getProjectDetail(dbMasterRef,projectRef);
        call.enqueue(new Callback<DetailStatusModel>() {
            @Override
            public void onResponse(Call<DetailStatusModel> call, Response<DetailStatusModel> response) {
                view.showMenuResults(response);
            }

            @Override
            public void onFailure(Call<DetailStatusModel> call, Throwable t) {
                view.showMenuFailure(t);

            }


        });

    }

    @Override
    public void getCommision(String dbMasterRef, String projectRef, String memberRef) {
        Call<CommisionStatusModel> call = service.getAPI().getCommision(dbMasterRef,projectRef,memberRef);
        call.enqueue(new Callback<CommisionStatusModel>() {
            @Override
            public void onResponse(Call<CommisionStatusModel> call, Response<CommisionStatusModel> response) {
                view.showCommisionResults(response);
            }

            @Override
            public void onFailure(Call<CommisionStatusModel> call, Throwable t) {
                view.showCommisionFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
