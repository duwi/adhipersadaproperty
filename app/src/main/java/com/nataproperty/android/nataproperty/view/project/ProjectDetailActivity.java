package com.nataproperty.android.nataproperty.view.project;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

/**
 * Created by User on 4/21/2016.
 */
public class ProjectDetailActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LINK_DETAIL = "linkDetail";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String URL_VIDEO = "urlVideo";
    private RelativeLayout snackBarBuatan;
    private TextView retry;

    private WebView webView;

    LinearLayout linearMaps;
    private GoogleMap googleMap;

    ImageView imgLogo;
    TextView txtProjectName;

    private long dbMasterRef;
    private String projectRef, projectName, locationName, locationRef, sublocationName, sublocationRef,linkDetail;

    private String projectDescription,urlVideo="",imageLogo;

    private double latitude,longitude;

    Button btnPropertyVideo;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Intent intent = getIntent();
        projectRef = intent.getStringExtra("projectRef");
        dbMasterRef = intent.getLongExtra("dbMasterRef",0);
        projectName = intent.getStringExtra("projectName");
        locationName = intent.getStringExtra("locationName");
        locationRef = intent.getStringExtra("locationRef");
        sublocationName = intent.getStringExtra("sublocationName");
        sublocationRef = intent.getStringExtra("sublocationRef");
        linkDetail = intent.getStringExtra(LINK_DETAIL);
        projectDescription = intent.getStringExtra("projectDescription");
        latitude = getIntent().getDoubleExtra(LATITUDE,0);
        longitude = getIntent().getDoubleExtra(LONGITUDE,0);
        urlVideo = intent.getStringExtra("urlVideo");
        imageLogo = intent.getStringExtra("imageLogo");
        Log.d("Cek Project ref", projectRef);
        title.setText(String.valueOf(projectName));
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);
        webView.loadUrl(linkDetail);
        Log.d("Projectdetail",""+linkDetail);
        if(MainMenuActivity.OFF_LINE_MODE){
            finish();
        }
        if (!urlVideo.equals("")){
            btnPropertyVideo.setVisibility(View.VISIBLE);
        }else {
            btnPropertyVideo.setVisibility(View.GONE);
        }
        if (latitude==0.0) {
            linearMaps.setVisibility(View.GONE);
        } else {
            linearMaps.setVisibility(View.VISIBLE);
        }
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

        btnPropertyVideo.setTypeface(font);
        btnPropertyVideo.setOnClickListener(this);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        webView = (WebView) findViewById(R.id.webView);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        linearMaps = (LinearLayout) findViewById(R.id.linier_maps);
        btnPropertyVideo = (Button) findViewById(R.id.btn_property_video);
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProjectDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_property_video:
                Intent intent = new Intent(this,YoutubeActivity.class);
                intent.putExtra(URL_VIDEO,urlVideo);
                startActivity(intent);
                break;


        }
    }
}
