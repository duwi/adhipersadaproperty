package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.IlustrationProductInteractor;
import com.nataproperty.android.nataproperty.model.ilustration.IlustrationModel;
import com.nataproperty.android.nataproperty.model.listing.IlustrationStatusModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationProductActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationProductPresenter implements IlustrationProductInteractor {
    private IlustrationProductActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public IlustrationProductPresenter(IlustrationProductActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }



    @Override
    public void getIlustration(String dbMasterRef, String projectRef, String clusterRef, String edtSearch) {
        Call<List<IlustrationModel>> call = service.getAPI().getIlustrationPayment(dbMasterRef,projectRef,clusterRef,edtSearch);
        call.enqueue(new Callback<List<IlustrationModel>>() {
            @Override
            public void onResponse(Call<List<IlustrationModel>> call, Response<List<IlustrationModel>> response) {
                view.showIlustrationResults(response);
            }

            @Override
            public void onFailure(Call<List<IlustrationModel>> call, Throwable t) {
                view.showIlustrationFailure(t);

            }


        });

    }

    @Override
    public void getClusterInfo(String dbMasterRef, String projectRef, String clusterRef) {
        Call<IlustrationStatusModel> call = service.getAPI().getClusterInfo(dbMasterRef,projectRef,clusterRef);
        call.enqueue(new Callback<IlustrationStatusModel>() {
            @Override
            public void onResponse(Call<IlustrationStatusModel> call, Response<IlustrationStatusModel> response) {
                view.showClusterInfoResults(response);
            }

            @Override
            public void onFailure(Call<IlustrationStatusModel> call, Throwable t) {
                view.showClusterInfoFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
