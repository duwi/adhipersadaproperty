package com.nataproperty.android.nataproperty.view.menuitem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.menuitem.NotificationAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.menuitem.NotificationModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {
    public static final String TAG = "NotificationActivity" ;

    public static final String PREF_NAME = "pref" ;

    public static final String TITLE = "title" ;
    public static final String DATE = "date" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String GCM_MESSAGE_REF = "gcmMessageRef" ;
    public static final String GCM_MESSAGE_TYPE_REF = "gcmMessageTypeRef" ;

    SharedPreferences sharedPreferences;

    ListView listView;

    private ArrayList<NotificationModel> listNotification = new ArrayList<NotificationModel>();
    private NotificationAdapter adapter;

    String memberRef,linkDetail,titleGcm,date,gcmMessageRef,gcmMessageTypeRef;

    LinearLayout linearNoData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_notification));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);

        listView = (ListView) findViewById(R.id.list_notification);
        adapter = new NotificationAdapter(this,listNotification);
        listView.setAdapter(adapter);

        requestListNotification();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                linkDetail = listNotification.get(position).getLinkDetail();
                titleGcm = listNotification.get(position).getGcmTitle();
                date = listNotification.get(position).getInputTime();
                gcmMessageRef = listNotification.get(position).getGcmMessageRef();
                gcmMessageTypeRef = listNotification.get(position).getGcmMessageTypeRef();

                Intent intent = new Intent(NotificationActivity.this,NotificationDetailActivity.class);
                intent.putExtra(TITLE,titleGcm);
                intent.putExtra(DATE,date);
                intent.putExtra(LINK_DETAIL,linkDetail);
                intent.putExtra(GCM_MESSAGE_REF,gcmMessageRef);
                intent.putExtra(GCM_MESSAGE_TYPE_REF,gcmMessageTypeRef);
                startActivity(intent);
            }
        });

    }

    private void requestListNotification () {
        String url = WebService.getListNotification();
        String urlPostParameter ="&memberRef="+memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d("result product",result);

            if (jsonArray.length()!=0){
                generateListNotification(jsonArray);
                linearNoData.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }else {
                linearNoData.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            }



        }catch (JSONException e){

        }
    }

    private void generateListNotification(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                NotificationModel notificationModel = new NotificationModel();
                notificationModel.setGcmMessageRef(jo.getString("gcmMessageRef"));
                notificationModel.setDbMasterRef(jo.getString("dbMasterRef"));
                notificationModel.setProjectRef(jo.getString("projectRef"));
                notificationModel.setGcmTitle(jo.getString("gcmTitle"));
                notificationModel.setGcmMessage(jo.getString("gcmMessage"));
                notificationModel.setGcmMessageTypeRef(jo.getString("gcmMessageTypeRef"));
                notificationModel.setInputTime(jo.getString("inputTime"));
                notificationModel.setLinkDetail(jo.getString("linkDetail"));

                listNotification.add(notificationModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NotificationActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
