package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterSearchListingInteractor {
    void getSearchListing(String agencyCompanyRef,String listingStatus,String pageNo
            ,String provinceCode,String cityCode,String locationRef,String listingTypeRef,String categoryType,String memberRef);

    void getListingCobroking(String agencyCompanyRef,String listingStatus,String pageNo
            ,String provinceCode,String cityCode,String locationRef,String listingTypeRef,String categoryType,String memberRef);
    void rxUnSubscribe();

}
