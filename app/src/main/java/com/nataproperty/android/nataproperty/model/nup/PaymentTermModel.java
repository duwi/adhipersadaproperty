package com.nataproperty.android.nataproperty.model.nup;

/**
 * Created by User on 11/3/2016.
 */
public class PaymentTermModel {
    String surveyPaymentTermRef,surveyPaymentTermName;

    public String getSurveyPaymentTermRef() {
        return surveyPaymentTermRef;
    }

    public void setSurveyPaymentTermRef(String surveyPaymentTermRef) {
        this.surveyPaymentTermRef = surveyPaymentTermRef;
    }

    public String getSurveyPaymentTermName() {
        return surveyPaymentTermName;
    }

    public void setSurveyPaymentTermName(String surveyPaymentTermName) {
        this.surveyPaymentTermName = surveyPaymentTermName;
    }
}
