package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class CarportTypeModel {
    String carportType;
    String carportTypeName;

    public String getCarportType() {
        return carportType;
    }

    public void setCarportType(String carportType) {
        this.carportType = carportType;
    }

    public String getCarportTypeName() {
        return carportTypeName;
    }

    public void setCarportTypeName(String carportTypeName) {
        this.carportTypeName = carportTypeName;
    }
}
