package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterProjectMenuInteractor {
    void getProjectDtl(String dbMasterRef,String projectRef);
    void getCommision(String dbMasterRef,String projectRef, String memberRef);
    void rxUnSubscribe();

}
