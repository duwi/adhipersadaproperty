package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by User on 7/26/2016.
 */
public class ListingPropertyModel {
    String listingRef,listingTitle,price,subLocationName,cityName,provinceName,
            listingTypeName,priceTypeName,imageCover,memberName,memberRef,agencyCompanyRef,brTypeName,maidBRTypeName,
            bathRTypeName,maidBathRTypeName,garageTypeName,facility,hp,companyName,categoryTypeName;
    boolean favorite;

    double buildArea,landArea,buildDimX,buildDimY,landDimX,landDimY;

    public String getHp() {
        return hp;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }

    public String getImageCover() {
        return imageCover;
    }

    public void setImageCover(String imageCover) {
        this.imageCover = imageCover;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(String memberRef) {
        this.memberRef = memberRef;
    }

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public String getBrTypeName() {
        return brTypeName;
    }

    public void setBrTypeName(String brTypeName) {
        this.brTypeName = brTypeName;
    }

    public String getMaidBRTypeName() {
        return maidBRTypeName;
    }

    public void setMaidBRTypeName(String maidBRTypeName) {
        this.maidBRTypeName = maidBRTypeName;
    }

    public String getBathRTypeName() {
        return bathRTypeName;
    }

    public void setBathRTypeName(String bathRTypeName) {
        this.bathRTypeName = bathRTypeName;
    }

    public String getMaidBathRTypeName() {
        return maidBathRTypeName;
    }

    public void setMaidBathRTypeName(String maidBathRTypeName) {
        this.maidBathRTypeName = maidBathRTypeName;
    }

    public String getGarageTypeName() {
        return garageTypeName;
    }

    public void setGarageTypeName(String garageTypeName) {
        this.garageTypeName = garageTypeName;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public double getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(double buildArea) {
        this.buildArea = buildArea;
    }

    public double getLandArea() {
        return landArea;
    }

    public void setLandArea(double landArea) {
        this.landArea = landArea;
    }

    public double getBuildDimX() {
        return buildDimX;
    }

    public void setBuildDimX(double buildDimX) {
        this.buildDimX = buildDimX;
    }

    public double getBuildDimY() {
        return buildDimY;
    }

    public void setBuildDimY(double buildDimY) {
        this.buildDimY = buildDimY;
    }

    public double getLandDimX() {
        return landDimX;
    }

    public void setLandDimX(double landDimX) {
        this.landDimX = landDimX;
    }

    public double getLandDimY() {
        return landDimY;
    }

    public void setLandDimY(double landDimY) {
        this.landDimY = landDimY;
    }
}
