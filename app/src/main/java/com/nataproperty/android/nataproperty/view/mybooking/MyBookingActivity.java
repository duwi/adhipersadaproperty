package com.nataproperty.android.nataproperty.view.mybooking;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.mybooking.MyBookingAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.mybooking.MyBookingModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.MyBookPresenter;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.project.ProjectActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 6/14/2016.
 */
public class MyBookingActivity extends AppCompatActivity {
    public static final String TAG = "MyBookingActivity";

    public static final String PREF_NAME = "pref";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String BOOKING_REF = "bookingRef";
    public static final String MEMBER_REF = "memberRef";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MyBookPresenter presenter;
//    ProgressDialog progressDialog;

    private List<MyBookingModel> listMyBooking = new ArrayList<>();
    private MyBookingAdapter adapter;
    private MyListView listView;

    private SharedPreferences sharedPreferences;

    private String memberRef, dbMasterRef, projectRef, bookingRef;

    ProgressDialog progressDialog;

    String result, projectBookingRef;
    LinearLayout itemListNoData;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        requestMyBooking();



    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemListNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (MyListView) findViewById(R.id.list_mybooking);
    }

    public void requestMyBooking() {

        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListBook(memberRef.toString());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void showListBookResults(retrofit2.Response<List<MyBookingModel>> response) {
        progressDialog.dismiss();
        listMyBooking=response.body();
        if (listMyBooking.size() >= 1) {
            initAdapter();

        } else {
            AlertDialog.Builder alertDialogBuilder =
                    new AlertDialog.Builder(MyBookingActivity.this);
            alertDialogBuilder.setMessage("Anda belum melakukan booking, anda ingin membuatnya?");
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    startActivity(new Intent(MyBookingActivity.this, ProjectActivity.class));
                    finish();
                }
            });
            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        Log.d("jsonArray", String.valueOf(listMyBooking.size()));
    }

    private void initAdapter() {
        adapter = new MyBookingAdapter(MyBookingActivity.this, listMyBooking);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListBookFailure(Throwable t) {
        progressDialog.dismiss();

    }
}
