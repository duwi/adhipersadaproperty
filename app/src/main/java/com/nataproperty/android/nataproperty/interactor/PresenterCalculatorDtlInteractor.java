package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCalculatorDtlInteractor {
    void getListTemplate(String project,String memberRef);
    void rxUnSubscribe();

}
