package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterCalculatorDtlInteractor;
import com.nataproperty.android.nataproperty.model.kpr.TemplateModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.kpr.CalculationDetailActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CalculatorDtlPresenter implements PresenterCalculatorDtlInteractor {
    private CalculationDetailActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CalculatorDtlPresenter(CalculationDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListTemplate(String project, String memberRef) {
        Call<List<TemplateModel>> call = service.getAPI().getListTemplate(project,memberRef);
        call.enqueue(new Callback<List<TemplateModel>>() {
            @Override
            public void onResponse(Call<List<TemplateModel>> call, Response<List<TemplateModel>> response) {
                view.showListTemplateResults(response);
            }

            @Override
            public void onFailure(Call<List<TemplateModel>> call, Throwable t) {
                view.showListTemplateFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
