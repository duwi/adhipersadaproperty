package com.nataproperty.android.nataproperty.view.loginregister;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.helper.MyEditTextLatoReguler;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

import java.util.Calendar;


/**
 * Created by Administrator on 4/6/2016.
 */
public class RegisterStepTwoActivity extends AppCompatActivity {
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";
    public static final String NAME = "name";
    public static final String BIRTHDATE = "birthDate";
    public static final String ISACTIVE = "isActive";

    private static final String TAG = RegisterStepTwoActivity.class.getSimpleName();

    private MyEditTextLatoReguler txtFullname,txtPhone, txtEmail, txtPassword,txtPasswordConfirm;
    private MyEditTextLatoReguler txtBrith;

    private Button next;

    String statusGoogleSignIn,name,birthDate;

    int isActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_step_two);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_register_two));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        String password = intent.getStringExtra("password");
        String confirmPassword = intent.getStringExtra("confirmPassword");
        statusGoogleSignIn = intent.getStringExtra(STATUS_GOOGLE_SIGN_IN);
        name = intent.getStringExtra(NAME);
        birthDate = intent.getStringExtra(BIRTHDATE);

        txtFullname = (MyEditTextLatoReguler) findViewById(R.id.txtFullname);
        txtBrith = (MyEditTextLatoReguler) findViewById(R.id.txtBirthDate);
        txtPhone = (MyEditTextLatoReguler) findViewById(R.id.txtPhone);
        txtEmail = (MyEditTextLatoReguler) findViewById(R.id.txtEmail);
        txtPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPassword);
        txtPasswordConfirm = (MyEditTextLatoReguler) findViewById(R.id.txtPasswordConfirm);

        txtFullname.setText(name);
        txtBrith.setText(birthDate);
        txtPhone.setText(phone);
        txtEmail.setText(email);
        txtPassword.setText(password);
        txtPasswordConfirm.setText(confirmPassword);
        isActive = intent.getIntExtra(ISACTIVE,0);

        next = (Button) findViewById(R.id.btnNext);

        txtBrith.setInputType(InputType.TYPE_NULL);
        txtBrith.setTextIsSelectable(true);
        txtBrith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(),"Date Picker");
            }
        });
        txtBrith.setFocusable(false);

        next.setTypeface(font);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onLoginFailed();
                    return;
                }

                next.setEnabled(false);

                String fullname = txtFullname.getText().toString().trim();
                String phone = txtPhone.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();
                String passwordConfirm = txtPasswordConfirm.getText().toString().trim();
                String brithDate = txtBrith.getText().toString().trim();

                if (!fullname.isEmpty() && !phone.isEmpty() && !email.isEmpty() && !password.isEmpty()
                        && !passwordConfirm.isEmpty() && password.equals(passwordConfirm) && !brithDate.isEmpty()) {

                    Intent intent = new Intent(RegisterStepTwoActivity.this, RegisterAgentTypeActivity.class);
                    intent.putExtra("fullname", fullname);
                    intent.putExtra("brithDate", brithDate);
                    intent.putExtra("phone", phone);
                    intent.putExtra("email", email);
                    intent.putExtra("password", password);
                    intent.putExtra("passwordConfrim", passwordConfirm);
                    intent.putExtra(STATUS_GOOGLE_SIGN_IN, statusGoogleSignIn);

                    startActivity(intent);

                } else  {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        //is_active
        Log.d("isActive",""+String.valueOf(isActive));
        if (isActive==1){
            txtEmail.setEnabled(false);
        }else {
            txtEmail.setEnabled(false);
        }

    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {

            TextView tv = (TextView) getActivity().findViewById(R.id.txtBirthDate);
            tv.setText(day+"/"+(month+1)+"/"+year);
        }
    }

    public boolean validate() {
        boolean valid = true;

        String fullname = txtFullname.getText().toString().trim();
        String phone = txtPhone.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String passwordConfirm = txtPasswordConfirm.getText().toString().trim();
        String brithDate = txtBrith.getText().toString().trim();

        if (fullname.isEmpty()) {
            txtFullname.setError("enter a fullname");
            valid = false;
        } else {
            txtFullname.setError(null);
        }

        if (phone.isEmpty()) {
            txtPhone.setError("enter a mobile phone");
            valid = false;
        } else {
            txtPhone.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtEmail.setError("enter a valid email address");
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        if (password.isEmpty()) {
            txtPassword.setError("enter a password");
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        if (passwordConfirm.isEmpty()) {
            txtPasswordConfirm.setError("enter a password");
            valid = false;
        } else if (!password.equals(passwordConfirm)){
            txtPasswordConfirm.setError("password don't match");
            valid = false;
        }
        else {
            txtPasswordConfirm.setError(null);
        }

        if (brithDate.isEmpty()) {
            txtBrith.setError("enter a birthdate");
            valid = false;
        } else {
            txtBrith.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
        //Toast.makeText(getBaseContext(), "Register failed", Toast.LENGTH_LONG).show();

        next.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        next.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}

