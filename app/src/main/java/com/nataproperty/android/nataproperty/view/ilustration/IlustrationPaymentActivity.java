package com.nataproperty.android.nataproperty.view.ilustration;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.ilustration.IlustrationPaymentAdapter;
import com.nataproperty.android.nataproperty.adapter.ilustration.IlustrationPaymentAdapter2;
import com.nataproperty.android.nataproperty.adapter.kpr.KprTableAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.kpr.DataKPR;
import com.nataproperty.android.nataproperty.model.kpr.DataSchedule;
import com.nataproperty.android.nataproperty.model.kpr.DataSchedule2;
import com.nataproperty.android.nataproperty.model.kpr.PaymentCCStatusModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.IlustrationPaymentPresenter;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.ilustration.booking.BookingTermActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/13/2016.
 */
public class IlustrationPaymentActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";
    public static final String IS_BOOKING = "isBooking";

    private List<DataSchedule> listPayment = new ArrayList<>();
    private List<DataSchedule2> listPayment2 = new ArrayList<>();
    private IlustrationPaymentAdapter adapter;
    private IlustrationPaymentAdapter2 adapter2;

    ImageView imgLogo;
    TextView txtProjectName;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtTerm;
    private TextView txtTotal;
    private MyListView listView, listViewKprTable;
    private Button btnBooking, btnSendtoFriend;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private IlustrationPaymentPresenter presenter;
    ProgressDialog progressDialog;

    String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, projectName;
    String propertys, category, product, unit, area, priceInc, bookingContact;
    String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    String total;
    String isBooking, stringListKpr;

    private int finType;
    private List<DataKPR> listKprTable = new ArrayList<>();
    private KprTableAdapter adapterKprTable;
    private LinearLayout linearLayout, linearLayoutByKPR;

    private TextView txtNilaiKpr;
    private String nilaiKpr;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;


    static IlustrationPaymentActivity ilustrationPaymentActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_payment);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationPaymentPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);
        finType = intent.getIntExtra("finType", 0);
        stringListKpr = intent.getStringExtra("stringListKpr");
        propertys = intent.getStringExtra("propertys");
        category = intent.getStringExtra("category");
        product = intent.getStringExtra("product");
        unit = intent.getStringExtra("unit");
        area = intent.getStringExtra("area");
        priceInc = intent.getStringExtra("priceInc");
        bookingContact = intent.getStringExtra("bookingContact");

        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ilustrationPaymentActivity = this;
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        Log.d("get intent", "finType" + finType + "-" + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unit + "-" +
                termRef + "-" + termNo);
        Log.d("isBooking ", "" + isBooking);

        if (finType == 2 && !"".equals(stringListKpr)) {
            requestPaymentKpr();
            linearLayout.setVisibility(View.VISIBLE);
            linearLayoutByKPR.setVisibility(View.VISIBLE);

        } else {
            requestPayment();
            linearLayout.setVisibility(View.GONE);
            linearLayoutByKPR.setVisibility(View.GONE);
        }

        txtPropertyName.setText(propertys);
        txtCategoryType.setText(category);
        txtProduct.setText(product);
        txtUnitNo.setText(unit);
        txtArea.setText(Html.fromHtml(area));
        txtEstimate.setText(priceInc);

        /**
         * listview
         */
//        adapter = new IlustrationPaymentAdapter(this, listPayment);
//        listView.setAdapter(adapter);
//        listView.setExpanded(true);

        /**
         * Kpr
         */
//        adapterKprTable = new KprTableAdapter(this, listKprTable);
//        listViewKprTable.setAdapter(adapterKprTable);
//        listViewKprTable.setExpanded(true);

        if (!isBooking.equals("0")) {
            //btnBooking.setEnabled(true);
            setupBtnBooking();
        } else {
            //btnBooking.setEnabled(false);
            if (bookingContact.equals("")) {
                btnBooking.setEnabled(false);
            } else {
                showBookingContact();
            }

        }

        /**
         * send to friend
         */
        btnSendtoFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSend = new Intent(IlustrationPaymentActivity.this, IlustrationSendToFriendActivity.class);
                intentSend.putExtra("dbMasterRef", dbMasterRef);
                intentSend.putExtra("projectRef", projectRef);
                intentSend.putExtra("categoryRef", categoryRef);
                intentSend.putExtra("clusterRef", clusterRef);
                intentSend.putExtra("productRef", productRef);
                intentSend.putExtra("unitRef", unitRef);
                intentSend.putExtra("termRef", termRef);
                intentSend.putExtra("termNo", termNo);
                intentSend.putExtra("paymentTerm", paymentTerm);
                intentSend.putExtra("propertys", propertys);
                intentSend.putExtra(PROJECT_NAME, projectName);
                intentSend.putExtra(IS_BOOKING, isBooking);
                intentSend.putExtra("finType", finType);
                intentSend.putExtra("stringListKpr", stringListKpr);
                startActivity(intentSend);
            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_payment_schedule));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);

        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        linearLayout = (LinearLayout) findViewById(R.id.linear_header_kpr);
        txtNilaiKpr = (TextView) findViewById(R.id.txt_nilai_kpr);
        linearLayoutByKPR = (LinearLayout) findViewById(R.id.linear_by_kpr);
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        listView = (MyListView) findViewById(R.id.list_payment_schedule);
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);
        txtTerm = (TextView) findViewById(R.id.txt_term);
        txtTotal = (TextView) findViewById(R.id.txt_total);
        listViewKprTable = (MyListView) findViewById(R.id.list_kpr_table);
        btnBooking = (Button) findViewById(R.id.btn_proses_booking);
        btnSendtoFriend = (Button) findViewById(R.id.btn_send_to_friend);
        btnSendtoFriend.setTypeface(font);
        btnBooking.setTypeface(font);

    }

    public void requestPayment() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getPayment(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo);

    }

    public void requestPaymentKpr() {

        presenter.getPaymentKpr(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, stringListKpr.toString());

    }

//    public static IlustrationPaymentActivity getInstance() {
//        return ilustrationPaymentActivity;
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        //requestPayment();
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationPaymentActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showPaymentResults(retrofit2.Response<PaymentCCStatusModel> response) {
//        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        total = response.body().getTotalPayment();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();
            txtNilaiKpr.setText(nilaiKpr);
            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

            txtTerm.setText(Html.fromHtml(termCondition));

            txtTotal.setText(total);
            listPayment = response.body().getDataSchedule();
            for (int i = 0; i < listPayment.size(); i++) {
                nilaiKpr = listPayment.get(i).getAmount();
            }
            initAdapterPayment();

        } else {

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    private void initAdapterPayment() {
        adapter = new IlustrationPaymentAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    private void initAdapterPaymentKpr() {
        adapter2 = new IlustrationPaymentAdapter2(this, listPayment2);
        listView.setAdapter(adapter2);
        listView.setExpanded(true);

        adapterKprTable = new KprTableAdapter(this, listKprTable);
        listViewKprTable.setAdapter(adapterKprTable);
        listViewKprTable.setExpanded(true);
    }

    public void showPaymentFailure(Throwable t) {
//        progressDialog.dismiss();
    }

    public void showPaymentKprResults(retrofit2.Response<PaymentCCStatusModel> response) {

        int status = response.body().getStatus();
        String message = response.body().getMessage();
        total = response.body().getTotalPayment();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

            txtTerm.setText(Html.fromHtml(termCondition));

            txtTotal.setText(total);
            listPayment2 = response.body().getDataSchedule2();
            for (int i = 0; i < listPayment2.size(); i++) {
                nilaiKpr = listPayment2.get(i).getAmount();
            }
            txtNilaiKpr.setText(nilaiKpr);
            listKprTable = response.body().getDataKPR();
            initAdapterPaymentKpr();

        } else {

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showPaymentKprFailure(Throwable t) {

    }

    private void showBookingContact() {
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationPaymentActivity.this);
                LayoutInflater inflater = IlustrationPaymentActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Booking Contact");
                txtspecialEnquiries.setText(bookingContact);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });

    }

    private void setupBtnBooking() {
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBooking = new Intent(IlustrationPaymentActivity.this, BookingTermActivity.class);
                intentBooking.putExtra("dbMasterRef", dbMasterRef);
                intentBooking.putExtra("projectRef", projectRef);
                intentBooking.putExtra("categoryRef", categoryRef);
                intentBooking.putExtra("clusterRef", clusterRef);
                intentBooking.putExtra("productRef", productRef);
                intentBooking.putExtra("unitRef", unitRef);
                intentBooking.putExtra("termRef", termRef);
                intentBooking.putExtra("termNo", termNo);
                intentBooking.putExtra(PROJECT_NAME, projectName);
                Log.d("projectName", "" + projectName);
                startActivity(intentBooking);
            }
        });
    }
}
