package com.nataproperty.android.nataproperty.adapter.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.gallery.ProjectGalleryModel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User on 5/11/2016.
 */
public class ProjectDetailImageAdapter extends PagerAdapter {
    public static final String TAG = "NewsDetailImageAdapter";

    Context context;
    private List<ProjectGalleryModel> list;

    PhotoViewAttacher attacher;

    public ProjectDetailImageAdapter(Context context, List<ProjectGalleryModel> list){
        this.context = context;
        this.list = list;
    }

    String projectName,fileName;

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_gallery_detail_image, container, false);
        final ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_gallery_detail_image);
        ImageView download = (ImageView) viewItem.findViewById(R.id.btn_shared);

        ProjectGalleryModel projectGalleryModel = list.get(position);
        projectName = projectGalleryModel.getProjectName();
        fileName = projectGalleryModel.getFileName();

        Glide.with(context).load(projectGalleryModel.getImgFile()).into(imageView);

        //zoom
        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();

                String root = Environment.getExternalStorageDirectory().toString();
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();
               /*Random gen = new Random();
                int n = 10000;
                n = gen.nextInt(n);*/
                String fotoname = projectName+"-"+fileName;
                File file = new File (wallpaperDirectory, fotoname);
                if (file.exists ()) file.delete ();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                    //Toast.makeText(context, "Saved to your folder", Toast.LENGTH_SHORT ).show();
                   /* File filePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + filename.toString() + "." + extension.toString());*/
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + projectName+"-"+fileName;
                    File imageFileToShare = new File(imagePath);
                    Uri uri = Uri.fromFile(imageFileToShare);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    context.startActivity(Intent.createChooser(share, "Share Image!"));

                } catch (Exception e) {

                }

            }

        });

        Log.d(TAG,""+projectGalleryModel.getImgFile());

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


}
