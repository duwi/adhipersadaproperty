package com.nataproperty.android.nataproperty.view.listing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCityAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingMemberInfoAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingProvinceAdapter;
import com.nataproperty.android.nataproperty.adapter.project.LocationAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.listing.ListingMemberInfoModel;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.model.project.LocationModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.MemberInfoPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingSearchMemberInfoActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingSearchMember";
    private ListingCityAdapter adapterCity;

    private ListView listView;
    private ArrayList<ListingMemberInfoModel> listMemberInfo = new ArrayList<ListingMemberInfoModel>();
    private ListingMemberInfoAdapter adapter;

    private SharedPreferences sharedPreferences;
    private LocationAdapter locationAdapter;
    private EditText edtSearch;
    private TextView txtClose;
    private LinearLayout linearLayoutNoData;
    private ImageView imageView;
    private TextView txtName, txtEmail, txtHp, txtPosition, txtAddress, txtAboutMe, txtQuotes;
    private AlertDialog alertDialog;
    int setProvince = 0;
    int setCity = 0;
    private ImageView imageProfile;

    private String agencyCompanyRef, psRef, imageLogo,memberRef,provinceCode,cityCode;
    Spinner spinCity,spinLoc;
    private int countTotal;
    private List<ProvinceModel> listProvince = new ArrayList<>();
    private int page = 1;
    Toolbar toolbar;
    TextView title;
    Display display;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MemberInfoPresenter presenter;
    private String locationRef;
    private List<LocationModel> listLocation = new ArrayList<>();
    private List<CityModel> listCity = new ArrayList<>();
    ProgressDialog progressDialog;
    Button btnNext;
    Typeface font;
    private ListingProvinceAdapter adapterProvince;
    private String name, email, hp, jabatan, listingMemberRef, address, aboutMe, quotes, memberType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_member_info);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MemberInfoPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        agencyCompanyRef = getIntent().getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        psRef = intent.getStringExtra("psRef");
        initWidget();
        requestLocation();
        btnNext.setOnClickListener(this);

    }

    private void requestLocation() {
        presenter.getProvinsiListing("INA");
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        spinLoc = (Spinner) findViewById(R.id.spn_area);
        spinCity = (Spinner)findViewById(R.id.spn_sub_kota);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Listing Property");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        btnNext = (Button) findViewById(R.id.btn_next);
    }

    public void showProvinsiResults(Response<List<ProvinceModel>> response) {
        listProvince = response.body();
        spinProvinsi();

    }

    private void spinProvinsi() {
        adapterProvince = new ListingProvinceAdapter(this, listProvince);
        spinLoc.setAdapter(adapterProvince);
        spinLoc.setSelection(setProvince);
        spinLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();
//                listCity.clear();
                presenter.getCityListing("INA", provinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showProvinsiFailure(Throwable t) {

    }

    public void showCityResults(Response<List<CityModel>> response) {
        listCity = response.body();
        spinnerCity();
    }

    private void spinnerCity() {
        adapterCity = new ListingCityAdapter(this, listCity);
        spinCity.setAdapter(adapterCity);
        spinCity.setSelection(setCity);
        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();
//                requestSubLocation("INA", provinceCode, cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showCityFailure(Throwable t) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                Intent intent = new Intent(this, ListingSearchResultMemberInfoActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("psRef",psRef);
                intent.putExtra("provinceCode", provinceCode);
                intent.putExtra("cityCode", cityCode);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }
}
