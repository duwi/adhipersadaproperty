package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by herlambang-nata on 11/1/2016.
 */
@Entity
public class ColorDiagramModel {
    @Id
    long memberRef;
    int status;
    String color1;
    String color2;


    @Generated(hash = 1822806774)
    public ColorDiagramModel(long memberRef, int status, String color1,
            String color2) {
        this.memberRef = memberRef;
        this.status = status;
        this.color1 = color1;
        this.color2 = color2;
    }

    @Generated(hash = 1459673012)
    public ColorDiagramModel() {
    }


    public long getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(long memberRef) {
        this.memberRef = memberRef;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getColor1() {
        return color1;
    }

    public void setColor1(String color1) {
        this.color1 = color1;
    }

    public String getColor2() {
        return color2;
    }

    public void setColor2(String color2) {
        this.color2 = color2;
    }
}
