package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 5/3/2016.
 */
@Entity
public class ProductModel {
    @Id
    long productRef;
    String dbMasterRef,projectRef, clusterRef, numOfBedrooms, numOfBathrooms, titleProduct, productDescription, numOfAdditionalrooms, numOfAdditionalBathrooms,
            image;

    @Generated(hash = 969983876)
    public ProductModel(long productRef, String dbMasterRef, String projectRef, String clusterRef, String numOfBedrooms, String numOfBathrooms,
            String titleProduct, String productDescription, String numOfAdditionalrooms, String numOfAdditionalBathrooms, String image) {
        this.productRef = productRef;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.clusterRef = clusterRef;
        this.numOfBedrooms = numOfBedrooms;
        this.numOfBathrooms = numOfBathrooms;
        this.titleProduct = titleProduct;
        this.productDescription = productDescription;
        this.numOfAdditionalrooms = numOfAdditionalrooms;
        this.numOfAdditionalBathrooms = numOfAdditionalBathrooms;
        this.image = image;
    }

    @Generated(hash = 522812530)
    public ProductModel() {
    }

    public long getProductRef() {
        return productRef;
    }

    public void setProductRef(long productRef) {
        this.productRef = productRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }


    public String getNumOfBedrooms() {
        return numOfBedrooms;
    }

    public void setNumOfBedrooms(String numOfBedrooms) {
        this.numOfBedrooms = numOfBedrooms;
    }

    public String getNumOfBathrooms() {
        return numOfBathrooms;
    }

    public void setNumOfBathrooms(String numOfBathrooms) {
        this.numOfBathrooms = numOfBathrooms;
    }

    public String getTitleProduct() {
        return titleProduct;
    }

    public void setTitleProduct(String titleProduct) {
        this.titleProduct = titleProduct;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }


    public String getNumOfAdditionalrooms() {
        return numOfAdditionalrooms;
    }

    public void setNumOfAdditionalrooms(String numOfAdditionalrooms) {
        this.numOfAdditionalrooms = numOfAdditionalrooms;
    }


    public String getNumOfAdditionalBathrooms() {
        return numOfAdditionalBathrooms;
    }

    public void setNumOfAdditionalBathrooms(String numOfAdditionalBathrooms) {
        this.numOfAdditionalBathrooms = numOfAdditionalBathrooms;
    }
}
