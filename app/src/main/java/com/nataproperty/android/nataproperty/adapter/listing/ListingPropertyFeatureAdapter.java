package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyFeatureModel;

import java.util.ArrayList;

/**
 * Created by User on 5/15/2016.
 */
public class ListingPropertyFeatureAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ListingPropertyFeatureModel> list;
    private ListFeatureHolder holder;

    public ListingPropertyFeatureAdapter(Context context, ArrayList<ListingPropertyFeatureModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_feature,null);
            holder = new ListFeatureHolder();
            holder.projectFeatureName = (TextView) convertView.findViewById(R.id.txt_feature);

            convertView.setTag(holder);
        }else{
            holder = (ListFeatureHolder) convertView.getTag();
        }

        ListingPropertyFeatureModel feature = list.get(position);
        holder.projectFeatureName.setText(feature.getProjectFeatureName());

       /* Picasso.with(context)
                .load("http://192.168.10.180/andro/wf/support/displayImage.aspx?is=cluster&dr=5&pr=1&cr=1&quot;).into(holder.clusterImg);*/

        return convertView;

    }

    private class ListFeatureHolder {
        TextView projectFeatureName;
    }
}
