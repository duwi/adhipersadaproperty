package com.nataproperty.android.nataproperty.view.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.profile.JobTitleAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.MaritalStatusAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.NationAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.OccupationAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.RegionAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.SexAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.TypeAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.profile.JobTitleModel;
import com.nataproperty.android.nataproperty.model.profile.MaritalStatusModel;
import com.nataproperty.android.nataproperty.model.profile.NationModel;
import com.nataproperty.android.nataproperty.model.profile.OccupationModel;
import com.nataproperty.android.nataproperty.model.profile.RegionModel;
import com.nataproperty.android.nataproperty.model.profile.SexModel;
import com.nataproperty.android.nataproperty.model.profile.TypeModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class OthersEditActivity extends AppCompatActivity {

    private List<NationModel> listNation = new ArrayList<NationModel>();
    private List<TypeModel> listType = new ArrayList<TypeModel>();
    private List<RegionModel> listRegion = new ArrayList<RegionModel>();
    private List<SexModel> listSex = new ArrayList<SexModel>();
    private List<OccupationModel> listOccupation = new ArrayList<OccupationModel>();
    private List<JobTitleModel> listJobTitle = new ArrayList<JobTitleModel>();
    private List<MaritalStatusModel> listMaritalStatus = new ArrayList<MaritalStatusModel>();

    private NationAdapter adapterNation;
    private TypeAdapter adapterType;
    private RegionAdapter adapterRegion;
    private SexAdapter adapterSex;
    private OccupationAdapter adapterOccupation;
    private JobTitleAdapter adapterJobTitle;
    private MaritalStatusAdapter adapterMaritalStatus;

    private EditText editBloodType,editFax,editPassportId,editSimId;
    private Spinner spnNation,spnType,spnRegion,spnSex,spnOccupation,spnJobTitle,spnMaritalStatus;
    private Button btnSave;

    String bloodType,fax,passportID,simid;
    String nationSortNo,typeSortNo,regionSortNo,sexSortNo,occupationSortNo,jobTitleSortNo,maritalStatusSortNo;
    String memberRef;
    String nationRef,typeRef,religionRef,sexRef,occupationRef,jobTitleRef,maritalStatus;

    ProgressDialog progressDialog;
    String message;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_others);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_other));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        editBloodType = (EditText) findViewById(R.id.edit_bloodType);
        editFax = (EditText) findViewById(R.id.edit_fax);
        editPassportId = (EditText) findViewById(R.id.edit_passportId);
        editSimId = (EditText) findViewById(R.id.edit_simId);

        spnNation = (Spinner) findViewById(R.id.spn_nation);
        spnType = (Spinner) findViewById(R.id.spn_type);
        spnRegion = (Spinner) findViewById(R.id.spn_region);
        spnSex = (Spinner) findViewById(R.id.spn_sex);
        spnOccupation = (Spinner) findViewById(R.id.spn_occupation);
        spnJobTitle = (Spinner) findViewById(R.id.spn_jobTitle);
        spnMaritalStatus = (Spinner) findViewById(R.id.spn_maritalStatus);

        Intent intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        bloodType = intent.getStringExtra("bloodType");
        fax = intent.getStringExtra("fax");
        passportID = intent.getStringExtra("passportID");
        simid = intent.getStringExtra("simid");

        nationSortNo = intent.getStringExtra("nationSortNo");
        typeSortNo = intent.getStringExtra("typeSortNo");
        regionSortNo = intent.getStringExtra("regionSortNo");
        sexSortNo = intent.getStringExtra("sexSortNo");
        occupationSortNo = intent.getStringExtra("occupationSortNo");
        jobTitleSortNo = intent.getStringExtra("jobTitleSortNo");
        maritalStatusSortNo = intent.getStringExtra("maritalStatusSortNo");

        /**
         * get data
         */
        requestOther();

        adapterNation = new NationAdapter(this,listNation);
        spnNation.setAdapter(adapterNation);
        spnNation.setSelection(Integer.parseInt(nationSortNo)-1);
        spnNation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nationRef = listNation.get(position).getNationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterType = new TypeAdapter(this,listType);
        spnType.setAdapter(adapterType);
        spnType.setSelection(Integer.parseInt(typeSortNo)-1);
        spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeRef = listType.get(position).getTypeRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterRegion = new RegionAdapter(this,listRegion);
        spnRegion.setAdapter(adapterRegion);
        spnRegion.setSelection(Integer.parseInt(regionSortNo)-1);
        spnRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                religionRef = listRegion.get(position).getRegionRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterSex = new SexAdapter(this,listSex);
        spnSex.setAdapter(adapterSex);
        spnSex.setSelection(Integer.parseInt(sexSortNo)-1);
        spnSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sexRef = listSex.get(position).getSexRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterOccupation = new OccupationAdapter(this,listOccupation);
        spnOccupation.setAdapter(adapterOccupation);
        spnOccupation.setSelection(Integer.parseInt(occupationSortNo)-1);
        spnOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                occupationRef = listOccupation.get(position).getOccupationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Log.d("JobSortNo"," "+jobTitleSortNo);

        adapterJobTitle = new JobTitleAdapter(this,listJobTitle);
        spnJobTitle.setAdapter(adapterJobTitle);
        spnJobTitle.setSelection(Integer.parseInt(jobTitleSortNo)-1);
        spnJobTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jobTitleRef = listJobTitle.get(position).getJobTitleRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterMaritalStatus = new MaritalStatusAdapter(this,listMaritalStatus);
        spnMaritalStatus.setAdapter(adapterMaritalStatus);
        spnMaritalStatus.setSelection(Integer.parseInt(maritalStatusSortNo)-1);
        spnMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maritalStatus = listMaritalStatus.get(position).getMaritalStatusRef();
                Log.d("Marital"," "+maritalStatus);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editBloodType.setText(bloodType);
        editFax.setText(fax);
        editPassportId.setText(passportID);
        editSimId.setText(simid);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setTypeface(font);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOther();
            }
        });
    }

    private void requestOther(){
        String url = WebService.getOtherProfile();
        String urlPostParameter ="";
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("cek result result",result);

        try {
            JSONObject jo = new JSONObject(result);
            int status = jo.getInt("status");
            String message = jo.getString("message");
            Log.d("payment",result);

            if (status==200){
                JSONArray jsonArrayNation = new JSONArray(jo.getJSONArray("nation").toString());
                Log.d("Nation array",jo.getJSONArray("nation").toString());
                generateNation(jsonArrayNation);

                JSONArray jsonArrayType = new JSONArray(jo.getJSONArray("type").toString());
                Log.d("Type array",jo.getJSONArray("type").toString());
                generateType(jsonArrayType);

                JSONArray jsonArrayRegion = new JSONArray(jo.getJSONArray("religion").toString());
                Log.d("religion array",jo.getJSONArray("religion").toString());
                generateRegion(jsonArrayRegion);

                JSONArray jsonArraySex = new JSONArray(jo.getJSONArray("sex").toString());
                Log.d("sex array",jo.getJSONArray("sex").toString());
                generatSex(jsonArraySex);

                JSONArray jsonArrayOccupation = new JSONArray(jo.getJSONArray("occupation").toString());
                Log.d("Nation array",jo.getJSONArray("occupation").toString());
                generateOccupation(jsonArrayOccupation);

                JSONArray jsonArrayMarital = new JSONArray(jo.getJSONArray("marital").toString());
                Log.d("marital array",jo.getJSONArray("marital").toString());
                generateMarital(jsonArrayMarital);

                JSONArray jsonArrayJobTitle = new JSONArray(jo.getJSONArray("jobTitle").toString());
                Log.d("job array",jo.getJSONArray("jobTitle").toString());
                generateJobTitle(jsonArrayJobTitle);

            }else {

                Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
            }

        }catch (JSONException e){

        }
    }

    /**
     * Nation
     * @param jsonArrayNation
     */

    private void generateNation(JSONArray jsonArrayNation){
        for (int i = 0; i < jsonArrayNation.length();i++) {
            try {
                JSONObject joArray = jsonArrayNation.getJSONObject(i);
                NationModel nation = new NationModel();
                nation.setNationRef(joArray.getString("nationRef"));
                nation.setNationName(joArray.getString("nationName"));
                nation.setNationSortNo(joArray.getString("nationSortNo"));

                listNation.add(nation);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    /**
     * Type
     * @param jsonArray
     */

    private void generateType(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                TypeModel type = new TypeModel();
                type.setTypeRef(joArray.getString("typeRef"));
                type.setTypeName(joArray.getString("typeName"));
                type.setTypeSortNo(joArray.getString("typeSortNo"));

                listType.add(type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    /**
     * region
     * @param jsonArray
     */

    private void generateRegion(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                RegionModel region = new RegionModel();
                region.setRegionRef(joArray.getString("religionRef"));
                region.setRegionName(joArray.getString("religionName"));
                region.setRegionSortNo(joArray.getString("religionSortNo"));

                listRegion.add(region);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    private void generatSex(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                SexModel sex = new SexModel();
                sex.setSexRef(joArray.getString("sexRef"));
                sex.setSexName(joArray.getString("sexName"));
                sex.setSexSortNo(joArray.getString("sexSortNo"));

                listSex.add(sex);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    private void generateOccupation(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                OccupationModel occupation = new OccupationModel();
                occupation.setOccupationRef(joArray.getString("occupationRef"));
                occupation.setOccupationName(joArray.getString("occupationName"));
                occupation.setOccupationSortNo(joArray.getString("occupationSortNo"));

                listOccupation.add(occupation);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    private void generateMarital(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                MaritalStatusModel marital = new MaritalStatusModel();
                marital.setMaritalStatusRef(joArray.getString("maritalStatus"));
                marital.setMaritalStatusName(joArray.getString("maritalStatusName"));
                marital.setMaritalStatusSorNo(joArray.getString("maritalSortNo"));

                listMaritalStatus.add(marital);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    private void generateJobTitle(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length();i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                JobTitleModel job = new JobTitleModel();
                job.setJobTitleRef(joArray.getString("jobTitleRef"));
                job.setJobTitleName(joArray.getString("jobTitleName"));
                job.setJobTitleSortNo(joArray.getString("jobTitleSortNo"));

                listJobTitle.add(job);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //adapter.notifyDataSetChanged();
    }

    private void updateOther (){
        String url = WebService.updateOtherProfileSvc();
        String urlPostParameter ="&memberRef="+memberRef.toString()+
                "&bloodType="+editBloodType.getText().toString()+
                "&nationRef="+nationRef.toString()+
                "&fax="+editFax.getText().toString()+
                "&passportID="+editPassportId.getText().toString()+
                "&typeRef="+typeRef.toString()+
                "&religionRef="+religionRef.toString()+
                "&sexRef="+sexRef.toString()+
                "&occupationRef="+occupationRef.toString()+
                "&jobTitleRef="+jobTitleRef.toString()+
                "&maritalStatus="+maritalStatus.toString()+
                "&SIMID="+editSimId.getText().toString();

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OthersEditActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            Log.d("cek result result",result);

            try {
                JSONObject jo = new JSONObject(result);
                int status = jo.getInt("status");
                message = jo.getString("message");
                Log.d("result save"," "+result);

                if (status==200){
                    Intent intent = new Intent(OthersEditActivity.this,SectionProfileActivity.class);
                    SectionProfileActivity.getInstance().finish();
                    startActivity(intent);
                    finish();

                }else {
                    //Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
                }

            }catch (JSONException e){

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(OthersEditActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
