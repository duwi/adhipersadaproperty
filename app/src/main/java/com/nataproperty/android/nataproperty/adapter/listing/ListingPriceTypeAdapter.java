package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingPriceTypeModel;

import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class ListingPriceTypeAdapter extends BaseAdapter {
    private Context context;
    private List<ListingPriceTypeModel> list;
    private ListHolder holder;

    public ListingPriceTypeAdapter(Context context, List<ListingPriceTypeModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_spiner_listing_type,null);
            holder = new ListHolder();
            holder.listingType = (TextView) convertView.findViewById(R.id.txt_listing_type);

            convertView.setTag(holder);
        }else{
            holder = (ListHolder) convertView.getTag();
        }

        ListingPriceTypeModel listingPriceTypeModel = list.get(position);
        holder.listingType.setText(listingPriceTypeModel.getPriceTypeName());

        return convertView;
    }

    private class ListHolder {
        TextView listingType;
    }
}
