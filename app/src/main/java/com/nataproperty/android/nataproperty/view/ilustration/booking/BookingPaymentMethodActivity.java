package com.nataproperty.android.nataproperty.view.ilustration.booking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationProductActivity;
import com.nataproperty.android.nataproperty.view.mybooking.MyBookingActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/21/2016.
 */
public class BookingPaymentMethodActivity extends AppCompatActivity {
    public static final String TAG = "BookingPaymentMethod";

    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TYPE_PAYMENT = "typePayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    CardView cardViewBankTranfer, veritransCC, veritransMandiri, veritransCimb, veritransBCA;

    ImageView imgLogo;
    TextView txtProjectName;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtBookingFee, txtBookingInfo;

    private String propertys, category, product, unit, area, priceInc;
    private String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    private String dbMasterRef, categoryRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, bookingRef, projectName;
    private String bookingFee, projectBookingRef,memberRef;

    int typePayment;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_payment_method);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_payment_method));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        typePayment = intent.getIntExtra(TYPE_PAYMENT, 0);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);

        Log.d(TAG, bookingRef + "-" + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" +
                unitRef + "-" + termRef + "-" + termNo + "-" + termNo);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * booking fee
         */
        txtBookingFee = (TextView) findViewById(R.id.txt_booking_fee);
        txtBookingInfo = (TextView) findViewById(R.id.txt_booking_info);

        //logo
        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        cardViewBankTranfer = (CardView) findViewById(R.id.bank_transfer);
        veritransCC = (CardView) findViewById(R.id.veritrans);
        veritransMandiri = (CardView) findViewById(R.id.veritrans_mandiri) ;
        veritransCimb = (CardView) findViewById(R.id.veritrans_cimb) ;
        veritransBCA = (CardView) findViewById(R.id.veritrans_bca) ;


        cardViewBankTranfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingPaymentMethodActivity.this, BookingBankTransferActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                startActivity(intent);
            }
        });

        //veritrans
        /*veritransCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingPaymentMethodActivity.this, BookingCreditCardWebviewActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                startActivity(intent);
            }
        });*/
        veritransCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeCreditCard,getString(R.string.txt_title_credit_card));

            }
        });

        veritransMandiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeMandiri,"Mandiri Klik Pay");

            }
        });

        veritransCimb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeCimb,"CIMB Clicks");

            }
        });

        veritransBCA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeBCA,"BCA Klik Pay");

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPayment();
        requestMyBookingDetail();
    }

    private void requestPayment() {
        String url = WebService.getPayment();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&clusterRef=" + clusterRef.toString() +
                "&productRef=" + productRef.toString() +
                "&unitRef=" + unitRef.toString() +
                "&termRef=" + termRef.toString() +
                "&termNo=" + termNo.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("cek param payment", " " + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unitRef + "-" +
                termRef + "-" + termNo);
        try {
            JSONObject jo = new JSONObject(result);
            int status = jo.getInt("status");
            String message = jo.getString("message");
            Log.d("Cek payment info", result);
            // Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();

            if (status == 200) {
                paymentTerm = jo.getJSONObject("dataPrice").getString("paymentTerm");
                priceIncVat = jo.getJSONObject("dataPrice").getString("priceInc");
                discPercent = jo.getJSONObject("dataPrice").getString("discPercent");
                discAmt = jo.getJSONObject("dataPrice").getString("discAmt");
                netPrice = jo.getJSONObject("dataPrice").getString("netPrice");
                termCondition = jo.getJSONObject("dataPrice").getString("termCondition");

                propertys = jo.getJSONObject("propertyInfo").getString("propertys");
                category = jo.getJSONObject("propertyInfo").getString("category");
                product = jo.getJSONObject("propertyInfo").getString("product");
                unit = jo.getJSONObject("propertyInfo").getString("unit");
                area = jo.getJSONObject("propertyInfo").getString("area");
                priceInc = jo.getJSONObject("propertyInfo").getString("priceInc");
                bookingFee = jo.getJSONObject("propertyInfo").getString("bookingFee");

                txtPropertyName.setText(propertys);
                txtCategoryType.setText(category);
                txtProduct.setText(product);
                txtUnitNo.setText(unit);
                txtArea.setText(Html.fromHtml(area));
                txtEstimate.setText(priceInc);

                txtPaymentTerms.setText(paymentTerm);
                txtPriceInc.setText(priceIncVat);
                txtDisconutPersen.setText(discPercent);
                txtdiscount.setText(discAmt);
                txtNetPrice.setText(netPrice);

                DecimalFormat decimalFormat = new DecimalFormat("###,##0");
                txtBookingFee.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(bookingFee))));

                imgLogo = (ImageView) findViewById(R.id.img_logo_project);
                txtProjectName = (TextView) findViewById(R.id.txt_project_name);
                txtProjectName.setText(propertys);
                Glide.with(this)
                        .load(WebService.getProjectImage() + dbMasterRef +
                                "&pr=" + projectRef).into(imgLogo);

            } else {

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {

        }
    }

    private void requestMyBookingDetail() {
        String url = WebService.getDetailBooking();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&bookingRef=" + projectBookingRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d(TAG, result);
            int status = jo.getInt("status");
            if (status == 200) {
                String bookDate = jo.getJSONObject("bookingInfo").getString("bookDate");
                String bookHour = jo.getJSONObject("bookingInfo").getString("bookHour");

                txtBookingInfo.setText("Lakukan pembayaran booking fee dalam waktu 3 jam atau sebelum pukul " + bookHour + ". " +
                        "jika tidak ada konfirmasi pembayaran maka unit booking akan kembali available");
            } else {
                Log.d(TAG, "get error");
            }

        } catch (JSONException e) {

        }
    }

    /**
     * paymentTypeCreditCard
     */
    private void paymentVeritrans(final String paymentType,final String titlePayment) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.veritransBookingVT(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    String link = jo.getString("redirectUrl");
                    Log.d(TAG, "redirectUrl  " + link);

                    if (status == 200) {
                        Intent intent = new Intent(BookingPaymentMethodActivity.this, BookingCreditCardWebviewActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(CLUSTER_REF,clusterRef);
                        intent.putExtra(PRODUCT_REF,productRef);
                        intent.putExtra(UNIT_REF,unitRef);
                        intent.putExtra(TERM_REF,termRef);
                        intent.putExtra(TERM_NO,termNo);

                        intent.putExtra(BOOKING_REF, bookingRef);
                        intent.putExtra(PROJECT_NAME,projectName);
                        intent.putExtra("link", link);
                        intent.putExtra("titlePayment", titlePayment);
                        startActivity(intent);
                    } else if (status == 406) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingPaymentMethodActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingPaymentMethodActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("memberRef", memberRef);
                params.put("bookingRef", bookingRef);
                params.put("termRef", termRef);
                params.put("paymentType", paymentType);

                Log.d("params", " " + dbMasterRef + " " + projectRef + " " +
                        memberRef + " " + bookingRef + " " + termRef + " " + termRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "updatePaymentTypeCreditCardBooking");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (typePayment == 0) {
            Intent intent = new Intent(BookingPaymentMethodActivity.this, IlustrationProductActivity.class);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
            intent.putExtra(CATEGORY_REF, categoryRef);
            intent.putExtra(CLUSTER_REF, clusterRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(BookingPaymentMethodActivity.this, MyBookingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }
}
