package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/28/2016.
 */
@Entity
public class ListUnitMappingModel {
    @Id
    long Id;
    String dbMasterReff;
    String unitName;

    @Generated(hash = 2087402427)
    public ListUnitMappingModel(long Id, String dbMasterReff, String unitName) {
        this.Id = Id;
        this.dbMasterReff = dbMasterReff;
        this.unitName = unitName;
    }

    @Generated(hash = 323323948)
    public ListUnitMappingModel() {
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getDbMasterReff() {
        return dbMasterReff;
    }

    public void setDbMasterReff(String dbMasterReff) {
        this.dbMasterReff = dbMasterReff;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
