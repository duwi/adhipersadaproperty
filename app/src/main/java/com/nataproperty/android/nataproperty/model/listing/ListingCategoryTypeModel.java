package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 10/6/2016.
 */
@Entity
public class ListingCategoryTypeModel {
    @Id
    long id;

    String categoryType, categoryTypeGroup, categoryTypeName;

    @Generated(hash = 1009726006)
    public ListingCategoryTypeModel(long id, String categoryType,
            String categoryTypeGroup, String categoryTypeName) {
        this.id = id;
        this.categoryType = categoryType;
        this.categoryTypeGroup = categoryTypeGroup;
        this.categoryTypeName = categoryTypeName;
    }

    @Generated(hash = 535555264)
    public ListingCategoryTypeModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryTypeGroup() {
        return categoryTypeGroup;
    }

    public void setCategoryTypeGroup(String categoryTypeGroup) {
        this.categoryTypeGroup = categoryTypeGroup;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }
}
