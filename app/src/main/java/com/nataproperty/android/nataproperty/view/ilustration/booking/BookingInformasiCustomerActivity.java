package com.nataproperty.android.nataproperty.view.ilustration.booking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/18/2016.
 */
public class BookingInformasiCustomerActivity extends AppCompatActivity {
    public static final String TAG = "BookingInformasi";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";

    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";

    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtPriortyPrice, txtTotal;
    EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId, editQty;
    ImageView imgKtp, imgNpwp;

    Button btnUploadKtp, btnUploadNpwp, btnNext;

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address;
    private String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, projectName;
    private String memberCustomerRef, projectDescription, status;
    private String type;
    private String ktpImg, npwpImg;
    String defaultQty = "1";
    double gTotal;
    private String cekKtp = "";
    private String cekNpwp = "";

    Bitmap bitmapKtp, bitmapNpwp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_informasi_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_bookng_informasi));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        Log.d("Cek", memberCustomerRef);

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);

        phone1 = intent.getStringExtra(PHONE);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);

        Log.d(TAG, "" + phone1 + " " + address + " " + ktpid);

        Log.d("Projectname", "" + projectName);

        //memberCustomerRef = intent.getStringExtra(MEMBER_CUSTOMER_REF);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);

        Log.d("intent get", " " + dbMasterRef + "-" + projectRef + "-" + memberCustomerRef + "-" + projectDescription);

        if (phone1 != null && address != null && ktpid != null) {
            editPhone = (EditText) findViewById(R.id.edit_phone);
            editAddress = (EditText) findViewById(R.id.edit_address);
            editKtpId = (EditText) findViewById(R.id.edit_ktp_id);

            editPhone.setText(phone1);
            editAddress.setText(address);
            editKtpId.setText(ktpid);
        }

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobile = (EditText) findViewById(R.id.edit_mobile);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        editAddress = (EditText) findViewById(R.id.edit_address);
        editKtpId = (EditText) findViewById(R.id.edit_ktp_id);

        txtPriortyPrice = (TextView) findViewById(R.id.txt_priority_pass);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnUploadKtp = (Button) findViewById(R.id.btn_upload_ktp);
        btnUploadKtp.setTypeface(font);
        btnUploadNpwp = (Button) findViewById(R.id.btn_upload_npwp);
        btnUploadNpwp.setTypeface(font);

        btnNext = (Button) findViewById(R.id.btn_next);

        imgKtp.setEnabled(false);
        imgNpwp.setEnabled(false);

        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                selectImage();
            }
        });

        imgNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                selectImage();
            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekFullname = editFullname.getText().toString();
                String cekPhone = editPhone.getText().toString();
                String cekMobile = editMobile.getText().toString();
                String cekEmail = editEmail.getText().toString();
                String cekAddress = editAddress.getText().toString();
                String cekKtpId = editKtpId.getText().toString();

                if (!cekFullname.isEmpty() && !cekPhone.isEmpty() && !cekMobile.isEmpty() && !cekEmail.isEmpty() && !cekAddress.isEmpty()
                        && !cekKtpId.isEmpty() && !ktpRef.equals("") && !npwpRef.equals("")) {
                    Intent intent = new Intent(BookingInformasiCustomerActivity.this, BookingReviewOrderActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CATEGORY_REF, categoryRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(UNIT_REF, unitRef);
                    intent.putExtra(TERM_REF, termRef);
                    intent.putExtra(TERM_NO, termNo);
                    intent.putExtra(PROJECT_NAME, projectName);

                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(KTP_REF, ktpRef);
                    intent.putExtra(NPWP_REF, npwpRef);
                    startActivity(intent);

                } else {
                    if (cekFullname.isEmpty()) {
                        editFullname.setError(getResources().getString(R.string.txt_no_fullname));
                    } else {
                        editFullname.setError(null);
                    }

                    if (cekPhone.isEmpty()) {
                        editPhone.setError(getResources().getString(R.string.txt_no_phone));
                    } else {
                        editPhone.setError(null);
                    }

                    if (cekMobile.isEmpty()) {
                        editMobile.setError(getResources().getString(R.string.txt_no_mobile));
                    } else {
                        editMobile.setError(null);
                    }

                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail.setError(getResources().getString(R.string.txt_no_email));
                    } else {
                        editEmail.setError(null);
                    }

                    if (cekAddress.isEmpty()) {
                        editAddress.setError(getResources().getString(R.string.txt_no_address));
                    } else {
                        editAddress.setError(null);
                    }

                    if (cekKtpId.isEmpty()) {
                        editKtpId.setError(getResources().getString(R.string.txt_no_ktp_id));
                    } else {
                        editKtpId.setError(null);
                    }

                    if (ktpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_ktp)), Toast.LENGTH_LONG).show();
                    } else if (npwpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_npwp)), Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        //generateTotal(editQty.getText().toString());
        requestPsInfo(memberCustomerRef);
    }

    private void requestPsInfo(final String memberCustomerRef) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int statusGet = jo.getInt("status");
                    String message = jo.getString("message");

                    if (statusGet == 200) {
                        //reuired
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        phone1 = jo.getJSONObject("data").getString("phone1");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        email1 = jo.getJSONObject("data").getString("email1");
                        address = jo.getJSONObject("data").getString("idAddr");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        if (!fullname.isEmpty()) {
                            editFullname.setText(fullname);
                            editFullname.setEnabled(false);
                        } else {
                            editFullname.setEnabled(true);
                        }

                        if (!email1.isEmpty()) {
                            editEmail.setText(email1);
                            editEmail.setEnabled(false);
                        } else {
                            editEmail.setEnabled(true);
                        }

                        if (!ktpid.isEmpty()) {
                            editKtpId.setText(ktpid);
                            editKtpId.setEnabled(false);
                        } else {
                            editKtpId.setEnabled(true);
                        }

                        if (!mobile1.isEmpty()) {
                            editMobile.setText(mobile1);
                            editMobile.setEnabled(false);
                        } else {
                            editMobile.setEnabled(true);
                        }

                        if (!phone1.isEmpty()) {
                            editPhone.setText(phone1);
                            editPhone.setEnabled(false);
                        } else {
                            editPhone.setEnabled(true);
                        }

                        if (!address.isEmpty()) {
                            editAddress.setText(address);
                            editAddress.setEnabled(false);
                        } else {
                            editAddress.setEnabled(true);
                        }

                        if (ktpRef.equals("")) {
                            imgKtp.setEnabled(true);
                        } else {
                            imgKtp.setEnabled(false);
                        }

                        if (npwpRef.equals("")) {
                            imgNpwp.setEnabled(true);
                        } else {
                            imgNpwp.setEnabled(false);
                        }

                        Glide.with(BookingInformasiCustomerActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgKtp);

                        Glide.with(BookingInformasiCustomerActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgNpwp);

                        Log.d("KTP/NPWP", " " + WebService.getKtp() + ktpRef + " " + WebService.getNpwp() + npwpRef);
                        Log.d("KTP/NPWP", "Ref " + ktpRef + " " + npwpRef);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        btnNext.setEnabled(false);
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingInformasiCustomerActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingInformasiCustomerActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberCustomerRef);
                Log.d("TAG Member Ref", memberCustomerRef);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(BookingInformasiCustomerActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra("type", type);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Use Existing Foto")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    intent.putExtra("type", type);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Foto"),
                            SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Log.d("Type Camera", " " + type);

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (type != null && type.equals("2")) {
            Intent i = new Intent(this, BookingKtpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("3")) {
            Intent i = new Intent(this, BookingNpwpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Log.d("Type Galery", " " + type);

        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        if (type != null && type.equals("2")) {
            Intent i = new Intent(this, BookingKtpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else if ( type != null && type.equals("3")) {
            Intent i = new Intent(this, BookingNpwpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
