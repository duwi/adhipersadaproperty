package com.nataproperty.android.nataproperty.view;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.gcm.GCMRegistrationIntentService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.StatusTokenGCMModel;
import com.nataproperty.android.nataproperty.model.listing.ListCountChatModel;
import com.nataproperty.android.nataproperty.model.listing.ListTicketModel;
import com.nataproperty.android.nataproperty.model.listing.ListingAgencyInfoModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.MainMenuPresenter;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.before_login.ViewpagerActivity;
import com.nataproperty.android.nataproperty.view.event.EventActivity;
import com.nataproperty.android.nataproperty.view.event.MyTicketActivity;
import com.nataproperty.android.nataproperty.view.kpr.CalculationActivity;
import com.nataproperty.android.nataproperty.view.listing.ListingMainActivity;
import com.nataproperty.android.nataproperty.view.menuitem.AboutUsActivity;
import com.nataproperty.android.nataproperty.view.menuitem.ChangePasswordActivity;
import com.nataproperty.android.nataproperty.view.menuitem.ContactUsActivity;
import com.nataproperty.android.nataproperty.view.menuitem.NotificationActivity;
import com.nataproperty.android.nataproperty.view.mybooking.MyBookingActivity;
import com.nataproperty.android.nataproperty.view.mynup.MyNupActivity;
import com.nataproperty.android.nataproperty.view.news.NewsActivity;
import com.nataproperty.android.nataproperty.view.profile.SectionProfileActivity;
import com.nataproperty.android.nataproperty.view.profile.SettingNotificationActivity;
import com.nataproperty.android.nataproperty.view.project.ListDownloadProjectActivity;
import com.nataproperty.android.nataproperty.view.project.MyListingActivity;
import com.nataproperty.android.nataproperty.view.project.ProjectActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by User on 4/21/2016.
 */
public class MainMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, View.OnClickListener {
    public static final String TAG = "MainMenuActivity";
    public static final String PREF_NAME = "pref";
    public static final String MEMBER_REF = "memberRef";
    public static final String GCM_TOKEN = "GCMToken";
    public static boolean OFF_LINE_MODE = false;
    public static boolean TICKET = true;
    private RelativeLayout snackBarBuatan;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MainMenuPresenter presenter;
    private TextView retry;
    ProgressDialog progressDialog;
    Context context;
    SharedPreferences sharedPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private List<ListTicketModel> listTicketModels = new ArrayList<>();
    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private TextView nameProfile, memberTypeName;
    private ImageView imgProfile;
    ImageButton ticket, all_project, download, commision, mylisting, nup, news, event, mybooking, calculator, listingProperty;
    TextView txtTicket, txtMyAgency;
    private static final int RC_SIGN_IN = 0;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private Tracker mTracker;
    String email, name;
    String memberRef, memberType, agencyCompanyRef, imageLogo,memberTypeCode,companyName;
    boolean state;
    LinearLayout linearLayoutTicetButton, linearLayoutTicetTextview;
    Button btnRegisterAgency;
    EditText edtCompanyCode;
    AlertDialog alertDialogSave;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Handler handler;
    Runnable r;
    int badgeCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MainMenuPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        context = this;
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        email = sharedPreferences.getString("isEmail", null);
        name = sharedPreferences.getString("isName", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);
        //googleAnalitic
        BaseApplication application = (BaseApplication) getApplication();
        mTracker = application.getDefaultTracker();
        animationButton();
        retry.setOnClickListener(this);
        all_project.setOnClickListener(this);
        mylisting.setOnClickListener(this);
        download.setOnClickListener(this);
        news.setOnClickListener(this);
        nup.setOnClickListener(this);
        event.setOnClickListener(this);
        commision.setOnClickListener(this);
        mybooking.setOnClickListener(this);
        calculator.setOnClickListener(this);
        listingProperty.setOnClickListener(this);
        ticket.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        textSet();
        contentMenu();
        serviceGCM();
        buidNewGoogleApiClient();
        cekChatting();
//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                cekChatting();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);


    }

    private void cekChatting() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getCountChat(memberRef);
    }

    private void serviceGCM() {
        //BroadcastReceiver GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Registration success
                    String token = intent.getStringExtra("token");
                    Log.d("GCMToken", token);
                    saveGCMTokenToServer(token);


                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {

                } else {

                }
            }
        };

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);

        }
    }

    private void contentMenu() {
        Glide.with(context).load(WebService.getProfile() + memberRef).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
        Log.d("memberRef", memberRef);

        /*if (memberType.startsWith("Property")) {
            listingProperty.setVisibility(View.VISIBLE);
            txtMyAgency.setVisibility(View.VISIBLE);
        } else {
            listingProperty.setVisibility(View.INVISIBLE);
            txtMyAgency.setVisibility(View.INVISIBLE);
            listingProperty.setEnabled(false);
        }*/

        if (memberType.startsWith("Inhouse")) {
            listingProperty.setVisibility(View.INVISIBLE);
            txtMyAgency.setVisibility(View.INVISIBLE);
            listingProperty.setEnabled(false);
        } else if (memberType.startsWith("Property")){
            listingProperty.setVisibility(View.VISIBLE);
            listingProperty.setBackgroundResource(R.drawable.selector_menu_listing_property);
            txtMyAgency.setVisibility(View.VISIBLE);
            //txtMyAgency.setText("My Agency");
        } else {
            listingProperty.setVisibility(View.VISIBLE);
            listingProperty.setBackgroundResource(R.drawable.selector_menu_cobroke);
            txtMyAgency.setVisibility(View.VISIBLE);
            //txtMyAgency.setText("Co-Broke");
        }
    }

    private void textSet() {
        nameProfile.setText(name);
        memberTypeName.setText(memberType);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("  Smart Selling Tools");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.icon_squares);

        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        all_project = (ImageButton) findViewById(R.id.image_all_project);
        download = (ImageButton) findViewById(R.id.image_download);
        commision = (ImageButton) findViewById(R.id.image_commision);
        mylisting = (ImageButton) findViewById(R.id.image_mylisting);
        nup = (ImageButton) findViewById(R.id.image_nup);
        event = (ImageButton) findViewById(R.id.image_event);
        news = (ImageButton) findViewById(R.id.image_news);
        mybooking = (ImageButton) findViewById(R.id.image_mybooking);
        calculator = (ImageButton) findViewById(R.id.image_calcultaor);
        listingProperty = (ImageButton) findViewById(R.id.image_listing_property);
        ticket = (ImageButton) findViewById(R.id.image_my_ticket);
        txtTicket = (TextView) findViewById(R.id.txt_ticket);
        txtMyAgency = (TextView) findViewById(R.id.txt_myAgency);

        linearLayoutTicetButton = (LinearLayout) findViewById(R.id.linear_imagebutton_ticket);
        linearLayoutTicetTextview = (LinearLayout) findViewById(R.id.linear_txt_tikcet);
        nameProfile = (TextView) findViewById(R.id.profilName);
        imgProfile = (ImageView) findViewById(R.id.profileImage);
        memberTypeName = (TextView) findViewById(R.id.memberTypeName);


    }

    private void animationButton() {
        Animation hyperspaceJump = AnimationUtils.loadAnimation(context, R.anim.hyperspace_jump);
        all_project.startAnimation(hyperspaceJump);
        download.startAnimation(hyperspaceJump);
        ticket.startAnimation(hyperspaceJump);
        commision.startAnimation(hyperspaceJump);
        mylisting.startAnimation(hyperspaceJump);
        nup.startAnimation(hyperspaceJump);
        news.startAnimation(hyperspaceJump);
        event.startAnimation(hyperspaceJump);
        mybooking.startAnimation(hyperspaceJump);
        calculator.startAnimation(hyperspaceJump);
        listingProperty.startAnimation(hyperspaceJump);
        stopService(new Intent(getApplicationContext(), GCMRegistrationIntentService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_profile:
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
                return true;

            case R.id.btn_change_password:
                startActivity(new Intent(MainMenuActivity.this, ChangePasswordActivity.class));
                return true;

            case R.id.btn_logout:
                dialogLogout();

                return true;

            case R.id.btn_contact_us:
                startActivity(new Intent(MainMenuActivity.this, ContactUsActivity.class));
                return true;

            case R.id.btn_about_us:
                startActivity(new Intent(MainMenuActivity.this, AboutUsActivity.class));
                return true;

            case R.id.btn_setting:
                startActivity(new Intent(MainMenuActivity.this, SettingNotificationActivity.class));
                return true;

            case R.id.btn_notification:
            startActivity(new Intent(MainMenuActivity.this, NotificationActivity.class));
            return true;

            case R.id.btn_exit:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogLogout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah anda ingin logout?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences sharedPreferences = MainMenuActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", false);
                        editor.putString("isEmail", "");
                        editor.putString("isName", "");
                        editor.putString("isMemberRef", "");
                        editor.putString("isMemberType", "");
                        editor.commit();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(MainMenuActivity.this, ViewpagerActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void buidNewGoogleApiClient() {

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, MainMenuActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cekChatting();

//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                cekChatting();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);


        requestListMyTicket();
//        cekChatting();
        String name = sharedPreferences.getString("isName", null);
        nameProfile.setText(name);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
        Glide.with(context).load(WebService.getProfile() + memberRef).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });

        Log.i(TAG, "Setting screen name: " + email);
        mTracker.setScreenName("Email ~ " + email);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void requestListMyTicket() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.requestTicket(memberRef);
    }

    //savegcmtoken
    private void saveGCMTokenToServer(final String GCMToken) {
        presenter.sendGCMToken(GCMToken, memberRef);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /*Myagency*/
    public void requestAgencyInfo() {
        presenter.requestAgencyInfo(memberRef);

    }

    public void registerCompany() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainMenuActivity.this);
        LayoutInflater inflater = MainMenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_insert_company_code, null);
        dialogBuilder.setView(dialogView);

        edtCompanyCode = (EditText) dialogView.findViewById(R.id.edt_company_code);
        btnRegisterAgency = (Button) dialogView.findViewById(R.id.btn_register_agency);

        dialogBuilder.setMessage("Masukkan Company Code");

        btnRegisterAgency.setTypeface(font);
        btnRegisterAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(WebService.linkRegisterAgency));
                startActivity(i);

            }
        });
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialogSave = dialogBuilder.create();
        alertDialogSave.setCancelable(false);
        alertDialogSave.show();
        Button positiveButton = alertDialogSave.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekCompanyCode = edtCompanyCode.getText().toString();

                if (cekCompanyCode.isEmpty()) {
                    edtCompanyCode.setError("Masukkan company code");
                } else {
                    insertAgecyCode();
                }

            }
        });
    }

    public void insertAgecyCode() {
        presenter.postRegisterAgency(memberRef, edtCompanyCode.getText().toString());
    }

    public void showRetroResults(retrofit2.Response<List<ListTicketModel>> response) {
        OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        progressDialog.dismiss();
        listTicketModels = response.body();
        if (listTicketModels.size() < 1) {
            TICKET = false;
        }
    }

    public void showRetroFailure(Throwable t) {
        OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
        progressDialog.dismiss();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_retry:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, LaunchActivity.class));
                finish();
                break;
            case R.id.image_all_project:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, ProjectActivity.class));
                break;
            case R.id.image_mylisting:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, MyListingActivity.class));
                break;
            case R.id.image_download:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, ListDownloadProjectActivity.class));
                break;
            case R.id.image_news:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, NewsActivity.class));
                break;
            case R.id.image_nup:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, MyNupActivity.class));
                break;
            case R.id.image_event:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, EventActivity.class));
                break;
            case R.id.image_commision:
//                handler.removeCallbacksAndMessages(r);
                Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
                break;
            case R.id.image_mybooking:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, MyBookingActivity.class));
                break;
            case R.id.image_calcultaor:
//                handler.removeCallbacksAndMessages(r);
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Kalkulator")
                        .setAction(email)
                        .build());
                startActivity(new Intent(MainMenuActivity.this, CalculationActivity.class));
                break;
            case R.id.image_listing_property:
//                handler.removeCallbacksAndMessages(r);
                requestAgencyInfo();
                break;
            case R.id.image_my_ticket:
//                handler.removeCallbacksAndMessages(r);
                if (TICKET) {
                    startActivity(new Intent(MainMenuActivity.this, MyTicketActivity.class));
                } else {
                    Toast.makeText(MainMenuActivity.this, "Anda belum memiiki ticket", Toast.LENGTH_SHORT).show();
                    ticket.setEnabled(true);
                }
                break;
            case R.id.profileImage:
//                handler.removeCallbacksAndMessages(r);
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
                break;

        }

    }

    public void showTicketResults(retrofit2.Response<StatusTokenGCMModel> responseToken) {
        int Status = responseToken.body().getStatus();
        String message = responseToken.body().getMessage();
        if (Status == 200) {
            Log.d("saveToken", message);
        } else {
            Log.d("saveToken", message);
        }


    }

    public void showTicketFailure(Throwable t) {

    }

    public void showAgencyResults(retrofit2.Response<ListingAgencyInfoModel> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            agencyCompanyRef = response.body().getAgencyCompanyRef();
            imageLogo = response.body().getImageLogo();
            memberTypeCode = response.body().getMemberType();
            companyName = response.body().getCompanyName();
            Intent intent = new Intent(MainMenuActivity.this, ListingMainActivity.class);
            intent.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent.putExtra("imageLogo", imageLogo);
            intent.putExtra("title",txtMyAgency.getText());
            intent.putExtra("memberTypeCode",memberTypeCode);
            intent.putExtra("companyName",companyName);
            startActivity(intent);
        } else if (status == 201) {
            registerCompany();
        }

    }

    public void showAgencyFailure(Throwable t) {

    }


    public void showRegisterResults(retrofit2.Response<ListingAgencyInfoModel> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            agencyCompanyRef = response.body().getAgencyCompanyRef();
            imageLogo = response.body().getImageLogo();
            memberTypeCode = response.body().getMemberType();
            companyName = response.body().getCompanyName();
            Intent intent = new Intent(MainMenuActivity.this, ListingMainActivity.class);
            intent.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent.putExtra("imageLogo", imageLogo);
            intent.putExtra("title",txtMyAgency.getText());
            intent.putExtra("memberTypeCode",memberTypeCode);
            intent.putExtra("companyName",companyName);
            startActivity(intent);
            alertDialogSave.dismiss();
        } else if (status == 201) {
            edtCompanyCode.setError("Company code tidak ditemukan");
        }
    }

    public void showRegisterFailure(Throwable t) {

    }

    public void showListCountResults(Response<ListCountChatModel> response) {
        String status = response.body().getStatus();
        if(status.equals("200")){
            int countChat = response.body().getMessage();

//            boolean success = ShortcutBadger.applyCount(this, countChat);
//            Intent intent = new Intent(Intent.ACTION_MAIN);
//            intent.addCategory(Intent.CATEGORY_HOME);
//            ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
//            String currentHomePackage = resolveInfo.activityInfo.packageName;
//            Log.d("Launcher",currentHomePackage);

            if(countChat>=1) {
                listingProperty.setBackgroundResource(R.drawable.selector_agency_msg_cobroke);
//                ShortcutBadger.applyCount(context, countChat);
               /* if (memberType.startsWith("Property")){
                    listingProperty.setBackgroundResource(R.drawable.selector_agency_msg);
                } else {
                    listingProperty.setBackgroundResource(R.drawable.selector_agency_msg_cobroke);
                }*/
                //listingProperty.setBackgroundResource(R.drawable.selector_agency_msg);
                /*BadgeFactory.create(this)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(25,25)
                .setBadgeBackground(Color.GREEN)
                .setTextSize(10)
                .setBadgeGravity(Gravity.RIGHT|Gravity.TOP)
                .setBadgeCount(countChat)
                .setShape(BadgeView.SHAPE_CIRCLE)
                .setMargin(0,0,5,0)
                .bind(listingProperty);*/
            }
            else {
                listingProperty.setBackgroundResource(R.drawable.selector_menu_cobroke);
                //listingProperty.setBackgroundResource(R.drawable.selector_menu_listing_property);
                /*if (memberType.startsWith("Property")){
                    listingProperty.setBackgroundResource(R.drawable.selector_menu_listing_property);
                } else {
                    listingProperty.setBackgroundResource(R.drawable.selector_menu_cobroke);
                }*/

            }
        }
    }

    public void showListCountFailure(Throwable t) {

    }
}
