package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterAddGaleryInteractor;
import com.nataproperty.android.nataproperty.network.IsCobrokeModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingAddGalleryActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AddGaleryPresenter implements PresenterAddGaleryInteractor {
    private ListingAddGalleryActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public AddGaleryPresenter(ListingAddGalleryActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void postIsCobroke(String listingRef, String memberRef, String isCobroke) {
        Call<IsCobrokeModel> call = service.getAPI().updateIsCobroke(listingRef,memberRef,isCobroke);
        call.enqueue(new Callback<IsCobrokeModel>() {
            @Override
            public void onResponse(Call<IsCobrokeModel> call, Response<IsCobrokeModel> response) {
                view.showUpdateCoBrokeResults(response);
            }

            @Override
            public void onFailure(Call<IsCobrokeModel> call, Throwable t) {
                view.showUpdateCoBrokeFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
