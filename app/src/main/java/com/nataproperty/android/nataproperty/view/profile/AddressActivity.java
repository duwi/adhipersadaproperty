package com.nataproperty.android.nataproperty.view.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.profile.CityAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.CountryAdapter;
import com.nataproperty.android.nataproperty.adapter.profile.ProvinceAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.CountryModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 4/25/2016.
 */
public class AddressActivity extends AppCompatActivity {
    private List<CountryModel> listCounty = new ArrayList<CountryModel>(); //model
    private CountryAdapter adapterCountry; //set adapter

    private List<ProvinceModel> listProvince = new ArrayList<ProvinceModel>(); //model
    private ProvinceAdapter adapterProvince; //set adapter

    private List<CityModel> listCity = new ArrayList<CityModel>(); //model
    private CityAdapter adapterCity; //set adapter

    private Spinner spinnerCountry, spinnerProvince, spinnerCity;
    private EditText editIdPostCode, editIdAddr;

    private Spinner spinnerCountryCorres, spinnerProvinceCorres, spinnerCityCorres;
    private EditText editIdPostCodeCorres, editIdAddrCorres;

    private Button btnSave;

    private CheckBox cbCorresAddress;

    LinearLayout rCorres;

    String memberRef;
    String idCountryCode, idProvinceCode, idCityCode, idAddr, idPostCode;
    String countryCode, provinceCode, cityCode;
    String countryCodeCorres, provinceCodeCorres, cityCodeCorres;
    String getIdCountryCode;
    String getProvinceSortNo, getCitySortNo, getCountrySortNo,
            getProvinceSortNoCorres, getCitySortNoCorres, getCountrySortNoCorres,
            getCorresPostCode, getCorresAddr, getMemberRef;

    ProgressDialog progressDialog;
    String message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_address));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        getIdCountryCode = intent.getStringExtra("idCountryCode");
        String getIdProvinceCode = intent.getStringExtra("idProvinceCode");
        String getIdCityCode = intent.getStringExtra("idCityCode");
        String getIdPostCode = intent.getStringExtra("idPostCode");
        String getIdAddr = intent.getStringExtra("idAddr");
        getProvinceSortNo = intent.getStringExtra("provinceSortNo");
        getCitySortNo = intent.getStringExtra("citySortNo");
        getCountrySortNo = intent.getStringExtra("countrySortNo");
        getMemberRef = intent.getStringExtra("memberRef");
        getCitySortNoCorres = intent.getStringExtra("citySortNoCorres");
        getCountrySortNoCorres = intent.getStringExtra("countrySortNoCorres");
        getProvinceSortNoCorres = intent.getStringExtra("proviceSortNoCorres");
        getCorresPostCode = intent.getStringExtra("corresPostCode");
        getCorresAddr = intent.getStringExtra("corresAddr");
        /**
         * id address
         */
        spinnerCountry = (Spinner) findViewById(R.id.spinner_country);
        spinnerProvince = (Spinner) findViewById(R.id.spinner_province);
        spinnerCity = (Spinner) findViewById(R.id.spinner_city);

        editIdPostCode = (EditText) findViewById(R.id.id_post_code);
        editIdAddr = (EditText) findViewById(R.id.id_address);

        editIdPostCode.setText(getIdPostCode);
        editIdAddr.setText(getIdAddr);

        /**
         * corress address
         */
        rCorres = (LinearLayout) findViewById(R.id.rCorres);
        spinnerCountryCorres = (Spinner) findViewById(R.id.spinner_country_corres);
        spinnerProvinceCorres = (Spinner) findViewById(R.id.spinner_province_corres);
        spinnerCityCorres = (Spinner) findViewById(R.id.spinner_city_corres);

        editIdPostCodeCorres = (EditText) findViewById(R.id.id_post_code_corres);
        editIdAddrCorres = (EditText) findViewById(R.id.id_address_corres);

        editIdPostCodeCorres.setText(getCorresPostCode);
        editIdAddrCorres.setText(getCorresAddr);
        /**
         * checkbox,button
         */
        cbCorresAddress = (CheckBox) findViewById(R.id.cb_corress_address);

        btnSave = (Button) findViewById(R.id.btnSaveAddress);
        btnSave.setTypeface(font);

        /**
         * id address
         */
        adapterCountry = new CountryAdapter(this, listCounty);
        spinnerCountry.setAdapter(adapterCountry);
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = listCounty.get(position).getCountryCode();

                Log.d("country code", countryCode);

                listProvince = new ArrayList<ProvinceModel>();
                requestProvince(countryCode);
                spinnerProvince.setAdapter(adapterProvince);

                if (!countryCode.equals("-")) {
                    if (getProvinceSortNo.equals("999")) {
                        spinnerProvince.setSelection(0);
                    } else {
                        spinnerProvince.setSelection(Integer.parseInt(getProvinceSortNo) - 1);
                    }
                    Log.d("SortNo province", getProvinceSortNo);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        requestCountry();

        spinnerProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();

                listCity = new ArrayList<CityModel>();
                requestCity(countryCode, provinceCode);
                spinnerCity.setAdapter(adapterCity);

                if (!provinceCode.equals("-")) {
                    if (getCitySortNo.equals("999")) {
                        spinnerCity.setSelection(0);
                    } else {
                        spinnerCity.setSelection(Integer.parseInt(getCitySortNo));
                    }
                    Log.d("SortNo city", getCitySortNo);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String cekPostCode = editIdPostCode.getText().toString();
        String cekPostCodeCorress = editIdPostCodeCorres.getText().toString();
        String cekAddress = editIdAddr.getText().toString();
        String cekAddressCorres = editIdAddrCorres.getText().toString();

        if(getCountrySortNoCorres.equals(getCountrySortNo) && getProvinceSortNoCorres.equals(getProvinceSortNo) &&
                getCitySortNoCorres.equals(getCitySortNo) && cekPostCode.equals(cekPostCodeCorress) &&
                cekAddress.equals(cekAddressCorres)){
            cbCorresAddress.setChecked(true);
            rCorres.setVisibility(View.GONE);

        }else{
            cbCorresAddress.setChecked(false);
            rCorres.setVisibility(View.VISIBLE);
        }

        //chekbox
        cbCorresAddress.setTypeface(font);
        cbCorresAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    rCorres.setVisibility(View.VISIBLE);
                } else {
                    rCorres.setVisibility(View.GONE);
                }
            }
        });

        /**
         * corress address
         */
        Log.d("cek get corres", "" + getCountrySortNoCorres + "-" + getProvinceSortNoCorres + "-" + getCitySortNoCorres);
        spinnerCountryCorres.setAdapter(adapterCountry);
        spinnerCountryCorres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCodeCorres = listCounty.get(position).getCountryCode();

//                Log.d("country code", countryCodeCorres);
                listProvince = new ArrayList<ProvinceModel>();
                requestProvince(countryCodeCorres);
                spinnerProvinceCorres.setAdapter(adapterProvince);

                if (!countryCodeCorres.equals("-")) {
                    if (getProvinceSortNoCorres.equals("999")) {
                        spinnerProvinceCorres.setSelection(0);
                    } else {
                        spinnerProvinceCorres.setSelection(Integer.parseInt(getProvinceSortNoCorres) - 1);
                    }
                    Log.d("SortNo province", getProvinceSortNoCorres);
                    Log.e("SortNo province", getProvinceSortNoCorres);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerProvinceCorres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCodeCorres = listProvince.get(position).getProvinceCode();

                listCity = new ArrayList<CityModel>();
                requestCity(countryCodeCorres, provinceCodeCorres);
                spinnerCityCorres.setAdapter(adapterCity);

                if (!provinceCodeCorres.equals("-")) {
                    if (getCitySortNoCorres.equals("999")) {
                        spinnerCityCorres.setSelection(0);
                    } else {
                        spinnerCityCorres.setSelection(Integer.parseInt(getCitySortNoCorres));
                    }
                    Log.d("SortNo city", getCitySortNoCorres);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCityCorres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCodeCorres = listCity.get(position).getCityCode();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        requestCountry();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateIdAddress(getMemberRef);
                Log.d("get", getMemberRef);
            }
        });

    }

    private void requestCountry() {
        String url = WebService.getCountry();
        String urlPostParameter = "";
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListCountry(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListCountry(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CountryModel contry = new CountryModel();
                contry.setCountryCode(jo.getString("countryCode"));
                contry.setCountryName(jo.getString("countryName"));

                listCounty.add(contry);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterCountry.notifyDataSetChanged();
        if (getCountrySortNo.equals("999")) {
            spinnerCountry.setSelection(Integer.parseInt(getProvinceSortNo));
        } else {
            spinnerCountry.setSelection(0);
        }

        if (getCountrySortNoCorres.equals("999")) {
            spinnerCountryCorres.setSelection(Integer.parseInt(getProvinceSortNoCorres));
        } else {
            spinnerCountryCorres.setSelection(0);
        }
    }

    private void requestProvince(final String countryCode) {
        String url = WebService.getProvince();
        String urlPostParameter = "&countryCode=" + countryCode.toString();
        Log.d("countryCode", countryCode.toString());
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListProvince(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListProvince(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProvinceModel province = new ProvinceModel();
                province.setProvinceCode(jo.getString("provinceCode"));
                province.setProvinceName(jo.getString("provinceName"));

                listProvince.add(province);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterProvince = new ProvinceAdapter(this, listProvince);
        adapterProvince.notifyDataSetChanged();
    }

    private void requestCity(final String countryCode, final String provinceCode) {
        String url = WebService.getCity();
        String urlPostParameter = "&countryCode=" + countryCode.toString() +
                "&provinceCode=" + provinceCode.toString();
        Log.d("countryCode", countryCode.toString() + " " + provinceCode.toString());
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListCity(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListCity(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CityModel city = new CityModel();
                city.setCityCode(jo.getString("cityCode"));
                city.setCityName(jo.getString("cityName"));

                listCity.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterCity = new CityAdapter(this, listCity);
        adapterCity.notifyDataSetChanged();
    }

    private void updateIdAddress(final String memberRef) {

        if(cbCorresAddress.isChecked()){
            countryCodeCorres = countryCode;
            provinceCodeCorres = provinceCode;
            cityCodeCorres = cityCode;
            editIdPostCodeCorres = editIdPostCode;
            editIdAddrCorres = editIdAddr;

        }

        String url = WebService.idAddress();
        String urlPostParameter = "&memberRef=" + memberRef +
                "&countryCode=" + countryCode +
                "&provinceCode=" + provinceCode +
                "&cityCode=" + cityCode +
                "&postCode=" + editIdPostCode.getText().toString() +
                "&address=" + editIdAddr.getText().toString() +
                "&corresCountryCode=" + countryCodeCorres +
                "&corresProvinceCode=" + provinceCodeCorres +
                "&corresCityCode=" + cityCodeCorres +
                "&corresPostCode=" + editIdPostCodeCorres.getText().toString() +
                "&corresAddress=" + editIdAddrCorres.getText().toString();

        Log.d("update idAddress", memberRef.toString() + " " + countryCode + " " + provinceCode + " " + cityCode + " " +
                editIdPostCode.getText().toString() + " " + editIdAddr.getText().toString());

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddressActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);

            try {
                JSONObject jo = new JSONObject(result);
                Log.d("result", result);
                int status = jo.getInt("status");
                message = jo.getString("message");
                if (status == 200) {

                    Intent intent = new Intent(AddressActivity.this, SectionProfileActivity.class);
                    SectionProfileActivity.getInstance().finish();
                    startActivity(intent);
                    finish();

                } else if (status == 201) {
                    message = jo.getString("message");

                } else {
                    //Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                //Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(AddressActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
