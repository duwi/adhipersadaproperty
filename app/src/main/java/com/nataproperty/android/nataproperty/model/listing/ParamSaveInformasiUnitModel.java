package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class ParamSaveInformasiUnitModel {
    String listingRef;
    String BRType;
    String maidBRType;
    String bathRType;
    String maidBathRType;
    String garageType;
    String carportType;
    String phoneLine;
    String furnishType;
    String electricType;
    String facingType;
    String viewType;
    String youTube1;
    String youTube2;

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }

    public String getBRType() {
        return BRType;
    }

    public void setBRType(String BRType) {
        this.BRType = BRType;
    }

    public String getMaidBRType() {
        return maidBRType;
    }

    public void setMaidBRType(String maidBRType) {
        this.maidBRType = maidBRType;
    }

    public String getBathRType() {
        return bathRType;
    }

    public void setBathRType(String bathRType) {
        this.bathRType = bathRType;
    }

    public String getMaidBathRType() {
        return maidBathRType;
    }

    public void setMaidBathRType(String maidBathRType) {
        this.maidBathRType = maidBathRType;
    }

    public String getGarageType() {
        return garageType;
    }

    public void setGarageType(String garageType) {
        this.garageType = garageType;
    }

    public String getCarportType() {
        return carportType;
    }

    public void setCarportType(String carportType) {
        this.carportType = carportType;
    }

    public String getPhoneLine() {
        return phoneLine;
    }

    public void setPhoneLine(String phoneLine) {
        this.phoneLine = phoneLine;
    }

    public String getFurnishType() {
        return furnishType;
    }

    public void setFurnishType(String furnishType) {
        this.furnishType = furnishType;
    }

    public String getElectricType() {
        return electricType;
    }

    public void setElectricType(String electricType) {
        this.electricType = electricType;
    }

    public String getFacingType() {
        return facingType;
    }

    public void setFacingType(String facingType) {
        this.facingType = facingType;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getYouTube1() {
        return youTube1;
    }

    public void setYouTube1(String youTube1) {
        this.youTube1 = youTube1;
    }

    public String getYouTube2() {
        return youTube2;
    }

    public void setYouTube2(String youTube2) {
        this.youTube2 = youTube2;
    }
}
