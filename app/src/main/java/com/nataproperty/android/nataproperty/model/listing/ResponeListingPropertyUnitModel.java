package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/7/2016.
 */

public class ResponeListingPropertyUnitModel {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String brType,maidBRType,bathRType,maidBathRType,garageType,carportType,phoneLine,furnishType,electricType,facingType,viewType,
            youtube1,youtube2;

    public String getBRType() {
        return brType;
    }

    public void setBRType(String BRType) {
        this.brType = BRType;
    }

    public String getMaidBRType() {
        return maidBRType;
    }

    public void setMaidBRType(String maidBRType) {
        this.maidBRType = maidBRType;
    }

    public String getBathRType() {
        return bathRType;
    }

    public void setBathRType(String bathRType) {
        this.bathRType = bathRType;
    }

    public String getMaidBathRType() {
        return maidBathRType;
    }

    public void setMaidBathRType(String maidBathRType) {
        this.maidBathRType = maidBathRType;
    }

    public String getGarageType() {
        return garageType;
    }

    public void setGarageType(String garageType) {
        this.garageType = garageType;
    }

    public String getCarportType() {
        return carportType;
    }

    public void setCarportType(String carportType) {
        this.carportType = carportType;
    }

    public String getPhoneLine() {
        return phoneLine;
    }

    public void setPhoneLine(String phoneLine) {
        this.phoneLine = phoneLine;
    }

    public String getFurnishType() {
        return furnishType;
    }

    public void setFurnishType(String furnishType) {
        this.furnishType = furnishType;
    }

    public String getElectricType() {
        return electricType;
    }

    public void setElectricType(String electricType) {
        this.electricType = electricType;
    }

    public String getFacingType() {
        return facingType;
    }

    public void setFacingType(String facingType) {
        this.facingType = facingType;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getBrType() {
        return brType;
    }

    public void setBrType(String brType) {
        this.brType = brType;
    }

    public String getYoutube1() {
        return youtube1;
    }

    public void setYoutube1(String youtube1) {
        this.youtube1 = youtube1;
    }

    public String getYoutube2() {
        return youtube2;
    }

    public void setYoutube2(String youtube2) {
        this.youtube2 = youtube2;
    }
}
