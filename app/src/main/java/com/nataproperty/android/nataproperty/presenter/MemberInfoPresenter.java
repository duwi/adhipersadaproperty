package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresentermemberInfoInteractor;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingSearchMemberInfoActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MemberInfoPresenter implements PresentermemberInfoInteractor {
    private ListingSearchMemberInfoActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MemberInfoPresenter(ListingSearchMemberInfoActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }




    @Override
    public void getProvinsiListing(String memberRef) {
        Call<List<ProvinceModel>> call = service.getAPI().getListProvinsiListListing(memberRef);
        call.enqueue(new Callback<List<ProvinceModel>>() {
            @Override
            public void onResponse(Call<List<ProvinceModel>> call, Response<List<ProvinceModel>> response) {
                view.showProvinsiResults(response);
            }

            @Override
            public void onFailure(Call<List<ProvinceModel>> call, Throwable t) {
                view.showProvinsiFailure(t);

            }


        });
    }

    @Override
    public void getCityListing(String countryCode, String provinceCode) {
        Call<List<CityModel>> call = service.getAPI().getListCityListListing(countryCode,provinceCode);
        call.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
                view.showCityResults(response);
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                view.showCityFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
