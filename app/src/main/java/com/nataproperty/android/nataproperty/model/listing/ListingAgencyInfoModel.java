package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by User on 9/26/2016.
 */
public class ListingAgencyInfoModel {
    int status;
    String message;
    String psRef;
    String agencyCompanyRef;
    String imageLogo;
    String memberType;
    String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }
}
