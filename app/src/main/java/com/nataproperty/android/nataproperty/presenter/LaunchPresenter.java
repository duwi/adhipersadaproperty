package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterLaunchInteractor;
import com.nataproperty.android.nataproperty.model.before_login.VersionModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LaunchPresenter implements PresenterLaunchInteractor {
    private LaunchActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public LaunchPresenter(LaunchActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getVersionApp(String version) {
        Call<VersionModel> call = service.getAPI().getNewVersion(version);
        call.enqueue(new Callback<VersionModel>() {
            @Override
            public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                view.showVersionAppResults(response);
            }

            @Override
            public void onFailure(Call<VersionModel> call, Throwable t) {
                view.showVersionAppFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
