package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterChattingRoomInteractor {
    void getSendMsg(String memberRefSender, String memberRefReceiver, String messaging);
    void rxUnSubscribe();

}
