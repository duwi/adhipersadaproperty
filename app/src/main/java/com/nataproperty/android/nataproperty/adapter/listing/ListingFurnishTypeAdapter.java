package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.FurnishTypeModel;

import java.util.List;

/**
 * Created by User on 4/17/2016.
 */
public class ListingFurnishTypeAdapter extends BaseAdapter {
    private Context context;
    private List<FurnishTypeModel> list;
    private ListProvinceHolder holder;

    public ListingFurnishTypeAdapter(Context context, List<FurnishTypeModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list,null);
            holder = new ListProvinceHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);

            convertView.setTag(holder);
        }else{
            holder = (ListProvinceHolder) convertView.getTag();
        }
        FurnishTypeModel model = list.get(position);
        holder.name.setText(model.getFurnishTypeName());

        return convertView;
    }

    private class ListProvinceHolder {
        TextView name;
    }
}
