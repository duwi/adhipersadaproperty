package com.nataproperty.android.nataproperty.view.project;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.MyListingAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.MyLIstingModel;
import com.nataproperty.android.nataproperty.model.project.MyLIstingModelDao;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.project.card.RVMyListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/2/2016.
 */
public class MyListingActivity extends AppCompatActivity {
    public static final String TAG = "MyListingActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    public static final String MEMBER_REF = "memberRef";

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<MyLIstingModel> listProject = new ArrayList<>();
    private MyListingAdapter adapter;

    SharedPreferences sharedPreferences;
    RecyclerView rv_myproject;
    private boolean state;
    private String memberRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Display display;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylisting);
        initWidget();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My Project");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        display = getWindowManager().getDefaultDisplay();


//        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
//        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);


        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        String name = sharedPreferences.getString("isName", null);
        requestProject();
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyListingActivity.this, LaunchActivity.class));
                finish();
            }
        });
//        listView = (ListView) findViewById(R.id.list_mylisting);


//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                listProject.clear();
//                requestProject();
//            }
//        });
//
//        swipeRefreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                swipeRefreshLayout.setRefreshing(true);
//                requestProject();
//            }
//        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(MyListingActivity.this, ProjectMenuActivity.class);
//                intent.putExtra(PROJECT_REF, listProject.get(position).getProjectRef());
//                intent.putExtra(DBMASTER_REF, listProject.get(position).getDbMasterRef());
//                intent.putExtra(PROJECT_NAME, listProject.get(position).getProjectName());
//                intent.putExtra(LOCATION_NAME, listProject.get(position).getLocationName());
//                intent.putExtra(LOCATION_REF, listProject.get(position).getLocationRef());
//                intent.putExtra(SUBLOCATION_NAME, listProject.get(position).getSubLocationName());
//                intent.putExtra(SUBLOCATION_REF, listProject.get(position).getSublocationRef());
//
//                Log.d("CEK", "Ref " + listProject.get(position).getProjectRef());
//
//                MyListingActivity.this.startActivity(intent);
//            }
//        });

    }

    public void requestProject() {
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        final ProgressDialog progress = ProgressDialog.show(this, "", "Please Wait...", true);
        //BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMyListing(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                progress.dismiss();
                snackBarBuatan.setVisibility(View.GONE);
                LoadingBar.stopLoader();
                //BaseApplication.getInstance().stopLoader();
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        generateListProject(jsonArray);
                        for ( int i = 0; i < jsonArray.length();i++) {
                            JSONObject data = jsonArray.getJSONObject(i);
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            MyLIstingModel projectModel = gson.fromJson("" + data, MyLIstingModel.class);
                            projectModel.setDbMasterRef(data.optLong("dbMasterRef"));
                            projectModel.setProjectRef(data.optString("projectRef"));
                            projectModel.setLocationRef(data.optString("locationRef"));
                            projectModel.setLocationName(data.optString("locationName"));
                            projectModel.setSublocationRef(data.optString("sublocationRef"));
                            projectModel.setSubLocationName(data.optString("subLocationName"));
                            projectModel.setProjectName(data.optString("projectName"));
                            projectModel.setIsJoin(data.optString("isJoin"));
                            MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
                            projectModelDao.insertOrReplace(projectModel);
                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder =
                                new AlertDialog.Builder(MyListingActivity.this);
                        alertDialogBuilder.setMessage("Anda belum join project, pilih project sekarang.");
                        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                startActivity(new Intent(MyListingActivity.this, ProjectActivity.class));
                                finish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

//                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackBarBuatan.setVisibility(View.VISIBLE);
//                        progress.dismiss();
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
//                        swipeRefreshLayout.setRefreshing(false);
                        MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
                        List<MyLIstingModel> projectModels = projectModelDao.queryBuilder()
                                .list();
                        Log.d("getProjectLocalDB", "" + projectModels.toString());
                        int numData = projectModels.size();
                        if (numData > 0) {
                            for (int i = 0; i<numData; i++) {
                                MyLIstingModel projectModel = projectModels.get(i);
                                listProject.add(projectModel);
                                initializeAdapter();

                            }
                        }
                        else {
                            finish();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(MEMBER_REF, memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inpute");

    }

    private void generateListProject(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                MyLIstingModel project = new MyLIstingModel();
                project.setDbMasterRef(jo.getLong("dbMasterRef"));
                project.setProjectRef(jo.getString("projectRef"));
                project.setLocationRef(jo.getString("locationRef"));
                project.setLocationName(jo.getString("locationName"));
                project.setSublocationRef(jo.getString("sublocationRef"));
                project.setSubLocationName(jo.getString("subLocationName"));
                project.setProjectName(jo.getString("projectName"));

                listProject.add(project);
                initializeAdapter();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void initListView() {
//        if (listProject.size() > 0) {
//            adapter = new MyListingAdapter(this, listProject, display);
//            adapter.notifyDataSetChanged();
//            listView.setAdapter(adapter);
//            listView.setTextFilterEnabled(true);
//        } else {
//            AlertDialog.Builder alertDialogBuilder =
//                    new AlertDialog.Builder(this);
//            alertDialogBuilder.setMessage("Anda belum memiliki project, anda ingin membuatnya?");
//            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    startActivity(new Intent(MyListingActivity.this, ProjectActivity.class));
//
//                }
//            });
//            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//
//                }
//            });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyListingActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initializeAdapter() {
        RVMyListAdapter adapter = new RVMyListAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);
        //swipeRefreshLayout.setRefreshing(false);
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

}
