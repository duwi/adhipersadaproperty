package com.nataproperty.android.nataproperty.view.listing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCategoryTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCityAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingListingTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPropertyAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingProvinceAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingSubLocationAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.listing.ListingCategoryTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingCategoryTypeModelDao;
import com.nataproperty.android.nataproperty.model.listing.ListingListingTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingListingTypeModelDao;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.model.listing.ResponeLookupListingPropertyModel;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModelDao;
import com.nataproperty.android.nataproperty.model.profile.SubLocationModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ListingSearchPropertyPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingSearchPropertyActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingSearchProperty";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListingSearchPropertyPresenter presenter;
    ProgressDialog progressDialog;
    int setCity = 0;

    private ListView listView;
    private ArrayList<ListingPropertyModel> listProperty = new ArrayList<ListingPropertyModel>();
    private List<CityModel> listCity = new ArrayList<>();
    private List<SubLocationModel> listSublocation = new ArrayList<>();
    int sublocationRef = 0;
    private ListingPropertyAdapter adapter;
    private ListingCityAdapter adapterCity;
    private ListingSubLocationAdapter adapterSubLocation;
    private ListingCategoryTypeAdapter listingCategoryTypeAdapter;
    private ListingListingTypeAdapter listingListingTypeAdapter;
    Spinner spinnerProvinsi, spinnerKota, spinnerArea, spinnerListing, spinnerProperty;

    private SharedPreferences sharedPreferences;
    private EditText edtSearch;
    private TextView txtClose;
    private LinearLayout linearLayoutNoData;
    private ImageView imageView;
    private TextView txtName, txtEmail, txtHp, txtPosition, txtAddress, txtAboutMe, txtQuotes;
    private AlertDialog alertDialog;
    private ImageView imageProfile;
    private ListingProvinceAdapter adapterProvince;

    private String agencyCompanyRef, psRef;
    private int countTotal;
    private int page = 1;
    Toolbar toolbar;
    Typeface font;
    Display display;
    Button nextBtn;
    MyTextViewLatoReguler title;
    int setProvince = 0;
    private List<ProvinceModel> listProvince = new ArrayList<>();
    private List<ListingCategoryTypeModel> listCategorytype = new ArrayList<>();
    private List<ListingListingTypeModel> listListingType = new ArrayList<>();
    private String imageLogo, propertyRef = "", countryCode = "INA", provinceCode = "", cityCode = "",
            locationRef = "", subLocationRef = "", categoryType, listingTypeRef;


    private String name, email, hp, jabatan, listingMemberRef, address, aboutMe, quotes, memberType,
            imageCover, listingRef, memberTypeCode, toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_property);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingSearchPropertyPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberType = sharedPreferences.getString("isMemberType", null);
        Intent intent = getIntent();
        psRef = intent.getStringExtra("psRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        toolbarTitle = intent.getStringExtra("toolbarTitle");

        initWidget();
        presenter.getProvinsiListing(countryCode);
        //presenter.getProperty("");
        presenter.getLookupListingProperty();
        nextBtn.setOnClickListener(this);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        title.setText("Search Property");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        spinnerProvinsi = (Spinner) findViewById(R.id.spn_province);
        spinnerKota = (Spinner) findViewById(R.id.spn_city);
        spinnerArea = (Spinner) findViewById(R.id.spn_sub_location);
        spinnerListing = (Spinner) findViewById(R.id.spn_listing_type);
        spinnerProperty = (Spinner) findViewById(R.id.spn_category_type);
        nextBtn = (Button) findViewById(R.id.btn_next);
    }

    private void requestCity(String countryCode, String provinceCode) {
        presenter.getCityListing(countryCode, provinceCode);
    }

    private void requestSubLocation(String countryCode, String provinceCode, String cityCode) {
        presenter.getSubLocListing(countryCode, provinceCode, cityCode);
    }

    public void showSubLocResults(retrofit2.Response<List<SubLocationModel>> response) {
        listSublocation = response.body();
        /*final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        for (int i = 0; i < listSublocation.size(); i++) {
            SubLocationModel subLocationModel = new SubLocationModel();
            subLocationModel.setId(i);
            subLocationModel.setSubLocationName(listSublocation.get(i).getSubLocationName());
            subLocationModel.setSubLocationRef(listSublocation.get(i).getSubLocationRef());
            SubLocationModelDao subLocationModelDao = daoSession.getSubLocationModelDao();
            subLocationModelDao.insertOrReplace(subLocationModel);
            String subLocationRefSet = listSublocation.get(i).getSubLocationRef();
            if (subLocationRef.equals(subLocationRefSet)) {
                sublocationRef = i;
            }

        }*/
        spinLocation();
    }

    public void showSubLocFailure(Throwable t) {
        /*final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        SubLocationModelDao subLocationModelDao = daoSession.getSubLocationModelDao();
        List<SubLocationModel> projectModels = subLocationModelDao.queryBuilder()
                .list();
        Log.d("getProjectLocalDB", "" + projectModels.toString());
        int numData = projectModels.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                SubLocationModel projectModel = projectModels.get(i);
                listSublocation.add(projectModel);
                spinLocation();
            }
        } else {
            finish();
        }*/
    }

    private void spinLocation() {
        adapterSubLocation = new ListingSubLocationAdapter(this, listSublocation);
        spinnerArea.setAdapter(adapterSubLocation);
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationRef = listSublocation.get(position).getSubLocationRef();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showCityResults(retrofit2.Response<List<CityModel>> response) {
        listCity = response.body();
        /*final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        for (int i = 0; i < listCity.size(); i++) {
            CityModel cityModel = new CityModel();
            cityModel.setId(i);
            cityModel.setProvinceCode(listCity.get(i).getProvinceCode());
            cityModel.setCountryCode(listCity.get(i).getCountryCode());
            cityModel.setCityCode(listCity.get(i).getCityCode());
            cityModel.setCityName(listCity.get(i).getCityName());
            CityModelDao cityModelDao = daoSession.getCityModelDao();
            cityModelDao.insertOrReplace(cityModel);
            String cityCodeSet = listCity.get(i).getCityCode();
            if (cityCode.equals(cityCodeSet)) {
                setCity = i;
            }
        }*/
        spinCity();

    }

    private void spinCity() {
        adapterCity = new ListingCityAdapter(this, listCity);
        spinnerKota.setAdapter(adapterCity);
        spinnerKota.setSelection(setCity);
        spinnerKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();
                requestSubLocation("INA", provinceCode, cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void showCityFailure(Throwable t) {
/*        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        CityModelDao cityModelDao = daoSession.getCityModelDao();
        List<CityModel> projectModels = cityModelDao.queryBuilder()
                .list();
        Log.d("getProjectLocalDB", "" + projectModels.toString());
        int numData = projectModels.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                CityModel projectModel = projectModels.get(i);
                listCity.add(projectModel);
                spinCity();
            }
        } else {
            finish();
        }*/

    }


    public void showProvinsiResults(retrofit2.Response<List<ProvinceModel>> response) {
        listProvince = response.body();
        /*final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        for (int i = 0; i < listProvince.size(); i++) {
            ProvinceModel provinceModel = new ProvinceModel();
            provinceModel.setId(i);
            provinceModel.setCountryCode(listProvince.get(i).getCountryCode());
            provinceModel.setProvinceCode(listProvince.get(i).getProvinceCode());
            provinceModel.setProvinceName(listProvince.get(i).getProvinceName());
            ProvinceModelDao provinceModelDao = daoSession.getProvinceModelDao();
            provinceModelDao.insertOrReplace(provinceModel);
            String provinceCodeSet = listProvince.get(i).getProvinceCode();
            if (provinceCode.equals(provinceCodeSet)) {
                setProvince = i;
            }

        }*/
        spinProvinsi();
    }

    private void spinProvinsi() {
        adapterProvince = new ListingProvinceAdapter(this, listProvince);
        spinnerProvinsi.setAdapter(adapterProvince);
        spinnerProvinsi.setSelection(setProvince);
        spinnerProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();
//                listCity.clear();
                requestCity(countryCode, provinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void showProvinsiFailure(Throwable t) {
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        ProvinceModelDao provinceModelDao = daoSession.getProvinceModelDao();
        List<ProvinceModel> projectModels = provinceModelDao.queryBuilder()
                .list();
        Log.d("getProjectLocalDB", "" + projectModels.toString());
        int numData = projectModels.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                ProvinceModel projectModel = projectModels.get(i);
                listProvince.add(projectModel);
                spinProvinsi();
            }
        } else {
            finish();
        }

    }

    private void spinProperty() {
        listingCategoryTypeAdapter = new ListingCategoryTypeAdapter(this, listCategorytype);
        spinnerProperty.setAdapter(listingCategoryTypeAdapter);
        spinnerProperty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryType = listCategorytype.get(position).getCategoryType();
                Log.d("categoryType", "" + categoryType.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void spinList() {
        listingListingTypeAdapter = new ListingListingTypeAdapter(this, listListingType);
        spinnerListing.setAdapter(listingListingTypeAdapter);
        spinnerListing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listingTypeRef = listListingType.get(position).getListingTypeRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showLookupListingPropertyResults(Response<ResponeLookupListingPropertyModel> response) {
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        listListingType = response.body().getListListingType();
        for (int i = 0; i < listListingType.size(); i++) {
            ListingListingTypeModel listingPropertyStatusModel = new ListingListingTypeModel();
            listingPropertyStatusModel.setId(i);
            listingPropertyStatusModel.setListingTypeName(listListingType.get(i).getListingTypeName());
            listingPropertyStatusModel.setListingTypeRef(listListingType.get(i).getListingTypeRef());
            ListingListingTypeModelDao listingListingTypeModelDao = daoSession.getListingListingTypeModelDao();
            listingListingTypeModelDao.insertOrReplace(listingPropertyStatusModel);

        }
        listCategorytype = response.body().getListCategoryType();
        for (int i = 0; i < listCategorytype.size(); i++) {
            ListingCategoryTypeModel listingCategoryTypeModel = new ListingCategoryTypeModel();
            listingCategoryTypeModel.setId(i);
            listingCategoryTypeModel.setCategoryType(listCategorytype.get(i).getCategoryType());
            listingCategoryTypeModel.setCategoryTypeGroup(listCategorytype.get(i).getCategoryTypeGroup());
            listingCategoryTypeModel.setCategoryTypeName(listCategorytype.get(i).getCategoryTypeName());
            ListingCategoryTypeModelDao listingCategoryTypeModelDao = daoSession.getListingCategoryTypeModelDao();
            listingCategoryTypeModelDao.insertOrReplace(listingCategoryTypeModel);

        }
        spinList();
        spinProperty();
    }

    public void showLookupListingPropertyFailure(Throwable t) {

    }

    public void showListPropertyResults(retrofit2.Response<ListingPropertyStatus> response) {


    }

    public void showListPropertyFailure(Throwable t) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                Intent intent = new Intent(this, ListingSearchResultActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("provinceCode", provinceCode);
                intent.putExtra("cityCode", cityCode);
                intent.putExtra("locationRef", locationRef);
                intent.putExtra("listingTypeRef", listingTypeRef);
                intent.putExtra("categoryType", categoryType);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("toolbarTitle", toolbarTitle);

                startActivity(intent);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
