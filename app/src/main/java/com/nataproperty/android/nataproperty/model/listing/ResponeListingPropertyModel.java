package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class ResponeListingPropertyModel {
    int status;
    String message;
    String listingRef;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }
}
