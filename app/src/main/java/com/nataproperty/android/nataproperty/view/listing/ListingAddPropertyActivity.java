package com.nataproperty.android.nataproperty.view.listing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCategoryTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCertificateAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCityAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCountryAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingListingTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPriceTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPropertyNameAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingProvinceAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingSubLocationAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.helper.NumberTextWatcher;
import com.nataproperty.android.nataproperty.model.listing.ListingCategoryTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingCertificateModel;
import com.nataproperty.android.nataproperty.model.listing.ListingListingTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPriceTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyNameModel;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.CountryModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.model.profile.SubLocationModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListingAddPropertyActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "ListingAddProperty";

    public static final String PREF_NAME = "pref";

    private ArrayList<String> list = new ArrayList<String>();

    private ArrayList<ListingPropertyNameModel> listPropertyName = new ArrayList<ListingPropertyNameModel>();
    private ArrayList<ListingListingTypeModel> listListingType = new ArrayList<ListingListingTypeModel>();
    private ArrayList<ListingCategoryTypeModel> listCategorytype = new ArrayList<ListingCategoryTypeModel>();
    private ArrayList<ListingCertificateModel> listCertificate = new ArrayList<ListingCertificateModel>();
    private ArrayList<ListingPriceTypeModel> listPriceType = new ArrayList<ListingPriceTypeModel>();

    private List<CountryModel> listCounty = new ArrayList<CountryModel>();
    private ListingCountryAdapter adapterCountry;
    private List<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    private ListingProvinceAdapter adapterProvince;
    private List<CityModel> listCity = new ArrayList<CityModel>();
    private ListingCityAdapter adapterCity;
    private List<SubLocationModel> listSublocation = new ArrayList<SubLocationModel>();
    private ListingSubLocationAdapter adapterSubLocation;

    private ListingPropertyNameAdapter listingPropertyNameAdapter;

    private ListingListingTypeAdapter listingListingTypeAdapter;
    private ListingCategoryTypeAdapter listingCategoryTypeAdapter;
    private ListingCertificateAdapter listingCertificateAdapter;
    private ListingPriceTypeAdapter listingPriceTypeAdapter;

    private String memberRef, psRef, agencyCompanyRef, imageLogo, propertyRef = "", countryCode = "",
            provinceCode = "", cityCode = "", subLocation = "", subLocationRef = "";
    private SharedPreferences sharedPreferences;

    private String listingTypeName, categoryTypeName, certName, priceTypeName, listingTitle, listingDesc,
            countryName, provinceName, cityName, subLocationName, block, no, postCode, gMap, price, buildArea,
            buildDimX, buildDimY, landArea, landDimX, landDimY, brTypeName, maidBRTypeName, bathRTypeName,
            maidBathRTypeName, garageTypeName, carportTypeName, phoneLineName, furnishTypeName, electricTypeName,
            facingName, viewTypeName, youTube1, youTube2, address, facilityName, cekFacility, countrySortNo, provinceSortNo, citySortNo,
            propertyName;

    private static String listingTypeRef = "1", categoryType = "1", priceType, certRef, listingRef = "",isCobrokePref;

    private Typeface font;

    private  ProgressDialog progressDialog;
//    String jual,sewa,rumah,apartement,ruko,kantor,gudang,tanah;

    //    @Bind(R.id.spn_listing_type)
//    Spinner spnListingType;
//    @Bind(R.id.spn_category_type)
//    Spinner spnCategoryType;
    @Bind(R.id.spn_country)
    Spinner spnCountry;
    @Bind(R.id.spn_province)
    Spinner spnProvince;
    @Bind(R.id.spn_city)
    Spinner spnCity;
    @Bind(R.id.spn_sub_location)
    Spinner spnSubLocation;
    @Bind(R.id.spn_price_type)
    Spinner spnPriceType;
    @Bind(R.id.spn_certificate)
    Spinner spnCertivicate;
    @Bind(R.id.edt_property_name)
    AutoCompleteTextView edtPropertyName;
    @Bind(R.id.edt_listing_title)
    EditText edtListingTitle;
    @Bind(R.id.edt_listing_desc)
    EditText edtListingDesc;
    @Bind(R.id.edt_price)
    EditText edtPrice;
    @Bind(R.id.edt_land)
    EditText edtLand;
    @Bind(R.id.edt_build)
    EditText edtBuild;
    @Bind(R.id.edt_no)
    EditText edtNo;
    @Bind(R.id.edt_block)
    EditText edtBlock;
    @Bind(R.id.edt_address)
    EditText edtAddress;
    @Bind(R.id.edt_post_code)
    EditText edtPostCode;
    @Bind(R.id.edt_buildDimX)
    EditText edtBuildDimX;
    @Bind(R.id.edt_buildDimY)
    EditText edtBuildDimY;
    @Bind(R.id.edt_landDimX)
    EditText edtLandDimX;
    @Bind(R.id.edt_landDimY)
    EditText edtLandDimY;

    @Bind(R.id.btn_next)
    Button btnNext;

    @Bind(R.id.txt_build)
    TextView txtBuild;
    TextView txtLand;

    MyAdapter dataAdapter = null;
    private ArrayList<ListingPropertyNameModel> suggestions = new ArrayList<>();
    Toolbar toolbar;

    MyTextViewLatoReguler title;
//    static String
    Button btnJual, btnSewa;
    private static String status, apartement, kondo, tanah, villa, ruko, kantor, pabrik, gudang, kios, type, memberTypeCode, isCobroke = "0";
    ImageButton btnRumah, btnApartemen, btnKondo, btnTanah, btnVilla, btnRuko, btnKantor, btnPabrik, btnGudang, btnKios;

    SwitchCompat switchCompatCobroke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_property);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        listingRef = intent.getStringExtra("listingRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        status = intent.getStringExtra("status");

        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        Log.d(TAG, "listingRef " + listingRef);
        initWidget();

        edtPropertyName.setThreshold(1);
        edtPropertyName.setTypeface(font);
        edtPropertyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) suggestions.get(position).getPropertyName();
                edtPropertyName.setText(str);

                propertyRef = suggestions.get(position).getPropertyRef();
                //Toast.makeText(ListingAddPropertyActivity.this, propertyRef, Toast.LENGTH_LONG).show();
            }
        });

//        listingListingTypeAdapter = new ListingListingTypeAdapter(this, listListingType);
//        spnListingType.setAdapter(listingListingTypeAdapter);
//        spnListingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                listingTypeRef = listListingType.get(position).getListingTypeRef();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

//        listingCategoryTypeAdapter = new ListingCategoryTypeAdapter(this, listCategorytype);
//        spnCategoryType.setAdapter(listingCategoryTypeAdapter);
//        spnCategoryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                categoryType = listCategorytype.get(position).getCategoryType();
//                Log.d("categoryType", "" + categoryType.toString());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        listingPriceTypeAdapter = new ListingPriceTypeAdapter(this, listPriceType);
        spnPriceType.setAdapter(listingPriceTypeAdapter);
        spnPriceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                priceType = listPriceType.get(position).getPriceType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingCertificateAdapter = new ListingCertificateAdapter(this, listCertificate);
        spnCertivicate.setAdapter(listingCertificateAdapter);
        spnCertivicate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                certRef = listCertificate.get(position).getCertRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterCountry = new ListingCountryAdapter(this, listCounty);
        spnCountry.setAdapter(adapterCountry);
        spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = listCounty.get(position).getCountryCode();
                listProvince.clear();
                requestProvince(countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterProvince = new ListingProvinceAdapter(this, listProvince);
        spnProvince.setAdapter(adapterProvince);
        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();
                listCity.clear();
                requestCity(countryCode, provinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterCity = new ListingCityAdapter(this, listCity);
        spnCity.setAdapter(adapterCity);
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();
                listSublocation.clear();
                requestSubLocation(countryCode, provinceCode, cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterSubLocation = new ListingSubLocationAdapter(this, listSublocation);
        spnSubLocation.setAdapter(adapterSubLocation);
        spnSubLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subLocation = listSublocation.get(position).getSubLocationRef();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        txtBuild.setText(Html.fromHtml("Luas Bangunan (m<sup><small>2</small></sup></td>)"));
        txtLand.setText(Html.fromHtml("Luas Tanah (m<sup><small>2</small></sup></td>)"));

        edtPrice.addTextChangedListener(new NumberTextWatcher(edtPrice));
        edtBuild.addTextChangedListener(new NumberTextWatcher(edtBuild));
        edtLand.addTextChangedListener(new NumberTextWatcher(edtLand));

        requestListingAddPropertyName();
        requestListingAddProperty();
        requestCountry();

        if (!listingRef.equals("")) {
            requestPropertyInfo(listingRef);
        } else {
            //propertyType();
        }

       /* if (memberTypeCode.equals("2")) {
            switchCompatCobroke.setVisibility(View.VISIBLE);
        } else {
            switchCompatCobroke.setVisibility(View.GONE);
        }

        switchCompatCobroke.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isCobroke = "1";
                } else {
                    isCobroke = "0";
                }

            }
        });*/
        propertyType();
        btnJual.setOnClickListener(this);
        btnSewa.setOnClickListener(this);
        btnRumah.setOnClickListener(this);
        btnApartemen.setOnClickListener(this);
        btnKondo.setOnClickListener(this);
        btnTanah.setOnClickListener(this);
        btnVilla.setOnClickListener(this);
        btnRuko.setOnClickListener(this);
        btnKantor.setOnClickListener(this);
        btnPabrik.setOnClickListener(this);
        btnGudang.setOnClickListener(this);
        btnKios.setOnClickListener(this);

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekPropertyName = edtPropertyName.getText().toString();
                String cekListingTitle = edtListingTitle.getText().toString();
                String cekPrice = edtPrice.getText().toString();
                String cekBuild = edtBuild.getText().toString();
                String cekLand = edtLand.getText().toString();

                if (cekPropertyName.isEmpty() || cekListingTitle.isEmpty() || cekPrice.isEmpty() || cekBuild.isEmpty() || cekLand.isEmpty()) {
                    if (cekListingTitle.isEmpty()) {
                        edtPropertyName.setError(getString(R.string.isEmpty));
                        //edtPropertyName.requestFocus();
                    } else {
                        edtPropertyName.setError(null);
                    }

                    if (cekListingTitle.isEmpty()) {
                        edtListingTitle.setError(getString(R.string.isEmpty));
                        //edtListingTitle.requestFocus();
                    } else {
                        edtListingTitle.setError(null);
                    }

                    if (cekPrice.isEmpty()) {
                        edtPrice.setError(getString(R.string.isEmpty));
                        //edtPrice.requestFocus();
                    } else {
                        edtPrice.setError(null);
                    }

                    if (cekBuild.isEmpty()) {
                        edtBuild.setError(getString(R.string.isEmpty));
                        //edtBuild.requestFocus();
                    } else {
                        edtBuild.setError(null);
                    }

                    if (cekLand.isEmpty()) {
                        edtLand.setError(getString(R.string.isEmpty));
                        //edtLand.requestFocus();
                    } else {
                        edtLand.setError(null);
                    }

                } else {

                    if (listingRef.equals("")) {
                        insertListingProperty();
                    } else {
                        updateListingProperty();
                    }

                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Listing Property");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtLand = (TextView) findViewById(R.id.txt_land);
        btnJual = (Button) findViewById(R.id.btn_type_jual);
        btnSewa = (Button) findViewById(R.id.btn_type_sewa);
        btnRumah = (ImageButton) findViewById(R.id.btn_rumah);
        btnApartemen = (ImageButton) findViewById(R.id.btn_apartemen);
        btnKondo = (ImageButton) findViewById(R.id.btn_kondo);
        btnTanah = (ImageButton) findViewById(R.id.btn_tanah);
        btnVilla = (ImageButton) findViewById(R.id.btn_villa);
        btnRuko = (ImageButton) findViewById(R.id.btn_ruko);
        btnKantor = (ImageButton) findViewById(R.id.btn_kantor);
        btnPabrik = (ImageButton) findViewById(R.id.btn_pabrik);
        btnGudang = (ImageButton) findViewById(R.id.btn_gudang);
        btnKios = (ImageButton) findViewById(R.id.btn_kios);
//        switchCompatCobroke = (SwitchCompat) findViewById(R.id.switch_compat_co_broke);

        ////ImageButton btnRumah,btnApartemen,btnKondo,btnTanah,btnVilla,btnRuko,btnKantor,btnPabrik,btnGudang,btnKios;

    }

    public void requestListingAddPropertyName() {
        final StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getListingAddPropertyName(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArrayPropertyName = new JSONArray(response);
                    generateListPropertyName(jsonArrayPropertyName);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "propertyListingName");

    }

    private void generateListPropertyName(JSONArray response) {
        Log.d(TAG, "propertyName " + response.length());
        for (int i = 0; i < response.length(); i++) {

            try {
                JSONObject jo = response.getJSONObject(i);
                ListingPropertyNameModel listingPropertyModel = new ListingPropertyNameModel();
                listingPropertyModel.setPropertyRef(jo.getString("propertyRef"));
                listingPropertyModel.setPropertyName(jo.getString("propertyName"));
                listPropertyName.add(listingPropertyModel);

                Log.d(TAG, "propertyName " + jo.getString("propertyName"));
                list.add(jo.getString("propertyName"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        /*AutoComplate*/
        //listingPropertyNameAdapter.notifyDataSetChanged();

        dataAdapter = new MyAdapter(this, listPropertyName);
        edtPropertyName.setAdapter(dataAdapter);


    }

    public void requestListingAddProperty() {
        final StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getListingAddProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArrayListingType = new JSONArray(jsonObject.getJSONArray("listingType").toString());
                    generateListListingType(jsonArrayListingType);
                    JSONArray jsonArrayCategoryType = new JSONArray(jsonObject.getJSONArray("categoryType").toString());
                    generateListCategoryType(jsonArrayCategoryType);
                    JSONArray jsonArrayCertificate = new JSONArray(jsonObject.getJSONArray("certificate").toString());
                    generateListCertificate(jsonArrayCertificate);
                    JSONArray jsonArrayPriceType = new JSONArray(jsonObject.getJSONArray("priceType").toString());
                    generateListPriceType(jsonArrayPriceType);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "propertyListing");

    }

    private void generateListListingType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingListingTypeModel listingListingTypeModel = new ListingListingTypeModel();
                listingListingTypeModel.setListingTypeRef(jo.getString("listingTypeRef"));
                listingListingTypeModel.setListingTypeName(jo.getString("listingTypeName"));
                listListingType.add(listingListingTypeModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        listingListingTypeAdapter.notifyDataSetChanged();

    }

    private void generateListCategoryType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingCategoryTypeModel listingCategoryTypeModel = new ListingCategoryTypeModel();
                listingCategoryTypeModel.setCategoryType(jo.getString("categoryType"));
                listingCategoryTypeModel.setCategoryTypeGroup(jo.getString("categoryTypeGroup"));
                listingCategoryTypeModel.setCategoryTypeName(jo.getString("categoryTypeName"));

                listCategorytype.add(listingCategoryTypeModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        listingCategoryTypeAdapter.notifyDataSetChanged();
    }

    private void generateListCertificate(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingCertificateModel listingCertificateModel = new ListingCertificateModel();
                listingCertificateModel.setCertRef(jo.getString("certRef"));
                listingCertificateModel.setCertName(jo.getString("certName"));

                listCertificate.add(listingCertificateModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        listingCertificateAdapter.notifyDataSetChanged();
    }

    private void generateListPriceType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingPriceTypeModel listingPriceTypeModel = new ListingPriceTypeModel();
                listingPriceTypeModel.setPriceType(jo.getString("priceType"));
                listingPriceTypeModel.setPriceTypeName(jo.getString("priceTypeName"));

                listPriceType.add(listingPriceTypeModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        listingPriceTypeAdapter.notifyDataSetChanged();
    }


    //location
    public void requestCountry() {
        final StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getCountry(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCountry(jsonArray);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "requestCountry");

    }

    int setCountry = 0;

    private void generateListCountry(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CountryModel contry = new CountryModel();
                contry.setCountryCode(jo.getString("countryCode"));
                contry.setCountryName(jo.getString("countryName"));
                String countyCodeSet = jo.getString("countryCode");
                if (countryCode.equals(countyCodeSet)) {
                    setCountry = i;
                }
                listCounty.add(contry);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterCountry.notifyDataSetChanged();
    }

    public void requestProvince(final String countryCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProvince(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListProvince(jsonArray);
                    Log.d("jsonArray", "" + jsonArray.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestProvince");

    }

    int setProvince = 0;

    private void generateListProvince(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProvinceModel province = new ProvinceModel();
                province.setProvinceCode(jo.getString("provinceCode"));
                province.setProvinceName(jo.getString("provinceName"));
                String provinceCodeSet = jo.getString("provinceCode");
                if (provinceCode.equals(provinceCodeSet)) {
                    setProvince = i;
                }
                listProvince.add(province);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnProvince.setSelection(setProvince);
        adapterProvince.notifyDataSetChanged();
    }

    public void requestCity(final String countryCode, final String provinceCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCity(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    generateListCity(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestCity");

    }

    int setCity = 0;

    private void generateListCity(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CityModel city = new CityModel();
                city.setCityCode(jo.getString("cityCode"));
                city.setCityName(jo.getString("cityName"));
                String cityCodeSet = jo.getString("cityCode");
                if (cityCode.equals(cityCodeSet)) {
                    setCity = i;
                }
                listCity.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnCity.setSelection(setCity);
        adapterCity.notifyDataSetChanged();
    }

    public void requestSubLocation(final String countryCode, final String provinceCode, final String cityCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getSubLocation(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    generateListSubLoaction(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);
                params.put("cityCode", cityCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestSubLocation");

    }

    int sublocationRef = 0;

    private void generateListSubLoaction(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                SubLocationModel subLocationModel = new SubLocationModel();
                subLocationModel.setSubLocationRef(jo.getString("subLocationRef"));
                subLocationModel.setSubLocationName(jo.getString("subLocationName"));
                String subLocationRefSet = jo.getString("subLocationRef");
                if (subLocationRef.equals(subLocationRefSet)) {
                    sublocationRef = i;
                }
                listSublocation.add(subLocationModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnSubLocation.setSelection(sublocationRef);
        adapterSubLocation.notifyDataSetChanged();
    }


    /**
     * onBackPress
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "resultCode " + resultCode);
        Log.d(TAG, "listingRef " + listingRef);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                listingRef = data.getStringExtra("listingRef");

            }
        }
    }

    public void insertListingProperty() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
//        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.insertPropertyListing(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
//                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    listingRef = jsonObject.getString("listingRef");

                    Intent intent = new Intent(ListingAddPropertyActivity.this, ListingAddPropertyUnitActivity.class);
                    intent.putExtra("listingRef", listingRef);
                    intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                    intent.putExtra("memberTypeCode", memberTypeCode);
                    intent.putExtra("status", status);
                    startActivityForResult(intent, 1);
                    //LoadingBar.stopLoader();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
//                        progressDialog.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String listingTitle = edtListingTitle.getText().toString();
                String listingDesc = edtListingDesc.getText().toString();
                String block = edtBlock.getText().toString();
                String no = edtNo.getText().toString();
                String postCode = edtPostCode.getText().toString();
                String price = edtPrice.getText().toString();
                String buildArea = edtBuild.getText().toString();
                String landArea = edtLand.getText().toString();
                String propertyName = edtPropertyName.getText().toString();
                String address = edtAddress.getText().toString();
                String buildDimX = edtBuildDimX.getText().toString();
                String buildDimY = edtBuildDimY.getText().toString();
                String landDimX = edtLandDimX.getText().toString();
                String landDimY = edtLandDimY.getText().toString();

                params.put("memberRef", memberRef);
                params.put("listingTypeRef", listingTypeRef);
                params.put("categoryType", categoryType);
                params.put("propertyRef", propertyRef);
                params.put("certRef", certRef);
                params.put("priceType", priceType);
                params.put("listingTitle", listingTitle);
                params.put("listingDesc", listingDesc);
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);
                params.put("cityCode", cityCode);
                params.put("areaRef", subLocation);
                params.put("block", block);
                params.put("no", no);
                params.put("postCode", postCode);
                params.put("price", price);
                params.put("buildArea", buildArea);
                params.put("buildDimX", buildDimX);
                params.put("buildDimY", buildDimY);
                params.put("landArea", landArea);
                params.put("landDimX", landDimX);
                params.put("landDimY", landDimY);
                params.put("propertyName", propertyName);
                params.put("address", address);
//                params.put("isCobroke", isCobroke);

                Log.d("param", memberRef + " - " + listingTypeRef + " - " + categoryType + " - " + propertyRef + " - " + certRef
                        + " - " + priceType + " - " + listingTitle + " - " + listingDesc + " - " + countryCode + " - " + provinceCode + " - " + cityCode
                        + " - " + subLocation + " - " + block + " - " + no + " - " + postCode + " - " + price + " - " + buildArea + " - " + landArea
                        + " - " + propertyName + " - " + address);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insert");

    }

    public void updateListingProperty() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
//        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updatePropertyListing(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
//                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    listingRef = jsonObject.getString("listingRef");

                    Intent intent = new Intent(ListingAddPropertyActivity.this, ListingAddPropertyUnitActivity.class);
                    intent.putExtra("listingRef", listingRef);
                    intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                    intent.putExtra("memberTypeCode", memberTypeCode);
                    startActivityForResult(intent, 1);
                    //LoadingBar.stopLoader();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
//                        progressDialog.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String listingTitle = edtListingTitle.getText().toString();
                String listingDesc = edtListingDesc.getText().toString();
                String block = edtBlock.getText().toString();
                String no = edtNo.getText().toString();
                String postCode = edtPostCode.getText().toString();
                String price = edtPrice.getText().toString();
                String buildArea = edtBuild.getText().toString();
                String landArea = edtLand.getText().toString();
                String propertyName = edtPropertyName.getText().toString();
                String address = edtAddress.getText().toString();
                String buildDimX = edtBuildDimX.getText().toString();
                String buildDimY = edtBuildDimY.getText().toString();
                String landDimX = edtLandDimX.getText().toString();
                String landDimY = edtLandDimY.getText().toString();

                params.put("listingRef", listingRef);
                params.put("memberRef", memberRef);
                params.put("listingTypeRef", listingTypeRef);
                params.put("categoryType", categoryType);
                params.put("propertyRef", propertyRef);
                params.put("certRef", certRef);
                params.put("priceType", priceType);
                params.put("listingTitle", listingTitle);
                params.put("listingDesc", listingDesc);
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);
                params.put("cityCode", cityCode);
                params.put("areaRef", subLocation);
                params.put("block", block);
                params.put("no", no);
                params.put("postCode", postCode);
                params.put("price", price);
                params.put("buildArea", buildArea);
                params.put("buildDimX", buildDimX);
                params.put("buildDimY", buildDimY);
                params.put("landArea", landArea);
                params.put("landDimX", landDimX);
                params.put("landDimY", landDimY);
                params.put("propertyName", propertyName);
                params.put("address", address);
//                params.put("isCobroke", isCobroke);

                Log.d(TAG, "param" + listingRef + "-" + memberRef + " - " + listingTypeRef + " - " + categoryType + " - " + propertyRef + " - " + certRef
                        + " - " + priceType + " - " + listingTitle + " - " + listingDesc + " - " + countryCode + " - " + provinceCode + " - " + cityCode
                        + " - " + subLocation + " - " + block + " - " + no + " - " + postCode + " - " + price + " - " + buildArea + " - " + landArea
                        + " - " + propertyName + " - " + address);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insert");

    }

    public void requestPropertyInfo(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingPropertyInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    listingDesc = jsonObject.optString("listingDesc");
                    listingTypeName = jsonObject.optString("listingTypeName");
                    categoryTypeName = jsonObject.optString("categoryTypeName");
                    propertyRef = jsonObject.optString("propertyRef");
                    certName = jsonObject.optString("certName");
                    priceTypeName = jsonObject.optString("priceTypeName");
                    listingTitle = jsonObject.optString("listingTitle");
                    countryName = jsonObject.optString("countryName");
                    provinceName = jsonObject.optString("provinceName");
                    cityName = jsonObject.optString("cityName");
                    subLocationName = jsonObject.optString("subLocationName");
                    address = jsonObject.optString("address");
                    block = jsonObject.optString("block");
                    no = jsonObject.optString("no");
                    postCode = jsonObject.optString("postCode");
                    price = jsonObject.optString("price");
                    buildArea = jsonObject.optString("buildArea");
                    buildDimX = jsonObject.optString("buildDimX");
                    buildDimY = jsonObject.optString("buildDimY");
                    landArea = jsonObject.optString("landArea");
                    landDimX = jsonObject.optString("landDimX");
                    landDimY = jsonObject.optString("landDimY");
                    listingTypeRef = jsonObject.optString("listingTypeRef");
                    categoryType = jsonObject.optString("categoryType");
                    certRef = jsonObject.optString("certRef");
                    priceType = jsonObject.optString("priceType");
                    provinceSortNo = jsonObject.optString("provinceSortNo");
                    citySortNo = jsonObject.optString("citySortNo");
                    countrySortNo = jsonObject.optString("countrySortNo");
                    propertyName = jsonObject.optString("propertyName");
                    countryCode = jsonObject.optString("countryCode");
                    provinceCode = jsonObject.optString("proviceCode");
                    cityCode = jsonObject.optString("cityCode");
                    subLocationRef = jsonObject.optString("subLocationRef");
                    isCobrokePref = jsonObject.optString("isCobroke");

//                    spnListingType.setSelection(Integer.parseInt(listingTypeRef) - 1);
//                    spnCategoryType.setSelection(Integer.parseInt(categoryType) - 1);
                    edtListingTitle.setText(listingTitle);
                    edtListingDesc.setText(listingDesc);
                    edtPropertyName.setText(propertyName);
                    edtPrice.setText(price);
                    edtBuild.setText(Html.fromHtml(buildArea));
                    edtBuildDimX.setText(buildDimX);
                    edtBuildDimY.setText(buildDimY);
                    edtLand.setText(Html.fromHtml(landArea));
                    edtLandDimX.setText(landDimX);
                    edtLandDimY.setText(landDimY);
                    edtAddress.setText(address);
                    edtBlock.setText(block);
                    edtNo.setText(no);
                    edtPostCode.setText(postCode);
                    spnPriceType.setSelection(Integer.parseInt(priceType) - 1);
                    spnCertivicate.setSelection(Integer.parseInt(certRef) - 1);

                   /* CoBrokeStatic coBrokeStatic = new CoBrokeStatic();
                    coBrokeStatic.setStatus(isCobroke);*/
                    SharedPreferences sharedPreferences = ListingAddPropertyActivity.this.
                            getSharedPreferences(PREF_NAME, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("isCobroke", isCobrokePref);
                    editor.commit();

                    Log.d(TAG, "isCobroke " + isCobrokePref);
//                    if (isCobroke.equals("1")){
//                        switchCompatCobroke.setChecked(true);
//                        propertyType();
//                    } else {
//                        switchCompatCobroke.setChecked(false);
//                        propertyType();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddPropertyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "propertyInfo");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_type_jual:
                listingTypeRef = "1";
                btnJual.setBackgroundResource(R.color.light_gray_inactive_icon);
                btnJual.setTextColor(getApplication().getResources().getColor(R.color.black));
                btnSewa.setBackgroundResource(R.color.background);
                btnSewa.setTextColor(getApplication().getResources().getColor(R.color.light_gray_inactive_icon));
                break;

            case R.id.btn_type_sewa:
                listingTypeRef = "2";
                btnJual.setBackgroundResource(R.color.background);
                btnSewa.setTextColor(getApplication().getResources().getColor(R.color.black));
                btnSewa.setBackgroundResource(R.color.light_gray_inactive_icon);
                btnJual.setTextColor(getApplication().getResources().getColor(R.color.light_gray_inactive_icon));
                break;

            case R.id.btn_rumah:
                categoryType = "1";
                btnRumah.setImageResource(R.drawable.ic_rumah_true2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);
                break;

            case R.id.btn_apartemen:
                categoryType = "2";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_true2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;

            case R.id.btn_kondo:
                categoryType = "3";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_true2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;

            case R.id.btn_tanah:
                categoryType = "4";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_false2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;

            case R.id.btn_villa:
                categoryType = "5";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_true2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;


            case R.id.btn_ruko:
                categoryType = "6";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_true2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;


            case R.id.btn_kantor:
                categoryType = "7";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_true2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;

            case R.id.btn_pabrik:
                categoryType = "8";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_true2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;


            case R.id.btn_gudang:
                categoryType = "9";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_true2);
                btnKios.setImageResource(R.drawable.ic_kios_true2);

                break;

            case R.id.btn_kios:
                categoryType = "10";
                btnRumah.setImageResource(R.drawable.ic_rumah_false2);
                btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
                btnKondo.setImageResource(R.drawable.ic_kondo_false2);
                btnTanah.setImageResource(R.drawable.ic_tanah_true2);
                btnVilla.setImageResource(R.drawable.ic_villa_false2);
                btnRuko.setImageResource(R.drawable.ic_ruko_false2);
                btnKantor.setImageResource(R.drawable.ic_kantor_false2);
                btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
                btnGudang.setImageResource(R.drawable.ic_gudang_false2);
                btnKios.setImageResource(R.drawable.ic_kios_false2);
                break;

        }
    }

    private void propertyType() {
        if (listingTypeRef.equals("1")) {
            btnJual.setBackgroundResource(R.color.light_gray_inactive_icon);
            btnJual.setTextColor(getApplication().getResources().getColor(R.color.black));
            btnSewa.setBackgroundResource(R.color.background);
            btnSewa.setTextColor(getApplication().getResources().getColor(R.color.light_gray_inactive_icon));
        } else {
            btnJual.setBackgroundResource(R.color.background);
            btnSewa.setTextColor(getApplication().getResources().getColor(R.color.black));
            btnSewa.setBackgroundResource(R.color.light_gray_inactive_icon);
            btnJual.setTextColor(getApplication().getResources().getColor(R.color.light_gray_inactive_icon));
        }
        if (categoryType.equals("1")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_true2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("2")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_true2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("3")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_true2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("4")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_false2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("5")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_true2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("6")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_true2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("7")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_true2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("8")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_true2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("9")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_true2);
            btnKios.setImageResource(R.drawable.ic_kios_true2);
        } else if (categoryType.equals("10")) {
            btnRumah.setImageResource(R.drawable.ic_rumah_false2);
            btnApartemen.setImageResource(R.drawable.ic_apartemen_false2);
            btnKondo.setImageResource(R.drawable.ic_kondo_false2);
            btnTanah.setImageResource(R.drawable.ic_tanah_true2);
            btnVilla.setImageResource(R.drawable.ic_villa_false2);
            btnRuko.setImageResource(R.drawable.ic_ruko_false2);
            btnKantor.setImageResource(R.drawable.ic_kantor_false2);
            btnPabrik.setImageResource(R.drawable.ic_pabrik_false2);
            btnGudang.setImageResource(R.drawable.ic_gudang_false2);
            btnKios.setImageResource(R.drawable.ic_kios_false2);
        }
    }

    public class MyAdapter extends BaseAdapter implements Filterable {

        private ArrayList<ListingPropertyNameModel> originalList;

        private Filter filter = new CustomFilter();

        public MyAdapter(Context context, ArrayList<ListingPropertyNameModel> originalList) {
            this.originalList = originalList;
        }

        @Override
        public int getCount() {
            return suggestions.size();
        }

        @Override
        public Object getItem(int position) {
            return suggestions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());

            ViewHolder holder;

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_list_agency_company, parent, false);
                holder = new ViewHolder();
                holder.autoText = (TextView) convertView.findViewById(R.id.autoText);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.autoText.setText(suggestions.get(position).getPropertyName());

            return convertView;
        }

        private class ViewHolder {
            TextView autoText;
        }

        @Override
        public Filter getFilter() {
            return filter;
        }

        private class CustomFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                suggestions.clear();

                if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                    for (int i = 0; i < originalList.size(); i++) {
                        if (originalList.get(i).getPropertyName().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                            suggestions.add(originalList.get(i)); // If TRUE add item in Suggestions.
                        }
                    }
                }
                FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
                results.values = suggestions;
                results.count = suggestions.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }
}
