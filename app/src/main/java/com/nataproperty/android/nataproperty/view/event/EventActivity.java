package com.nataproperty.android.nataproperty.view.event;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.event.EventAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.event.EventModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EventActivity extends AppCompatActivity {
    public static final String TAG = "EventActivity";

    public static final String PREF_NAME = "pref";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String CONTENT_TYPE = "contentType";
    public static final String CONTENT_REF = "contentRef";
    public static final String TITLE = "title";
    public static final String SYNOPSIS = "synopsis";
    public static final String SCHADULE = "schedule";
    public static final String LINK_DETAIL = "linkDetail";

    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());

    private ArrayList<EventModel> listEvent = new ArrayList<EventModel>();
    private EventAdapter adapter;

    CompactCalendarView compactCalendarView;

    long timeInMilliseconds;

    TextView txtMonth, txtNoDataMonth,txtNoDataDay;

    String dbMasterRef, projectRef, contentType, contentRef, txtTitle, synopsis, schedule, linkDetail;

    String formattedDate;

    MyListView listView;

    String givenDateString,memberRef;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_event));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        txtMonth = (TextView) findViewById(R.id.txt_month);
        txtNoDataMonth = (TextView) findViewById(R.id.txt_no_list_event_month);
        txtNoDataDay = (TextView) findViewById(R.id.txt_no_list_event_day);
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);

        txtMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        listView = (MyListView) findViewById(R.id.list_event);
        adapter = new EventAdapter(this, listEvent, compactCalendarView);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        /*String givenDateString = "Tue Apr 23 16:08 2016";
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm yyyy");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            Log.d(TAG, "Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listEvent.get(position).getDbMasterRef();
                projectRef = listEvent.get(position).getProjectRef();
                txtTitle = listEvent.get(position).getTitle();
                schedule = listEvent.get(position).getSchedule();
                synopsis = listEvent.get(position).getSynopsis();
                contentType = listEvent.get(position).getContentType();
                contentRef = listEvent.get(position).getContentRef();
                linkDetail = listEvent.get(position).getLinkDetail();

                Intent intent = new Intent(EventActivity.this, EventDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(TITLE, txtTitle);
                intent.putExtra(SYNOPSIS, synopsis);
                intent.putExtra(SCHADULE, schedule);
                intent.putExtra(CONTENT_TYPE, contentType);
                intent.putExtra(CONTENT_REF, contentRef);
                intent.putExtra(LINK_DETAIL, linkDetail);
                startActivity(intent);
            }
        });

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                String newText = dateformat.format(dateClicked);
                Log.d(TAG, "Date: " + dateformat.format(dateClicked));

                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorBlockDiagramMatic));

                Filter filter = adapter.getFilter();
                if (newText.isEmpty()) {
                    filter.filter("");
                    txtNoDataMonth.setVisibility(View.GONE);
                    txtNoDataDay.setVisibility(View.VISIBLE);
                } else {
                    filter.filter(newText);
                    Log.d(TAG, "Date New Text: " + newText);
                }

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                txtMonth.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                SimpleDateFormat dateformat = new SimpleDateFormat("MM/yyyy");
                Log.d(TAG, "Month: " + dateformat.format(firstDayOfNewMonth));

                formattedDate = dateformat.format(firstDayOfNewMonth);

                listEvent.clear();
                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorWhite));
                compactCalendarView.removeAllEvents();
                requestEvent();

                Filter filter = adapter.getFilter();
                if (formattedDate.isEmpty()) {
                    filter.filter("");
                } else {
                    filter.filter(formattedDate);
                    Log.d(TAG, "Date New Text: " + formattedDate);
                }

            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/yyyy");
        formattedDate = df.format(c.getTime());
        Log.d(TAG, "Month: " + formattedDate);

        txtMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filter filter = adapter.getFilter();
                filter.filter("");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");

        listEvent.clear();
        compactCalendarView.removeAllEvents();
        requestEvent();

    }

    private void requestEvent() {
        String url = WebService.getListEvent();
        String urlPostParameter = "&contentDate=" + formattedDate.toString()+
                "&memberRef=" + memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d("result event", String.valueOf(jsonArray.length()));
            generateListEvent(jsonArray);

        } catch (JSONException e) {

        }

    }

    public void rrequestEvent() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListEvent(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListEvent(jsonArray);

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(EventActivity.this, "error connection", Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("contentDate", formattedDate.toString());
                params.put("memberRef", memberRef);

                Log.d(TAG,"param "+formattedDate.toString()+" "+memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "event");

    }

    private void generateListEvent(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                EventModel Event = new EventModel();
                Event.setDbMasterRef(jo.getString("dbMasterRef"));
                Event.setProjectRef(jo.getString("projectRef"));
                Event.setTitle(jo.getString("title"));
                Event.setContentRef(jo.getString("contentRef"));
                Event.setContentType(jo.getString("contentType"));
                Event.setSynopsis(jo.getString("synopsis"));
                Event.setSchedule(jo.getString("schedule"));
                Event.setEventDate(jo.getString("eventDate"));
                Event.setLinkDetail(jo.getString("linkDetail"));
                givenDateString = (jo.getString("eventDate"));
                Event.setStatus(jo.getString("status"));
                listEvent.add(Event);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date mDate = sdf.parse(givenDateString);
                    timeInMilliseconds = mDate.getTime();
                    Log.d("EventAdapter", "Date in milli :: " + timeInMilliseconds);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Event ev1 = new Event(getResources().getColor(R.color.colorHeaderDiagramMatic),
                        timeInMilliseconds);
                compactCalendarView.addEvent(ev1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();

        if (response.length()==0){
            txtNoDataMonth.setVisibility(View.VISIBLE);
        }else {
            txtNoDataMonth.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
