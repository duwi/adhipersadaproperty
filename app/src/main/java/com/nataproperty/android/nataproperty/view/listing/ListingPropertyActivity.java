package com.nataproperty.android.nataproperty.view.listing;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.ybq.endless.Endless;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCategoryTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCityAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingListingTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingProvinceAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingSubLocationAdapter;
import com.nataproperty.android.nataproperty.adapter.project.LocationAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.listing.ListingCategoryTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingCertificateModel;
import com.nataproperty.android.nataproperty.model.listing.ListingListingTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPriceTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.model.profile.SubLocationModel;
import com.nataproperty.android.nataproperty.model.project.LocationModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ListingPropertyPresenter;
import com.nataproperty.android.nataproperty.view.project.card.RVListingSearchPropertyAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingPropertyActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingPropertyActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListingPropertyPresenter presenter;
    ProgressDialog progressDialog;
    private ListingCityAdapter adapterCity;
    private ListingSubLocationAdapter adapterSubLocation;
    private ListingCategoryTypeAdapter listingCategoryTypeAdapter;
    private ListingListingTypeAdapter listingListingTypeAdapter;

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<ListingPropertyModel> listProperty = new ArrayList<>();
    private List<ListingPropertyModel> listProperty2 = new ArrayList<>();
    private List<SubLocationModel> listSublocation = new ArrayList<>();
    private List<ListingPriceTypeModel> listPriceType = new ArrayList<>();
    private AlertDialog alertDialog;
    RVListingSearchPropertyAdapter adapter;

    int type;

    private String keyword = "", agencyCompanyRef, listingRef, imageCover, listingMemberRef;
    private int page = 1;
    private int countTotal;
    private String psRef = "", memberRef;

    private LinearLayout linearLayoutNoData;

    private FloatingActionButton fab;
    private SharedPreferences sharedPreferences;

    private CardView cardViewSpinner;
    private List<LocationModel> listLocation = new ArrayList<>();
    private List<ProvinceModel> listProvince = new ArrayList<>();
    private List<ListingCategoryTypeModel> listCategorytype = new ArrayList<>();
    private List<ListingListingTypeModel> listListingType = new ArrayList<>();
    private List<ListingCertificateModel> listCertificate = new ArrayList<>();
    private List<CityModel> listCity = new ArrayList<>();
    private LocationAdapter locationAdapter;
    private LocationModel locationModel;
    private ListingProvinceAdapter adapterProvince;
    private String locationRef;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    View loadingView;
    int setProvince = 0;
    Typeface font;
    int setCity = 0;
    Display display;
    Endless endless;
    RecyclerView rvSearchProperty;
    Spinner spinnerPropinsi, spinnerCity, spinnerListing, spinnerProperty, spinnerArea;
    private String memberTypeCode, imageLogo, propertyRef = "", countryCode = "", provinceCode = "", cityCode = "", subLocation = "", subLocationRef = "", categoryType, listingTypeRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_property);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingPropertyPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        psRef = intent.getStringExtra("psRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();
        requestLocation();

        Log.d(TAG, "listSize " + listProperty2.size());
        adapter = new RVListingSearchPropertyAdapter(this, listProperty2, display,memberTypeCode,agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        //initAdapter();

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(rvSearchProperty,
                loadingView
        );
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                requestListProperty(page, locationRef);
            }
        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                listingRef = listProperty.get(position).getListingRef();
//                listingMemberRef = listProperty.get(position).getMemberRef();
//                Intent intent = new Intent(ListingPropertyActivity.this, ListingPropertyTabsActivity.class);
//                intent.putExtra("listingRef", listingRef);
//                intent.putExtra("imageCover", imageCover);
//                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
//                intent.putExtra("listingMemberRef", listingMemberRef);
//
//                Log.d(TAG," agencyCompanyRef = " + agencyCompanyRef);
//                startActivity(intent);
//            }
//        });
//        listView.setOnScrollListener(new EndlessScrollListener() {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount) {
//
//                requestListProperty(page,locationRef);
//            }
//        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListingPropertyActivity.this, ListingAddPropertyActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("listingRef", "");
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("status", "new");
                startActivity(intent);
            }
        });

//        registerForContextMenu(listView);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Agency Listing");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();

//        listView = (ListView) findViewById(R.id.list_property);
        linearLayoutNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        cardViewSpinner = (CardView) findViewById(R.id.cv_spiner);
        rvSearchProperty = (RecyclerView) findViewById(R.id.rv_list_search_project);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvSearchProperty.setLayoutManager(llm);
    }

    public void requestLocation() {

        //presenter.getLocation(memberRef);
        presenter.getLocationListing(agencyCompanyRef);
    }


    private void spinnerLoc() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_nav);

        locationAdapter = new LocationAdapter(this, listLocation);
        spinner.setAdapter(locationAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationRef = String.valueOf(listLocation.get(position).getLocationRef());

                listProperty2.clear();
                Log.d(TAG, "req " + page + " " + locationRef);
                requestListProperty(page, locationRef);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void requestListProperty(final int page, final String locationRef) {
       /* progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);*/
//        listProperty2.clear();
        presenter.getSearchListing(agencyCompanyRef, WebService.listingStatus, String.valueOf(page), "All", "All", locationRef, "All", "All", memberRef);
//        presenter.getSearchListing(agencyCompanyRef,psRef,WebService.listingStatus,String.valueOf(page),locationRef,"","","");
    }

    public void showListPropertyResults(retrofit2.Response<ListingPropertyStatus> response) {
//        progressDialog.dismiss();
//        countTotal = response.body().getTotalPage();
//
//        if (countTotal != 0) {
//            if (page <= countTotal) {
//                listProperty=response.body().getData();
//                for(int i = 0;i<listProperty.size();i++){
//                    ListingFavoritModel listingFavoritModel = new ListingFavoritModel();
//                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
//                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
//                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
//                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
//                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
//                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
//                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
//                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
//                    listingFavoritModel.setListingRef(Long.parseLong(listProperty.get(i).getListingRef()));
//                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
//                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
//                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
//                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
//                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
//                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
//                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
//                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
//                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
//                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
//                    listProperty2.add(listingFavoritModel);
//                }
//            }
//        } else {
//            linearLayoutNoData.setVisibility(View.VISIBLE);
//        }
//
//        Log.d(TAG, "count=" + page + " " + countTotal);
//        initAdapter();

    }

    private void initAdapter() {
//        listView.removeAllViews();
//        adapter.notifyDataSetChanged();
        adapter = new RVListingSearchPropertyAdapter(this, listProperty2, display,memberTypeCode,agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
    }

    public void showListPropertyFailure(Throwable t) {
       // progressDialog.dismiss();

    }

    public void showLocationResults(retrofit2.Response<List<LocationModel>> response) {
        listLocation = response.body();
        cardViewSpinner.setVisibility(View.VISIBLE);
        spinnerLoc();
    }

    public void showLocationFailure(Throwable t) {

    }

    public void showSearchResults(Response<ListingPropertyStatus> response, String page) {
        //progressDialog.dismiss();
        int totalPage = response.body().getTotalPage();
        agencyCompanyRef = response.body().getAgencyCompanyRef();
        listProperty = response.body().getData();
        Log.d(TAG, "bodySize " + listProperty.size());
        Log.d(TAG, "responsePage " + page);

        if (Integer.parseInt(page) > totalPage) {
            //Toast.makeText(ListingPropertyActivity.this, "No data available", Toast.LENGTH_LONG).show();
            loadingView.setVisibility(View.GONE);
        } else {
            if (Integer.parseInt(page) == 1) {
                for (int i = 0; i < listProperty.size(); i++) {
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
                adapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < listProperty.size(); i++) {
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
                adapter.notifyDataSetChanged();
                endless.loadMoreComplete();
            }
        }

       /* countTotal = response.body().getTotalPage();
        agencyCompanyRef = response.body().getAgencyCompanyRef();

        if (countTotal != 0) {
            if (page <= countTotal) {
                listProperty = response.body().getData();
                if (countTotal == 1) {
                    for (int i = 0; i < listProperty.size(); i++) {
                        ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                        listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                        listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                        listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                        listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                        listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                        listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                        listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                        listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                        listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                        listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                        listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                        listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                        listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                        listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                        listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                        listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                        listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                        listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                        listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                        listingFavoritModel.setHp(listProperty.get(i).getHp());
                        listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                        listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                        listProperty2.add(listingFavoritModel);
                    }

                } else {
                    for (int i = 0; i < listProperty.size(); i++) {
                        ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                        listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                        listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                        listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                        listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                        listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                        listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                        listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                        listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                        listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                        listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                        listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                        listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                        listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                        listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                        listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                        listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                        listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                        listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                        listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                        listingFavoritModel.setHp(listProperty.get(i).getHp());
                        listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                        listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                        listProperty2.add(listingFavoritModel);
                    }
                    endless.loadMoreComplete();
                }
            } else {
                linearLayoutNoData.setVisibility(View.VISIBLE);
            }

            Log.d(TAG, "count=" + page + " " + countTotal);
            initAdapter();
        }*/

    }

    public void showSearchFailure(Throwable t) {
        //progressDialog.dismiss();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               /* Intent intent1 = new Intent(this, MainMenuActivity.class);
                startActivity(intent1);*/
                onBackPressed();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ListingPropertyActivity.this, ListingSearchPropertyActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("imageCover", imageCover);
                intent.putExtra("psRef", psRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("toolbarTitle", title.getText());
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
