package com.nataproperty.android.nataproperty.view.project;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.FeatureAdapter;
import com.nataproperty.android.nataproperty.adapter.project.ProductDetailAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.FeatureModel;
import com.nataproperty.android.nataproperty.model.project.FeatureModelDao;
import com.nataproperty.android.nataproperty.model.project.ProductDetailModel;
import com.nataproperty.android.nataproperty.model.project.ProductDetailsModel;
import com.nataproperty.android.nataproperty.model.project.ProductDetailsModelDao;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationProductActivity;
import com.nataproperty.android.nataproperty.view.nup.NupTermActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by User on 5/10/2016.
 */
public class ProductDetailActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_DESCRIPTION = "clusterDescription";
    public static final String TITLE_PRODUCT = "titleProduct";
    public static final String BATHROOM = "bathroom";
    public static final String BEDROOM = "bedroom";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_NUP = "isNUP";
    public static final String IS_BOOKING = "isBooking";
    public static final String NUP_AMT = "nupAmt";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String URL_VIDEO = "urlVideo";
    public static final String IMAGE_LOGO = "imageLogo";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    SharedPreferences sharedPreferences;
    private List<ProductDetailModel> listProductDetail = new ArrayList<ProductDetailModel>();
    private ProductDetailAdapter adapter;
    private List<FeatureModel> listFeature = new ArrayList<FeatureModel>();
    private FeatureAdapter adapterFeature;

    long dbMasterRef;
    String projectRef, clusterRef, productRef, projectName, categoryRef, titleProduct;
    TextView txtType, txtLocation, txtBedroom, txtBathroom, txtProductName;
    String type, location, bedroom, bathroom;
    String productDescription,numOfBedrooms,numOfBathrooms,categoryName;

    private ImageView imgProductDetail, imgExspanImages;
    private TextView txtDiscription, txtTitleProduct;
    HtmlTextView textDescription;

    ViewPager viewPager;
    Timer timer;
    int page = 0;
    int pageView;

    RelativeLayout rPage;

    String image;
    private Double latitude, longitude;

    private MyListView listView;

    LinearLayout linearMaps;
    LinearLayout linearVideo;

    private GoogleMap googleMap;
    // YouTube player view
    FragmentManager manager;

    private YouTubePlayerView youTubeView;

    Typeface fontLight;

    Button btnNUP, btnCariUnit, btnPropertyVideo, btnSpecialEnquiries;
    String isNUP, nupAmt, isBooking,imageLogo;
    String isShowAvailableUnit;
    String urlVideo, specialEnquiries;
    private RelativeLayout snackBarBuatan;
    private TextView retry;

    CircleIndicator indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
//        if (MainMenuActivity.OFF_LINE_MODE) {
//            snackBarBuatan.setVisibility(View.VISIBLE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ProductDetailActivity.this, LaunchActivity.class));
                    finish();
                }
            });
//        }
//        else {
//            snackBarBuatan.setVisibility(View.GONE);
//        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Product Detail");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        imgProductDetail = (ImageView) findViewById(R.id.img_product_detail);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        txtTitleProduct = (TextView) findViewById(R.id.txt_title_product);
        txtType = (TextView) findViewById(R.id.txt_type);
        txtLocation = (TextView) findViewById(R.id.txt_location);
        txtBedroom = (TextView) findViewById(R.id.txt_bedrooms);
        txtBathroom = (TextView) findViewById(R.id.txt_bathrooms);
        txtProductName = (TextView) findViewById(R.id.txt_project_name);
        txtDiscription = (TextView) findViewById(R.id.txt_discription_product);
        listView = (MyListView) findViewById(R.id.list_feature);
        textDescription = (HtmlTextView) findViewById(R.id.html_text_description);
        linearMaps = (LinearLayout) findViewById(R.id.linier_maps);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);
        btnPropertyVideo = (Button) findViewById(R.id.btn_property_video);
        btnSpecialEnquiries = (Button) findViewById(R.id.btn_specialEnquiries);
        btnCariUnit = (Button) findViewById(R.id.btn_cari_unit);
        btnNUP = (Button) findViewById(R.id.btn_NUP);

        final Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.777777777777778;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(DBMASTER_REF,0);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        titleProduct = intent.getStringExtra(TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);
        latitude = getIntent().getDoubleExtra(LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(LONGITUDE, 0);
        isNUP = intent.getStringExtra(IS_NUP);
        isBooking = intent.getStringExtra(IS_BOOKING);
        nupAmt = getIntent().getStringExtra(NUP_AMT);
        imageLogo = intent.getStringExtra(IMAGE_LOGO);
        Log.d("maps", " " + latitude + longitude);
        Log.d("cek productDetail", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + isShowAvailableUnit);

        if (latitude.equals(0.0)) {
            linearMaps.setVisibility(View.GONE);
        } else {
            linearMaps.setVisibility(View.VISIBLE);
        }

        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        if (!isBooking.equals("0")) {
            btnCariUnit.setVisibility(View.VISIBLE);
        } else {
            btnCariUnit.setVisibility(View.GONE);
        }

        btnPropertyVideo.setTypeface(font);
        btnPropertyVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, YoutubeActivity.class);
                intent.putExtra(URL_VIDEO, urlVideo);
                startActivity(intent);
            }
        });

        //booking
        btnCariUnit.setTypeface(font);
        btnCariUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentProduct = new Intent(ProductDetailActivity.this, IlustrationProductActivity.class);
                intentProduct.putExtra(PROJECT_REF, projectRef);
                intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                intentProduct.putExtra(CLUSTER_REF, clusterRef);
                intentProduct.putExtra(CATEGORY_REF, categoryRef);
                intentProduct.putExtra(CLUSTER_DESCRIPTION, "");
                intentProduct.putExtra(PROJECT_NAME, projectName);
                intentProduct.putExtra(PRODUCT_REF, productRef);
                intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentProduct.putExtra(TITLE_PRODUCT, titleProduct);
                startActivity(intentProduct);
            }
        });

        btnNUP.setTypeface(font);
        btnNUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNup = new Intent(ProductDetailActivity.this, NupTermActivity.class);
                intentNup.putExtra(PROJECT_REF, projectRef);
                intentNup.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentNup.putExtra(PROJECT_NAME, projectName);
                intentNup.putExtra(NUP_AMT, nupAmt);
                startActivity(intentNup);
            }
        });


        adapter = new ProductDetailAdapter(this, listProductDetail);
        viewPager.setAdapter(adapter);
        pageSwitcher(4);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

        requestProductDetail();

        requestProductDetailImage();

        /**
         * feature
         */
        adapterFeature = new FeatureAdapter(this, listFeature);
        listView.setAdapter(adapterFeature);
        listView.setExpanded(true);

        btnSpecialEnquiries.setTypeface(font);
        btnSpecialEnquiries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductDetailActivity.this);
                LayoutInflater inflater = ProductDetailActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Special Enquiries");
                txtspecialEnquiries.setText(specialEnquiries);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });
    }


    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay

    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {
                    //viewPager.setCurrentItem(page++);
                    if (page == pageView) { // In my case the number of pages are 5
                        //timer.cancel();
                        page = 0;
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void requestProductDetail() {
        final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProductDetail(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MainMenuActivity.OFF_LINE_MODE=false;
                snackBarBuatan.setVisibility(View.GONE);
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result product detail", response);
                    int status = jo.getInt("status");
                    if (status == 200) {
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ProductDetailsModel productDetailsModel = gson.fromJson("" + jo, ProductDetailsModel.class);
                        String message = jo.getString("message");
                        productDescription = jo.getJSONObject("data").getString("productDescription");
                        urlVideo = jo.getJSONObject("data").getString("urlVideo");

                        numOfBedrooms = jo.getJSONObject("data").getString("numOfBedrooms");
                        numOfBathrooms = jo.getJSONObject("data").getString("numOfBathrooms");
                        location = jo.getJSONObject("data").getString("location");
                        categoryName = jo.getJSONObject("data").getString("categoryName");
                        specialEnquiries = jo.getJSONObject("data").getString("specialEnquiries");
                        productDetailsModel.setDbMasterRef(jo.getJSONObject("data").optLong("dbMasterRef"));
                        productDetailsModel.setProductDescription(jo.getJSONObject("data").optString("productDescription"));
                        productDetailsModel.setUrlVideo(jo.getJSONObject("data").optString("urlVideo"));
                        productDetailsModel.setNumOfBedrooms(jo.getJSONObject("data").optString("numOfBedrooms"));
                        productDetailsModel.setNumOfBathrooms(jo.getJSONObject("data").optString("numOfBathrooms"));
                        productDetailsModel.setLocation(jo.getJSONObject("data").optString("location"));
                        productDetailsModel.setCategoryName(jo.getJSONObject("data").optString("categoryName"));
                        productDetailsModel.setSpecialEnquiries(jo.getJSONObject("data").optString("specialEnquiries"));
                        ProductDetailsModelDao productDetailsModelDao = daoSession.getProductDetailsModelDao();
                        productDetailsModelDao.insertOrReplace(productDetailsModel);

                        if (!urlVideo.equals("")) {
                            btnPropertyVideo.setVisibility(View.VISIBLE);
                        } else {
                            btnPropertyVideo.setVisibility(View.GONE);
                        }

                        if (!specialEnquiries.equals("")) {
                            btnSpecialEnquiries.setVisibility(View.VISIBLE);
                        }

                        /*Spanned htmlAsSpanned = Html.fromHtml(productDescription);
                        txtDiscription.setText(htmlAsSpanned);*/
                        txtTitleProduct.setText(titleProduct);
                        txtBedroom.setText(numOfBedrooms);
                        txtBathroom.setText(numOfBathrooms);
                        txtLocation.setText(location);
                        txtType.setText(categoryName);

                        textDescription.setHtmlFromString(productDescription, new HtmlTextView.RemoteImageGetter());
                        textDescription.setTypeface(fontLight);

                        requestFeature();

                    } else {
                        String message = jo.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainMenuActivity.OFF_LINE_MODE=true;
                        snackBarBuatan.setVisibility(View.VISIBLE);
                        ProductDetailsModelDao productDetailsModelDao = daoSession.getProductDetailsModelDao();
                        List<ProductDetailsModel> productDetailsModels = productDetailsModelDao.queryBuilder().where(ProductDetailsModelDao.Properties.DbMasterRef.eq(dbMasterRef))
                                .list();
                        Log.d("getProjectLocalDB", "" + productDetailsModels.toString());
                        productDescription = productDetailsModels.get(0).getProductDescription();
                        urlVideo = productDetailsModels.get(0).getUrlVideo();

                        numOfBedrooms = productDetailsModels.get(0).getNumOfBedrooms();
                        numOfBathrooms = productDetailsModels.get(0).getNumOfBathrooms();
                        location = productDetailsModels.get(0).getLocation();
                        categoryName = productDetailsModels.get(0).getCategoryName();
                        specialEnquiries = productDetailsModels.get(0).getSpecialEnquiries();
                        if (!urlVideo.equals("")) {
                            btnPropertyVideo.setVisibility(View.VISIBLE);
                        } else {
                            btnPropertyVideo.setVisibility(View.GONE);
                        }

                        if (!specialEnquiries.equals("")) {
                            btnSpecialEnquiries.setVisibility(View.VISIBLE);
                        }

                        /*Spanned htmlAsSpanned = Html.fromHtml(productDescription);
                        txtDiscription.setText(htmlAsSpanned);*/
                        txtTitleProduct.setText(titleProduct);
                        txtBedroom.setText(numOfBedrooms);
                        txtBathroom.setText(numOfBathrooms);
                        txtLocation.setText(location);
                        txtType.setText(categoryName);

                        textDescription.setHtmlFromString(productDescription, new HtmlTextView.RemoteImageGetter());
                        textDescription.setTypeface(fontLight);

                        requestFeature();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productDeatil");

    }

    public void requestProductDetailImage() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListProductImage(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result feature", " " + response);
                    generateListProductImage(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("productRef", productRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productImage");

    }

    private void generateListProductImage(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProductDetailModel imageProduct = new ProductDetailModel();
                imageProduct.setDbMasterRef(jo.getString("dbMasterRef"));
                imageProduct.setProjectRef(jo.getString("projectRef"));
                imageProduct.setProductRef(jo.getString("productRef"));
                imageProduct.setDbMasterProjectProductFileRef(jo.getString("dbMasterProjectProductFileRef"));
                imageProduct.setQuerystring(jo.getString("querystring"));

                listProductDetail.add(imageProduct);

                pageView = response.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void requestFeature() {
        listFeature.clear();
        final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getFeatureList(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result feature", " " + response);
                    generateListFeature(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        FeatureModelDao featureModelDao = daoSession.getFeatureModelDao();
                        List<FeatureModel> featureModels = featureModelDao.queryBuilder().where(FeatureModelDao.Properties.DbMasterRef.eq(String.valueOf(dbMasterRef)))
                                .list();
                        Log.d("getProjectLocalDB", "" + featureModels.toString());
                        int numData = featureModels.size();
                        if (numData > 0) {
                            for (int i = 0; i<numData; i++) {
                                FeatureModel featureModel = featureModels.get(i);
                                listFeature.add(featureModel);


                            }
                        }
                        else {
                            finish();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productFeature");

    }

    private void generateListFeature(JSONArray response) {
        final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                Gson gson = new GsonBuilder().serializeNulls().create();
                FeatureModel featureModel = gson.fromJson("" + jo, FeatureModel.class);
                featureModel.setProjectFeature(jo.getLong("projectFeature"));
                featureModel.setDbMasterRef(jo.getString("dbMasterRef"));
                featureModel.setProjectRef(jo.getString("projectRef"));
                featureModel.setProjectFeatureName(jo.getString("projectFeatureName"));
                FeatureModelDao featureModelDao = daoSession.getFeatureModelDao();
                featureModelDao.insertOrReplace(featureModel);
                listFeature.add(featureModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterFeature.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProductDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
