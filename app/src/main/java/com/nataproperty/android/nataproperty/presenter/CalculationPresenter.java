package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterCalculationInteractor;
import com.nataproperty.android.nataproperty.model.kpr.CalculationModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.kpr.CalculationActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CalculationPresenter implements PresenterCalculationInteractor {
    private CalculationActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CalculationPresenter(CalculationActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListCal(String memberRef) {
        Call<List<CalculationModel>> call = service.getAPI().getListCal(memberRef);
        call.enqueue(new Callback<List<CalculationModel>>() {
            @Override
            public void onResponse(Call<List<CalculationModel>> call, Response<List<CalculationModel>> response) {
                view.showListCalResults(response);
            }

            @Override
            public void onFailure(Call<List<CalculationModel>> call, Throwable t) {
                view.showListCalFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
