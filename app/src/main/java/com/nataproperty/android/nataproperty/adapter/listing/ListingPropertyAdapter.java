package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.UpdatePromote;
import com.nataproperty.android.nataproperty.network.NataApi;
import com.nataproperty.android.nataproperty.network.NataService;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.chat.ChattingRoomActivity;
import com.nataproperty.android.nataproperty.view.listing.ListingAddPropertyActivity;
import com.nataproperty.android.nataproperty.view.listing.ListingPropertyTabsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by User on 4/19/2016.
 */
public class ListingPropertyAdapter extends BaseAdapter implements Filterable {
    public static final String TAG = "ListingPropertyAdapter";

    public static final String PREF_NAME = "pref";

    private int mCount = 3;
    private Context context;
    private List<ListingPropertyModel> list;
    public List<ListingPropertyModel> orig;
    public List<ListingPropertyModel> list2;
    String memberName, promoteStatus;
    String memberRefReceiver, name, favorite;


    private Display display;
    private ListPropertyHolder holder;

    String listingRef, imageCover, listingTitle, agencyCompanyRef, memberRef, memberRefModel, closingPrice = "", listingMemberRef;

    public ListingPropertyAdapter(Context context, List<ListingPropertyModel> list, Display display, String agencyCompanyRef) {
        this.context = context;
        this.list = list;
        this.display = display;
        this.agencyCompanyRef = agencyCompanyRef;

    }

    SharedPreferences sharedPreferences;
    private boolean state;
    int listingStatus;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberName = sharedPreferences.getString("isName", null);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_listing_property, null);
            holder = new ListPropertyHolder();
//            holder.propertyName = (TextView) convertView.findViewById(R.id.txt_property_name);
            holder.address = (TextView) convertView.findViewById(R.id.text_dec);
            holder.landArea = (TextView) convertView.findViewById(R.id.txt_land_area);//
            holder.luas = (TextView) convertView.findViewById(R.id.text_demension);//
            holder.price = (TextView) convertView.findViewById(R.id.text_price);//
            holder.imageHeader = (ImageView) convertView.findViewById(R.id.img_project);//
            holder.memberName = (TextView) convertView.findViewById(R.id.text_name);//
            holder.car = (TextView) convertView.findViewById(R.id.text_car);
            holder.bed = (TextView) convertView.findViewById(R.id.text_bed);
            holder.bath = (TextView) convertView.findViewById(R.id.text_bath);
            holder.type = (TextView) convertView.findViewById(R.id.text_type);//
            holder.property = (TextView) convertView.findViewById(R.id.text_property);//
            holder.facility = (TextView) convertView.findViewById(R.id.text_facility);
            holder.btnFavorit = (ImageView) convertView.findViewById(R.id.btn_favorite);
            holder.btnMessage = (ImageView) convertView.findViewById(R.id.btn_chat);
            holder.btnCall = (ImageView) convertView.findViewById(R.id.btn_call);
            holder.btnPromote = (ImageView) convertView.findViewById(R.id.btn_promote);
            holder.btnMenu = (ImageView) convertView.findViewById(R.id.ic_menu_item);
            holder.title = (TextView) convertView.findViewById(R.id.text_title);

            holder.linearItemList = (CardView) convertView.findViewById(R.id.linear_item_list_listing);

            convertView.setTag(holder);
        } else {
            holder = (ListPropertyHolder) convertView.getTag();
        }
        final ListingPropertyModel propertyModel = list.get(position);
        holder.type.setText(propertyModel.getListingTypeName());
        holder.property.setText(propertyModel.getSubLocationName());
        holder.title.setText(propertyModel.getListingTitle());
        listingRef = propertyModel.getListingRef();
        memberRefReceiver = propertyModel.getMemberRef();
        name = propertyModel.getMemberName();
        holder.car.setText(propertyModel.getGarageTypeName());
        holder.bed.setText(propertyModel.getBrTypeName() + " +" + propertyModel.getMaidBRTypeName());
        holder.bath.setText(propertyModel.getBathRTypeName() + " +" + propertyModel.getMaidBathRTypeName());
        holder.facility.setText(propertyModel.getFacility());
        if (propertyModel.isFavorite()) {
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnFavorit.setImageResource(R.drawable.ic_favorite_true2);
        }else{
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnFavorit.setImageResource(R.drawable.ic_favorite_false);
        }
        if (memberName.equals(name)) {
            holder.btnPromote.setVisibility(View.VISIBLE);
            holder.btnFavorit.setVisibility(View.GONE);
        } else {
            holder.btnPromote.setVisibility(View.GONE);
            holder.btnFavorit.setVisibility(View.VISIBLE);
        }


//        holder.facility.setText(propertyModel.);

//        holder.propertyName.setText(propertyModel.getListingTitle() + " (" + propertyModel.getListingTypeName() + ")");
        holder.address.setText(propertyModel.getSubLocationName() + ", " + propertyModel.getCityName() + ", "
                + propertyModel.getProvinceName());
        holder.price.setText("Rp. " + propertyModel.getPrice() + " (" + propertyModel.getPriceTypeName() + ")");
        holder.luas.setText(Html.fromHtml(String.valueOf(propertyModel.getBuildArea()) + " / " + Html.fromHtml(String.valueOf(propertyModel.getLandArea())) + "m<sup><small>2</small></sup>"));

//        holder.buildArea.setText(Html.fromHtml(String.valueOf(propertyModel.getBuildArea() + " m<sup><small>2</small></sup>") +
//                " (" + propertyModel.getBuildDimX() + "x" + propertyModel.getBuildDimY() + ")"));
//        holder.landArea.setText(Html.fromHtml(String.valueOf(propertyModel.getLandArea() + " m<sup><small>2</small></sup>") +
//                " (" + propertyModel.getLandDimX() + "x" + propertyModel.getLandDimY() + ")"));
        holder.memberName.setText(propertyModel.getMemberName());

        Glide.with(context)
                .load(propertyModel.getImageCover()).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.imageHeader);

        Log.d(TAG, "buildArea " + propertyModel.getLandArea());

//        holder.btnBookmark.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });
        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //phone call
            }
        });
        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattingRoomActivity.class);
                intent.putExtra("memberRefSender", memberRef);
                intent.putExtra("memberRefReceiver", memberRefReceiver);
                intent.putExtra("name", name);
                context.startActivity(intent);
            }
        });
        holder.btnFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NataApi service = NataService.getClient().create(NataApi.class);
                final Call<UpdatePromote> call = service.updateFavorite(listingRef, memberRef, "true");
                call.enqueue(new Callback<UpdatePromote>() {
                    @Override
                    public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                        int status = response.body().getStatus();
                        String message = response.body().getMessage();
                        if (status == 200) {
                            Toast.makeText(context, "Add Favorite is success", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Failed. Please check your network", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<UpdatePromote> call, Throwable t) {

                    }
                });
            }
        });
        holder.btnPromote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NataApi service = NataService.getClient().create(NataApi.class);
                final Call<UpdatePromote> call = service.updatePromote(listingRef);
                call.enqueue(new Callback<UpdatePromote>() {
                    @Override
                    public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                        int status = response.body().getStatus();
                        String message = response.body().getMessage();
                        if (status == 200) {
                            Toast.makeText(context, "Promote has been successfull", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Sory you can't promote again. Please try after 3 hour.", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<UpdatePromote> call, Throwable t) {

                    }
                });
            }
        });

        holder.linearItemList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listingRef = list.get(position).getListingRef();
                imageCover = list.get(position).getImageCover();
                listingTitle = propertyModel.getListingTitle() + " (" + propertyModel.getListingTypeName() + ")";
                listingMemberRef = list.get(position).getMemberRef();

                Intent intent = new Intent(context, ListingPropertyTabsActivity.class);
                intent.putExtra("listingTitle", listingTitle);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("imageCover", imageCover);
                intent.putExtra("listingMemberRef", listingMemberRef);
                context.startActivity(intent);
            }
        });

        memberRefModel = propertyModel.getMemberRef();

        if (memberRef.equals(memberRefModel)) {
            holder.btnMenu.setVisibility(View.VISIBLE);

        } else {
            holder.btnMenu.setVisibility(View.GONE);
        }


        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(context, v);
                popup.getMenuInflater().inflate(R.menu.menu_item_listview, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.btn_delete:
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setMessage("Apakah anda ingin menghapus listing property?");
                                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        listingRef = list.get(position).getListingRef();
                                        deleteListing(listingRef, position);

                                    }
                                });
                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                return true;

                            case R.id.btn_edit:
                                listingRef = list.get(position).getListingRef();
                                //agencyCompanyRef = list.get(position).getAgencyCompanyRef();
                                Intent intent = new Intent(context, ListingAddPropertyActivity.class);
                                intent.putExtra("listingRef", listingRef);
                                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                                context.startActivity(intent);
                                return true;

                            case R.id.btn_mark_as:
                                listingRef = list.get(position).getListingRef();
                                popupListMarkAs(position);
                                return true;

                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        return convertView;
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ListingPropertyModel> results = new ArrayList<ListingPropertyModel>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ListingPropertyModel PropertyModel : orig) {
                            if (PropertyModel.getListingTitle().toLowerCase().contains(constraint.toString()))
                                results.add(PropertyModel);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count = results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<ListingPropertyModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void deleteListing(final String listingRef, final int position) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        list.remove(position);
                        notifyDataSetChanged();
                        notifyDataSetInvalidated();
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            //Toast.makeText(ListingAddGalleryActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteListing");

    }

    private void popupListMarkAs(final int position) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                android.R.layout.select_dialog_item);
        arrayAdapter.add("Not Active");
        arrayAdapter.add("Sold / Rented");
        arrayAdapter.add("Closed");

        builderSingle.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //listingStatus
                        listingStatus = which + 2;
                        String strName = arrayAdapter.getItem(which);
                        if (strName.equals("Not Active")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Not Active adalah status sementara, dan property ini tidak akan muncul di listing Aplikasi.\n" +
                                    "Silakan mengubah status menjadi Active kembali via Web Dashboard anda.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Sold / Rented")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Apakah anda ingin mengubah ke status sold / rented ?");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Closed")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Dengan mengubah status mennjadi closed, " +
                                    "maka listing ini tidak bisa diaktifkan kembali.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();
                        } else {

                        }


                    }
                });
        builderSingle.show();
    }

    public void updateListingStatus(final int listingStatus, final int position) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updateStatusListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        list.remove(position);
                        notifyDataSetChanged();
                        notifyDataSetInvalidated();
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                params.put("listingStatus", String.valueOf(listingStatus));
                params.put("closingPrice", closingPrice);
                params.put("memberRef", memberRef);

                Log.d("param", listingRef + " - " + listingStatus + " - " + closingPrice + " - " + memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insertFacility");

    }

    private class ListPropertyHolder {
        TextView address, luas, landArea, price, memberName, car, bed, bath, type, property, facility, title;
        ImageView imageHeader, btnFavorit, btnMessage, btnCall, btnPromote, btnMenu;
        CardView linearItemList;
    }

}
