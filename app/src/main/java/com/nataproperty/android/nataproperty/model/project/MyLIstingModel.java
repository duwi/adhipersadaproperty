package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by herlambang-nata on 11/3/2016.
 */
@Entity
public class MyLIstingModel {
    @Id
    long dbMasterRef;
    String projectRef;
    String locationRef;
    String locationName;
    String sublocationRef;
    String subLocationName;
    String projectName;
    String isJoin;
    String image;

    @Generated(hash = 100738818)
    public MyLIstingModel(long dbMasterRef, String projectRef, String locationRef,
            String locationName, String sublocationRef, String subLocationName,
            String projectName, String isJoin, String image) {
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.locationRef = locationRef;
        this.locationName = locationName;
        this.sublocationRef = sublocationRef;
        this.subLocationName = subLocationName;
        this.projectName = projectName;
        this.isJoin = isJoin;
        this.image = image;
    }

    @Generated(hash = 1654610400)
    public MyLIstingModel() {
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getLocationRef() {
        return locationRef;
    }

    public void setLocationRef(String locationRef) {
        this.locationRef = locationRef;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSublocationRef() {
        return sublocationRef;
    }

    public void setSublocationRef(String sublocationRef) {
        this.sublocationRef = sublocationRef;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(String isJoin) {
        this.isJoin = isJoin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
