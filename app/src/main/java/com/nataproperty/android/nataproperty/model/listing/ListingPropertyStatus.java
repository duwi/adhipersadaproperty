package com.nataproperty.android.nataproperty.model.listing;

import java.util.List;

/**
 * Created by nata on 12/5/2016.
 */

public class ListingPropertyStatus {
    int status;
    String message;
    String count;
    int totalPage;
    String agencyCompanyRef;
    List<ListingPropertyModel> data;

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<ListingPropertyModel> getData() {
        return data;
    }

    public void setData(List<ListingPropertyModel> data) {
        this.data = data;
    }

}
