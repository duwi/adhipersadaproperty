package com.nataproperty.android.nataproperty.view.ilustration.booking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

public class BookingCreditCardWebviewActivity extends AppCompatActivity {
    public static final String TAG = "BookingCreditCard";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String STATUS_PAYMENT = "statusPayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private SharedPreferences sharedPreferences;

    private String memberRef, dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, bookingRef, projectName,
            projectBookingRef,link,titlePayment;

    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_credit_card_webview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Credit Card");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        link = intent.getStringExtra("link");
        titlePayment = intent.getStringExtra("titlePayment");

        title.setText(titlePayment);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(link);
        Log.d("lingWebView",""+link);

        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
    }

    public class JavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        public void intentFinish() {
            Intent intent = new Intent(BookingCreditCardWebviewActivity.this,BookingFinishActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(CLUSTER_REF,clusterRef);
            intent.putExtra(PRODUCT_REF,productRef);
            intent.putExtra(UNIT_REF,unitRef);
            intent.putExtra(TERM_REF,termRef);
            intent.putExtra(TERM_NO,termNo);

            intent.putExtra(BOOKING_REF, bookingRef);
            intent.putExtra(PROJECT_NAME,projectName);
            intent.putExtra(STATUS_PAYMENT, WebService.updatePaymentTypeBankTransfer);
            startActivity(intent);
            finish();
        }

        public void intentPaymentMethod() {
            Toast.makeText(mContext, "Failed to Pay", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
