package com.nataproperty.android.nataproperty.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 10/27/2016.
 */
public class NataService extends BaseService {
    //private static String BASE_URL ="http://192.168.10.252/services/AppsServices.asmx/";
//        public static final String BASE_URL = "http://192.168.10.186/services/AppsServices.asmx/";
    public static final String BASE_URL = "http://192.168.10.128/services/AppsServices.asmx/";
     //public static final String BASE_URL = "http://192.168.43.83/services/AppsServices.asmx/";
   // private static String BASE_URL = "http://192.168.10.186/services/AppsServices.asmx/";
    //    public static final String BASE_URL = "http://www.nataproperty.com/services/appsServices.asmx/";
    //http://192.168.10.180/services/AppsServices.asmx?op=getListProjectSvc
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
