package com.nataproperty.android.nataproperty.view.listing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.RvChatHistoryAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.listing.ChatHistoryModel;
import com.nataproperty.android.nataproperty.model.listing.ChatHistoryModelDao;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ListHistoryChatPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingHistoryChatActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    private String memberRef,agencyCompanyRef;
    private SharedPreferences sharedPreferences;
    public static final String TAG = "ListingMainActivity" ;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListHistoryChatPresenter presenter;
    ProgressDialog progressDialog;
    private Typeface font;
    String memberName;
    private RecyclerView recyclerView;
    private List<ChatHistoryModel> historyModelList = new ArrayList<>();
    private Display display;
    Intent intent;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    LinearLayoutManager linearLayoutManager;

    private FloatingActionButton floatingActionButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_history_chat);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListHistoryChatPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();
        intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );
        memberName = sharedPreferences.getString("isName",null );
        requestHistory();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingHistoryChatActivity.this, ListingContactChatActivity.class);
                intent.putExtra("agencyCompanyRef",agencyCompanyRef);
                startActivity(intent);
           }
        });


    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Pesan");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        recyclerView = (RecyclerView) findViewById(R.id.list_history);
        linearLayoutManager = new LinearLayoutManager(ListingHistoryChatActivity.this,LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        historyModelList.clear();
        requestHistory();
    }

    private void requestHistory(){
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getHistoryChat(memberRef);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    public void showHistoryChatResults(Response<List<ChatHistoryModel>> response) {
//        progressDialog.dismiss();


        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();

        historyModelList = response.body();
        for (int i = 0; i < historyModelList.size(); i++) {
            ChatHistoryModel chatHistoryModel = new ChatHistoryModel();
            chatHistoryModel.setChatRef(response.body().get(i).getChatRef());
            chatHistoryModel.setMemberRefReceiver(response.body().get(i).getMemberRefReceiver());
            chatHistoryModel.setMemberNameReceiver(response.body().get(i).getMemberNameReceiver());
            chatHistoryModel.setLastChat(response.body().get(i).getLastChat());
            chatHistoryModel.setLastTime(response.body().get(i).getLastTime());
            chatHistoryModel.setNewMessageCount(response.body().get(i).getNewMessageCount());
            chatHistoryModel.setIdChatHistory(i);
            ChatHistoryModelDao chatHistoryModelDao = daoSession.getChatHistoryModelDao();
            chatHistoryModelDao.insertOrReplace(chatHistoryModel);
        }
        initAdapter();




    }

    private void initAdapter() {
        RvChatHistoryAdapter adapter = new RvChatHistoryAdapter(ListingHistoryChatActivity.this, historyModelList, display);
        recyclerView.setAdapter(adapter);
    }

    public void showHistoryChatFailure(Throwable t) {
//        progressDialog.dismiss();
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        ChatHistoryModelDao chatHistoryModelDao = daoSession.getChatHistoryModelDao();
        List<ChatHistoryModel> chatHistoryModels = chatHistoryModelDao.queryBuilder()
                .list();
        Log.d("getProjectLocalDB", "" + chatHistoryModels.toString());
        int numData = chatHistoryModels.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                ChatHistoryModel chatHistoryModel = chatHistoryModels.get(i);
                historyModelList.add(chatHistoryModel);
                initAdapter();

            }
        }
    }
}
