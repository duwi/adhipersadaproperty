package com.nataproperty.android.nataproperty.view.listing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.Static.ListingStatic;
import com.nataproperty.android.nataproperty.model.listing.ListCountChatModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ListMainPresenter;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingMainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";

    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private ListMainPresenter presenter;
    ProgressDialog progressDialog;

    ImageView imageView;
    ImageButton btnCompanyInfo, btnMemberInfo, btnListingProperty, btnMessage, btnBookmark, btnMember, btnAllCoBroke, btnNews;
    String memberRef, psRef, agencyCompanyRef, imageLogo, titleToolbar;
    SharedPreferences sharedPreferences;
    String memberRefId, memberType, memberTypeCode, companyName;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result, widthButton, heightButton;
    Handler handler;
    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_listing_main);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListMainPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberRefId = sharedPreferences.getString("isName", null);
        memberType = sharedPreferences.getString("isMemberType", null);
        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        titleToolbar = intent.getStringExtra("title");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        companyName = intent.getStringExtra("companyName");

        if (memberTypeCode.equals("2")) {
            setContentView(R.layout.activity_listing_main);
        } else {
            setContentView(R.layout.activity_listing_main_cobroke);
        }

        initWidget();
        Log.d(TAG, "memberTypeCode " + memberTypeCode);
        Glide.with(this).load(imageLogo).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);

        rowModel();

        btnListingProperty.setOnClickListener(this);
        btnCompanyInfo.setOnClickListener(this);
        btnMemberInfo.setOnClickListener(this);
        btnMessage.setOnClickListener(this);
        btnBookmark.setOnClickListener(this);
        btnMember.setOnClickListener(this);
        btnAllCoBroke.setOnClickListener(this);
        checkChat();
//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                checkChat();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void rowModel() {
        /*Row1*/
        ViewGroup.LayoutParams paramsInfo = btnCompanyInfo.getLayoutParams();
        paramsInfo.height = heightButton.intValue();
        btnCompanyInfo.setLayoutParams(paramsInfo);
        btnCompanyInfo.requestLayout();

        /*Row2*/
        ViewGroup.LayoutParams paramsListing = btnListingProperty.getLayoutParams();
        paramsListing.height = heightButton.intValue();
        btnListingProperty.setLayoutParams(paramsListing);
        btnListingProperty.requestLayout();

         /*Row3*/
        ViewGroup.LayoutParams paramsMessage = btnAllCoBroke.getLayoutParams();
        paramsMessage.height = heightButton.intValue();
        btnAllCoBroke.setLayoutParams(paramsMessage);
        btnAllCoBroke.requestLayout();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(titleToolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.image_projek);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        widthButton = size.x / 3.0;
        heightButton = widthButton / 1.0;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        btnCompanyInfo = (ImageButton) findViewById(R.id.btn_company_info);
        btnMemberInfo = (ImageButton) findViewById(R.id.btn_member_info);
        btnListingProperty = (ImageButton) findViewById(R.id.btn_listing_property);
        btnMessage = (ImageButton) findViewById(R.id.btn_message);
        btnBookmark = (ImageButton) findViewById(R.id.btn_favorit);
        btnMember = (ImageButton) findViewById(R.id.btn_member);
        btnAllCoBroke = (ImageButton) findViewById(R.id.btn_cobroke);

        if (memberTypeCode.startsWith("2")) {
            btnListingProperty.setVisibility(View.VISIBLE);
            btnListingProperty.setBackgroundResource(R.drawable.selector_listing_menu_listing_agency);
        } else {
            btnListingProperty.setVisibility(View.VISIBLE);
            btnListingProperty.setBackgroundResource(R.drawable.selector_listing_menu_listing_cobroke);

        }
    }

    private void checkChat() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getCountChat(memberRef);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkChat();
//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                checkChat();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                handler.removeCallbacksAndMessages(r);
                Intent intent1 = new Intent(this, MainMenuActivity.class);
                startActivity(intent1);
                return true;

            case R.id.action_top_right:
//                handler.removeCallbacksAndMessages(r);
                Intent intentProjectMenu = new Intent(ListingMainActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_listing_property:
                ListingStatic listingStatic = new ListingStatic();
                listingStatic.setAgencyCompanyRef(agencyCompanyRef);
                listingStatic.setImageLogo(imageLogo);
                listingStatic.setMemberTypeCode(memberTypeCode);
                listingStatic.setPsRef("");
                listingStatic.setActivityFrom("ListingPropertyActivity");
//                handler.removeCallbacksAndMessages(r);
                Intent intent = new Intent(ListingMainActivity.this, ListingPropertyActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("imageLogo", imageLogo);
                intent.putExtra("psRef", "");
                intent.putExtra("memberTypeCode", memberTypeCode);
                startActivity(intent);
                break;
            case R.id.btn_company_info:
//                handler.removeCallbacksAndMessages(r);
                Intent intent2 = new Intent(ListingMainActivity.this, ListingCompanyInfoActivity.class);
                intent2.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent2.putExtra("imageLogo", imageLogo);
                startActivity(intent2);
                break;
            case R.id.btn_member_info:
                ListingStatic listingStatic4 = new ListingStatic();
                listingStatic4.setAgencyCompanyRef(agencyCompanyRef);
                listingStatic4.setImageLogo(imageLogo);
                listingStatic4.setMemberTypeCode(memberTypeCode);
                listingStatic4.setActivityFrom("ListingMemberInfoActivity");
                listingStatic4.setPsRef("");

//                handler.removeCallbacksAndMessages(r);
                Intent intent3 = new Intent(ListingMainActivity.this, ListingMemberInfoActivity.class);
                intent3.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent3.putExtra("imageLogo", imageLogo);
                startActivity(intent3);
                break;
            case R.id.btn_message:
//                handler.removeCallbacksAndMessages(r);
                Intent intent4 = new Intent(ListingMainActivity.this, ListingHistoryChatActivity.class);
                intent4.putExtra("agencyCompanyRef", agencyCompanyRef);
                startActivity(intent4);
                break;
            case R.id.btn_favorit:
                Intent favoritIntent = new Intent(this, ListingBookmarkActivity.class);
                favoritIntent.putExtra("agencyCompanyRef", agencyCompanyRef);
                favoritIntent.putExtra("imageLogo", imageLogo);
                favoritIntent.putExtra("psRef", "");
                favoritIntent.putExtra("memberTypeCode", memberTypeCode);
                startActivity(favoritIntent);
                break;
            case R.id.btn_member:
                ListingStatic listingStatic3 = new ListingStatic();
                listingStatic3.setAgencyCompanyRef(agencyCompanyRef);
                listingStatic3.setImageLogo(imageLogo);
                listingStatic3.setMemberTypeCode(memberTypeCode);
                listingStatic3.setActivityFrom("ListingPropertyMemberActivity");
                listingStatic3.setPsRef("");
                Intent member = new Intent(ListingMainActivity.this, ListingPropertyMemberActivity.class);
                member.putExtra("agencyCompanyRef", agencyCompanyRef);
                member.putExtra("imageLogo", imageLogo);
                member.putExtra("psRef", "");
                member.putExtra("memberTypeCode", memberTypeCode);
                startActivity(member);
                break;
            case R.id.btn_cobroke:
                ListingStatic listingStatic2 = new ListingStatic();
                listingStatic2.setAgencyCompanyRef(agencyCompanyRef);
                listingStatic2.setImageLogo(imageLogo);
                listingStatic2.setMemberTypeCode(memberTypeCode);
                listingStatic2.setPsRef("");
                listingStatic2.setActivityFrom("ListingPropertyCoBrokeActivity");

                Intent cobroke = new Intent(ListingMainActivity.this, ListingPropertyCoBrokeActivity.class);
                cobroke.putExtra("agencyCompanyRef", agencyCompanyRef);
                cobroke.putExtra("imageLogo", imageLogo);
                cobroke.putExtra("psRef", "");
                cobroke.putExtra("memberTypeCode", memberTypeCode);
                startActivity(cobroke);
                break;


        }
    }

    public void showListCountResults(Response<ListCountChatModel> response) {
//        progressDialog.dismiss();
        String status = response.body().getStatus();
        if (status.equals("200")) {
            int countChat = response.body().getMessage();
            if (countChat >= 1) {
                btnMessage.setBackgroundResource(R.drawable.selector_chat_count);
            } else {

                btnMessage.setBackgroundResource(R.drawable.selector_message_menu);
            }
        }


    }


    public void showListCountFailure(Throwable t) {
//        progressDialog.dismiss();

    }
}
