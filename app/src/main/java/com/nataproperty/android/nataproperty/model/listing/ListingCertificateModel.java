package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 10/6/2016.
 */
@Entity
public class ListingCertificateModel {
    @Id
    long id;
    String certRef, certName;


    @Generated(hash = 666272276)
    public ListingCertificateModel(long id, String certRef, String certName) {
        this.id = id;
        this.certRef = certRef;
        this.certName = certName;
    }

    @Generated(hash = 1480815612)
    public ListingCertificateModel() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCertRef() {
        return certRef;
    }

    public void setCertRef(String certRef) {
        this.certRef = certRef;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }
}
