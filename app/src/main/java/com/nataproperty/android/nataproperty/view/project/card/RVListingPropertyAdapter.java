package com.nataproperty.android.nataproperty.view.project.card;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyModel;

import java.util.List;

/**
 * Created by User on 10/28/2016.
 */
public class RVListingPropertyAdapter extends RecyclerView.Adapter<RVListingPropertyAdapter.ProjectRequestHolder>{
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    List<ListingPropertyModel> projectModels;
    private Context context;
    private Display display;
    ListingPropertyModel projectsModel;


    Integer position=0; //Need to declare because onbind is executed after oncreate, and idk how to get position on oncreate

    public RVListingPropertyAdapter(Context context, List<ListingPropertyModel> projectModels1, Display display){
        this.context = context;
        this.display = display;
        this.projectModels = projectModels1;
    }

    public static class ProjectRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CardView cv;
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btn_project, btnJoined;
        Context context;
        SharedPreferences sharedPreferences;
        private boolean state;
        private String memberRef, dbMasterRef, projectRef;


        ProjectRequestHolder(View itemView) {
            super(itemView);


            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            context=itemView.getContext();
            cv = (CardView)itemView.findViewById(R.id.cv);
            projectName = (TextView)itemView.findViewById(R.id.projectName);
            locationName = (TextView)itemView.findViewById(R.id.locationName);
            subLocationName = (TextView)itemView.findViewById(R.id.subLocationName);
            projectImg = (ImageView) itemView.findViewById(R.id.imgProject);
            btn_project = (ImageView) itemView.findViewById(R.id.btnProject);
            btnJoined = (ImageView) itemView.findViewById(R.id.btn_joined);

//            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(final View v) {

        }

    }

    @Override
    public ProjectRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_card_projectrequest, parent, false);
        final ProjectRequestHolder holder = new ProjectRequestHolder(v);
/*        final Context context = holder.header.getContext();*/
        ProjectRequestHolder crh = new ProjectRequestHolder(v);

        return crh;
    }

    @Override
    public void onBindViewHolder(final ProjectRequestHolder holder, final int position) {
        this.position=position+1;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        holder.state = holder.sharedPreferences.getBoolean("isLogin", false);
        holder.memberRef = holder.sharedPreferences.getString("isMemberRef", null);

//        final ListingPropertyModel project = projectModels.get(position);
//        holder.projectName.setText(project.getProjectName().toUpperCase());
//        holder.locationName.setText(project.getLocationName());
//        holder.subLocationName.setText(project.getSubLocationName());
//
//        Log.d("RVProject",project.getIsJoin());
//
//        if (project.getIsJoin().equals("0")) {
//            holder.btn_project.setVisibility(View.VISIBLE);
//            holder.btnJoined.setVisibility(View.GONE);
//        } else {
//            holder.btnJoined.setVisibility(View.VISIBLE);
//            holder.btn_project.setVisibility(View.GONE);
//        }
//
//        Point size = new Point();
//        display.getSize(size);
//        Integer width = size.x;
//        Double result = width / 1.233333333333333;
//        Log.d("screen width", result.toString() + "--" + Math.round(result));
//
//        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
//        params.width = width;
//        params.height = result.intValue();
//        holder.projectImg.setLayoutParams(params);
//        holder.projectImg.requestLayout();
//        if(MainMenuActivity.OFF_LINE_MODE){
//            holder.projectImg.setImageResource(R.drawable.ic_default_project1);
//        }else {
//            Glide.with(context).load(project.getImage()).into(holder.projectImg);
//        }
//
////        holder.btnJoined.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                projectsModel = projectModels.get(position);
////                DaoSession daoSession = ((BaseApplication) context.getApplicationContext()).getDaoSession();
////                ProjectModelDao projectModelDao = daoSession.getProjectModelDao();
////                QueryBuilder qb = projectModelDao.queryBuilder();
////                qb.where(ProjectModelDao.Properties.DbMasterRef.eq(projectsModel.getDbMasterRef()));
////                List messageReceived = qb.list();
////                projectModelDao.deleteInTx(messageReceived);
////            }
////        });
//
//        holder.projectImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, ProjectMenuActivity.class);
//                intent.putExtra(PROJECT_REF, project.getProjectRef());
//                intent.putExtra(DBMASTER_REF, project.getDbMasterRef());
//                intent.putExtra(PROJECT_NAME, project.getProjectName());
//                intent.putExtra(LOCATION_NAME, project.getLocationName());
//                intent.putExtra(LOCATION_REF, project.getLocationRef());
//                intent.putExtra(SUBLOCATION_NAME, project.getSubLocationName());
//                intent.putExtra(SUBLOCATION_REF, project.getSublocationRef());
//                context.startActivity(intent);
//            }
//        });
//
//        holder.btn_project.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("position ref", holder.memberRef + " " + projectModels.get(position).getDbMasterRef() + " " +
//                        projectModels.get(position).getProjectRef());
//
//                holder.dbMasterRef = String.valueOf(project.getDbMasterRef());
//                holder.projectRef = project.getProjectRef();
//
//                android.support.v7.app.AlertDialog.Builder alertDialogBuilder =
//                        new android.support.v7.app.AlertDialog.Builder(context);
//                alertDialogBuilder.setMessage(R.string.subscribe);
//                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        subscribeProject();
//                    }
//
//                    private void subscribeProject() {
//                        final StringRequest request = new StringRequest(Request.Method.POST,
//                                WebService.subscribeProject(), new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//                                Log.d("getListAutoComplite", "" + response.toString());
//                                try {
//                                    JSONObject jo = new JSONObject(response);
//                                    Log.d("result detail project", response);
//                                    int status = jo.getInt("status");
//                                    String message = jo.getString("message");
//
//                                    if (status == 200) {
//                                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//                                        Intent intent = new Intent(context, ProjectActivity.class);
//                                        ((Activity) context).finish();
//                                        context.startActivity(intent);
//                                    } else if (status == 201) {
//                                        Intent intent = new Intent(context, ValidateInhouseCode.class);
//                                        intent.putExtra("memberRef", holder.memberRef);
//                                        intent.putExtra("dbMasterRef", holder.dbMasterRef);
//                                        intent.putExtra("projectRef", holder.projectRef);
//                                        context.startActivity(intent);
//                                    } else {
//                                        String error = jo.getString("message");
//                                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
//                                    }
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        },
//                                new Response.ErrorListener() {
//                                    @Override
//                                    public void onErrorResponse(VolleyError error) {
////                                        BaseApplication.getInstance().stopLoader();
//                                        LoadingBar.stopLoader();
//                                        NetworkResponse networkResponse = error.networkResponse;
//                                        if (networkResponse != null) {
//                                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
//                                        }
//                                        if (error instanceof TimeoutError) {
//                                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
//                                            Log.d("Volley", "TimeoutError");
//                                        } else if (error instanceof NoConnectionError) {
//                                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
//                                            Log.d("Volley", "NoConnectionError");
//                                        } else if (error instanceof AuthFailureError) {
//                                            Log.d("Volley", "AuthFailureError");
//                                        } else if (error instanceof ServerError) {
//                                            Log.d("Volley", "ServerError");
//                                        } else if (error instanceof NetworkError) {
//                                            Log.d("Volley", "NetworkError");
//                                        } else if (error instanceof ParseError) {
//                                            Log.d("Volley", "ParseError");
//                                        }
//                                    }
//                                }) {
//                            @Override
//                            protected Map<String, String> getParams() throws AuthFailureError {
//                                Map<String, String> params = new HashMap<String, String>();
//                                params.put("memberRef", holder.memberRef);
//                                params.put("dbMasterRef", holder.dbMasterRef.toString());
//                                params.put("projectRef", holder.projectRef.toString());
//
//                                return params;
//                            }
//                        };
//
//                        BaseApplication.getInstance().addToRequestQueue(request, "project");
//                    }
//                });
//                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//
//            }
//        });
 }



    @Override
    public int getItemCount() {
        return projectModels.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



}
