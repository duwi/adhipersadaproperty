package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/25/2016.
 */
@Entity

public class CommisionDetailModel {
    @Id
    long id;
    String commision;
    Double commissionAmt;
    String closingFee;
    String description;

    @Generated(hash = 1577529750)
    public CommisionDetailModel(long id, String commision, Double commissionAmt,
            String closingFee, String description) {
        this.id = id;
        this.commision = commision;
        this.commissionAmt = commissionAmt;
        this.closingFee = closingFee;
        this.description = description;
    }

    @Generated(hash = 364353465)
    public CommisionDetailModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public Double getCommissionAmt() {
        return commissionAmt;
    }

    public void setCommissionAmt(Double commissionAmt) {
        this.commissionAmt = commissionAmt;
    }

    public String getClosingFee() {
        return closingFee;
    }

    public void setClosingFee(String closingFee) {
        this.closingFee = closingFee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
