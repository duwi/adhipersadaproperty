package com.nataproperty.android.nataproperty.view.nup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/21/2016.
 */
public class NupPaymentMethodActivity extends AppCompatActivity {
    public static final String TAG = "NupPaymentMethod";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String NUP_AMT = "nupAmt";
    public static final String TOTAL = "total";

    SharedPreferences sharedPreferences;

    CardView cardViewBankTranfer, veritransCC, veritransMandiri, veritransCimb, veritransBCA;

    ProgressDialog progressDialog;
    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtTotal;
    private String dbMasterRef, projectRef, projectName, nupAmt;
    private boolean mylistNup;
    private String nupOrderRef, total, memberRef,paymentType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_payment_method);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_nup_payment));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        nupAmt = intent.getStringExtra(NUP_AMT);
        total = intent.getStringExtra(TOTAL);

        //fromMyListNup
        mylistNup = intent.getBooleanExtra("mylistNup",false);

        Log.d(TAG, dbMasterRef + " " + projectRef + " " + nupOrderRef + " " + projectName + " " + total);
        Log.d(TAG,"mylistNup "+mylistNup);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtTotal.setText(total);

        cardViewBankTranfer = (CardView) findViewById(R.id.bank_transfer);
        veritransCC = (CardView) findViewById(R.id.veritrans);
        veritransMandiri = (CardView) findViewById(R.id.veritrans_mandiri) ;
        veritransCimb = (CardView) findViewById(R.id.veritrans_cimb) ;
        veritransBCA = (CardView) findViewById(R.id.veritrans_bca) ;


        cardViewBankTranfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentTypeBankTranfer(WebService.updatePaymentTypeBankTransfer);

            }
        });


        veritransCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeCreditCard,getString(R.string.txt_title_credit_card));

            }
        });

        veritransMandiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeMandiri,"Mandiri Klik Pay");

            }
        });

        veritransCimb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeCimb,"CIMB Clicks");

            }
        });

        veritransBCA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentVeritrans(WebService.updatePaymentTypeBCA,"BCA Klik Pay");

            }
        });

    }

    /**
     * paymentTypeBankTranfer
     */
    private void paymentTypeBankTranfer(final String paymentType) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updatePaymentTypeOrder(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d(TAG, "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result nup payment", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    String link = jo.getString("redirectUrl");

                    if (status == 200) {
                        Intent intent = new Intent(NupPaymentMethodActivity.this, NupBankTransferActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                        intent.putExtra(TOTAL, total);
                        intent.putExtra("paymentType", paymentType);
                        startActivity(intent);
                    } else if (status == 300){
                        Intent intent = new Intent(NupPaymentMethodActivity.this, NupCreditCardWebviewActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                        intent.putExtra(TOTAL, total);
                        intent.putExtra("link", link);
                        intent.putExtra("titlePayment", getString(R.string.txt_title_bank_transfer));
                        intent.putExtra("paymentType", paymentType);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupPaymentMethodActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupPaymentMethodActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nupOrderRef", nupOrderRef);
                params.put("paymentType", WebService.updatePaymentTypeBankTransfer);
                params.put("memberRef", memberRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "updatePaymentTypeBankTransfer");
    }

    /**
     * paymentTypeCreditCard
     */
    private void paymentVeritrans(final String paymentType,final String titlePayment) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updatePaymentTypeOrderVT(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    String link = jo.getString("redirectUrl");
                    Log.d(TAG, "redirectUrl  " + link);

                    if (status == 200) {
                        Intent intent = new Intent(NupPaymentMethodActivity.this, NupCreditCardWebviewActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                        intent.putExtra(TOTAL, total);
                        intent.putExtra("link", link);
                        intent.putExtra("titlePayment", titlePayment);
                        intent.putExtra("paymentType", paymentType);
                        startActivity(intent);
                    } else if (status == 406) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupPaymentMethodActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupPaymentMethodActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();

                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nupOrderRef", nupOrderRef);
                params.put("paymentType", paymentType);
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("memberRef", memberRef);
                params.put("nupAmt", nupAmt);
                params.put("total", total);

                Log.d("params", " " + nupOrderRef + " " + WebService.updatePaymentTypeCreditCard + " " +
                        dbMasterRef + " " + projectRef + " " + memberRef + " " + nupAmt + " " + total);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "updatePaymentTypeCreditCard");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NupPaymentMethodActivity.this, ProjectMenuActivity.class);
        intent.putExtra(PROJECT_REF, projectRef);
        intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
        intent.putExtra(PROJECT_NAME, projectName);
        intent.putExtra("EXIT", mylistNup);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }
}
