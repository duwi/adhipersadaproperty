package com.nataproperty.android.nataproperty.model.listing;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Nata on 12/6/2016.
 */

public class ResponeListingAddPropertyUnitModel {
    int status ;
    String message;
    @SerializedName("brType")
    ArrayList<BrTypeModel>  listBrType;
    @SerializedName("maidBRType")
    ArrayList<MaidBRTypeModel> listMaidBrType;
    @SerializedName("bathRType")
    ArrayList<BathRTypeModel> listBathRType;
    @SerializedName("maidBathRType")
    ArrayList<MaidBathRTypeModel> listMaidBathRType;
    @SerializedName("garageType")
    ArrayList<GarageTypeModel> listGarageType;
    @SerializedName("carportType")
    ArrayList<CarportTypeModel> listCarportType;
    @SerializedName("phoneLine")
    ArrayList<PhoneLineModel> listPhoneLine;
    @SerializedName("furnishType")
    ArrayList<FurnishTypeModel> listFurnishType;
    @SerializedName("electricType")
    ArrayList<ElectricTypeModel> listElectricType;
    @SerializedName("facingType")
    ArrayList<FacingTypeModel> listFacingType;
    @SerializedName("viewType")
    ArrayList<ViewTypeModel> listViewType;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<BrTypeModel> getListBrType() {
        return listBrType;
    }

    public void setListBrType(ArrayList<BrTypeModel> listBrType) {
        this.listBrType = listBrType;
    }

    public ArrayList<MaidBRTypeModel> getListMaidBrType() {
        return listMaidBrType;
    }

    public void setListMaidBrType(ArrayList<MaidBRTypeModel> listMaidBrType) {
        this.listMaidBrType = listMaidBrType;
    }

    public ArrayList<BathRTypeModel> getListBathRType() {
        return listBathRType;
    }

    public void setListBathRType(ArrayList<BathRTypeModel> listBathRType) {
        this.listBathRType = listBathRType;
    }

    public ArrayList<MaidBathRTypeModel> getListMaidBathRType() {
        return listMaidBathRType;
    }

    public void setListMaidBathRType(ArrayList<MaidBathRTypeModel> listMaidBathRType) {
        this.listMaidBathRType = listMaidBathRType;
    }

    public ArrayList<GarageTypeModel> getListGarageType() {
        return listGarageType;
    }

    public void setListGarageType(ArrayList<GarageTypeModel> listGarageType) {
        this.listGarageType = listGarageType;
    }

    public ArrayList<CarportTypeModel> getListCarportType() {
        return listCarportType;
    }

    public void setListCarportType(ArrayList<CarportTypeModel> listCarportType) {
        this.listCarportType = listCarportType;
    }

    public ArrayList<PhoneLineModel> getListPhoneLine() {
        return listPhoneLine;
    }

    public void setListPhoneLine(ArrayList<PhoneLineModel> listPhoneLine) {
        this.listPhoneLine = listPhoneLine;
    }

    public ArrayList<FurnishTypeModel> getListFurnishType() {
        return listFurnishType;
    }

    public void setListFurnishType(ArrayList<FurnishTypeModel> listFurnishType) {
        this.listFurnishType = listFurnishType;
    }

    public ArrayList<ElectricTypeModel> getListElectricType() {
        return listElectricType;
    }

    public void setListElectricType(ArrayList<ElectricTypeModel> listElectricType) {
        this.listElectricType = listElectricType;
    }

    public ArrayList<FacingTypeModel> getListFacingType() {
        return listFacingType;
    }

    public void setListFacingType(ArrayList<FacingTypeModel> listFacingType) {
        this.listFacingType = listFacingType;
    }

    public ArrayList<ViewTypeModel> getListViewType() {
        return listViewType;
    }

    public void setListViewType(ArrayList<ViewTypeModel> listViewType) {
        this.listViewType = listViewType;
    }
}
