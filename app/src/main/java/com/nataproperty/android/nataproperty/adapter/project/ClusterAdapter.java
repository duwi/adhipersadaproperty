package com.nataproperty.android.nataproperty.adapter.project;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.project.ClusterModel;

import java.util.List;

/**
 * Created by User on 4/19/2016.
 */
public class ClusterAdapter extends BaseAdapter {
    public static final String TAG = "ClusterAdapter" ;

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private List<ClusterModel> list;
    private ListClusterHolder holder;

    private Display display;

    public ClusterAdapter(Context context, List<ClusterModel> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_cluster,null);
            holder = new ListClusterHolder();
            holder.clusterName = (TextView) convertView.findViewById(R.id.txt_title_cluster);
            holder.clusterImg = (ImageView) convertView.findViewById(R.id.image_cluster);

            convertView.setTag(holder);
        }else{
            holder = (ListClusterHolder) convertView.getTag();
        }

        ClusterModel cluster = list.get(position);
        holder.clusterName.setText(cluster.getClusterDescription());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.233333333333333;

        ViewGroup.LayoutParams params = holder.clusterImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.clusterImg.setLayoutParams(params);
        holder.clusterImg.requestLayout();

        Glide.with(context)
                .load(WebService.getClusterImage()+cluster.getDbMasterRef()+
                        "&pr="+cluster.getProjectRef()+"&cr="+cluster.getClusterRef())
                .into(holder.clusterImg);

        Log.d(TAG,WebService.getClusterImage()+cluster.getDbMasterRef()+
                "&pr="+cluster.getProjectRef()+"&cr="+cluster.getClusterRef());

        return convertView;
    }

    private class ListClusterHolder {
        TextView clusterName;
        ImageView clusterImg;
    }
}
