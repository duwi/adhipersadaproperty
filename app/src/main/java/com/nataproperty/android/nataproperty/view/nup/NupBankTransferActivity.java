package com.nataproperty.android.nataproperty.view.nup;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.nup.AccountBankAdapter;
import com.nataproperty.android.nataproperty.adapter.nup.BankAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.nup.AccountBankModel;
import com.nataproperty.android.nataproperty.model.nup.BankModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/21/2016.
 */
public class NupBankTransferActivity extends AppCompatActivity {
    public static final String TAG = "NupBankTransferActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String TOTAL = "total";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    private List<AccountBankModel> listAccountBank = new ArrayList<AccountBankModel>();
    private AccountBankAdapter adapterAccountbank;
    MyListView listView;

    private List<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;

    SharedPreferences sharedPreferences;

    ImageView imgPayment;
    Spinner bankList;
    EditText accountName, accountNumber;
    Button btnConfirmPayment;
    TextView txtTotal;

    String dbMasterRef, projectRef, projectName, memberRef, nupOrderRef,total,paymentType;
    String bankRef, fileName, filePath, extension;
    String cekImg = "";

    ProgressDialog progressDialog;

    private Bitmap bitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_bank_transfer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Bank Transfer");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        total = intent.getStringExtra(TOTAL);
        paymentType = intent.getStringExtra("paymentType");

        Log.d(TAG,"paymentType "+paymentType);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout)findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/1.233333333333333;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtTotal.setText(total);

        listView = (MyListView) findViewById(R.id.list_account_bank);

        bankList = (Spinner) findViewById(R.id.list_bank_tranfer);
        accountName = (EditText) findViewById(R.id.edit_account_name);
        accountNumber = (EditText) findViewById(R.id.edit_account_number);

        btnConfirmPayment = (Button) findViewById(R.id.btn_confirm);
        btnConfirmPayment.setTypeface(font);

        imgPayment = (ImageView) findViewById(R.id.img_receipt);
        imgPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isBankRef", bankRef);
                editor.commit();
                Log.d("isBankRef",""+bankRef);
                selectImage();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        listAccountBank.clear();
        listBank.clear();
        requestBank();
        adapterBank = new BankAdapter(this, listBank);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        bankRef = sharedPreferences.getString("isBankRef", null);

        bankList.setAdapter(adapterBank);

        if (bankRef!=null){
            bankList.setSelection(Integer.parseInt(bankRef));
        }else {

        }
        bankList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankRef = listBank.get(position).getBankRef();
                Log.d("bank ref"," "+bankRef);

                bankList.setSelection(Integer.parseInt(bankRef));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        requestAcoountBank();
        adapterAccountbank = new AccountBankAdapter(this, listAccountBank);
        listView.setAdapter(adapterAccountbank);
        listView.setExpanded(true);

        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekAccName = accountName.getText().toString();
                String cekAccNum = accountNumber.getText().toString();
                String cekBank = bankRef;

                if (!cekAccName.isEmpty() && !cekAccNum.isEmpty() && !cekBank.equals("0") && !cekImg.equals("")) {
                    confirmPayment();
                } else {
                    /*Toast.makeText(NupBankTransferActivity.this,"Please enter your details!", Toast.LENGTH_LONG)
                            .show();*/

                    if (cekAccName.isEmpty()) {
                        accountName.setError("Account name must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (cekAccNum.isEmpty()) {
                        accountNumber.setError("Account number must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (bankRef.equals("0")) {
                        Toast.makeText(NupBankTransferActivity.this, "Bank must be filld", Toast.LENGTH_LONG)
                                .show();
                    } else if (cekImg.equals("")) {
                        Toast.makeText(NupBankTransferActivity.this, "You must upload receipt", Toast.LENGTH_LONG)
                                .show();
                    }
                }

            }
        });

    }

    private void requestBank() {
        String url = WebService.getBank();
        String urlPostParameter = "";
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListBank(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListBank(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                BankModel bank = new BankModel();
                bank.setBankRef(jo.getString("bankRef"));
                bank.setBankName(jo.getString("bankName"));

                listBank.add(bank);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapterBank.notifyDataSetChanged();
    }

    private void requestAcoountBank() {
        String url = WebService.getAccountBank();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d("result account bank" +
                    "", result);
            generateListAccountBank(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListAccountBank(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                AccountBankModel accountBankModel = new AccountBankModel();
                accountBankModel.setBankName(jo.getString("bankName"));
                accountBankModel.setAccName(jo.getString("accName"));
                accountBankModel.setAccNo(jo.getString("accNo"));

                listAccountBank.add(accountBankModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapter.notifyDataSetChanged();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NupBankTransferActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //intent.putExtra("type",type);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Use Existing Foto")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    //intent.putExtra("type",type);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Foto"),
                            SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

       /* File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");*/

        File destination = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
        Log.d("directory", destination.toString());
        // have the object build the directory structure, if needed.
        destination.mkdirs();

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination + "/" + "payment.jpg");
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imgPayment.setImageBitmap(bitmap);
        fileName = "";
        cekImg = "1";

    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        String filePath = cursor.getString(column_index);
        extension = filePath.substring(filePath.lastIndexOf(".") + 1);

        String fileNameSegments[] = selectedImagePath.split("/");
        fileName = fileNameSegments[fileNameSegments.length - 1];

        // Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        imgPayment.setImageBitmap(bitmap);
        cekImg = "1";
        Log.d("file", " " + extension + " &@& " + fileName);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void confirmPayment() {
        String url = WebService.getSaveBankTranfer();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef +
                "&projectRef=" + projectRef +
                "&nupOrderRef=" + nupOrderRef +
                "&memberRef=" + memberRef +
                "&image=" + getStringImage(bitmap) +
                "&bankRef=" + bankRef +
                "&accName=" + accountName.getText().toString() +
                "&accNumber=" + accountNumber.getText().toString() +
                "&fileName=" + fileName;

        Log.d("param save bank", dbMasterRef + "-" + projectRef + "-" + nupOrderRef + "-" + memberRef + "-" +
                bankRef + "-" + accountName.getText().toString() + "-" +
                accountNumber.getText().toString() + "-" + fileName);
        Log.d("param save bank", "Img" + getStringImage(bitmap));

        new requestDialog().execute(url, urlPostParameter);


    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NupBankTransferActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = Network.PostHttp(params[0].toString(), params[1].toString());
            try {
                JSONObject jo = new JSONObject(result);
                int status = jo.getInt("status");
                String message = jo.getString("message");

                if (status == 200) {
                    Intent intent = new Intent(NupBankTransferActivity.this, NupFinishActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                    intent.putExtra("paymentType", paymentType);

                    SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                            getSharedPreferences(PREF_NAME, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("isMemberCostumerRef", "");
                    editor.putString("isBankRef", "0");
                    editor.commit();

                    startActivity(intent);
                    Log.d("cek result", " " + message);
                } else {
                    Log.d("cek result", " " + message);
                }

            } catch (JSONException e) {

            }
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("isBankRef", "0");
        editor.commit();
    }
}
