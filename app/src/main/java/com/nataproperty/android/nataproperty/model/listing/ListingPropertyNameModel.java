package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by User on 10/6/2016.
 */
public class ListingPropertyNameModel {
    String propertyRef,propertyName,propertyDesc,countryCode,provinceCode,cityCode,areaRef,inputTime,inputUN;

    public String getPropertyRef() {
        return propertyRef;
    }

    public void setPropertyRef(String propertyRef) {
        this.propertyRef = propertyRef;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyDesc() {
        return propertyDesc;
    }

    public void setPropertyDesc(String propertyDesc) {
        this.propertyDesc = propertyDesc;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaRef() {
        return areaRef;
    }

    public void setAreaRef(String areaRef) {
        this.areaRef = areaRef;
    }

    public String getInputTime() {
        return inputTime;
    }

    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    public String getInputUN() {
        return inputUN;
    }

    public void setInputUN(String inputUN) {
        this.inputUN = inputUN;
    }
}
