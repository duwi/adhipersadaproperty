package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterListContactChatInteractor;
import com.nataproperty.android.nataproperty.model.profile.BaseData;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingContactChatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListContactChatPresenter implements PresenterListContactChatInteractor {
    private ListingContactChatActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListContactChatPresenter(ListingContactChatActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }





    @Override
    public void getContactChat(String agencyCompanyRef) {
        Call<BaseData> call = service.getAPI().getContactMessageAll(agencyCompanyRef);
        call.enqueue(new Callback<BaseData>() {
            @Override
            public void onResponse(Call<BaseData> call, Response<BaseData> response) {
                view.showContactChatResults(response);
            }

            @Override
            public void onFailure(Call<BaseData> call, Throwable t) {
                view.showContactChatFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
