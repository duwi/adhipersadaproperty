package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class ViewTypeModel {
    String viewType;
    String viewTypeName;

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getViewTypeName() {
        return viewTypeName;
    }

    public void setViewTypeName(String viewTypeName) {
        this.viewTypeName = viewTypeName;
    }
}
