package com.nataproperty.android.nataproperty.view.before_login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Window;

import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.before_login.VersionModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.LaunchPresenter;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

/**
 * Created by User on 4/13/2016.
 */
public class LaunchActivity extends Activity {
    public static final String TAG = "LaunchActivity";
    public static final String PREF_NAME = "pref";
    //    public static boolean OFF_LINE_MODE = false;
    boolean ifNotExists = false;
    private static int splashInterval = 1;

    SharedPreferences sharedPreferences;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    boolean state;

    String versionName = "";
    int versionCode = -1;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private LaunchPresenter presenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Log.d(TAG, "onCreate");
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new LaunchPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getVersionInfo();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        requestVersion();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void requestVersion() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getVersionApp("");

    }
    private void getVersionInfo() {

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
            String test = "";

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Log.d(TAG, versionName + " " + versionCode);

    }

    public void showVersionAppResults(retrofit2.Response<VersionModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        String versionApp = response.body().getVersionApp();
        if(status == 200){
            if (versionCode < Integer.parseInt(message)) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LaunchActivity.this);
                alertDialogBuilder.setMessage("Update Available v" + versionApp);
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(WebService.linkAppStore)));
                    }
                });
                //Showing the alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } else {
                if (state) {
                    // open main activity
                    Intent intent = new Intent(LaunchActivity.this, MainMenuActivity.class);
                    startActivity(intent);
//                                finish();
                } else {
                    Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
                    startActivity(intent);
//                                finish();
                }
            }

        }
    }

    public void showVersionAppFailure(Throwable t) {
        if (state) {
            // open main activity
            Intent intent = new Intent(LaunchActivity.this, MainMenuActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
            startActivity(intent);
            finish();
        }
    }


}
