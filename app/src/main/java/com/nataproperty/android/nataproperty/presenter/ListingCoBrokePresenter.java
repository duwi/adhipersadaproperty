package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterListingCoBrokeInteractor;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.model.project.LocationModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingPropertyCoBrokeActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListingCoBrokePresenter implements PresenterListingCoBrokeInteractor {
    private ListingPropertyCoBrokeActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingCoBrokePresenter(ListingPropertyCoBrokeActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getLocationListing() {
        Call<List<LocationModel>> call = service.getAPI().getLocationListingCobroke();
        call.enqueue(new Callback<List<LocationModel>>() {
            @Override
            public void onResponse(Call<List<LocationModel>> call, Response<List<LocationModel>> response) {
                view.showLocationResults(response);
            }

            @Override
            public void onFailure(Call<List<LocationModel>> call, Throwable t) {
                view.showLocationFailure(t);

            }


        });
    }

    @Override
    public void getListingMember(String agencyCompanyRef, String listingStatus, final String pageNo
            , String provinceCode, String cityCode, String locationRef, String listingTypeRef, String categoryType, String memberRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingAllCoBroke(agencyCompanyRef,listingStatus,pageNo,provinceCode,cityCode,
                locationRef,listingTypeRef,categoryType,memberRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showListingMemberResults(response,pageNo);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showListingMemberFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
