package com.nataproperty.android.nataproperty.view.listing;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.youtube.player.YouTubePlayer;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPropertyFeatureAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyFeatureModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.project.YoutubeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ListingPropertyDescriptionFragment extends Fragment {
    public static final String TAG = "PropertyDescription";
    public static final String PREF_NAME = "pref";

    String listingTypeName, categoryTypeName, propertyRef, certName, priceTypeName, listingTitle, listingDesc,
            countryName, provinceName, cityName, subLocationName, block, no, postCode, gMap, price,
            buildDimX, buildDimY, landDimX, landDimY, brTypeName, maidBRTypeName, bathRTypeName,
            maidBathRTypeName, garageTypeName, carportTypeName, phoneLineName, furnishTypeName, electricTypeName,
            facingName, viewTypeName, youTube1, youTube2, address, facilityName, cekFacility, memberRefOwner;

    int imageCount, documentCount;

    double landArea, buildArea;

    private ArrayList<ListingPropertyFeatureModel> listPropertyFeature = new ArrayList<ListingPropertyFeatureModel>();
    private ListingPropertyFeatureAdapter adapter;

    public ListingPropertyDescriptionFragment() {
        // Required empty public constructor
    }

    private View myFragmentView;

    MyListView listView;

    String listingRef, agencyCompanyRef, imageCover, memberRef, listingMemberRef;

    ArrayList<String> facility = new ArrayList<String>();

    @Bind(R.id.txt_desc)
    TextView txtDesc;
    @Bind(R.id.txt_listing_type)
    TextView txtListingType;
    @Bind(R.id.txt_property_type)
    TextView txtPropertyType;
    @Bind(R.id.txt_property_name)
    TextView txtPropertyName;
    @Bind(R.id.txt_sub_location_name)
    TextView txtSubLocationName;
    /*@Bind(R.id.txt_city_name)
    TextView txtCityName;
    @Bind(R.id.txt_province_name)*/
    TextView txtProvinceName;
    @Bind(R.id.txt_address)
    TextView txtAddress;
    @Bind(R.id.txt_block)
    TextView txtBlock;
    @Bind(R.id.txt_no)
    TextView txtNo;
    @Bind(R.id.txt_post_code)
    TextView txtPostCode;
    @Bind(R.id.txt_price)
    TextView txtPrice;
    @Bind(R.id.txt_price_type_name)
    TextView txtPriceTypeName;
    @Bind(R.id.txt_build_area)
    TextView txtBuildArea;
    @Bind(R.id.txt_land_area)
    TextView txtLandArea;
    @Bind(R.id.txt_br_type_name)
    TextView txtBrTypeName;
    @Bind(R.id.txt_maid_br_type_name)
    TextView txtMaidBrTypeName;
    @Bind(R.id.txt_bath_r_type_name)
    TextView txtBathRTypeName;
    @Bind(R.id.txt_maid_bath_r_type_name)
    TextView txtMaidBathRTypeName;
    @Bind(R.id.txt_garage_type_name)
    TextView txtGarageTypeName;
    @Bind(R.id.txt_carport_type_name)
    TextView txtCarportTypeName;
    @Bind(R.id.txt_phone_line_name)
    TextView txtPhoneLineName;
    @Bind(R.id.txt_furnish_type_name)
    TextView txtFurnishTypeName;
    @Bind(R.id.txt_electric_type_name)
    TextView txtElectricTypeName;
    @Bind(R.id.txt_facing_name)
    TextView txtFacingName;
    @Bind(R.id.txt_view_type_name)
    TextView txtViewTypeName;
    @Bind(R.id.txt_facility)
    TextView txtFacility;

    @Bind(R.id.linear_info)
    LinearLayout linearLayoutInfo;
    @Bind(R.id.linear_facility)
    LinearLayout linearLayoutFacility;

    @Bind(R.id.btn_download)
    Button btnDownload;

    @Bind(R.id.btn_video1)
    Button btnVideo1;
    @Bind(R.id.btn_video2)
    Button btnVideo2;
    //@Bind(R.id.btn_send_to_friend)
    Button btnSendToFriend;
    Button btnSendNotif;

    Typeface font;

    EditText edtEmail;
    AlertDialog alertDialog;
    SharedPreferences sharedPreferences;

    private FragmentActivity myContext;

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "xyz";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    @Override
    public void onAttach(Activity activity) {

        if (activity instanceof FragmentActivity) {
            myContext = (FragmentActivity) activity;
        }

        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        //agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        listingRef = intent.getStringExtra("listingRef");
        listingMemberRef = intent.getStringExtra("listingMemberRef");
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listing_property_description, container, false);
        ButterKnife.bind(this, rootView);


        Log.d("listingRef", "" + listingRef);
        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");

        sharedPreferences = getActivity().getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Log.d("listingMemberRef", "" + listingMemberRef + " " + memberRef);

        btnDownload.setTypeface(font);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListingDownloadActivity.class);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("imageCover", imageCover);
                startActivity(intent);
            }
        });

        btnVideo1.setTypeface(font);
        btnVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YoutubeActivity.class);
                intent.putExtra("urlVideo", youTube1);
                startActivity(intent);

            }
        });

        btnVideo2.setTypeface(font);
        btnVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YoutubeActivity.class);
                intent.putExtra("urlVideo", youTube2);
                startActivity(intent);

            }
        });

        btnSendToFriend = (Button) rootView.findViewById(R.id.btn_send_to_friend);
        btnSendToFriend.setVisibility(View.VISIBLE);
        btnSendToFriend.setTypeface(font);
        btnSendToFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpSendToFriend();
            }
        });

        btnSendNotif = (Button) rootView.findViewById(R.id.btn_send_notif);
        btnSendNotif.setTypeface(font);

        btnSendNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotif();
            }
        });

        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();
        facility.clear();
        requestPropertyInfo(listingRef);
    }

    public void requestPropertyInfo(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingPropertyInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    listingDesc = jsonObject.getString("listingDesc");
                    listingTypeName = jsonObject.getString("listingTypeName");
                    categoryTypeName = jsonObject.getString("categoryTypeName");
                    propertyRef = jsonObject.getString("propertyRef");
                    certName = jsonObject.getString("certName");
                    priceTypeName = jsonObject.getString("priceTypeName");
                    listingTitle = jsonObject.getString("listingTitle");
                    countryName = jsonObject.getString("countryName");
                    provinceName = jsonObject.getString("provinceName");
                    cityName = jsonObject.getString("cityName");
                    subLocationName = jsonObject.getString("subLocationName");
                    address = jsonObject.getString("address");
                    block = jsonObject.getString("block");
                    no = jsonObject.getString("no");
                    postCode = jsonObject.getString("postCode");
                    price = jsonObject.getString("priceFormat");
                    buildArea = jsonObject.getDouble("buildArea");
                    buildDimX = jsonObject.getString("buildDimX");
                    buildDimY = jsonObject.getString("buildDimY");
                    landArea = jsonObject.getDouble("landArea");
                    landDimX = jsonObject.getString("landDimX");
                    landDimY = jsonObject.getString("landDimY");
                    brTypeName = jsonObject.getString("brTypeName");
                    maidBRTypeName = jsonObject.getString("maidBRTypeName");
                    bathRTypeName = jsonObject.getString("bathRTypeName");
                    maidBathRTypeName = jsonObject.getString("maidBathRTypeName");
                    garageTypeName = jsonObject.getString("garageTypeName");
                    carportTypeName = jsonObject.getString("carportTypeName");
                    phoneLineName = jsonObject.getString("phoneLineName");
                    furnishTypeName = jsonObject.getString("furnishTypeName");
                    electricTypeName = jsonObject.getString("electricTypeName");
                    facingName = jsonObject.getString("facingName");
                    viewTypeName = jsonObject.getString("viewTypeName");
                    youTube1 = jsonObject.getString("youTube1");
                    youTube2 = jsonObject.getString("youTube2");
                    imageCount = jsonObject.getInt("imageCount");
                    documentCount = jsonObject.getInt("documentCount");
                    memberRefOwner = jsonObject.getString("memberRefOwner");
                    agencyCompanyRef = jsonObject.getString("agencyCompanyRefOwner");
                    JSONArray jsonArrayTable1 = new JSONArray(jsonObject.getJSONArray("facility").toString());
                    Log.d("jsonArrayTable1", "" + jsonArrayTable1.length());
                    generateFeature(jsonArrayTable1);

                    if (listingDesc.equals("")) {
                        txtDesc.setText("-");
                    } else {
                        txtDesc.setText(listingDesc);
                    }

                    txtListingType.setText(listingTypeName);
                    txtPropertyType.setText(categoryTypeName);
                    txtPriceTypeName.setText(priceTypeName);
                    txtPropertyName.setText(listingTitle);
                    txtSubLocationName.setText(subLocationName + ", " + cityName + ", " + provinceName);

                    if (address.equals("")) {
                        txtAddress.setText("-");
                    } else {
                        txtAddress.setText(address);
                    }

                    if (block.equals("")) {
                        txtBlock.setText("-");
                    } else {
                        txtBlock.setText(block);
                    }

                    if (no.equals("")) {
                        txtNo.setText("-");
                    } else {
                        txtNo.setText(no);
                    }

                    if (postCode.equals("")) {
                        txtPostCode.setText("-");
                    } else {
                        txtPostCode.setText(postCode);
                    }

                    txtPrice.setText(price);

                    if (!buildDimX.equals("0") && !buildDimY.equals("0")) {
                        txtBuildArea.setText(Html.fromHtml(String.valueOf(buildArea)+ " m<sup><small>2</small></sup>")
                                +" ("+buildDimX+"x"+buildDimY+")");
                    } else {
                        txtBuildArea.setText(Html.fromHtml(String.valueOf(buildArea) + " m<sup><small>2</small></sup>"));
                    }

                    if (!landDimX.equals("0") && !landDimY.equals("0")){
                        txtLandArea.setText(Html.fromHtml(String.valueOf(landArea) + " m<sup><small>2</small></sup>")
                        + " ("+landDimX+"x"+landDimY+")");
                    } else {
                        txtLandArea.setText(Html.fromHtml(String.valueOf(landArea) + " m<sup><small>2</small></sup>"));
                    }

                    txtBrTypeName.setText(brTypeName);
                    txtMaidBrTypeName.setText(maidBRTypeName);
                    txtBathRTypeName.setText(bathRTypeName);
                    txtMaidBathRTypeName.setText(maidBathRTypeName);
                    txtGarageTypeName.setText(garageTypeName);
                    txtCarportTypeName.setText(carportTypeName);
                    txtPhoneLineName.setText(phoneLineName);
                    txtFurnishTypeName.setText(furnishTypeName);
                    txtElectricTypeName.setText(electricTypeName);
                    txtFacingName.setText(facingName);
                    txtViewTypeName.setText(viewTypeName);
                    txtListingType.setText(listingTypeName);
                    //txtPropertyName.setText(carportTypeName);

                    if (youTube1.equals("") || youTube2.equals("")) {
                        if (jsonObject.getString("youTube1").trim().equals("")) {
                            btnVideo1.setVisibility(View.GONE);
                        } else {
                            btnVideo1.setVisibility(View.VISIBLE);
                            btnVideo1.setText("Video");
                        }

                        if (jsonObject.getString("youTube2").trim().equals("")) {
                            btnVideo2.setVisibility(View.GONE);
                        } else {
                            btnVideo2.setVisibility(View.VISIBLE);
                            btnVideo2.setText("Video");
                        }
                    } else {
                        btnVideo1.setVisibility(View.VISIBLE);
                        btnVideo2.setVisibility(View.VISIBLE);
                    }

                    if (imageCount > 0 && memberRefOwner.equals(memberRef)) {
                        btnSendNotif.setVisibility(View.VISIBLE);
                    } else {
                        btnSendNotif.setVisibility(View.GONE);
                    }

                    if (documentCount == 0) {
                        btnDownload.setVisibility(View.GONE);
                    } else {
                        btnDownload.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "propertyInfo");

    }

    private void generateFeature(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                facility.add(jo.getString("facilityName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (response.length() == 0) {
            linearLayoutFacility.setVisibility(View.GONE);
        } else {
            txtFacility.setText(facility.toString().replace("[", "").replace("]", ""));
        }

    }

    public void popUpSendToFriend() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listing_send_to_friend, null);
        dialogBuilder.setView(dialogView);
        edtEmail = (EditText) dialogView.findViewById(R.id.edt_email);
        dialogBuilder.setTitle("Shared this listing");
        dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_send), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {


            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekEmail = edtEmail.getText().toString().trim();
                if (cekEmail.isEmpty()) {
                    edtEmail.setError("Email harus diisi");
                } /*else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(cekEmail).matches()) {
                    edtEmail.setError("Email ini tidak valid");
                } */ else {
                    sendToFriend();
                    edtEmail.setError(null);
                    alertDialog.dismiss();
                }

            }


        });


    }

    public void sendToFriend() {
//        BaseApplication.getInstance().startLoader(getActivity());
        LoadingBar.startLoader(getActivity());
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.sendToFriendListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", edtEmail.getText().toString().trim());
                params.put("memberRef", memberRef);
                params.put("listingRef", listingRef);

                Log.d("param", "" + edtEmail.getText().toString().trim() + "-" + memberRef + "-" + listingRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "sendToFriend");

    }

    public void sendNotif() {
//        BaseApplication.getInstance().startLoader(getActivity());
        LoadingBar.startLoader(getActivity());
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.sendNotifToAgencyMember(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(getContext(), getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("agencyCompanyRef", agencyCompanyRef);
                params.put("listingRef", listingRef);
                params.put("memberRef", memberRef);
                Log.d(TAG, "param " + agencyCompanyRef + " " + listingRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "sendToNotif");

    }

}
