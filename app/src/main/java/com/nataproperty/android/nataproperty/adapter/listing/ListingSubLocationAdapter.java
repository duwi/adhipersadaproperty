package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.profile.SubLocationModel;

import java.util.List;

/**
 * Created by User on 4/17/2016.
 */
public class ListingSubLocationAdapter extends BaseAdapter {
    private Context context;
    private List<SubLocationModel> list;
    private ListCityHolder holder;

    public ListingSubLocationAdapter(Context context, List<SubLocationModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list,null);
            holder = new ListCityHolder();
            holder.provinceName = (TextView) convertView.findViewById(R.id.txt_name);

            convertView.setTag(holder);
        }else{
            holder = (ListCityHolder) convertView.getTag();
        }
        SubLocationModel subLocationModel = list.get(position);
        holder.provinceName.setText(subLocationModel.getSubLocationName());

        return convertView;
    }

    private class ListCityHolder {
        TextView provinceName;
    }
}
