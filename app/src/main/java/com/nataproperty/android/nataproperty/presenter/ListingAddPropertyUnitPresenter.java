package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterListingAddPropertyUnitInteractor;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingAddPropertyUnitModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyUnitModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingAddPropertyUnitActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by Nata on 12/6/2016.
 */

public class ListingAddPropertyUnitPresenter implements PresenterListingAddPropertyUnitInteractor {
    private ListingAddPropertyUnitActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingAddPropertyUnitPresenter(ListingAddPropertyUnitActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListingAddPropertyUnitSvc() {
        Call<ResponeListingAddPropertyUnitModel> call = service.getAPI().getListingPropertyUnit();
        call.enqueue(new Callback<ResponeListingAddPropertyUnitModel>() {
            @Override
            public void onResponse(Call<ResponeListingAddPropertyUnitModel> call, Response<ResponeListingAddPropertyUnitModel> response) {
                view.showListingPropertyUnitResults(response);
            }

            @Override
            public void onFailure(Call<ResponeListingAddPropertyUnitModel> call, Throwable t) {
                view.showListingPropertyUnitFailure(t);
            }
        });

    }

    @Override
    public void getListingPropertyUnitInfoSvc(String listingRef) {
        Call<ResponeListingPropertyUnitModel> call = service.getAPI().getListingPropertyUnitInfo(listingRef);
        call.enqueue(new Callback<ResponeListingPropertyUnitModel>() {
            @Override
            public void onResponse(Call<ResponeListingPropertyUnitModel> call, Response<ResponeListingPropertyUnitModel> response) {
                view.showListingPropertyUnitInfoResults(response);
            }

            @Override
            public void onFailure(Call<ResponeListingPropertyUnitModel> call, Throwable t) {
                view.showListingPropertyUnitInfoFailure(t);
            }
        });
    }


    @Override
    public void saveListingInformasiUnit(Map<String, String> fields) {
        Call<ResponeListingPropertyModel> call = service.getAPI().saveInformasiUnit(fields);
        call.enqueue(new Callback<ResponeListingPropertyModel>() {
            @Override
            public void onResponse(Call<ResponeListingPropertyModel> call, Response<ResponeListingPropertyModel> response) {
                view.showResultInformation(response);

            }

            @Override
            public void onFailure(Call<ResponeListingPropertyModel> call, Throwable t) {
                view.showResultInformationFailure(t);

            }
        });
    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
