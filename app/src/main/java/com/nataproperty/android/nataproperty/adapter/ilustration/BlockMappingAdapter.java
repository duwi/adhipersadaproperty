package com.nataproperty.android.nataproperty.adapter.ilustration;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.helper.MyGridView;
import com.nataproperty.android.nataproperty.model.ilustration.BlockMappingModel;
import com.nataproperty.android.nataproperty.model.ilustration.UnitMappingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class BlockMappingAdapter extends BaseAdapter {
    private Context context;
    private List<BlockMappingModel> list;
    private ListBlockMappingHolder holder;

    MyGridView listMaping;
    private ArrayList<UnitMappingModel> listUnitMapping = new ArrayList<UnitMappingModel>();
    private UnitMappingAdapter adapterUnit;

    ArrayList<String> unutStatusList = new ArrayList<String>();

    String unitMapingRows, unitStatusRows, productRefList,dbMasterRef,projectRef,clusterRef,categoryRef, projectName;
    String isBooking, unitRefList;

    ProgressDialog progressDialog;

    TextView txtTotalBlock;

    public BlockMappingAdapter(Context context, List<BlockMappingModel> list, ProgressDialog progressDialog) {
        this.context = context;
        this.list = list;
        this.progressDialog = progressDialog;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_block_mapping,null);
            holder = new ListBlockMappingHolder();
            holder.blockName = (TextView) convertView.findViewById(R.id.txt_block_name);

            convertView.setTag(holder);
        }else{
            holder = (ListBlockMappingHolder) convertView.getTag();
        }
        BlockMappingModel block = list.get(position);
        holder.blockName.setText(block.getBlockName());

        unitMapingRows = block.getUnitMaping();
        unitStatusRows = block.getUnitStatus();




        listMaping = (MyGridView) convertView.findViewById(R.id.list_unit_mapping_block);

        listUnitMapping.clear();


        //txtTotalBlock = (TextView) convertView.findViewById(R.id.txtTotalBlock);

        String result = generateListUnitMapping(convertView.getContext(),unitMapingRows, listUnitMapping, txtTotalBlock,progressDialog, listMaping,
                unitStatusRows, block);


        adapterUnit = new UnitMappingAdapter(convertView.getContext(), listUnitMapping, progressDialog, listMaping);
                //requestUnitMapping(block.getBlockName());
        listMaping.setAdapter(adapterUnit);
        listMaping.setExpanded(true);

        progressDialog.dismiss();
        /*if(!result.equals(""))
        {
            Log.d("adapterUnit", ""+adapterUnit.getCount());
            progressDialog.dismiss();
        }
*/

        return convertView;
    }


    private static String generateListUnitMapping(final Context context, String response, final ArrayList<UnitMappingModel> listUnitMapping,
                                                  TextView txtTotalBlock, ProgressDialog progressDialog, MyGridView listMaping,
                                                  String unitStatusRows, BlockMappingModel block) {

        String result = "";
        String[] dataUnitMaping = response.split(",");
        String[] dataUnitStatus = unitStatusRows.split(",");

        String productRefList = block.getProductRefList();
        String dbMasterRef = block.getDbMasterRef();
        String projectRef = block.getProjectRef();
        String clusterRef = block.getClusterRef();
        String categoryRef = block.getCategoryRef();
        String projectName = block.getProjectName();
        String isBooking = block.getIsBooking();
        String unitRefList = block.getUnitRef();


        String[] dataProductRef = productRefList.split(",");
        String[] dataUnitRef = unitRefList.split(",");

        for (int i = 0; i < dataUnitMaping.length; i++) {
            try {

                if(!dataUnitMaping[i].equals(" ")){



                    UnitMappingModel unit = new UnitMappingModel();
                    unit.setUnitName(dataUnitMaping[i]);
                    unit.setUnitStatus(dataUnitStatus[i]);
                    unit.setProductRef(dataProductRef[i]);
                    unit.setUnitRef(dataUnitRef[i]);
                    unit.setDbMasterRef(dbMasterRef);
                    unit.setProjectRef(projectRef);
                    unit.setClusterRef(clusterRef);
                    unit.setCategoryRef(categoryRef);
                    unit.setProjectName(projectName);
                    unit.setIsBooking(isBooking);
                    final String unitName;
                    unitName = dataUnitMaping[i];

                    /*listMaping.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Toast.makeText(context, unitName, Toast.LENGTH_SHORT).show();
                        }
                    });*/

                /*unit.setUnitRef(jo.getString("unitRef"));
                unit.setProductRef(jo.getString("productRef"));
                unit.setUnitStatus(jo.getString("unitStatus"));*/

                    listUnitMapping.add(unit);



                    Log.d("UnitName", dataUnitMaping[i]);
                }else{
                    Log.d("UnitNameElse", dataUnitMaping[i]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String x = String.valueOf(i+1);
            Log.d("abc",""+x);
            Log.d("abcZ",""+dataUnitMaping.length);

           /* txtTotalBlock.setText(""+dataUnitMaping.length);
            String Y = txtTotalBlock.getText().toString();
            if(x.equals(String.valueOf(dataUnitMaping.length))){
                Log.d("abcK",""+x);
                Log.d("abcJ",""+Y);

                result = x;
            }*/
        }
        return result;
    }



    private class ListBlockMappingHolder {
        TextView blockName;
    }

}

