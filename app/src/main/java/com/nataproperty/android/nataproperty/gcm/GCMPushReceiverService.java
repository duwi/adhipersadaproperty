package com.nataproperty.android.nataproperty.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GcmListenerService;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.view.event.EventDetailActivity;
import com.nataproperty.android.nataproperty.view.listing.ListingHistoryChatActivity;
import com.nataproperty.android.nataproperty.view.listing.ListingPropertyTabsActivity;
import com.nataproperty.android.nataproperty.view.menuitem.NotificationActivity;
import com.nataproperty.android.nataproperty.view.news.NewsActivity;
import com.nataproperty.android.nataproperty.view.news.NewsDetailActivity;
import com.nataproperty.android.nataproperty.view.project.DownloadActivity;
import com.nataproperty.android.nataproperty.view.project.MyListingActivity;
import com.nataproperty.android.nataproperty.view.project.ProjectActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/31/2016.
 */
public class GCMPushReceiverService extends GcmListenerService {
    public static final String TAG = "GCMPushReceiverService";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String CONTENT_TYPE = "contentType";
    public static final String CONTENT_REF = "contentRef";
    public static final String TITLE = "title";
    public static final String SYNOPSIS = "synopsis";
    public static final String SCHADULE = "schedule";
    public static final String LINK_DETAIL = "linkDetail";

    SharedPreferences sharedPreferences;
    String GCMMessageRef, memberRef, agencyCompanyRef, imageLogo, listingRef, imageCover, listingTitle;
    String gcmMessageTypeRef, titleNotif;
    int status;

    TextView title;
    HtmlTextView subTitle;

    PendingIntent pendingIntent;

    String message, contentType, contentRef, txtTitle, synopsis, schedule, linkDetail, titleEvent, publishDate,
            titleNews, developerName;

    boolean notificationNata, notificationCobroke, state;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        GCMMessageRef = data.getString("message");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        notificationNata = sharedPreferences.getBoolean("isNotificationNata", true);
        notificationCobroke = sharedPreferences.getBoolean("isNotificationCobroke", true);

        if (state) {
            // open main activity ini sudah login
            getGCMMessage();
        } else {

        }

    }

    //getmessage
    private void ggetGCMMessage() {
        String url = WebService.getGCMMessage();
        String urlPostParameter = "&GCMMessageRef=" + GCMMessageRef.toString() +
                "&memberRef=" + memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);

        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result save GCM Token", result + "--" + jo.getInt("status") + "--" + jo.getString("message"));
            status = jo.getInt("status");
            gcmMessageTypeRef = jo.getString("gcmMessageTypeRef");

            contentRef = jo.getString("contentRef");
            schedule = jo.getString("schedule");
            titleEvent = jo.getString("titleEvent");
            linkDetail = jo.getString("linkDetail");

            if (status == 200) {
                if (gcmMessageTypeRef.equals(WebService.notificationDowloadProject)) {
                    String dbMasterRef = jo.getString("dbMasterRef");
                    String projectRef = jo.getString("projectRef");
                    String projectName = jo.getString("projectName");

                    Intent intentDownload = new Intent(this, DownloadActivity.class);
                    intentDownload.putExtra(PROJECT_REF, projectRef);
                    intentDownload.putExtra(DBMASTER_REF, dbMasterRef);
                    intentDownload.putExtra(PROJECT_NAME, projectName);
                    intentDownload.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentDownload, PendingIntent.FLAG_ONE_SHOT);

                } else if (gcmMessageTypeRef.equals(WebService.notificationNews)) {
                    Intent intentNews = new Intent(this, NewsActivity.class);
                    intentNews.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentNews, PendingIntent.FLAG_ONE_SHOT);

                } else if (gcmMessageTypeRef.equals(WebService.notificationNewProject)) {
                    Intent intentProject = new Intent(this, ProjectActivity.class);
                    intentProject.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentProject, PendingIntent.FLAG_ONE_SHOT);

                } else if (gcmMessageTypeRef.equals(WebService.notificationGeneral)) {
                    Intent intentNotification = new Intent(this, NotificationActivity.class);
                    intentNotification.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentNotification, PendingIntent.FLAG_ONE_SHOT);

                } else if (gcmMessageTypeRef.equals(WebService.notificationUpdate)) {
                    Intent intentUpdate = new Intent(Intent.ACTION_VIEW, Uri.parse(WebService.linkAppStore));
                    intentUpdate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentUpdate, PendingIntent.FLAG_ONE_SHOT);

                } else if (gcmMessageTypeRef.equals(WebService.notificationEventDetail)) {
                    Intent intentEventDetail = new Intent(this, EventDetailActivity.class);
                    intentEventDetail.putExtra(CONTENT_REF, contentRef);
                    intentEventDetail.putExtra(SCHADULE, schedule);
                    intentEventDetail.putExtra(TITLE, titleEvent);
                    intentEventDetail.putExtra(LINK_DETAIL, linkDetail);
                    intentEventDetail.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int requestCode = 0;//Your request code
                    pendingIntent = PendingIntent.getActivity(this, requestCode, intentEventDetail, PendingIntent.FLAG_ONE_SHOT);
                }

                //Sound
                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                /*Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/seagulls_chatting");*/
                //Build notification
                //message = jo.getString("message");
                //subTitle.setHtmlFromString(jo.getString("message"), new HtmlTextView.RemoteImageGetter());
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(jo.getString("title"))
                        .setContentText(Html.fromHtml(jo.getString("message")))
                        .setAutoCancel(true)
                        .setSound(sound)
                        .setContentIntent(pendingIntent)
                        .setPriority(Notification.PRIORITY_HIGH);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, noBuilder.build()); //0 = ID of notification

            } else {

                Log.d("getGCMMessage", "error");
            }

        } catch (JSONException e) {

        }
    }

    public void getGCMMessage() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getGCMMessage(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result save GCM Token", response + "--" + jo.getInt("status") + "--" + jo.getString("message"));
                    status = jo.getInt("status");
                    titleNotif = jo.getString("title");
                    message = jo.getString("message");

                    gcmMessageTypeRef = jo.getString("gcmMessageTypeRef");

                    /*Event*/
                    contentRef = jo.getString("contentRef");
                    schedule = jo.getString("schedule");
                    titleEvent = jo.getString("titleEvent");
                    linkDetail = jo.getString("linkDetail");

                    /*Listing*/
                    agencyCompanyRef = jo.getString("agencyCompanyRef");
                    imageCover = jo.getString("imageCover");
                    listingRef = jo.getString("listingRef");
                    listingTitle = jo.getString("listingTitle");

                    /*News*/
                    contentRef = jo.getString("contentRef");
                    publishDate = jo.getString("publishDate");
                    titleNews = jo.getString("titleNews");
                    developerName = jo.getString("developerName");

                    Log.d(TAG, listingRef + "-" + imageCover + "-" + agencyCompanyRef + "-" + listingTitle);

                    if (status == 200) {
                        if (gcmMessageTypeRef.equals(WebService.notificationDowloadProject)) {
                            String dbMasterRef = jo.getString("dbMasterRef");
                            String projectRef = jo.getString("projectRef");
                            String projectName = jo.getString("projectName");

                            Intent intentDownload = new Intent(GCMPushReceiverService.this, DownloadActivity.class);
                            intentDownload.putExtra(PROJECT_REF, projectRef);
                            intentDownload.putExtra(DBMASTER_REF, dbMasterRef);
                            intentDownload.putExtra(PROJECT_NAME, projectName);
                            intentDownload.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentDownload, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationNews)) {
                            Intent intentNews = new Intent(GCMPushReceiverService.this, NewsDetailActivity.class);
                            intentNews.putExtra(CONTENT_REF, contentRef);
                            intentNews.putExtra("publishDate", publishDate);
                            intentNews.putExtra("developerName", developerName);
                            intentNews.putExtra(TITLE, titleNews);
                            intentNews.putExtra(LINK_DETAIL, linkDetail);
                            intentNews.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentNews, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationNewProject)) {
                            Intent intentProject = new Intent(GCMPushReceiverService.this, ProjectActivity.class);
                            intentProject.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentProject, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationGeneral)) {
                            Intent intentNotification = new Intent(GCMPushReceiverService.this, NotificationActivity.class);
                            intentNotification.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentNotification, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationUpdate)) {
                            Intent intentUpdate = new Intent(Intent.ACTION_VIEW, Uri.parse(WebService.linkAppStore));
                            intentUpdate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentUpdate, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationEventDetail)) {
                            Intent intentEventDetail = new Intent(GCMPushReceiverService.this, EventDetailActivity.class);
                            intentEventDetail.putExtra(CONTENT_REF, contentRef);
                            intentEventDetail.putExtra(SCHADULE, schedule);
                            intentEventDetail.putExtra(TITLE, titleEvent);
                            intentEventDetail.putExtra(LINK_DETAIL, linkDetail);
                            intentEventDetail.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentEventDetail, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationNata){
                                sendNotification(titleNotif, message);
                            }

                        } else if (gcmMessageTypeRef.equals(WebService.notificationListingProperty)) {
                            Intent intentEventDetail = new Intent(GCMPushReceiverService.this, ListingPropertyTabsActivity.class);
                            intentEventDetail.putExtra("agencyCompanyRef", agencyCompanyRef);
                            intentEventDetail.putExtra("imageCover", imageCover);
                            intentEventDetail.putExtra("listingRef", listingRef);
                            intentEventDetail.putExtra("listingTitle", listingTitle);
                            intentEventDetail.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentEventDetail, PendingIntent.FLAG_ONE_SHOT);

                            if (notificationCobroke){
                                sendNotification(titleNotif, message);
                            }


                        } else if (gcmMessageTypeRef.equals(WebService.notificationChat)) {
                            Intent intentHistoryChat = new Intent(GCMPushReceiverService.this, ListingHistoryChatActivity.class);
                            intentHistoryChat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentHistoryChat, PendingIntent.FLAG_ONE_SHOT);
                            Intent pushNotification = new Intent(ConfigGCM.PUSH_NOTIFICATION);
//                            pushNotification.putExtra("type", ConfigGCM.PUSH_TYPE_CHATROOM);
                            pushNotification.putExtra("message", message);
//                            pushNotification.putExtra("chat_room_id", chatRoomId);
                            LocalBroadcastManager.getInstance(GCMPushReceiverService.this).sendBroadcast(pushNotification);

                            sendNotification(titleNotif, message);

                        } else if (gcmMessageTypeRef.equals(WebService.notificationApprovalProject)) {
                            Intent intentHistoryChat = new Intent(GCMPushReceiverService.this, MyListingActivity.class);
                            intentHistoryChat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            int requestCode = 0;//Your request code
                            pendingIntent = PendingIntent.getActivity(GCMPushReceiverService.this,
                                    requestCode, intentHistoryChat, PendingIntent.FLAG_ONE_SHOT);

                            sendNotification(titleNotif, message);

                        }else {

                        }


                    } else {

                        Log.d("getGCMMessage", "error");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(GCMPushReceiverService.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(GCMPushReceiverService.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("GCMMessageRef", GCMMessageRef);
                params.put("memberRef", memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "GCMMessage");

    }

    private void sendNotification(String title, String message) {
        //Log.d("sendnotif",message);
//Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        /*Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/seagulls_chatting");*/
        //Build notification
        //message = jo.getString("message");
        //subTitle.setHtmlFromString(jo.getString("message"), new HtmlTextView.RemoteImageGetter());
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(GCMPushReceiverService.this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setContentText(Html.fromHtml(message))
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
    }
}
