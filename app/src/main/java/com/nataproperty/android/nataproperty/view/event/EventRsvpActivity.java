package com.nataproperty.android.nataproperty.view.event;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class EventRsvpActivity extends AppCompatActivity {
    public static final String TAG = "EventRsvpActivity";

    public static final String CONTENT_REF = "contentRef";
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String GUEST_NAME = "guest_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String GUEST_PHONE = "guest_phone";
    public static final String GUEST_MOBILE1 = "guest_hp1";
    public static final String GUEST_MOBILE2 = "guest_hp2";
    public static final String GUEST_EMAIL = "guest_email";
    public static final String STATUS = "status";
    public static final String LINK_DETAIL = "linkDetail";

    public static final String PREF_NAME = "pref" ;

    ProgressDialog progressDialog;
    String message;
    int getStatus;

    EditText edtGuestName, edtCompanyName, edtPhone, edtMobile1, edtMobile2, edtEmail;
    Button btnSubmit;
    String contentRef,eventScheduleRef,status,linkDetail,memberRef,email,name;

    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_rsvp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_rsvp));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        contentRef = intent.getStringExtra(CONTENT_REF);
        eventScheduleRef = intent.getStringExtra(EVENT_SCHEDULE_REF);
        status = intent.getStringExtra(STATUS);
        //linkDetail = intent.getStringExtra(LINK_DETAIL);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );
        email = sharedPreferences.getString("isEmail",null );
        name = sharedPreferences.getString("isName", null);

        Log.d(TAG,contentRef+" "+eventScheduleRef+" "+status);

        edtGuestName = (EditText) findViewById(R.id.edt_guest_name);
        edtCompanyName = (EditText) findViewById(R.id.edt_company_name);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtMobile1 = (EditText) findViewById(R.id.edt_mobile1);
        edtMobile2 = (EditText) findViewById(R.id.edt_mobile2);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtGuestName.setText(name);
        edtEmail.setText(email);

        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnSubmit.setTypeface(font);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekGuest = edtGuestName.getText().toString().trim();
                String cekPhone = edtPhone.getText().toString().trim();
                String cekMobile1 = edtMobile1.getText().toString().trim();
                String cekEmail = edtEmail.getText().toString().trim();

                if (cekGuest.isEmpty() || cekMobile1.isEmpty() || cekEmail.isEmpty()){

                    if (cekGuest.isEmpty()){
                        edtGuestName.setError("Guest name is required");
                    }else {
                        edtGuestName.setError(null);
                    }
                   /* if (cekPhone.isEmpty()){
                        edtPhone.setError("Phone is required");
                    }else {
                        edtPhone.setError(null);
                    }*/
                    if (cekMobile1.isEmpty()){
                        edtMobile1.setError("Mobile 1 is required");
                    }else {
                        edtMobile1.setError(null);
                    }
                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(cekEmail).matches()){
                        edtEmail.setError("Email is required");
                    }else {
                        edtEmail.setError(null);
                    }

                }else {

                    registerRsvp();
                }

            }
        });

    }

    private void registerRsvp() {
        String url = WebService.registerRsvp();
        String urlPostParameter =  "&memberRef=" + memberRef.toString() +
                "&contentRef=" + contentRef.toString() +
                "&eventScheduleRef=" + eventScheduleRef.toString() +
                "&guest_name=" + edtGuestName.getText().toString() +
                "&company_name=" + edtCompanyName.getText().toString() +
                "&guest_phone=" + edtPhone.getText().toString() +
                "&guest_hp1=" + edtMobile1.getText().toString() +
                "&guest_hp2=" + edtMobile2.getText().toString() +
                "&guest_email=" + edtEmail.getText().toString()+
                "&status=" + status.toString();

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(EventRsvpActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            Log.d(TAG, result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                getStatus = jsonObject.getInt("status");
                message = jsonObject.getString("message");
                linkDetail = jsonObject.getString("linkRsvp");

                if (getStatus==200){
                    Intent intent = new Intent(EventRsvpActivity.this,EventRsvpFinishActivity.class);
                    intent.putExtra(CONTENT_REF,contentRef);
                    intent.putExtra(LINK_DETAIL,linkDetail);
                    //EventDetailActivity.getInstance().finish();
                    startActivity(intent);
                    finish();
                }else if (getStatus==201){
                    Intent intent = new Intent(EventRsvpActivity.this,EventRsvpFinishActivity.class);
                    intent.putExtra(CONTENT_REF,contentRef);
                    intent.putExtra(LINK_DETAIL,linkDetail);
                    //EventDetailActivity.getInstance().finish();
                    startActivity(intent);
                    finish();
                }else if (getStatus==202) {
                   /* edtEmail.setError(message);
                    btnSubmit.setEnabled(true);*/
                }else {

                }


            } catch (JSONException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            //Toast.makeText(EventRsvpActivity.this, message, Toast.LENGTH_SHORT).show();
            if (getStatus==202) {
                edtEmail.setError(message);
                btnSubmit.setEnabled(true);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventRsvpActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
