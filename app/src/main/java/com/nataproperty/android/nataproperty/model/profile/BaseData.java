package com.nataproperty.android.nataproperty.model.profile;

import com.nataproperty.android.nataproperty.model.listing.ListingMemberInfoModel;

import java.util.List;

/**
 * Created by nata on 11/18/2016.
 */

public class BaseData {
    private String status;
    private String message;
    private String count;
    private String totalPage;
    private List<ListingMemberInfoModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<ListingMemberInfoModel> getData() {
        return data;
    }

    public void setData(List<ListingMemberInfoModel> data) {
        this.data = data;
    }
}
