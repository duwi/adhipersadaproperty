package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterTableMainInteractor;
import com.nataproperty.android.nataproperty.model.ilustration.DiagramColor;
import com.nataproperty.android.nataproperty.model.listing.LIstBlockDiagramModel;
import com.nataproperty.android.nataproperty.model.listing.ListUnitMappingModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.mapping.TableMainLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class TableMainPresenter implements PresenterTableMainInteractor {
    private TableMainLayout view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public TableMainPresenter(TableMainLayout view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getBlockMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef) {
        Call<List<LIstBlockDiagramModel>> call = service.getAPI().getListBlockDiagram(dbMasterRef,projectRef,categoryRef,clusterRef);
        call.enqueue(new Callback<List<LIstBlockDiagramModel>>() {
            @Override
            public void onResponse(Call<List<LIstBlockDiagramModel>> call, Response<List<LIstBlockDiagramModel>> response) {
                view.showBlockMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<LIstBlockDiagramModel>> call, Throwable t) {
                view.showBlockMappingFailure(t);

            }


        });
    }

    @Override
    public void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef) {
        Call<List<ListUnitMappingModel>> call = service.getAPI().getListUnitDiagram(dbMasterRef,projectRef,categoryRef,clusterRef);
        call.enqueue(new Callback<List<ListUnitMappingModel>>() {
            @Override
            public void onResponse(Call<List<ListUnitMappingModel>> call, Response<List<ListUnitMappingModel>> response) {
                view.showUnitMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<ListUnitMappingModel>> call, Throwable t) {
                view.showUnitMappingFailure(t);

            }


        });
    }

    @Override
    public void getDiagramColor(String memberRef) {
        Call<DiagramColor> call = service.getAPI().getDiagramColour(memberRef);
        call.enqueue(new Callback<DiagramColor>() {
            @Override
            public void onResponse(Call<DiagramColor> call, Response<DiagramColor> response) {
                view.showDiagramColorResults(response);
            }

            @Override
            public void onFailure(Call<DiagramColor> call, Throwable t) {
                view.showDiagramColorFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
