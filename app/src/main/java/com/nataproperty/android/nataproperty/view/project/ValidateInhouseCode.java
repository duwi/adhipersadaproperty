package com.nataproperty.android.nataproperty.view.project;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by User on 5/16/2016.
 */
public class ValidateInhouseCode extends AppCompatActivity {
    EditText edtInhouseCode;
    Button btnValidate;

    String memberRef, dbMasterRef, projectRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_inhouse);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_inhouse_code));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");

        Log.d("validate inhouse", " " + memberRef + "-" + dbMasterRef + "-" + projectRef);

        edtInhouseCode = (EditText) findViewById(R.id.txt_inhouse_code);
        btnValidate = (Button) findViewById(R.id.btn_validate);

        btnValidate.setTypeface(font);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subscribeProjectInhouse();
            }
        });
    }

    private void subscribeProjectInhouse() {
        String url = WebService.subscribeProjectInhouse();
        String urlPostParameter = "&memberRef=" + memberRef.toString() +
                "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&inhouseCode=" + edtInhouseCode.getText().toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result detail project", result);
            int status = jo.getInt("status");
            String message = jo.getString("message");

            if (status == 200) {
                Toast.makeText(ValidateInhouseCode.this, message, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ValidateInhouseCode.this, ProjectActivity.class);
                startActivity(intent);
                finish();
            } else if (status == 201) {
                Toast.makeText(ValidateInhouseCode.this, message, Toast.LENGTH_LONG).show();
                btnValidate.setEnabled(true);
            } else {
                String error = jo.getString("message");
                Toast.makeText(ValidateInhouseCode.this, error, Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
