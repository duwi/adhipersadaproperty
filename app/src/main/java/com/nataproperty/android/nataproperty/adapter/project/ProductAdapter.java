package com.nataproperty.android.nataproperty.adapter.project;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.project.ProductModel;

import java.util.List;

/**
 * Created by User on 4/19/2016.
 */
public class ProductAdapter extends BaseAdapter {

    private Context context;
    private List<ProductModel> list;
    private ListProductHolder holder;

    private Display display;

    public ProductAdapter(Context context, List<ProductModel>list,Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_product,null);
            holder = new ListProductHolder();
            holder.productName = (TextView) convertView.findViewById(R.id.txt_title_product);
            holder.productImg = (ImageView) convertView.findViewById(R.id.image_product);

            holder.productBad = (TextView) convertView.findViewById(R.id.txt_bad);
            holder.productBath = (TextView) convertView.findViewById(R.id.txt_bath);

            holder.productAditional = (TextView) convertView.findViewById(R.id.txt_aditional);
            holder.productBathAditional = (TextView) convertView.findViewById(R.id.txt_bath_additional);

            convertView.setTag(holder);
        }else{
            holder = (ListProductHolder) convertView.getTag();
        }

        ProductModel product = list.get(position);
        holder.productName.setText(product.getTitleProduct().toUpperCase());
        holder.productBad.setText(product.getNumOfBedrooms());
        holder.productBath.setText(product.getNumOfBathrooms());

        if (product.getNumOfAdditionalrooms().equals("0")){
            String productAditionalNew = product.getNumOfAdditionalrooms().replace("0","");
            holder.productAditional.setText(productAditionalNew);
        }else {
            holder.productAditional.setText("+"+product.getNumOfAdditionalrooms());
        }

        if (product.getNumOfAdditionalBathrooms().equals("0")){
            String bathAditionalNew = product.getNumOfAdditionalBathrooms().replace("0","");
            holder.productBathAditional.setText(bathAditionalNew);
        }else {
            holder.productBathAditional.setText("+"+product.getNumOfAdditionalBathrooms());
        }

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.233333333333333;

        ViewGroup.LayoutParams params = holder.productImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.productImg.setLayoutParams(params);
        holder.productImg.requestLayout();

        Glide.with(context).load(WebService.getProductImage()+product.getDbMasterRef()+"&pr="+product.getProjectRef()+
                "&pd="+product.getProductRef())
                .into(holder.productImg);
        //Log.d("Holder Product",product.getDbMasterRef()+" "+product.getProjectRef()+" "+product.getProductRef());
        return convertView;
    }

    private class ListProductHolder {
        TextView productName,productBath,productBad,productBathAditional,productAditional;
        ImageView productImg;
    }
}
