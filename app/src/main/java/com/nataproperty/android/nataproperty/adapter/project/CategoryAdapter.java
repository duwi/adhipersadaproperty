package com.nataproperty.android.nataproperty.adapter.project;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.project.CategoryModel;

import java.util.List;

/**
 * Created by User on 4/19/2016.
 */
public class CategoryAdapter extends BaseAdapter {

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private List<CategoryModel> list;
    private ListCategorytHolder holder;

    private Display display;

    public CategoryAdapter(Context context, List<CategoryModel> list,Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_category,null);
            holder = new ListCategorytHolder();
            holder.categoryName = (TextView) convertView.findViewById(R.id.txt_title_category);

            holder.categoryImg = (ImageView) convertView.findViewById(R.id.image_category);

            convertView.setTag(holder);
        }else{
            holder = (ListCategorytHolder) convertView.getTag();
        }

        CategoryModel category = list.get(position);
        holder.categoryName.setText(category.getCategoryDescription());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.233333333333333;

        ViewGroup.LayoutParams params = holder.categoryImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.categoryImg.setLayoutParams(params);
        holder.categoryImg.requestLayout();

        Glide.with(context)
                .load(WebService.getCategoryImage()+category.getDbMasterRef()+
                        "&pr="+category.getProjectRef()+"&ct="+category.getCategoryRef()).into(holder.categoryImg);

        return convertView;
    }

    private class ListCategorytHolder {
        TextView categoryName;
        ImageView categoryImg;
    }
}
