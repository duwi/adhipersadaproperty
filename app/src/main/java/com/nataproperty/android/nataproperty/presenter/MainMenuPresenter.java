package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterMainInteractor;
import com.nataproperty.android.nataproperty.model.StatusTokenGCMModel;
import com.nataproperty.android.nataproperty.model.listing.ListCountChatModel;
import com.nataproperty.android.nataproperty.model.listing.ListTicketModel;
import com.nataproperty.android.nataproperty.model.listing.ListingAgencyInfoModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MainMenuPresenter implements PresenterMainInteractor {
    private MainMenuActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MainMenuPresenter(MainMenuActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void sendGCMToken(String GCMToken,String memberRef) {
        Call<StatusTokenGCMModel> call = service.getAPI().postGCMToken(GCMToken,memberRef);
        call.enqueue(new Callback<StatusTokenGCMModel>() {
            @Override
            public void onResponse(Call<StatusTokenGCMModel> call, Response<StatusTokenGCMModel> responseToken) {
                view.showTicketResults(responseToken);
            }

            @Override
            public void onFailure(Call<StatusTokenGCMModel> call, Throwable t) {
                view.showTicketFailure(t);

            }


        });

    }

    @Override
    public void requestTicket(String memberRef) {
        Call<List<ListTicketModel>> call = service.getAPI().getTicket(memberRef);
        call.enqueue(new Callback<List<ListTicketModel>>() {
            @Override
            public void onResponse(Call<List<ListTicketModel>> call, Response<List<ListTicketModel>> response) {
                view.showRetroResults(response);
            }

            @Override
            public void onFailure(Call<List<ListTicketModel>> call, Throwable t) {
                view.showRetroFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }

    @Override
    public void requestAgencyInfo(String memberRef) {
        Call<ListingAgencyInfoModel> call = service.getAPI().getAgencyInfo(memberRef);
        call.enqueue(new Callback<ListingAgencyInfoModel>() {
            @Override
            public void onResponse(Call<ListingAgencyInfoModel> call, Response<ListingAgencyInfoModel> response) {
                view.showAgencyResults(response);
            }

            @Override
            public void onFailure(Call<ListingAgencyInfoModel> call, Throwable t) {
                view.showAgencyFailure(t);

            }


        });
    }

    @Override
    public void postRegisterAgency(String memberRef, String edtCompanyCode) {
        Call<ListingAgencyInfoModel> call = service.getAPI().postRegisterAgency(memberRef,edtCompanyCode);
        call.enqueue(new Callback<ListingAgencyInfoModel>() {
            @Override
            public void onResponse(Call<ListingAgencyInfoModel> call, Response<ListingAgencyInfoModel> response) {
                view.showRegisterResults(response);
            }

            @Override
            public void onFailure(Call<ListingAgencyInfoModel> call, Throwable t) {
                view.showRegisterFailure(t);

            }


        });
    }

    @Override
    public void getCountChat(String memberRef) {
        Call<ListCountChatModel> call = service.getAPI().getCountChat(memberRef);
        call.enqueue(new Callback<ListCountChatModel>() {
            @Override
            public void onResponse(Call<ListCountChatModel> call, Response<ListCountChatModel> response) {
                view.showListCountResults(response);
            }

            @Override
            public void onFailure(Call<ListCountChatModel> call, Throwable t) {
                view.showListCountFailure(t);

            }


        });
    }
}
