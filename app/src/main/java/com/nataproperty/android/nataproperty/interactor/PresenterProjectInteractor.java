package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterProjectInteractor {
    void getLocation(String memberRef);
    void getListProject(String memberRef, String locationRef);
    void rxUnSubscribe();

}
