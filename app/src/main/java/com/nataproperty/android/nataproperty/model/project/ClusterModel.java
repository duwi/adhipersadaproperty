package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 5/8/2016.
 */
@Entity
public class ClusterModel {
    @Id
    long clusterRef;
     String dbMasterRef;
    String projectRef, categoryRef , clusterDescription,isShowAvailableUnit,image;


    @Generated(hash = 1972548964)
    public ClusterModel(long clusterRef, String dbMasterRef, String projectRef,
            String categoryRef, String clusterDescription,
            String isShowAvailableUnit, String image) {
        this.clusterRef = clusterRef;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.categoryRef = categoryRef;
        this.clusterDescription = clusterDescription;
        this.isShowAvailableUnit = isShowAvailableUnit;
        this.image = image;
    }

    @Generated(hash = 1629039536)
    public ClusterModel() {
    }
    

    public long getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(long clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }


    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }


    public String getClusterDescription() {
        return clusterDescription;
    }

    public void setClusterDescription(String clusterDescription) {
        this.clusterDescription = clusterDescription;
    }
}
