package com.nataproperty.android.nataproperty.view.menuitem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.event.EventDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationDetailActivity extends AppCompatActivity {
    public static final String TAG = "NotificationDetail";

    public static final String PREF_NAME = "pref" ;

    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String GCM_MESSAGE_REF = "gcmMessageRef";
    public static final String GCM_MESSAGE_TYPE_REF = "gcmMessageTypeRef";

    public static final String CONTENT_REF = "contentRef";
    public static final String SCHADULE = "schedule";

    SharedPreferences sharedPreferences;

    TextView txtTitle, txtDate;
    WebView webView;
    Button btnReadMore;

    String linkDetail, titleGcm, date, gcmMessageRef, gcmMessageTypeRef,memberRef,contentRef,schedule,titleEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_notification));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        Intent intent = getIntent();
        linkDetail = intent.getStringExtra(LINK_DETAIL);
        titleGcm = intent.getStringExtra(TITLE);
        date = intent.getStringExtra(DATE);
        gcmMessageRef = intent.getStringExtra(GCM_MESSAGE_REF);
        gcmMessageTypeRef = intent.getStringExtra(GCM_MESSAGE_TYPE_REF);

        Log.d(TAG, "" + gcmMessageTypeRef);

        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtDate = (TextView) findViewById(R.id.txt_date);
        webView = (WebView) findViewById(R.id.web_content);
        btnReadMore = (Button) findViewById(R.id.btn_read_more);
        btnReadMore.setTypeface(font);

        txtTitle.setText(titleGcm);
        txtDate.setText(date);
        webView.loadUrl(linkDetail);

        if (gcmMessageTypeRef.equals(WebService.notificationEventDetail)) {
            btnReadMore.setVisibility(View.VISIBLE);

            getGCMMessage();
        }

        btnReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEventDetail = new Intent(NotificationDetailActivity.this, EventDetailActivity.class);
                intentEventDetail.putExtra(CONTENT_REF, contentRef);
                intentEventDetail.putExtra(SCHADULE, schedule);
                intentEventDetail.putExtra(TITLE, titleEvent);
                intentEventDetail.putExtra(LINK_DETAIL, linkDetail);
                startActivity(intentEventDetail);
            }
        });

    }

    //getmessage
    private void getGCMMessage() {
        String url = WebService.getGCMMessage();
        String urlPostParameter = "&GCMMessageRef=" + gcmMessageRef.toString() +
                "&memberRef=" + memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        /*final String title, messageType, message;
        final int status;*/

        try {
            JSONObject jo = new JSONObject(result);
            contentRef = jo.getString("contentRef");
            schedule = jo.getString("schedule");
            titleEvent = jo.getString("titleEvent");
            linkDetail = jo.getString("linkDetail");

        } catch (JSONException e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NotificationDetailActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
