package com.nataproperty.android.nataproperty.config;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nataproperty.android.nataproperty.BuildConfig;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.ilustration.DaoMaster;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.network.NataService;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Administrator on 3/31/2016.
 */
public class BaseApplication extends Application {
    private Tracker mTracker;
    public DaoSession daoSession;
    private static final int TIMEOUT_MS = 10000; // 15second
    private ServiceRetrofit networkService;

    private RequestQueue requestQueue;
    private static BaseApplication instance;
    public static final String TAG = BaseApplication.class.getSimpleName();
    private ProgressDialog dialog;
    private NataService nataService;
    String versionName = "";
    int versionCode = -1;
    SQLiteDatabase db = null;
    DaoMaster.DevOpenHelper helper = null;
    DaoMaster daoMaster;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        networkService = new ServiceRetrofit();
        if (BuildConfig.DEBUG) {
            //Fabric.with(this, new Crashlytics());
        } else {
            Fabric.with(this, new Crashlytics());
        }
        instance = this;
        nataService = new NataService();
        helper = new DaoMaster.DevOpenHelper(this, "SmartSellingToolsDBNew3", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();



//        T.devOpenHelper= new DaoMaster.DevOpenHelper(context, "mydatabase", null);
//        T.sqLiteDatabase= T.devOpenHelper.getWritableDatabase();
//        T.daoMaster= new DaoMaster(T.sqLiteDatabase);
//        T.daoSession= T.daoMaster.newSession();
//        T.dao_myEntity= T.daoSession.getMyEntityDao();

//        try {
//            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            versionName = packageInfo.versionName;
//            versionCode = packageInfo.versionCode;
//            String test = "";
//
//
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

//        if(versionCode < 34){
//            boolean ifExists = true;
//            ProjectModelDao.dropTable(db,true);
//
//
//        }


//        daoMaster.dropAllTables(db, true);

    }
    public ServiceRetrofit getNetworkService(){
        return networkService;
    }

    public static synchronized BaseApplication getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        //Utils.TRACE("BaseApplication", "addToRequestQueue : " + request.getUrl());
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        // set retry policy
        request.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(request);
    }

    public void cancelPendingRequest(Object tag) {
        if (requestQueue != null)
            requestQueue.cancelAll(tag);
    }


//    public void startLoader(Context context){
//        dialog = new ProgressDialog(context);
//        dialog.setMessage("Loading..");
//        dialog.setCancelable(false);
//        dialog.setInverseBackgroundForced(false);
//        dialog.show();
//    }

    public DaoSession getDaoSession() {
            return daoSession;
        }

//    public void stopLoader(){
//        if(dialog !=null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
//    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }


}
