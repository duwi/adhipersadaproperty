package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by herlambang-nata on 10/31/2016.
 */
@Entity
public class ProductDetailsModel {
    @Id
    long dbMasterRef;
    String productDescription;
    String urlVideo;
    String numOfBedrooms;
    String numOfBathrooms;
    String location;
    String categoryName;
    String specialEnquiries;

    @Generated(hash = 1160870980)
    public ProductDetailsModel(long dbMasterRef, String productDescription,
            String urlVideo, String numOfBedrooms, String numOfBathrooms,
            String location, String categoryName, String specialEnquiries) {
        this.dbMasterRef = dbMasterRef;
        this.productDescription = productDescription;
        this.urlVideo = urlVideo;
        this.numOfBedrooms = numOfBedrooms;
        this.numOfBathrooms = numOfBathrooms;
        this.location = location;
        this.categoryName = categoryName;
        this.specialEnquiries = specialEnquiries;
    }

    @Generated(hash = 1853105835)
    public ProductDetailsModel() {
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getNumOfBedrooms() {
        return numOfBedrooms;
    }

    public void setNumOfBedrooms(String numOfBedrooms) {
        this.numOfBedrooms = numOfBedrooms;
    }

    public String getNumOfBathrooms() {
        return numOfBathrooms;
    }

    public void setNumOfBathrooms(String numOfBathrooms) {
        this.numOfBathrooms = numOfBathrooms;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSpecialEnquiries() {
        return specialEnquiries;
    }

    public void setSpecialEnquiries(String specialEnquiries) {
        this.specialEnquiries = specialEnquiries;
    }
}
