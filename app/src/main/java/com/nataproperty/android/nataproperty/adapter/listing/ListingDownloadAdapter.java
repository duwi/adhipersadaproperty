package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingDownloadModel;

import java.io.File;
import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class ListingDownloadAdapter extends BaseAdapter {
    private Context context;
    private List<ListingDownloadModel> list;
    private ListDownloadHolder holder;

    public ListingDownloadAdapter(Context context, List<ListingDownloadModel> list) {
        this.context = context;
        this.list = list;

    }

    private String fileRef, filename, extension;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_download,null);
            holder = new ListDownloadHolder();
            holder.fileName = (TextView) convertView.findViewById(R.id.txt_fileName);
            //holder.extension = (TextView) convertView.findViewById(R.id.txt_extension);
            holder.fileJPG = (ImageView) convertView.findViewById(R.id.ic_file_jpg);
            holder.filePNG = (ImageView) convertView.findViewById(R.id.ic_file_png);
            holder.filePDF = (ImageView) convertView.findViewById(R.id.ic_file_pdf);
            holder.shared = (ImageView) convertView.findViewById(R.id.ic_action_shared);

            convertView.setTag(holder);
        }else{
            holder = (ListDownloadHolder) convertView.getTag();
        }

        ListingDownloadModel download = list.get(position);
        holder.fileName.setText(download.getDocFN());

        if (download.getExtension().equals("jpg")){
            holder.fileJPG.setVisibility(View.VISIBLE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.GONE);
        } else if (download.getExtension().equals("png")){
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.VISIBLE);
            holder.filePDF.setVisibility(View.GONE);
        } else if (download.getExtension().equals("pdf")){
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.VISIBLE);
        } else {
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.INVISIBLE);
        }

        final ListingDownloadModel downloadModel = list.get(position);

        final String url = downloadModel.getLinkDownload();
        Log.d("urldAdapter", url);

        holder.shared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileRef = downloadModel.getDocRef();
                filename = downloadModel.getDocFN();
                extension = downloadModel.getExtension();

                String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                        + filename.toString();
                File f = new File(path);
                if (f.exists()) {
                    if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png")) {
                        try {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/*");
                            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    "/nataproperty/" + downloadModel.getDocFN().toString();
                            File imageFileToShare = new File(imagePath);
                            Uri uri = Uri.fromFile(imageFileToShare);
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            context.startActivity(Intent.createChooser(share, "Share Image!"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
                        }

                    } else if (extension.equals("pdf")) {
                        try {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("document/*");
                            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    "/nataproperty/" + downloadModel.getDocFN().toString();
                            File imageFileToShare = new File(imagePath);
                            Uri uri = Uri.fromFile(imageFileToShare);
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            context.startActivity(Intent.createChooser(share, "Share Document!"));

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "File harus didownload terlebih dahulu", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return convertView;
    }

    private class ListDownloadHolder {
        TextView fileName,extension;
        ImageView fileJPG,filePNG,filePDF,shared;
    }
}
