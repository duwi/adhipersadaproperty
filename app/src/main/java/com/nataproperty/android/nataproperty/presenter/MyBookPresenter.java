package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterMyBookInteractor;
import com.nataproperty.android.nataproperty.model.mybooking.MyBookingModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.mybooking.MyBookingActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookPresenter implements PresenterMyBookInteractor {
    private MyBookingActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyBookPresenter(MyBookingActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBook(String memberRef) {
        Call<List<MyBookingModel>> call = service.getAPI().getListBook(memberRef);
        call.enqueue(new Callback<List<MyBookingModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingModel>> call, Response<List<MyBookingModel>> response) {
                view.showListBookResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingModel>> call, Throwable t) {
                view.showListBookFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
