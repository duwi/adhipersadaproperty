package com.nataproperty.android.nataproperty.view.mybooking;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by User on 8/15/2016.
 */
public class MyBookingInfoActivity extends AppCompatActivity {

    public static final String PREF_NAME = "pref";
    public static final String TAG = "MyBookingInfoActivity";

    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    SharedPreferences sharedPreferences;

    TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    TextView txtFullname, txtEmail, txtMobile, txtPhone, txtAddress, txtKtpId, txtQty;
    TextView txtPaymentDate, txtPaymentType, txtPaymentAmt, txtAccountBank, txtAccountName, txtAccountNomor;
    TextView txtProjectName, txtCostumerName, txtCostumerMobile, txtCostumerPhone, txtCostumerAddress, txtCostumerEmail, txtMemberEmail;
    String paymentDate, paymentType, paymentAmt, accountBank, accountName, accountNomor;

    ImageView imgKtp, imgNpwp;

    String bookingRef;
    String memberCostumerRef, costumerName, costumerMobile, costumerPhone, costumerEmail, costumerAddress, linkDownload;
    String ktpid,ktpRef,npwpRef;
    String projectBookingRef,dbMasterRef,projectRef;
    int waitingVerification;

    LinearLayout linearLayoutWaiting;
    Button btnDownload,btnIlustrasi;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;

    private long enqueue;
    private DownloadManager dm;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        Intent intent = getIntent();
        waitingVerification = intent.getIntExtra("waitingVerification", 0);
        linkDownload = intent.getStringExtra("linkDownload");
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);

        Log.d("linkDownload",""+linkDownload);

        txtPaymentDate = (TextView) findViewById(R.id.txt_payment_date);
        txtPaymentType = (TextView) findViewById(R.id.txt_payment_type);
        txtPaymentAmt = (TextView) findViewById(R.id.txt_payment_amt);
        txtAccountBank = (TextView) findViewById(R.id.txt_account_bank);
        txtAccountName = (TextView) findViewById(R.id.txt_account_name);
        txtAccountNomor = (TextView) findViewById(R.id.txt_account_nomor);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * informasion customer
         */
        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        linearLayoutWaiting = (LinearLayout) findViewById(R.id.linear_waiting_verification);
        btnIlustrasi = (Button) findViewById(R.id.btn_ilustration);
        btnDownload = (Button) findViewById(R.id.btn_download);

        if (waitingVerification == 1) {
            linearLayoutWaiting.setVisibility(View.VISIBLE);
            btnDownload.setVisibility(View.GONE);
        } else {
            linearLayoutWaiting.setVisibility(View.GONE);
            btnDownload.setVisibility(View.VISIBLE);
        }

        requestMyBookingInfo();
        requestMemberInfo();
        requestMyBookingDetail();

        btnDownload.setTypeface(font);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                        + paymentDate.toString() + ".pdf";
                File f = new File(path);
                /*if (f.exists()) {
                    try {
                        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                                "/nataproperty/" + paymentDate.toString() + ".pdf");
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(MyBookingInfoActivity.this, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    startDownload(linkDownload);
                }*/
                dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(linkDownload));
                enqueue = dm.enqueue(request);
                //startDownload(linkDownload);

            }
        });

        btnIlustrasi.setTypeface(font);
        btnIlustrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MyBookingInfoActivity.this,MyBookingDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                startActivity(intent);
            }
        });

        //Download manager
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            Intent i = new Intent();
                            i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
                            startActivity(i);
                            Toast.makeText(MyBookingInfoActivity.this,"Download successed",Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(MyBookingInfoActivity.this,"Download failed",Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void requestMyBookingInfo() {
        String url = WebService.getBookingPaymentInfo();
        String urlPostParameter = "&bookingRef=" + bookingRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d(TAG, "info :" + result);

            paymentType = (jo.getString("paymentTypeName"));
            paymentDate = (jo.getString("paymentDate"));
            paymentAmt = (jo.getString("paymentAmt"));
            accountBank = (jo.getString("bankName"));
            accountName = (jo.getString("bankAccName"));
            accountNomor = (jo.getString("bankAccNo"));

            txtPaymentType.setText(paymentType);
            txtPaymentDate.setText(paymentDate);
            txtPaymentAmt.setText(paymentAmt);
            txtAccountBank.setText(accountBank);
            txtAccountName.setText(accountName);
            txtAccountNomor.setText(accountNomor);


        } catch (JSONException e) {

        }
    }

    private void requestMemberInfo() {
        String url = WebService.getMemberInfo();
        String urlPostParameter = "&memberRef=" + memberCostumerRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result nup review", result);
            int status = jo.getInt("status");
            String message = jo.getString("message");

            if (status == 200) {
                costumerName = jo.getJSONObject("data").getString("name");
                costumerMobile = jo.getJSONObject("data").getString("hP1");
                costumerPhone = jo.getJSONObject("data").getString("phone1");
                costumerEmail = jo.getJSONObject("data").getString("email1");
                costumerAddress = jo.getJSONObject("data").getString("idAddr");
                ktpid = jo.getJSONObject("data").getString("ktpid");
                ktpRef = jo.getJSONObject("data").getString("ktpRef");
                npwpRef = jo.getJSONObject("data").getString("npwpRef");

                txtFullname.setText(costumerName);
                txtMobile.setText(costumerMobile);
                txtPhone.setText(costumerPhone);
                txtEmail.setText(costumerEmail);
                txtAddress.setText(costumerAddress);
                txtKtpId.setText(ktpid);

                Glide.with(MyBookingInfoActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(imgKtp);

                Glide.with(MyBookingInfoActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(imgNpwp);


            } else {
                Log.d("error", " " + message);

            }

        } catch (JSONException e) {

        }

    }

    private void requestMyBookingDetail() {
        String url = WebService.getDetailBooking();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&bookingRef=" + projectBookingRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d(TAG, result);
            int status = jo.getInt("status");
            if (status == 200) {
                String bookDate = jo.getJSONObject("bookingInfo").getString("bookDate");
                String salesReferral = jo.getJSONObject("bookingInfo").getString("salesReferral");
                String salesEvent = jo.getJSONObject("bookingInfo").getString("salesEvent");
                String purpose = jo.getJSONObject("bookingInfo").getString("purpose");
                String salesLocation = jo.getJSONObject("bookingInfo").getString("salesLocation");

                String category = jo.getJSONObject("unitInfo").getString("category");
                String detail = jo.getJSONObject("unitInfo").getString("detail");
                String cluster = jo.getJSONObject("unitInfo").getString("cluster");
                String block = jo.getJSONObject("unitInfo").getString("block");
                String product = jo.getJSONObject("unitInfo").getString("product");
                String unitName = jo.getJSONObject("unitInfo").getString("unitName");
                String projectName = jo.getJSONObject("unitInfo").getString("projectName");
                String area = jo.getJSONObject("unitInfo").getString("area");
                String total = jo.getJSONObject("unitInfo").getString("total");
                String payTerm = jo.getJSONObject("unitInfo").getString("payTerm");

                /*txtBookDate.setText(bookDate);
                txtSalesReferral.setText(salesReferral);
                txtSalesEvent.setText(salesEvent);
                txtPurpose.setText(purpose);
                txtSalesLocation.setText(salesLocation);*/

                txtPropertyName.setText(projectName);
                txtCategoryType.setText(category+" | "+cluster);
                txtProduct.setText(product);
                txtUnitNo.setText(block+" - "+unitName);
                txtArea.setText(Html.fromHtml(area));

                txtPaymentTerms.setText(payTerm);
                txtNetPrice.setText(total);

                Log.d("price", payTerm+" "+total);

            } else {
                Log.d(TAG, "get error");
            }

        } catch (JSONException e) {

        }
    }

    private void startDownload(String url) {

        if (!url.equals("")) {
            new DownloadFileAsync().execute(url);
        } else {
            Toast.makeText(MyBookingInfoActivity.this, "No input url", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }


    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {

                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(wallpaperDirectory + "/" + "document" + ".pdf");
                Log.d("filename",""+paymentDate);

                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                notification();
            } catch (Exception e) {
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

    }

    public void notification() {
        //Intent intent = new Intent();
        //intent.setAction(android.content.Intent.ACTION_VIEW);
        File file = new File("/" + Environment.getExternalStorageDirectory().getPath()
                + "/nataproperty/" +"document" + ".pdf");

        Log.d("FileUri", file.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingInfoActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
