package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class GarageTypeModel {
    String garageType;
    String garageTypeName;

    public String getGarageType() {
        return garageType;
    }

    public void setGarageType(String garageType) {
        this.garageType = garageType;
    }

    public String getGarageTypeName() {
        return garageTypeName;
    }

    public void setGarageTypeName(String garageTypeName) {
        this.garageTypeName = garageTypeName;
    }
}
