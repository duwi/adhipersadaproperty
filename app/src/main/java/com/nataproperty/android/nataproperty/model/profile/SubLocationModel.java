package com.nataproperty.android.nataproperty.model.profile;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 4/25/2016.
 */
@Entity
public class SubLocationModel {
    @Id
    long id;
    String subLocationRef;
    String subLocationName;

    @Generated(hash = 2047328508)
    public SubLocationModel(long id, String subLocationRef, String subLocationName) {
        this.id = id;
        this.subLocationRef = subLocationRef;
        this.subLocationName = subLocationName;
    }

    @Generated(hash = 1838473870)
    public SubLocationModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSubLocationRef() {
        return subLocationRef;
    }

    public void setSubLocationRef(String subLocationRef) {
        this.subLocationRef = subLocationRef;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }
}
