package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListMainInteractor {
    void getCountChat(String memberRef);
    void rxUnSubscribe();

}
