package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/17/2016.
 */
@Entity
public class ChatRoomModel {
    @Id
    long idChat;
    String memberRefSender;
    String memberRefReceiver;
    String chatMessage;
    String status;
    String sendTime;
    String readMessage;

    @Generated(hash = 1091958942)
    public ChatRoomModel(long idChat, String memberRefSender,
            String memberRefReceiver, String chatMessage, String status,
            String sendTime, String readMessage) {
        this.idChat = idChat;
        this.memberRefSender = memberRefSender;
        this.memberRefReceiver = memberRefReceiver;
        this.chatMessage = chatMessage;
        this.status = status;
        this.sendTime = sendTime;
        this.readMessage = readMessage;
    }

    @Generated(hash = 560443281)
    public ChatRoomModel() {
    }

    public long getIdChat() {
        return idChat;
    }

    public void setIdChat(long idChat) {
        this.idChat = idChat;
    }

    public String getMemberRefSender() {
        return memberRefSender;
    }

    public void setMemberRefSender(String memberRefSender) {
        this.memberRefSender = memberRefSender;
    }

    public String getMemberRefReceiver() {
        return memberRefReceiver;
    }

    public void setMemberRefReceiver(String memberRefReceiver) {
        this.memberRefReceiver = memberRefReceiver;
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getReadMessage() {
        return readMessage;
    }

    public void setReadMessage(String readMessage) {
        this.readMessage = readMessage;
    }
}
