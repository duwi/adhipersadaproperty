package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCalculationInteractor {
    void getListCal(String memberRef);
    void rxUnSubscribe();

}
