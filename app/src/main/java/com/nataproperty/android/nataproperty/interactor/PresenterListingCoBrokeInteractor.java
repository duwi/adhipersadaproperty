package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListingCoBrokeInteractor {
    void getLocationListing();
    void getListingMember(String agencyCompanyRef,String listingStatus,String pageNo
            ,String provinceCode,String cityCode,String locationRef,String listingTypeRef,String categoryType,String memberRef);
    void rxUnSubscribe();

}
