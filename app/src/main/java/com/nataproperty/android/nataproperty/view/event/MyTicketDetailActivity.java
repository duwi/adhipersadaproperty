package com.nataproperty.android.nataproperty.view.event;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.event.MyTicketDetailImageAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.event.MyTicketDetailModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class MyTicketDetailActivity extends AppCompatActivity {
    public static final String TAG = "MyTicketActivity" ;

    public static final String PREF_NAME = "pref" ;

    public static final String TITLE = "title" ;
    public static final String DATE = "date" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String EVENT_SCHEDULE_DATE = "eventScheduleDate";
    SharedPreferences sharedPreferences;

    String memberRef;

    ViewPager viewPager;
    private List<MyTicketDetailModel> listGallery = new ArrayList<MyTicketDetailModel>();
    private MyTicketDetailImageAdapter adapter;

    String eventScheduleRef,txtTitle,eventScheduleDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        eventScheduleRef = getIntent().getStringExtra(EVENT_SCHEDULE_REF);
        eventScheduleDate = getIntent().getStringExtra(EVENT_SCHEDULE_DATE);
        txtTitle = getIntent().getStringExtra(TITLE);
        title.setText(txtTitle);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        listGallery.clear();
        requestPropertyInfo();

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new MyTicketDetailImageAdapter(this,listGallery);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        //viewPager.setCurrentItem(position);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/0.8;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();
    }

    public void requestPropertyInfo() {
        String url = WebService.getListTiketQRCode();
        String urlPostParameter = "&memberRef=" + memberRef.toString()+
                "&eventScheduleRef=" + eventScheduleRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d(TAG, result);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateGallery(jsonArray);

        } catch (JSONException e) {

        }

    }

    private void generateGallery(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                MyTicketDetailModel myTicketDetailModel = new MyTicketDetailModel();
                myTicketDetailModel.setRsvpRef(jo.getString("rsvpRef"));
                myTicketDetailModel.setLinkCode(jo.getString("linkCode"));
                myTicketDetailModel.setGuestName(jo.getString("guestName"));
                myTicketDetailModel.setEventScheduleDate(eventScheduleDate);

                listGallery.add(myTicketDetailModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyTicketDetailActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
