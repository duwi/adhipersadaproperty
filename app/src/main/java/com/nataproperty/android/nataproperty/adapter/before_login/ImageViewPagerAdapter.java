package com.nataproperty.android.nataproperty.adapter.before_login;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.nataproperty.android.nataproperty.view.before_login.ImageFiveFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageFourFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageOneFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageSevenFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageSixFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageThreeFragment;
import com.nataproperty.android.nataproperty.view.before_login.ImageTwoFragment;


public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage = 7;

    public ImageViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch (position) {
            case 0:
                f = new ImageOneFragment();
                break;
            case 1:
                f = new ImageTwoFragment();
                break;
            case 2:
                f = new ImageThreeFragment();
                break;
            case 3:
                f = new ImageFourFragment();
                break;
            case 4:
                f = new ImageFiveFragment();
                break;
            case 5:
                f = new ImageSixFragment();
                break;
            case 6:
                f = new ImageSevenFragment();
                break;
        }
        return f;
    }

    @Override
    public int getCount() {
        return totalPage;
    }

}

