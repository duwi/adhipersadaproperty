package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 10/6/2016.
 */
@Entity
public class ListingPriceTypeModel {
    @Id
    long id;

    String priceType, priceTypeName;


    @Generated(hash = 668756278)
    public ListingPriceTypeModel(long id, String priceType, String priceTypeName) {
        this.id = id;
        this.priceType = priceType;
        this.priceTypeName = priceTypeName;
    }

    @Generated(hash = 795781568)
    public ListingPriceTypeModel() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }
}
