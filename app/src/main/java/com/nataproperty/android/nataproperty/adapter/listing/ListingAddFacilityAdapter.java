package com.nataproperty.android.nataproperty.adapter.listing;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingFacilityModel;

import java.util.List;

/**
 * Created by nata on 11/14/2016.
 */

public class ListingAddFacilityAdapter extends BaseAdapter {

    private Context context;
    public List<ListingFacilityModel> list;
    private ListHolder holder;
    Typeface font;

    public ListingAddFacilityAdapter(Context context, List<ListingFacilityModel> list) {
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_listing_facility_chekbox, null);
            holder = new ListHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);

            holder.checkBox.setTypeface(font);
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    ListingFacilityModel country = (ListingFacilityModel) cb.getTag();
                    country.setChecked(cb.isChecked());
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ListHolder) convertView.getTag();
        }

        ListingFacilityModel listingFacilityModel = list.get(position);
        holder.checkBox.setText(listingFacilityModel.getFacilityName());
        holder.checkBox.setChecked(listingFacilityModel.isChecked());
        holder.checkBox.setTag(listingFacilityModel);
        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ListHolder {
        CheckBox checkBox;
    }

}
