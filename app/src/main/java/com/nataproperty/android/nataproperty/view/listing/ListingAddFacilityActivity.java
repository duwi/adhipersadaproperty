package com.nataproperty.android.nataproperty.view.listing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingAddFacilityAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.listing.ListingFacilityModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

public class ListingAddFacilityActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    public static final String TAG = "ListingAddFacility" ;

    private ArrayList<ListingFacilityModel> listingFacilityModels = new ArrayList<ListingFacilityModel>();
    private ArrayList<String> stringArrayListFacility = new ArrayList<String>();

    private SharedPreferences sharedPreferences;
    private String memberRef,listingRef="",listFacility="",agencyCompanyRef,memberTypeCode;

    //MyCustomAdapter dataAdapter = null;
    private MyListView listView;
    Typeface font;

    private ListingAddFacilityAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_facility);
        ButterKnife.bind(this);
        LoadingBar.stopLoader();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Facility");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        listingRef = intent.getStringExtra("listingRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        Log.d(TAG,"listingRef "+listingRef);
        Log.d(TAG,"agencyCompanyRef "+agencyCompanyRef);

        listView = (MyListView) findViewById(R.id.list_facility);

        checkButtonClick();
        requestFacility(listingRef);
    }

    public void requestFacility(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingAddFacility(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArrayTable1 = new JSONArray(jsonObject.getJSONArray("facility").toString());
                    Log.d("jsonArrayTable1", "" + jsonArrayTable1.length());
                    generateFeature(jsonArrayTable1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddFacilityActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "facility");

    }

    private void generateFeature(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingFacilityModel facilityModel = new ListingFacilityModel();
                facilityModel.setFacility(jo.getString("facility"));
                facilityModel.setFacilityName(jo.getString("facilityName"));
                facilityModel.setChecked(jo.getBoolean("isCheck"));
                listingFacilityModels.add(facilityModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //dataAdapter = new MyCustomAdapter(this, R.layout.item_listing_facility_chekbox, listingFacilityModels);
        adapter = new ListingAddFacilityAdapter(this,listingFacilityModels);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }

   /* private class MyCustomAdapter extends ArrayAdapter<ListingFacilityModel> {

        private ArrayList<ListingFacilityModel> facilityModels;

        public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<ListingFacilityModel> listingFacilityModels) {
            super(context, textViewResourceId, listingFacilityModels);
            this.facilityModels = new ArrayList<ListingFacilityModel>();
            this.facilityModels.addAll(listingFacilityModels);
        }

        private class ViewHolder {
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.item_listing_facility_chekbox, null);

                holder = new ViewHolder();
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setTypeface(font);
                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        ListingFacilityModel country = (ListingFacilityModel) cb.getTag();
                        country.setChecked(cb.isChecked());
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListingFacilityModel country = facilityModels.get(position);
            holder.name.setText(country.getFacilityName());
            holder.name.setChecked(country.isChecked());
            holder.name.setTag(country);
            return convertView;

        }

    }*/

    private void checkButtonClick() {
        Button myButton = (Button) findViewById(R.id.btn_next);
        myButton.setTypeface(font);
        myButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stringArrayListFacility.clear();
                List<ListingFacilityModel> countryList = adapter.list;
                for (int i = 0; i < countryList.size(); i++) {
                    ListingFacilityModel country = countryList.get(i);
                    if(country.isChecked()){
                        stringArrayListFacility.add(country.getFacility());
                    }
                }

                listFacility = stringArrayListFacility.toString().replace("[", "").replace("]", "");
               // Toast.makeText(getApplicationContext(), listFacility, Toast.LENGTH_LONG).show();
                insertFacilityPropertyListing();
            }
        });

    }

    public void insertFacilityPropertyListing() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.insertFacilityPropertyListing(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
//                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    //listingRef = jsonObject.getString("listingRef");
                    Intent intent = new Intent(ListingAddFacilityActivity.this,ListingAddGalleryActivity.class);
                    intent.putExtra("listingRef",listingRef);
                    intent.putExtra("agencyCompanyRef",agencyCompanyRef);
                    intent.putExtra("memberTypeCode",memberTypeCode);

                    startActivityForResult(intent, 1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddFacilityActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                params.put("listFacility", listFacility);

                Log.d("param",listingRef+" - "+listFacility);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insertFacility");

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                listingRef = data.getStringExtra("listingRef");
                //requestPropertyInfo(listingRef);

                Log.d(TAG,"listingRef"+listingRef);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("listingRef", listingRef);
        setResult(RESULT_OK, intent);

        super.onBackPressed();
    }


}
