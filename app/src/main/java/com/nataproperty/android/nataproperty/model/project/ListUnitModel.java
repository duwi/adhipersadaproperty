package com.nataproperty.android.nataproperty.model.project;

import android.support.annotation.IntDef;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by herlambang-nata on 11/1/2016.
 */
@Entity
public class ListUnitModel {
    @Id
    long dbMasterRef;
    String unitName;

    @Generated(hash = 1890177568)
    public ListUnitModel(long dbMasterRef, String unitName) {
        this.dbMasterRef = dbMasterRef;
        this.unitName = unitName;
    }

    @Generated(hash = 1789857800)
    public ListUnitModel() {
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
