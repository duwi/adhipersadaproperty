package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class FurnishTypeModel {
    String furnishType;
    String furnishTypeName;

    public String getFurnishType() {
        return furnishType;
    }

    public void setFurnishType(String furnishType) {
        this.furnishType = furnishType;
    }

    public String getFurnishTypeName() {
        return furnishTypeName;
    }

    public void setFurnishTypeName(String furnishTypeName) {
        this.furnishTypeName = furnishTypeName;
    }
}
