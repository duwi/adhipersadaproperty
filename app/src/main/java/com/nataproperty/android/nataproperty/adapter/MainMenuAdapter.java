package com.nataproperty.android.nataproperty.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

/**
 * Created by User on 4/21/2016.
 */
public class MainMenuAdapter extends BaseAdapter {
    String[] title;
    int[] imageId;
    Context context;

    private static LayoutInflater inflater = null;

    public MainMenuAdapter(MainMenuActivity mainMenuActivity, String[] menuTitle, int[] menuImages) {
        // TODO Auto-generated constructor stub
        context = mainMenuActivity;
        title = menuTitle;
        imageId = menuImages;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_main_menu, null);
        holder.titleMenu=(TextView) rowView.findViewById(R.id.btn_main_menu_title);
        holder.imgMenu=(ImageView) rowView.findViewById(R.id.btn_main_menu);
        holder.titleMenu.setText(title[position]);
        holder.imgMenu.setImageResource(imageId[position]);

        return rowView;
    }

    public class Holder
    {
        TextView titleMenu;
        ImageView imgMenu;
    }
}