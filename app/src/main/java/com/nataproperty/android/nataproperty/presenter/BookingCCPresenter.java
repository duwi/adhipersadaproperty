package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterBookingCCInteractor;
import com.nataproperty.android.nataproperty.model.kpr.PaymentCCStatusModel;
import com.nataproperty.android.nataproperty.model.mybooking.BookingDetailStatusModel;
import com.nataproperty.android.nataproperty.model.nup.MouthModel;
import com.nataproperty.android.nataproperty.model.nup.YearModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.booking.BookingCreditCardActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingCCPresenter implements PresenterBookingCCInteractor {
    private BookingCreditCardActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public BookingCCPresenter(BookingCreditCardActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }
    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }

    @Override
    public void getMontList(String month) {
        Call<List<MouthModel>> call = service.getAPI().getMonthList(month);
        call.enqueue(new Callback<List<MouthModel>>() {
            @Override
            public void onResponse(Call<List<MouthModel>> call, Response<List<MouthModel>> response) {
                view.showMonthResults(response);
            }

            @Override
            public void onFailure(Call<List<MouthModel>> call, Throwable t) {
                view.showMonthFailure(t);

            }


        });
    }

    @Override
    public void getYearsList(String years) {
        Call<List<YearModel>> call = service.getAPI().getYearsList(years);
        call.enqueue(new Callback<List<YearModel>>() {
            @Override
            public void onResponse(Call<List<YearModel>> call, Response<List<YearModel>> response) {
                view.showYearsResults(response);
            }

            @Override
            public void onFailure(Call<List<YearModel>> call, Throwable t) {
                view.showYearsFailure(t);

            }


        });
    }

    @Override
    public void getPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentCCStatusModel> call = service.getAPI().getPaymentCC(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                view.showPaymentResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                view.showPaymentFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetail(String dbMasterRef, String projectRef, String bookingRef) {
        Call<BookingDetailStatusModel> call = service.getAPI().getBookingDetail(dbMasterRef,projectRef,bookingRef);
        call.enqueue(new Callback<BookingDetailStatusModel>() {
            @Override
            public void onResponse(Call<BookingDetailStatusModel> call, Response<BookingDetailStatusModel> response) {
                view.showBookingDtlResults(response);
            }

            @Override
            public void onFailure(Call<BookingDetailStatusModel> call, Throwable t) {
                view.showBookingDtlFailure(t);

            }


        });
    }
}
