package com.nataproperty.android.nataproperty.model;

/**
 * Created by User on 4/19/2016.
 */
public class HomeModel {
    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getMemberTypeName() {
        return memberTypeName;
    }

    public void setMemberTypeName(String memberTypeName) {
        this.memberTypeName = memberTypeName;
    }

    private String memberType;
    private String memberTypeName;
}
