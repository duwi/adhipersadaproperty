package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterListingBookmarkInteractor;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingBookmarkActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListingBookmarkPresenter implements PresenterListingBookmarkInteractor {
    private ListingBookmarkActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingBookmarkPresenter(ListingBookmarkActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListingMember(String agencyCompanyRef, String listingStatus,final String pageNo, String memberRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingBookmark(agencyCompanyRef,listingStatus,pageNo,memberRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showListingMemberResults(response,pageNo);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showListingMemberFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
