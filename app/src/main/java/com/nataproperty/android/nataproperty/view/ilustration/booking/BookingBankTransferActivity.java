package com.nataproperty.android.nataproperty.view.ilustration.booking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.nup.AccountBankAdapter;
import com.nataproperty.android.nataproperty.adapter.nup.BankAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.nup.AccountBankModel;
import com.nataproperty.android.nataproperty.model.nup.BankModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/21/2016.
 */
public class BookingBankTransferActivity extends AppCompatActivity {
    public static final String TAG = "BookingBankTransfer";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String STATUS_PAYMENT = "statusPayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private List<AccountBankModel> listAccountBank = new ArrayList<AccountBankModel>();
    private AccountBankAdapter adapterAccountbank;
    MyListView listView;

    private List<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    SharedPreferences sharedPreferences;

    ImageView imgPayment;
    Spinner bankList;
    EditText accountName, accountNumber;
    Button btnConfirmPayment;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtBookingFee,txtBookingInfo;

    private String propertys, category, product, unit, area, priceInc;
    private String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    String dbMasterRef, projectRef, memberRef, projectName, bookingRef, clusterRef, productRef, unitRef, termRef, termNo;
    String bankRef, fileName, bankRefSave, extension;
    String cekImg = "";
    String projectBookingRef;
    private String bookingFee;

    ProgressDialog progressDialog;

    private Bitmap bitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_bank_transfer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Bank Transfer");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);

        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);

        Log.d("Project name", projectName);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * booking fee
         */
        txtBookingFee = (TextView) findViewById(R.id.txt_booking_fee);
        txtBookingInfo = (TextView) findViewById(R.id.txt_booking_info);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyListView) findViewById(R.id.list_account_bank);

        bankList = (Spinner) findViewById(R.id.list_bank_tranfer);
        accountName = (EditText) findViewById(R.id.edit_account_name);
        accountNumber = (EditText) findViewById(R.id.edit_account_number);

        btnConfirmPayment = (Button) findViewById(R.id.btn_confirm);
        btnConfirmPayment.setTypeface(font);

        imgPayment = (ImageView) findViewById(R.id.img_receipt);
        imgPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isBankRef", bankRef);
                editor.commit();
                Log.d("isBankRef",""+bankRef);
                selectImage();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        listBank.clear();
        requestPayment();
        requestMyBookingDetail();
        requestBank();

        adapterBank = new BankAdapter(this, listBank);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        bankRef = sharedPreferences.getString("isBankRef", null);

        bankList.setAdapter(adapterBank);

        if (bankRef!=null){
            bankList.setSelection(Integer.parseInt(bankRef));
        }else {

        }

        bankList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankRef = listBank.get(position).getBankRef();
                Log.d("bank ref"," "+bankRef);

                bankList.setSelection(Integer.parseInt(bankRef));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        requestAcoountBank();
        adapterAccountbank = new AccountBankAdapter(this, listAccountBank);
        listView.setAdapter(adapterAccountbank);
        listView.setExpanded(true);

        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekAccName = accountName.getText().toString();
                String cekAccNum = accountNumber.getText().toString();
                String cekBank = bankRef;

                if (!cekAccName.isEmpty() && !cekAccNum.isEmpty() && !cekBank.equals("0") && !cekImg.equals("")) {
                    confirmPayment();
                } else {
                    if (cekAccName.isEmpty()) {
                        accountName.setError("Account name must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (cekAccNum.isEmpty()) {
                        accountNumber.setError("Account number must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (bankRef.equals("0")) {
                        Toast.makeText(BookingBankTransferActivity.this, "Bank must be filld", Toast.LENGTH_LONG)
                                .show();
                    } else if (cekImg.equals("")) {
                        Toast.makeText(BookingBankTransferActivity.this, "You must upload receipt", Toast.LENGTH_LONG)
                                .show();
                    }
                }

            }
        });

    }

    private void requestPayment() {
        String url = WebService.getPayment();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&clusterRef=" + clusterRef.toString() +
                "&productRef=" + productRef.toString() +
                "&unitRef=" + unitRef.toString() +
                "&termRef=" + termRef.toString() +
                "&termNo=" + termNo.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("cek param payment", " " + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unitRef + "-" +
                termRef + "-" + termNo);
        try {
            JSONObject jo = new JSONObject(result);
            int status = jo.getInt("status");
            String message = jo.getString("message");
            Log.d("Cek payment info", result);


            if (status == 200) {
                paymentTerm = jo.getJSONObject("dataPrice").getString("paymentTerm");
                priceIncVat = jo.getJSONObject("dataPrice").getString("priceInc");
                discPercent = jo.getJSONObject("dataPrice").getString("discPercent");
                discAmt = jo.getJSONObject("dataPrice").getString("discAmt");
                netPrice = jo.getJSONObject("dataPrice").getString("netPrice");
                termCondition = jo.getJSONObject("dataPrice").getString("termCondition");

                propertys = jo.getJSONObject("propertyInfo").getString("propertys");
                category = jo.getJSONObject("propertyInfo").getString("category");
                product = jo.getJSONObject("propertyInfo").getString("product");
                unit = jo.getJSONObject("propertyInfo").getString("unit");
                area = jo.getJSONObject("propertyInfo").getString("area");
                priceInc = jo.getJSONObject("propertyInfo").getString("priceInc");

                bookingFee = jo.getJSONObject("propertyInfo").getString("bookingFee");

                txtPropertyName.setText(propertys);
                txtCategoryType.setText(category);
                txtProduct.setText(product);
                txtUnitNo.setText(unit);
                txtArea.setText(Html.fromHtml(area));
                txtEstimate.setText(priceInc);

                txtPaymentTerms.setText(paymentTerm);
                txtPriceInc.setText(priceIncVat);
                txtDisconutPersen.setText(discPercent);
                txtdiscount.setText(discAmt);
                txtNetPrice.setText(netPrice);

                DecimalFormat decimalFormat = new DecimalFormat("###,##0");
                txtBookingFee.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(bookingFee))));

            } else {

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {

        }
    }

    private void requestBank() {
        String url = WebService.getBank();
        String urlPostParameter = "";
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListBank(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListBank(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                BankModel bank = new BankModel();
                bank.setBankRef(jo.getString("bankRef"));
                bank.setBankName(jo.getString("bankName"));

                listBank.add(bank);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapterBank.notifyDataSetChanged();
    }

    private void requestAcoountBank() {
        String url = WebService.getAccountBank();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d("result account bank" + "", result);
            generateListAccountBank(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListAccountBank(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                AccountBankModel accountBankModel = new AccountBankModel();
                accountBankModel.setBankName(jo.getString("bankName"));
                accountBankModel.setAccName(jo.getString("accName"));
                accountBankModel.setAccNo(jo.getString("accNo"));

                listAccountBank.add(accountBankModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapter.notifyDataSetChanged();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(BookingBankTransferActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Use Existing Foto")) {
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Foto"),
                            SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    private void requestMyBookingDetail() {
        String url = WebService.getDetailBooking();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef.toString() +
                "&projectRef=" + projectRef.toString() +
                "&bookingRef=" + projectBookingRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d(TAG, result);
            int status = jo.getInt("status");
            if (status == 200) {
                String bookDate = jo.getJSONObject("bookingInfo").getString("bookDate");
                String bookHour = jo.getJSONObject("bookingInfo").getString("bookHour");

                txtBookingInfo.setText("Lakukan pembayaran booking fee dalam waktu 3 jam atau sebelum pukul "+bookHour+". " +
                        "jika tidak ada konfirmasi pembayaran maka unit booking akan kembali available");
            } else {
                Log.d(TAG, "get error");
            }

        } catch (JSONException e) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

       /* File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");*/

        File destination = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
        Log.d("directory", destination.toString());
        destination.mkdirs();

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination + "/" + "payment.jpg");
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imgPayment.setImageBitmap(bitmap);
        fileName = "";
        cekImg = "1";

    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        String filePath = cursor.getString(column_index);
        extension = filePath.substring(filePath.lastIndexOf(".") + 1);

        String fileNameSegments[] = selectedImagePath.split("/");
        fileName = fileNameSegments[fileNameSegments.length - 1];

        // Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        imgPayment.setImageBitmap(bitmap);
        cekImg = "1";
        Log.d("file", " " + extension + " &@& " + fileName);

    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void confirmPayment() {
        String url = WebService.getSaveBookingBankTranfer();
        String urlPostParameter = "&dbMasterRef=" + dbMasterRef +
                "&projectRef=" + projectRef +
                "&bookingRef=" + bookingRef +
                "&termRef=" + termRef +
                "&memberRef=" + memberRef +
                "&image=" + getStringImage(bitmap) +
                "&bankRef=" + bankRef +
                "&fileName=" + fileName +
                "&accName=" + accountName.getText().toString() +
                "&accNumber=" + accountNumber.getText().toString();

        Log.d("param save bank", dbMasterRef + "-" + projectRef + "-" + bookingRef + "-" + memberRef + "-" +
                bankRef + "-" + accountName.getText().toString() + "-" +
                accountNumber.getText().toString() + "-" + fileName);
        Log.d("param save bank", "Img" + getStringImage(bitmap));

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(BookingBankTransferActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = Network.PostHttp(params[0].toString(), params[1].toString());
            try {
                JSONObject jo = new JSONObject(result);
                int status = jo.getInt("status");
                String message = jo.getString("message");

                if (status == 200) {
                    Intent intent = new Intent(BookingBankTransferActivity.this, BookingFinishActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(UNIT_REF, unitRef);
                    intent.putExtra(TERM_REF, termRef);
                    intent.putExtra(TERM_NO, termNo);

                    intent.putExtra(BOOKING_REF, bookingRef);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(STATUS_PAYMENT, "1");

                    SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                            getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("isBankRef", "0");
                    editor.commit();

                    startActivity(intent);
                    Log.d("cek result", " " + message);
                } else {
                    Log.d("cek result", " " + message);
                }

            } catch (JSONException e) {

            }
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("isBankRef", "0");
        editor.commit();
    }
}
