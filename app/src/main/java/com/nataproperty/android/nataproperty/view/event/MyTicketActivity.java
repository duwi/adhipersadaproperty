package com.nataproperty.android.nataproperty.view.event;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.event.MyTikcetAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.event.MyTicketModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyTicketActivity extends AppCompatActivity {
    public static final String TAG = "MyTicketActivity";

    public static final String PREF_NAME = "pref";

    public static final String TITLE = "title";
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String EVENT_SCHEDULE_DATE = "eventScheduleDate";

    SharedPreferences sharedPreferences;

    ListView listView;

    private ArrayList<MyTicketModel> listMyTicket = new ArrayList<MyTicketModel>();
    private MyTikcetAdapter adapter;

    String memberRef, linkDetail, titleGcm, date,eventScheduleRef,txtTitle,eventScheduleDate;

    LinearLayout linearNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_ticket));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);

        listView = (ListView) findViewById(R.id.list_myticket);
        adapter = new MyTikcetAdapter(this, listMyTicket);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                eventScheduleRef = listMyTicket.get(position).getEventScheduleRef();
                txtTitle = listMyTicket.get(position).getTitle();
                eventScheduleDate = listMyTicket.get(position).getEventScheduleDate();
                Intent intent = new Intent(MyTicketActivity.this, MyTicketDetailActivity.class);
                intent.putExtra(EVENT_SCHEDULE_REF,eventScheduleRef);
                intent.putExtra(TITLE,txtTitle);
                intent.putExtra(EVENT_SCHEDULE_DATE,eventScheduleDate);
                startActivity(intent);
            }
        });

        if (memberRef.equals("0")){
            linearNoData.setVisibility(View.VISIBLE);
        }else {
            requestListMyTicket();
        }

    }

    private void requestListMyTicket() {
        String url = WebService.getListTiketRsvp();
        String urlPostParameter = "&memberRef=" + memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d(TAG, result);

            if (jsonArray.length() != 0) {
                generateListMyTikcet(jsonArray);
                linearNoData.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            } else {
                linearNoData.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            }


        } catch (JSONException e) {

        }
    }

    private void generateListMyTikcet(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                MyTicketModel myTicketModel = new MyTicketModel();
                myTicketModel.setEventScheduleRef(jo.getString("eventScheduleRef"));
                myTicketModel.setTitle(jo.getString("title"));
                myTicketModel.setEventScheduleDate(jo.getString("eventScheduleDate"));
                myTicketModel.setCountRsvp(jo.getString("countRsvp"));

                listMyTicket.add(myTicketModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyTicketActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
