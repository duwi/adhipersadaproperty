package com.nataproperty.android.nataproperty.view.event;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.event.EventDetailAdapter;
import com.nataproperty.android.nataproperty.adapter.event.EventSchaduleAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.event.EventDetailModel;
import com.nataproperty.android.nataproperty.model.event.EventSchaduleModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class EventDetailActivity extends AppCompatActivity {
    public static final String TAG = "EventDetailActivity";

    public static final String DBMASTER_REF = "dbMasterRef" ;
    public static final String PROJECT_REF = "projectRef" ;
    public static final String CONTENT_TYPE = "contentType" ;
    public static final String CONTENT_REF = "contentRef" ;
    public static final String TITLE = "title" ;
    public static final String SYNOPSIS = "synopsis" ;
    public static final String SCHADULE = "schedule" ;
    public static final String LINK_DETAIL = "linkDetail" ;

    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String GUEST_NAME = "guest_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String GUEST_PHONE = "guest_phone";
    public static final String GUEST_MOBILE1 = "guest_hp1";
    public static final String GUEST_MOBILE2 = "guest_hp2";
    public static final String GUEST_EMAIL = "guest_email";
    public static final String STATUS = "status";

    String dbMasterRef,projectRef,contentType,contentRef,txtTitle,synopsis,schedule,linkDetail;
    String eventScheduleRef,guest_name,company_name,guest_phone,guest_hp1,guest_hp2,guest_email,status;

    private List<EventDetailModel> listImage = new ArrayList<EventDetailModel>();
    private ArrayList<EventSchaduleModel> listSchadule = new ArrayList<EventSchaduleModel>();
    private EventDetailAdapter adapter;
    private EventSchaduleAdapter adapterSchadule;
    ViewPager viewPager;

    TextView txtTitleEvent,txtSchedule;
    String titleEvent,schadule;

    WebView webView;

    static EventDetailActivity eventDetailActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_event));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        eventDetailActivity = this;

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        txtTitle = intent.getStringExtra(TITLE);
        schedule = intent.getStringExtra(SCHADULE);
        synopsis = intent.getStringExtra(SYNOPSIS);
        contentType = intent.getStringExtra(CONTENT_TYPE);
        contentRef = intent.getStringExtra(CONTENT_REF);
        linkDetail= intent.getStringExtra(LINK_DETAIL);

        Log.d(TAG,"Get "+linkDetail+" "+contentRef );

        requestImage();
        requestListEventSchadule();

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.viewpager_event_detail);
        adapter = new EventDetailAdapter(this, listImage);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        txtTitleEvent = (TextView)findViewById(R.id.txt_title);
        txtSchedule = (TextView)findViewById(R.id.txt_schedule);

        txtTitleEvent.setText(txtTitle);
        txtSchedule.setText(schedule);

        webView = (WebView) findViewById(R.id.web_content);
        //webView.loadData(content, "text/html", "UTF-8");
        webView.loadUrl(linkDetail);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.78125;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        MyListView listView = (MyListView) findViewById(R.id.list_event_schadule);
        adapterSchadule = new EventSchaduleAdapter(this,listSchadule);
        listView.setAdapter(adapterSchadule);
        listView.setExpanded(true);

       /* listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG,"item click "+position);
                eventScheduleRef = listSchadule.get(position).getEventScheduleRef();
                status = listSchadule.get(position).getStatus();
                contentRef = listSchadule.get(position).getContentRef();

                Intent intent = new Intent(EventDetailActivity.this,EventRsvpActivity.class);
                intent.putExtra(CONTENT_REF,contentRef);
                intent.putExtra(EVENT_SCHEDULE_REF,eventScheduleRef);
                intent.putExtra(STATUS,status);
                startActivity(intent);
            }
        });*/

    }

    private void requestImage() {
        String url = WebService.getImageSlider();
        String urlPostParameter ="&contentRef="+contentRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d(TAG,result);
            generateListImage(jsonArray);

        }catch (JSONException e){

        }
    }

    private void generateListImage(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                EventDetailModel eventDetailModel = new EventDetailModel();
                eventDetailModel.setImageSlider(jo.getString("imageSlider"));

                listImage.add(eventDetailModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //adapter.notifyDataSetChanged();
    }

    private void requestListEventSchadule() {
        String url = WebService.getListEventSchadule();
        String urlPostParameter ="&contentRef="+contentRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d(TAG,result);
            generateListEventSchadule(jsonArray);

        }catch (JSONException e){

        }
    }

    private void generateListEventSchadule(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                EventSchaduleModel eventSchaduleModel = new EventSchaduleModel();
                eventSchaduleModel.setEventScheduleRef(jo.getString("eventScheduleRef"));
                eventSchaduleModel.setEventScheduleDate(jo.getString("eventScheduleDate"));
                eventSchaduleModel.setStatus(jo.getString("status"));
                eventSchaduleModel.setContentRef(jo.getString("contentRef"));
                eventSchaduleModel.setLinkDetail(linkDetail);

                listSchadule.add(eventSchaduleModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //adapter.notifyDataSetChanged();
    }

    public static EventDetailActivity getInstance(){
        return  eventDetailActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventDetailActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
