package com.nataproperty.android.nataproperty.view.kpr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CalculationSendToFriendActivity extends AppCompatActivity {
    public static final String TAG = "CalculationSendToFriend";

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;

    EditText edtEmail, edtMessage;
    Button btnSendToFriend;

    String calcPayTermRef, calcUserProjectRef, payTermName, installmentType, dpType, bookingFeeDate,
            bookingFee, numOfInst, daysOfInst, listDP, KPRYear;

    String project, cluster, block, product, unit, price, type;

    ProgressDialog progressDialog;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_send_to_friend);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        calcUserProjectRef = intent.getStringExtra("calcUserProjectRef");
        payTermName = intent.getStringExtra("payTermName");
        installmentType = intent.getStringExtra("installmentType");
        dpType = intent.getStringExtra("DPType");
        bookingFeeDate = intent.getStringExtra("bookingFeeDate");
        bookingFee = intent.getStringExtra("bookingFee");
        numOfInst = intent.getStringExtra("numOfInst");
        daysOfInst = intent.getStringExtra("daysOfInst");
        listDP = intent.getStringExtra("listDP");
        KPRYear = intent.getStringExtra("KPRYear");
        project = intent.getStringExtra("project");
        cluster = intent.getStringExtra("cluster");
        product = intent.getStringExtra("product");
        block = intent.getStringExtra("block");
        unit = intent.getStringExtra("unit");
        price = intent.getStringExtra("price");

        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtMessage = (EditText) findViewById(R.id.edt_message);
        btnSendToFriend = (Button) findViewById(R.id.btn_send_to_friend);

        if (installmentType.equals("1")) {
            type = "Cicilan";
        } else {
            type = "KPR";
        }

        edtMessage.setText("For info, berikut terlampir ilustrasi pembayaran "+ type + " " +project+ "\n" +
                getResources().getString(R.string.hint_message_send_to_friend));

        btnSendToFriend.setTypeface(font);
        btnSendToFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekEmail = edtEmail.getText().toString();
                String cekMessage = edtMessage.getText().toString();

                if (cekEmail.isEmpty() || cekMessage.isEmpty()) {
                    if (cekEmail.isEmpty()) {
                        edtEmail.setError("email a empty");
                    } else {
                        edtEmail.setError(null);
                    }

                    if (cekMessage.isEmpty()) {
                        edtMessage.setError("message a empty");
                    } else {
                        edtMessage.setError(null);
                    }
                } else {

                    sendToFriend();
                }

            }
        });
    }

    public void ssendToFriend() {
        String url = WebService.sendToFriendCalcUserProject();
        String urlPostParameter = "&memberRef=" + memberRef.toString() +
                "&calcUserProjectRef=" + calcUserProjectRef.toString() +
                "&payTermName=" + payTermName.toString() +
                "&installmentType=" + installmentType.toString() +
                "&DPType=" + dpType.toString() +
                "&bookingFeeDate=" + bookingFeeDate.toString() +
                "&bookingFee=" + bookingFee.toString() +
                "&numOfInst=" + numOfInst.toString() +
                "&daysOfInst=" + daysOfInst.toString() +
                "&listDP=" + listDP.toString() +
                "&KPRYear=" + KPRYear.toString() +
                "&project=" + project.toString() +
                "&cluster=" + cluster.toString() +
                "&product=" + product.toString() +
                "&block=" + block.toString() +
                "&unit=" + unit.toString() +
                "&price=" + price.toString() +
                "&email=" + edtEmail.getText().toString() +
                "&message=" + edtMessage.getText().toString();

        new requestDialog().execute(url, urlPostParameter);
    }

    public void sendToFriend() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.sendToFriendCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);

                    int status = jsonObject.getInt("status");
                    message = jsonObject.getString("message");

                    if (status == 200) {
                        Intent intent = new Intent(CalculationSendToFriendActivity.this, CalculationIlustasiActivity.class);
                        intent.putExtra("calcUserProjectRef", calcUserProjectRef);
                        intent.putExtra("payTermName", payTermName);
                        intent.putExtra("installmentType", installmentType);
                        intent.putExtra("DPType", dpType);
                        intent.putExtra("bookingFeeDate", bookingFeeDate);
                        intent.putExtra("bookingFee", bookingFee);
                        intent.putExtra("numOfInst", numOfInst);
                        intent.putExtra("daysOfInst", daysOfInst);
                        intent.putExtra("listDP", listDP);
                        intent.putExtra("KPRYear", KPRYear);
                        intent.putExtra("project", project);
                        intent.putExtra("cluster", cluster);
                        intent.putExtra("product", product);
                        intent.putExtra("block", block);
                        intent.putExtra("unit", unit);
                        intent.putExtra("price", price);
                        startActivity(intent);
                        finish();
                        CalculationIlustasiActivity.getInstance().finish();

                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(CalculationSendToFriendActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef.toString());
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("payTermName", payTermName.toString());
                params.put("installmentType", installmentType.toString());
                params.put("DPType", dpType.toString());
                params.put("bookingFeeDate", bookingFeeDate.toString());
                params.put("bookingFee", bookingFee.toString());
                params.put("numOfInst", numOfInst.toString());
                params.put("daysOfInst", daysOfInst.toString());
                params.put("listDP", listDP.toString());
                params.put("KPRYear", KPRYear.toString());
                params.put("project", project.toString());
                params.put("cluster", cluster.toString());
                params.put("product", product.toString());
                params.put("block", block.toString());
                params.put("unit", unit.toString());
                params.put("price", price.toString());
                params.put("email", edtEmail.getText().toString());
                params.put("message", edtMessage.getText().toString());
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calcSend");

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CalculationSendToFriendActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            try {
                JSONObject jsonObject = new JSONObject(result);
                Log.d(TAG, result);

                int status = jsonObject.getInt("status");
                message = jsonObject.getString("message");

                if (status == 200) {
                    Intent intent = new Intent(CalculationSendToFriendActivity.this, CalculationIlustasiActivity.class);
                    intent.putExtra("calcUserProjectRef", calcUserProjectRef);
                    intent.putExtra("payTermName", payTermName);
                    intent.putExtra("installmentType", installmentType);
                    intent.putExtra("DPType", dpType);
                    intent.putExtra("bookingFeeDate", bookingFeeDate);
                    intent.putExtra("bookingFee", bookingFee);
                    intent.putExtra("numOfInst", numOfInst);
                    intent.putExtra("daysOfInst", daysOfInst);
                    intent.putExtra("listDP", listDP);
                    intent.putExtra("KPRYear", KPRYear);
                    intent.putExtra("project", project);
                    intent.putExtra("cluster", cluster);
                    intent.putExtra("product", product);
                    intent.putExtra("block", block);
                    intent.putExtra("unit", unit);
                    intent.putExtra("price", price);
                    intent.putExtra("source", "3");
                    startActivity(intent);
                    finish();
                    CalculationIlustasiActivity.getInstance().finish();

                }

            } catch (JSONException e) {

            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Toast.makeText(CalculationSendToFriendActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationSendToFriendActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
