package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterIlustrationCategoryInteractor {
    void getListCategory(String dbMasterRef, String projectRef);
    void rxUnSubscribe();

}
