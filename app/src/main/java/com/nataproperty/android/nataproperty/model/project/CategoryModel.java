package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 5/3/2016.
 */
@Entity
public class CategoryModel {
    @Id
    long categoryRef;
    String dbMasterRef;
    String projectRef;
    String projectCategoryType;
    String categoryDescription;
    String isShowAvailableUnit;
    String countCluster;
    String clusterRef;
    String image;

    @Generated(hash = 1331606764)
    public CategoryModel(long categoryRef, String dbMasterRef, String projectRef,
            String projectCategoryType, String categoryDescription,
            String isShowAvailableUnit, String countCluster, String clusterRef,
            String image) {
        this.categoryRef = categoryRef;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.projectCategoryType = projectCategoryType;
        this.categoryDescription = categoryDescription;
        this.isShowAvailableUnit = isShowAvailableUnit;
        this.countCluster = countCluster;
        this.clusterRef = clusterRef;
        this.image = image;
    }

    @Generated(hash = 1421288718)
    public CategoryModel() {
    }

    public long getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(long categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectCategoryType() {
        return projectCategoryType;
    }

    public void setProjectCategoryType(String projectCategoryType) {
        this.projectCategoryType = projectCategoryType;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public String getCountCluster() {
        return countCluster;
    }

    public void setCountCluster(String countCluster) {
        this.countCluster = countCluster;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
