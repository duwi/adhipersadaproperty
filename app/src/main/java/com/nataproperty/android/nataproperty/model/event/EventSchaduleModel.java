package com.nataproperty.android.nataproperty.model.event;

/**
 * Created by User on 6/12/2016.
 */
public class EventSchaduleModel {
    String eventScheduleRef,eventScheduleDate,status,contentRef,linkDetail;

    public String getLinkDetail() {
        return linkDetail;
    }

    public void setLinkDetail(String linkDetail) {
        this.linkDetail = linkDetail;
    }

    public String getContentRef() {
        return contentRef;
    }

    public void setContentRef(String contentRef) {
        this.contentRef = contentRef;
    }

    public String getEventScheduleRef() {
        return eventScheduleRef;
    }

    public void setEventScheduleRef(String eventScheduleRef) {
        this.eventScheduleRef = eventScheduleRef;
    }

    public String getEventScheduleDate() {
        return eventScheduleDate;
    }

    public void setEventScheduleDate(String eventScheduleDate) {
        this.eventScheduleDate = eventScheduleDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
