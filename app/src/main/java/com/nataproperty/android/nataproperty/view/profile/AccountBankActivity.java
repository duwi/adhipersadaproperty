package com.nataproperty.android.nataproperty.view.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.nup.BankAdapter;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.nup.BankModel;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 6/3/2016.
 */
public class AccountBankActivity extends AppCompatActivity {
    private List<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;

    ProgressDialog progressDialog;
    String message;

    EditText edtBankBranch, edtAccountName, edtAccountNumber;
    Spinner spnBank;
    Button btnSave;

    String bankRef;
    String memberRef, bankBranch, accName, accNo;
    Typeface font;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface fontLight;
    Intent intent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_bank);
        intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        bankRef = intent.getStringExtra("bankRef");
        bankBranch = intent.getStringExtra("bankBranch");
        accName = intent.getStringExtra("accName");
        accNo = intent.getStringExtra("accNo");
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Log.d("Accont",""+bankRef);
        edtBankBranch.setText(bankBranch);
        edtAccountName.setText(accName);
        edtAccountNumber.setText(accNo);
        requestBank();
        adapterBank = new BankAdapter(this, listBank);
        spnBank.setAdapter(adapterBank);
        spnBank.setSelection(Integer.parseInt(bankRef));
        spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                bankRef = listBank.get(position).getBankRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAccountBank();
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_account_bank));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        edtBankBranch = (EditText) findViewById(R.id.edt_bank_branch);
        edtAccountName = (EditText) findViewById(R.id.edt_account_name);
        edtAccountNumber = (EditText) findViewById(R.id.edt_account_number);
        spnBank = (Spinner) findViewById(R.id.list_bank);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);
    }

    private void requestBank() {
        String url = WebService.getBank();
        String urlPostParameter = "";
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListBank(jsonArray);

        } catch (JSONException e) {

        }
    }

    private void generateListBank(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                BankModel bank = new BankModel();
                bank.setBankRef(jo.getString("bankRef"));
                bank.setBankName(jo.getString("bankName"));

                listBank.add(bank);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapterBank.notifyDataSetChanged();
    }

    private void updateAccountBank() {
        String url = WebService.updateAccontBank();
        String urlPostParameter = "&memberRef=" + memberRef.toString() +
                "&bankRef=" + bankRef +
                "&bankBranch=" + edtBankBranch.getText().toString() +
                "&accName=" + edtAccountName.getText().toString() +
                "&accNo=" + edtAccountNumber.getText().toString();

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AccountBankActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            Log.d("cek updateAccontBank", result);
            try {
                JSONObject jo = new JSONObject(result);
                int status = jo.getInt("status");
                message = jo.getString("message");

                if (status == 200) {
                    Intent intent = new Intent(AccountBankActivity.this, SectionProfileActivity.class);
                    SectionProfileActivity.getInstance().finish();
                    startActivity(intent);
                    finish();

                } else {

                    //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(AccountBankActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
