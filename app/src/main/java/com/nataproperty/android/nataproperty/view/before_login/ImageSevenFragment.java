package com.nataproperty.android.nataproperty.view.before_login;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nataproperty.android.nataproperty.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageSevenFragment extends Fragment {


    public ImageSevenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_seven, container, false);
    }


}
