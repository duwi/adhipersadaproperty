package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 9/23/2016.
 */
@Entity
public class ListingMemberInfoModel {
    @Id
    long idAgent;
    String psRef, listingMemberRef, name, email, hp1, posision, address, aboutMe, quotes, memberRef;

    @Generated(hash = 1600610658)
    public ListingMemberInfoModel(long idAgent, String psRef, String listingMemberRef, String name,
            String email, String hp1, String posision, String address, String aboutMe, String quotes,
            String memberRef) {
        this.idAgent = idAgent;
        this.psRef = psRef;
        this.listingMemberRef = listingMemberRef;
        this.name = name;
        this.email = email;
        this.hp1 = hp1;
        this.posision = posision;
        this.address = address;
        this.aboutMe = aboutMe;
        this.quotes = quotes;
        this.memberRef = memberRef;
    }

    @Generated(hash = 1227372885)
    public ListingMemberInfoModel() {
    }

    public long getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(long idAgent) {
        this.idAgent = idAgent;
    }

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getListingMemberRef() {
        return listingMemberRef;
    }

    public void setListingMemberRef(String listingMemberRef) {
        this.listingMemberRef = listingMemberRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHp1() {
        return hp1;
    }

    public void setHp1(String hp1) {
        this.hp1 = hp1;
    }

    public String getPosision() {
        return posision;
    }

    public void setPosision(String posision) {
        this.posision = posision;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getQuotes() {
        return quotes;
    }

    public void setQuotes(String quotes) {
        this.quotes = quotes;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(String memberRef) {
        this.memberRef = memberRef;
    }
}
