package com.nataproperty.android.nataproperty.view.menuitem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by User on 6/3/2016.
 */
public class ContactUsActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    SharedPreferences sharedPreferences;

    private GoogleMap googleMap;

    TextView addressContact, phoneContact, emailContact, webContact;
    EditText edtName, edtEmail, edtSubject, edtMassage;
    Button btnSend;

    private double latitude, longitude;

    private String name, email, subject, message;
    private String addressNata, phoneNata, emailNata, webNata;

    ProgressDialog progressDialog;

    int status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_contact_us));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        String email = sharedPreferences.getString("isEmail",null );
        String name = sharedPreferences.getString("isName",null );

        addressContact = (TextView) findViewById(R.id.txt_address);
        phoneContact = (TextView) findViewById(R.id.txt_phone);
        emailContact = (TextView) findViewById(R.id.txt_email);
        webContact = (TextView) findViewById(R.id.txt_web);

        edtName = (EditText) findViewById(R.id.edt_name);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtSubject = (EditText) findViewById(R.id.edt_subject);
        edtMassage = (EditText) findViewById(R.id.edt_message);

        edtName.setText(name);
        edtEmail.setText(email);

        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setTypeface(font);

        latitude = -6.227145;
        longitude = 106.657108;

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                String email = edtEmail.getText().toString();
                String message = edtMassage.getText().toString();

                if (name.isEmpty() && email.isEmpty() && message.isEmpty()) {
                    if (name.isEmpty()) {
                        edtName.setError("name a empty");
                    } else {
                        edtName.setError(null);
                    }

                    if (email.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                        edtEmail.setError("enter a valid email address");
                    } else {
                        edtEmail.setError(null);
                    }

                    if (message.isEmpty()){
                        edtMassage.setError("message a empty");
                    }else {
                        edtMassage.setError(null);
                    }

                }else {

                    saveFeedback();

                }

            }
        });

        requestContactUs();
        /**
         *maps
         */
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestContactUs() {
        String url = WebService.getContactUs();
        String urlPostParameter = "";
        String result = Network.PostHttp(url, urlPostParameter);

        try {
            JSONObject jo = new JSONObject(result);
            addressNata = jo.getString("address");
            phoneNata = jo.getString("phone");
            emailNata = jo.getString("email");
            webNata = jo.getString("website");

            Log.d("Contact Us", " " + addressContact + " " + phoneNata + " " + emailNata + " " + webNata);

            addressContact.setText(addressNata);
            phoneContact.setText(phoneNata);
            emailContact.setText(emailNata);
            webContact.setText(webNata);

        } catch (JSONException e) {

        }
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps_contact_us)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void saveFeedback() {
        String url = WebService.saveFeedback();
        String urlPostParameter = "&contactName=" + edtName.getText().toString() +
                "&contactEmail=" + edtEmail.getText().toString() +
                "&contactSubject=" + edtSubject.getText().toString() +
                "&contactMessage=" + edtMassage.getText().toString();

        new requestDialog().execute(url, urlPostParameter);

    }

    class requestDialog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ContactUsActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0].toString();
            String urlPostParameter = params[1].toString();

            String result = Network.PostHttp(url, urlPostParameter);
            Log.d("cek contact us", result);
            try {
                JSONObject jo = new JSONObject(result);
                status = jo.getInt("status");
                message = jo.getString("message");

                if (status == 200) {

                } else {

                }

            } catch (JSONException e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            if (status == 200) {
                edtSubject.setText("");
                edtMassage.setText("");

                btnSend.setEnabled(true);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ContactUsActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
