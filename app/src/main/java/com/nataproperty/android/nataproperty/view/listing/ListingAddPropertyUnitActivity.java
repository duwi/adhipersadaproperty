package com.nataproperty.android.nataproperty.view.listing;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingBathRTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingBrTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingCarportTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingElectricTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingFacingTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingFurnishTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingGarageTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingMaidBRTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingMaidBathRTypeAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPhoneLineAdapter;
import com.nataproperty.android.nataproperty.adapter.listing.ListingVewTypeAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.listing.BathRTypeModel;
import com.nataproperty.android.nataproperty.model.listing.BrTypeModel;
import com.nataproperty.android.nataproperty.model.listing.CarportTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ElectricTypeModel;
import com.nataproperty.android.nataproperty.model.listing.FacingTypeModel;
import com.nataproperty.android.nataproperty.model.listing.FurnishTypeModel;
import com.nataproperty.android.nataproperty.model.listing.GarageTypeModel;
import com.nataproperty.android.nataproperty.model.listing.MaidBRTypeModel;
import com.nataproperty.android.nataproperty.model.listing.MaidBathRTypeModel;
import com.nataproperty.android.nataproperty.model.listing.ParamSaveInformasiUnitModel;
import com.nataproperty.android.nataproperty.model.listing.PhoneLineModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingAddPropertyUnitModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyUnitModel;
import com.nataproperty.android.nataproperty.model.listing.ViewTypeModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ListingAddPropertyUnitPresenter;
import com.nataproperty.android.nataproperty.utils.LoadingBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Response;

//import static com.nataproperty.android.nataproperty.view.listing.ListingAddGalleryActivity.isCobroke;

public class ListingAddPropertyUnitActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";
    public static final String TAG = "ListingAddPropertyUnit";

    @Bind(R.id.spn_br_type)
    Spinner spnBRType;
    @Bind(R.id.spn_maidBRType)
    Spinner spnMaidBRType;
    @Bind(R.id.spn_bathRType)
    Spinner spnBathRType;
    @Bind(R.id.spn_maidBathRType)
    Spinner spnMaidBathRType;
    @Bind(R.id.spn_garageType)
    Spinner spnGarageType;
    @Bind(R.id.spn_carportType)
    Spinner spnCarportType;
    @Bind(R.id.spn_phoneLine)
    Spinner spnPhoneLine;
    @Bind(R.id.spn_furnishType)
    Spinner spnFurnishType;
    @Bind(R.id.spn_electricType)
    Spinner spnElectricType;
    @Bind(R.id.spn_facingType)
    Spinner spnFacingType;
    @Bind(R.id.spn_viewType)
    Spinner spnViewType;
    @Bind(R.id.edt_youtube1)
    EditText edtYoutube1;
    @Bind(R.id.edt_youtube2)
    EditText edtYoutube2;
    @Bind(R.id.btn_save)
    Button btnSave;

    Typeface font;
    Toolbar toolbar;
    MyTextViewLatoReguler title;

    private ServiceRetrofit service;
    private ListingAddPropertyUnitPresenter presenter;
    private boolean rxCallInWorks = false;

    int status;
    String message;
    ArrayList<BrTypeModel> listBrType = new ArrayList<>();
    ArrayList<MaidBRTypeModel> listMaidBrType = new ArrayList<>();
    ArrayList<BathRTypeModel> listBathRType = new ArrayList<>();
    ArrayList<MaidBathRTypeModel> listMaidBathRType = new ArrayList<>();
    ArrayList<GarageTypeModel> listGarageType = new ArrayList<>();
    ArrayList<CarportTypeModel> listCarportType = new ArrayList<>();
    ArrayList<PhoneLineModel> listPhoneLine = new ArrayList<>();
    ArrayList<FurnishTypeModel> listFurnishType = new ArrayList<>();
    ArrayList<ElectricTypeModel> listElectricType = new ArrayList<>();
    ArrayList<FacingTypeModel> listFacingType = new ArrayList<>();
    ArrayList<ViewTypeModel> listViewType = new ArrayList<>();
    private ListingBrTypeAdapter listingBrTypeAdapter;
    private ListingMaidBRTypeAdapter listingMaidBRTypeAdapter;
    private ListingBathRTypeAdapter listingBathRTypeAdapter;
    private ListingMaidBathRTypeAdapter listingMaidBathRTypeAdapter;
    private ListingGarageTypeAdapter listingGarageTypeAdapter;
    private ListingCarportTypeAdapter listingCarportTypeAdapter;
    private ListingPhoneLineAdapter listingPhoneLineAdapter;
    private ListingFurnishTypeAdapter listingFurnishTypeAdapter;
    private ListingElectricTypeAdapter listingElectricTypeAdapter;
    private ListingFacingTypeAdapter listingFacingTypeAdapter;
    private ListingVewTypeAdapter listingVewTypeAdapter;

    String agencyCompanyRef, listingRef, brType = "1", maidBRType = "1", bathRType = "1", maidBathRType = "1",
            garageType = "1", carportType = "1", phoneLine = "1", furnishType = "1", electricType = "0",
            facingType = "1", viewType = "1", youtube1 = "", youtube2 = "",memberTypeCode,statusAdd;

    ParamSaveInformasiUnitModel model;
    LinearLayout snackBar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_property_unit);
        ButterKnife.bind(this);
        initToolbar();
        snackBar = (LinearLayout) findViewById(R.id.main_snackBar);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingAddPropertyUnitPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        LoadingBar.stopLoader();

        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        listingRef = intent.getStringExtra("listingRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        statusAdd = intent.getStringExtra("status");
//        setupSpinner();

        requestAddProeprtyUnit();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInvormasiUnit();
            }


        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void saveInvormasiUnit() {
        youtube1 = edtYoutube1.getText().toString();
        youtube2 = edtYoutube2.getText().toString();
        Map<String, String> fields = new HashMap<>();
        fields.put("listingRef", listingRef);
        fields.put("BRType", brType);
        fields.put("maidBRType", maidBRType);
        fields.put("bathRType", bathRType);
        fields.put("maidBathRType", maidBathRType);
        fields.put("garageType", garageType);
        fields.put("carportType", carportType);
        fields.put("phoneLine", phoneLine);
        fields.put("furnishType", furnishType);
        fields.put("electricType", electricType);
        fields.put("facingType", facingType);
        fields.put("viewType", viewType);
        fields.put("youTube1", youtube1);
        fields.put("youTube2", youtube2);
        LoadingBar.startLoader(this);
        presenter.saveListingInformasiUnit(fields);

    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Listing Property");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void requestAddProeprtyUnit() {
        presenter.getListingAddPropertyUnitSvc();
    }

    private void requestProeprtyUnit() {
        presenter.getListingPropertyUnitInfoSvc(listingRef);
    }

    public void showListingPropertyUnitResults(Response<ResponeListingAddPropertyUnitModel> response) {
        status = response.body().getStatus();
        message = response.body().getMessage();
        if (status == 200) {
            listBrType = response.body().getListBrType();
            listMaidBrType = response.body().getListMaidBrType();
            listBathRType = response.body().getListBathRType();
            listMaidBathRType = response.body().getListMaidBathRType();
            listGarageType = response.body().getListGarageType();
            listCarportType = response.body().getListCarportType();
            listPhoneLine = response.body().getListPhoneLine();
            listFurnishType = response.body().getListFurnishType();
            listElectricType = response.body().getListElectricType();
            listFacingType = response.body().getListFacingType();
            listViewType = response.body().getListViewType();
        }
        setupSpinner();
        requestProeprtyUnit();
//        setupSpinner();
        /*if(statusAdd.equals("edit")) {
//            setupSpinner();
            requestProeprtyUnit();
        }
        else{
            setupSpinner();
        }*/
    }

    public void showListingPropertyUnitFailure(Throwable t) {

    }

    public void showListingPropertyUnitInfoResults(Response<ResponeListingPropertyUnitModel> response) {
        status = response.body().getStatus();
        message = response.body().getMessage();
        brType = response.body().getBRType();
        maidBRType = response.body().getMaidBRType();
        bathRType = response.body().getBathRType();
        maidBathRType = response.body().getMaidBathRType();
        garageType = response.body().getGarageType();
        carportType = response.body().getCarportType();
        phoneLine = response.body().getPhoneLine();
        furnishType = response.body().getFurnishType();
        electricType = response.body().getElectricType();
        facingType = response.body().getFacingType();
        viewType = response.body().getViewType();
        youtube2 = response.body().getYoutube2();
        youtube1 = response.body().getYoutube1();

//        spnBRType.setSelection(Integer.parseInt(brType));
//        spnMaidBRType.setSelection(Integer.parseInt(maidBRType));
//        spnBathRType.setSelection(Integer.parseInt(bathRType));
//        spnMaidBathRType.setSelection(Integer.parseInt(maidBathRType));
//        spnGarageType.setSelection(Integer.parseInt(garageType));
//        spnCarportType.setSelection(Integer.parseInt(carportType));
//        spnFurnishType.setSelection(Integer.parseInt(furnishType));
//        spnElectricType.setSelection(Integer.parseInt(electricType));
//        spnFacingType.setSelection(Integer.parseInt(facingType));
//        spnViewType.setSelection(Integer.parseInt(viewType));
        edtYoutube1.setText(youtube1);
        edtYoutube2.setText(youtube2);
        setupSpinner2();
    }

    public void showListingPropertyUnitInfoFailure(Throwable t) {

    }

    private void setupSpinner() {
        listingBrTypeAdapter = new ListingBrTypeAdapter(this, listBrType);
        spnBRType.setAdapter(listingBrTypeAdapter);
        spnBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brType = listBrType.get(position).getBrType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBRTypeAdapter = new ListingMaidBRTypeAdapter(this, listMaidBrType);
        spnMaidBRType.setAdapter(listingMaidBRTypeAdapter);
        spnMaidBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBRType = listMaidBrType.get(position).getMaidBRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingBathRTypeAdapter = new ListingBathRTypeAdapter(this, listBathRType);
        spnBathRType.setAdapter(listingBathRTypeAdapter);
        spnBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bathRType = listBathRType.get(position).getBathRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBathRTypeAdapter = new ListingMaidBathRTypeAdapter(this, listMaidBathRType);
        spnMaidBathRType.setAdapter(listingMaidBathRTypeAdapter);
        spnMaidBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBathRType = listMaidBathRType.get(position).getMaidBathRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingGarageTypeAdapter = new ListingGarageTypeAdapter(this, listGarageType);
        spnGarageType.setAdapter(listingGarageTypeAdapter);
        spnGarageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                garageType = listGarageType.get(position).getGarageType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingCarportTypeAdapter = new ListingCarportTypeAdapter(this, listCarportType);
        spnCarportType.setAdapter(listingCarportTypeAdapter);
        spnCarportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carportType = listCarportType.get(position).getCarportType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingPhoneLineAdapter = new ListingPhoneLineAdapter(this, listPhoneLine);
        spnPhoneLine.setAdapter(listingPhoneLineAdapter);
        spnPhoneLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneLine = listPhoneLine.get(position).getPhoneLine();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFurnishTypeAdapter = new ListingFurnishTypeAdapter(this, listFurnishType);
        spnFurnishType.setAdapter(listingFurnishTypeAdapter);
        spnFurnishType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                furnishType = listFurnishType.get(position).getFurnishType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingElectricTypeAdapter = new ListingElectricTypeAdapter(this, listElectricType);
        spnElectricType.setAdapter(listingElectricTypeAdapter);
        spnElectricType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                electricType = listElectricType.get(position).getElectricType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFacingTypeAdapter = new ListingFacingTypeAdapter(this, listFacingType);
        spnFacingType.setAdapter(listingFacingTypeAdapter);
        spnFacingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                facingType = listFacingType.get(position).getFacingType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingVewTypeAdapter = new ListingVewTypeAdapter(this, listViewType);
        spnViewType.setAdapter(listingVewTypeAdapter);
        spnViewType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewType = listViewType.get(position).getViewType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setupSpinner2() {
        listingBrTypeAdapter = new ListingBrTypeAdapter(this, listBrType);
        spnBRType.setAdapter(listingBrTypeAdapter);
        spnBRType.setSelection(Integer.parseInt(brType)-1);
        spnBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brType = listBrType.get(position).getBrType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBRTypeAdapter = new ListingMaidBRTypeAdapter(this, listMaidBrType);
        spnMaidBRType.setAdapter(listingMaidBRTypeAdapter);
        spnMaidBRType.setSelection(Integer.parseInt(maidBRType)-1);
        spnMaidBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBRType = listMaidBrType.get(position).getMaidBRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingBathRTypeAdapter = new ListingBathRTypeAdapter(this, listBathRType);
        spnBathRType.setAdapter(listingBathRTypeAdapter);
        spnBathRType.setSelection(Integer.parseInt(bathRType)-1);
        spnBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bathRType = listBathRType.get(position).getBathRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBathRTypeAdapter = new ListingMaidBathRTypeAdapter(this, listMaidBathRType);
        spnMaidBathRType.setAdapter(listingMaidBathRTypeAdapter);
        spnMaidBathRType.setSelection(Integer.parseInt(maidBathRType)-1);
        spnMaidBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBathRType = listMaidBathRType.get(position).getMaidBathRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingGarageTypeAdapter = new ListingGarageTypeAdapter(this, listGarageType);
        spnGarageType.setAdapter(listingGarageTypeAdapter);
        spnGarageType.setSelection(Integer.parseInt(garageType)-1);
        spnGarageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                garageType = listGarageType.get(position).getGarageType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingCarportTypeAdapter = new ListingCarportTypeAdapter(this, listCarportType);
        spnCarportType.setAdapter(listingCarportTypeAdapter);
        spnCarportType.setSelection(Integer.parseInt(carportType)-1);
        spnCarportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carportType = listCarportType.get(position).getCarportType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingPhoneLineAdapter = new ListingPhoneLineAdapter(this, listPhoneLine);
        spnPhoneLine.setAdapter(listingPhoneLineAdapter);
        spnPhoneLine.setSelection(Integer.parseInt(phoneLine)-1);
        spnPhoneLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneLine = listPhoneLine.get(position).getPhoneLine();//phoneLine



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFurnishTypeAdapter = new ListingFurnishTypeAdapter(this, listFurnishType);
        spnFurnishType.setAdapter(listingFurnishTypeAdapter);
        spnFurnishType.setSelection(Integer.parseInt(furnishType)-1);
        spnFurnishType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                furnishType = listFurnishType.get(position).getFurnishType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingElectricTypeAdapter = new ListingElectricTypeAdapter(this, listElectricType);
        spnElectricType.setAdapter(listingElectricTypeAdapter);
        spnElectricType.setSelection(Integer.parseInt(electricType)-1);
        spnElectricType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                electricType = listElectricType.get(position).getElectricType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFacingTypeAdapter = new ListingFacingTypeAdapter(this, listFacingType);
        spnFacingType.setAdapter(listingFacingTypeAdapter);
        spnFacingType.setSelection(Integer.parseInt(facingType)-1);
        spnFacingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                facingType = listFacingType.get(position).getFacingType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingVewTypeAdapter = new ListingVewTypeAdapter(this, listViewType);
        spnViewType.setAdapter(listingVewTypeAdapter);
        spnViewType.setSelection(Integer.parseInt(viewType)-1);
        spnViewType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewType = listViewType.get(position).getViewType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showResultInformation(Response<ResponeListingPropertyModel> response) {
        int test = response.body().getStatus();
        String message = response.body().getMessage();
//        Log.d("result",String.valueOf(test));
        //insert succeed
        if (message.equals("insert succeed")) {
            Intent intent = new Intent(this, ListingAddFacilityActivity.class);
            intent.putExtra("listingRef", listingRef);
            intent.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent.putExtra("memberTypeCode", memberTypeCode);
            startActivityForResult(intent, 1);
        } else {
            Snackbar snackbar = Snackbar
                    .make(snackBar, "Submit Failed.. Please Check Your Network", Snackbar.LENGTH_LONG);

            snackbar.show();

        }

    }

    public void showResultInformationFailure(Throwable t) {
        Snackbar snackbar = Snackbar
                .make(snackBar, "Submit Failed.. Please Check Your Network", Snackbar.LENGTH_LONG);

        snackbar.show();

    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ListingAddPropertyUnit Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("listingRef", listingRef);
        setResult(RESULT_OK, intent);

        super.onBackPressed();
    }

}
