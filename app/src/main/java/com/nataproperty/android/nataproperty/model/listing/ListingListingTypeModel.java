package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 10/6/2016.
 */
@Entity
public class ListingListingTypeModel {
    @Id
    long id;
    String listingTypeRef, listingTypeName;

    @Generated(hash = 1617047935)
    public ListingListingTypeModel(long id, String listingTypeRef,
            String listingTypeName) {
        this.id = id;
        this.listingTypeRef = listingTypeRef;
        this.listingTypeName = listingTypeName;
    }

    @Generated(hash = 1867551126)
    public ListingListingTypeModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getListingTypeRef() {
        return listingTypeRef;
    }

    public void setListingTypeRef(String listingTypeRef) {
        this.listingTypeRef = listingTypeRef;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }
}
