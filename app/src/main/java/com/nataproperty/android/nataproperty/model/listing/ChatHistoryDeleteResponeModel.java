package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by nata on 11/21/2016.
 */

public class ChatHistoryDeleteResponeModel {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
