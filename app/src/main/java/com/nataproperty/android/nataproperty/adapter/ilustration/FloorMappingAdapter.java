package com.nataproperty.android.nataproperty.adapter.ilustration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.ilustration.BlockMappingModel;

import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class FloorMappingAdapter extends BaseAdapter {
    private Context context;
    private List<BlockMappingModel> list;
    private ListBlockMappingHolder holder;

    public FloorMappingAdapter(Context context, List<BlockMappingModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_floor_mapping,null);
            holder = new ListBlockMappingHolder();
            holder.blockName = (TextView) convertView.findViewById(R.id.txt_block_name);

            convertView.setTag(holder);
        }else{
            holder = (ListBlockMappingHolder) convertView.getTag();
        }
        BlockMappingModel block = list.get(position);
        holder.blockName.setText(block.getBlockName());

        return convertView;
    }

    private class ListBlockMappingHolder {
        TextView blockName;
    }
}
