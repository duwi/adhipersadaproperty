package com.nataproperty.android.nataproperty.view.kpr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.Network;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculationInputProjectActivity extends AppCompatActivity {
    public static final String TAG = "CalculationInputProject";
    public static final String PREF_NAME = "pref";
    public static final String CALC_USER_PROJECT_REF = "calcUserProjectRef";
    public static final String PROJECT = "project";
    SharedPreferences sharedPreferences;
    String memberRef;

    EditText edtProject, edtCluster, edtProduct, edtBlock, edtUnit, edtPrice;
    AutoCompleteTextView autoProject, autoCluster, autoProduct, autoBlock, autoUnit;

    private ArrayList<String> listProject = new ArrayList<String>();
    private ArrayList<String> listCluster = new ArrayList<String>();
    private ArrayList<String> listProduct = new ArrayList<String>();
    private ArrayList<String> listBlock = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_input_project);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        requestListProject();

        Log.d(TAG, listProject.toString());

        /*Project*/
        autoProject = (AutoCompleteTextView) findViewById(R.id.auto_project);
        ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProject);
        autoProject.setAdapter(adapterProject);
        autoProject.setTypeface(font);
        autoProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(CalculationInputProjectActivity.this, autoProject.getText(), Toast.LENGTH_LONG).show();
                requestListCluster();
            }
        });

        /*Cluster*/
        autoCluster = (AutoCompleteTextView) findViewById(R.id.auto_cluster);
        ArrayAdapter<String> adapterCluster = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listCluster);
        autoCluster.setAdapter(adapterCluster);
        autoCluster.setTypeface(font);
        autoCluster.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                requestListProduct();
            }
        });

        /*Product*/
        autoProduct = (AutoCompleteTextView) findViewById(R.id.auto_product);
        ArrayAdapter<String> adapterProduct = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProduct);
        autoProduct.setAdapter(adapterProduct);
        autoProduct.setTypeface(font);
        autoProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                requestListBlock();
            }
        });

        /*Block*/
        autoBlock = (AutoCompleteTextView) findViewById(R.id.auto_block);
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listBlock);
        autoBlock.setAdapter(adapterBlock);
        autoBlock.setTypeface(font);

        edtProject = (EditText) findViewById(R.id.edt_project_name);
        edtCluster = (EditText) findViewById(R.id.edt_cluster);
        edtProduct = (EditText) findViewById(R.id.edt_product);
        edtBlock = (EditText) findViewById(R.id.edt_block);
        edtUnit = (EditText) findViewById(R.id.edt_unit);
        edtPrice = (EditText) findViewById(R.id.edt_price);

        Button btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekProject = edtProject.getText().toString();
                String cekCluster = edtCluster.getText().toString();
                String cekProduct = edtProduct.getText().toString();
                String cekBlock = edtBlock.getText().toString();
                String cekUnit = edtUnit.getText().toString();
                String cekPrice = edtPrice.getText().toString();

                if (cekProject.isEmpty()) {
                    if (cekProject.isEmpty()) {
                        edtProject.setError("enter a project name");
                    } else {
                        edtProject.setError(null);
                    }

                } else {

                    saveNewCalcUserProject();

                }

            }
        });

    }

    private void ssaveNewCalcUserProject() {
        String url = WebService.saveNewCalcUserProject();
        String urlPostParameter = "&memberRef=" + memberRef.toString() +
                "&project=" + edtProject.getText().toString() +
                "&cluster=" + "" +
                "&product=" + "" +
                "&block=" + "" +
                "&unit=" + "" +
                "&price=" + "0";
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jsonObject = new JSONObject(result);
            Log.d(TAG, result);
            int status = jsonObject.getInt("status");
            final String calcUserProjectRef = jsonObject.getString("calcUserProjectRef");

            if (status == 200) {

                /*final CharSequence[] items = {"Cicilan", "KPR"};

                AlertDialog.Builder builder = new AlertDialog.Builder(CalculationInputProjectActivity.this);
                //builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Cicilan")) {
                            Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                            intent.putExtra("type", 0);
                            intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                            startActivity(intent);
                            finish();
                        } else if (items[item].equals("KPR")) {
                            Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                            intent.putExtra("type", 1);
                            intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
                builder.show();*/

                Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                intent.putExtra("type", 0);
                intent.putExtra("noteSave", 1);
                intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                startActivity(intent);
                finish();
            }

        } catch (JSONException e) {

        }
    }

    public void saveNewCalcUserProject() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.saveNewCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jsonObject.getInt("status");
                    final String calcUserProjectRef = jsonObject.getString("calcUserProjectRef");

                    if (status == 200) {
                        Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                        intent.putExtra("type", 0);
                        intent.putExtra("noteSave", 1);
                        intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                        intent.putExtra(PROJECT, edtProject.getText().toString());
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(CalculationInputProjectActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef.toString());
                params.put("project", edtProject.getText().toString());
                params.put("cluster", "");
                params.put("product", "");
                params.put("block", "");
                params.put("unit", "");
                params.put("price", "0");

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inputProject");

    }

    private void requestListProject() {
        String url = WebService.getListCalcUserProject();
        String urlPostParameter = "&memberRef=" + memberRef.toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.d("result product", result);

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jo = jsonArray.getJSONObject(i);

                    listProject.add(jo.getString("project"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {

        }
    }

    private void requestListCluster() {
        String url = WebService.getListCalcCluster();
        String urlPostParameter = "&project=" + autoProject.getText().toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jo = jsonArray.getJSONObject(i);

                    listCluster.add(jo.getString("cluster"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {

        }
    }

    private void requestListProduct() {
        String url = WebService.getListCalcProduct();
        String urlPostParameter = "&project=" + autoProject.getText().toString() +
                "&cluster=" + autoCluster.getText().toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jo = jsonArray.getJSONObject(i);

                    listProduct.add(jo.getString("product"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {

        }
    }

    private void requestListBlock() {
        String url = WebService.getListCalcBlock();
        String urlPostParameter = "&project=" + autoProject.getText().toString() +
                "&cluster=" + autoCluster.getText().toString() +
                "&product=" + autoProduct.getText().toString();
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONArray jsonArray = new JSONArray(result);
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jo = jsonArray.getJSONObject(i);

                    listBlock.add(jo.getString("block"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationInputProjectActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
