package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterTableMainInteractor {
    void getBlockMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef);
    void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef);
    void getDiagramColor(String memberRef);
    void rxUnSubscribe();

}
