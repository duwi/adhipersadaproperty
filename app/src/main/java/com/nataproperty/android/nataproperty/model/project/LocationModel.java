package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 10/3/2016.
 */
@Entity
public class LocationModel {
    @Id
    long  locationRef;
    String locationName;

    @Generated(hash = 1227471499)
    public LocationModel(long locationRef, String locationName) {
        this.locationRef = locationRef;
        this.locationName = locationName;
    }

    @Generated(hash = 536868411)
    public LocationModel() {
    }

    public long getLocationRef() {
        return locationRef;
    }

    public void setLocationRef(long locationRef) {
        this.locationRef = locationRef;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
