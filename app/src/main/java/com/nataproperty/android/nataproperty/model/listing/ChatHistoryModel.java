package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/17/2016.
 */
@Entity
public class ChatHistoryModel {
    @Id
    long idChatHistory;
    String chatRef;
    String memberRefReceiver;
    String memberNameReceiver;
    String lastChat;
    String lastTime;
    String newMessageCount;


    @Generated(hash = 1201170257)
    public ChatHistoryModel(long idChatHistory, String chatRef,
            String memberRefReceiver, String memberNameReceiver, String lastChat,
            String lastTime, String newMessageCount) {
        this.idChatHistory = idChatHistory;
        this.chatRef = chatRef;
        this.memberRefReceiver = memberRefReceiver;
        this.memberNameReceiver = memberNameReceiver;
        this.lastChat = lastChat;
        this.lastTime = lastTime;
        this.newMessageCount = newMessageCount;
    }

    @Generated(hash = 1498327241)
    public ChatHistoryModel() {
    }


    public String getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(String newMessageCount) {
        this.newMessageCount = newMessageCount;
    }

    public long getIdChatHistory() {
        return idChatHistory;
    }

    public void setIdChatHistory(long idChatHistory) {
        this.idChatHistory = idChatHistory;
    }

    public String getChatRef() {
        return chatRef;
    }

    public void setChatRef(String chatRef) {
        this.chatRef = chatRef;
    }

    public String getMemberRefReceiver() {
        return memberRefReceiver;
    }

    public void setMemberRefReceiver(String memberRefReceiver) {
        this.memberRefReceiver = memberRefReceiver;
    }

    public String getMemberNameReceiver() {
        return memberNameReceiver;
    }

    public void setMemberNameReceiver(String memberNameReceiver) {
        this.memberNameReceiver = memberNameReceiver;
    }

    public String getLastChat() {
        return lastChat;
    }

    public void setLastChat(String lastChat) {
        this.lastChat = lastChat;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }
}
