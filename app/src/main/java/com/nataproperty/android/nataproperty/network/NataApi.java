package com.nataproperty.android.nataproperty.network;

import com.nataproperty.android.nataproperty.model.listing.ChatHistoryDeleteResponeModel;
import com.nataproperty.android.nataproperty.model.listing.ChatHistoryModel;
import com.nataproperty.android.nataproperty.model.listing.ListTicketModel;
import com.nataproperty.android.nataproperty.model.listing.UpdatePromote;
import com.nataproperty.android.nataproperty.model.profile.BaseData;
import com.nataproperty.android.nataproperty.model.project.ChatRoomModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by User on 10/27/2016.
 */
public interface NataApi {

    @FormUrlEncoded
    @POST("getListMyAgencyChatInsertSvc")
    Call<List<ChatRoomModel>> postMessaging(
            @Field("memberRefSender") String memberRefSender,
            @Field("memberRefReceiver") String memberRefReceiver,
            @Field("chatMessage") String chatMessage);

    @FormUrlEncoded
    @POST("getListMyAgencyChatHistorySvc")
    Call<List<ChatHistoryModel>> getHistoryMessage(
            @Field("memberRef") String memberRef);

    @FormUrlEncoded
    @POST("getListingMemberInfoKeywordSvc")
    Call<BaseData> getContactMessage(
            @Field("agencyCompanyRef") String agencyCompanyRef,
            @Field("keyword") String keyword,
            @Field("pageNo") String pageNo);

    @FormUrlEncoded
    @POST("veritransBookingSvc")
    Call<BaseData> postVeritransBooking(
            @Field("tokenid") String tokenid,
            @Field("dbMasterRef") String dbMasterRef,
            @Field("projectRef") String projectRef,
            @Field("memberRef") String memberRef,
            @Field("bookingRef") String bookingRef,
            @Field("termRef") String termRef);

    @FormUrlEncoded
    @POST("getListTiketRsvp")
    Call<List<ListTicketModel>> getTicket(
            @Field("memberRef") String memberRef);

    @FormUrlEncoded
    @POST("deleteMyAgencyChatHistorySvc")
    Call<ChatHistoryDeleteResponeModel> deleteMyAgencyChatHistory(
            @Field("chatRef") String chatRef);

    @FormUrlEncoded
    @POST("updatePromotePropertyListingSvc")
    Call<UpdatePromote> updatePromote(
            @Field("listingRef") String listingRef);

    @FormUrlEncoded
    @POST("insertPropertyListingFavoriteSvc")
    Call<UpdatePromote> updateFavorite(
            @Field("listingRef") String listingRef,@Field("memberRef") String memberRef,@Field("favorite") String favorite);


}
