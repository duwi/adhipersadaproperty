package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterUnitMappingInteractor;
import com.nataproperty.android.nataproperty.model.ilustration.DiagramColor;
import com.nataproperty.android.nataproperty.model.ilustration.MapingModelNew;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.floor.UnitMappingActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class UnitMappingPresenter implements PresenterUnitMappingInteractor {
    private UnitMappingActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public UnitMappingPresenter(UnitMappingActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }




    @Override
    public void getDiagramColor(String memberRef) {
        Call<DiagramColor> call = service.getAPI().getDiagramColour(memberRef);
        call.enqueue(new Callback<DiagramColor>() {
            @Override
            public void onResponse(Call<DiagramColor> call, Response<DiagramColor> response) {
                view.showDiagramColorResults(response);
            }

            @Override
            public void onFailure(Call<DiagramColor> call, Throwable t) {
                view.showDiagramColorFailure(t);

            }


        });

    }

    @Override
    public void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef, String blockName) {
        Call<List<MapingModelNew>> call = service.getAPI().getUnitMapping(dbMasterRef,projectRef,categoryRef,clusterRef,blockName);
        call.enqueue(new Callback<List<MapingModelNew>>() {
            @Override
            public void onResponse(Call<List<MapingModelNew>> call, Response<List<MapingModelNew>> response) {
                view.showUnitMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<MapingModelNew>> call, Throwable t) {
                view.showUnitMappingFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
