package com.nataproperty.android.nataproperty.view.project;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.LocationAdapter;
import com.nataproperty.android.nataproperty.adapter.project.ProjectAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.helper.MyEditTextLatoReguler;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.LocationModel;
import com.nataproperty.android.nataproperty.model.project.LocationModelDao;
import com.nataproperty.android.nataproperty.network.ProjectModelNew;
import com.nataproperty.android.nataproperty.network.ProjectModelNewDao;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ProjectPresenter;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.project.card.RVProjectAdapter;

import java.util.ArrayList;
import java.util.List;

//import com.nataproperty.android.nataproperty.model.project.ProjectTestingModelDao;

/**
 * Created by User on 4/21/2016.
 */
public class ProjectActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    public static final String MEMBER_REF = "memberRef";
    public static boolean OFF_LINE_MODE = false;
    private static final String TAG = ProjectActivity.class.getSimpleName();

    private MyEditTextLatoReguler search;
    Context context;

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<ProjectModelNew> listProject = new ArrayList<>();
    //    private List<ProjectTestingModel> listProject2 = new ArrayList<>();
    private ProjectAdapter adapter;
    //    private ProgressDialog progress;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ProjectPresenter presenter;
    ProgressDialog progressDialog;

    Display display;
    RecyclerView rv_myproject, rv_myproject2;

    SharedPreferences sharedPreferences;
    private boolean state;
    String memberRef, offline;

    private List<LocationModel> listLocation = new ArrayList<>();
    private LocationAdapter locationAdapter;
    private LocationModel locationModel;
    private String locationRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;

    private CardView cardViewSpinner;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        String email = sharedPreferences.getString("isEmail", null);
        String name = sharedPreferences.getString("isName", null);
        requestLocation();
//        spinnerLoc();
        retry.setOnClickListener(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        display = getWindowManager().getDefaultDisplay();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initializeAdapter() {
        RVProjectAdapter adapter = new RVProjectAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);
        //swipeRefreshLayout.setRefreshing(false);
    }


    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        cardViewSpinner = (CardView) findViewById(R.id.cv_spiner);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("All Project");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

    private void spinnerLoc() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_nav);

        locationAdapter = new LocationAdapter(this, listLocation);
        spinner.setAdapter(locationAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationRef = String.valueOf(listLocation.get(position).getLocationRef());

                listProject.clear();
                requestProject();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void requestProject() {

        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListProject(memberRef, locationRef);
    }

//    private void generateListProject(JSONArray response) {
//        for (int i = 0; i < response.length(); i++) {
//            try {
//                JSONObject jo = response.getJSONObject(i);
//                ProjectModel2 project = new ProjectModel2();
//                project.setDbMasterRef(jo.getLong("dbMasterRef"));
//                project.setProjectRef(jo.getString("projectRef"));
//                project.setLocationRef(jo.getString("locationRef"));
//                project.setLocationName(jo.getString("locationName"));
//                project.setSublocationRef(jo.getString("sublocationRef"));
//                project.setSubLocationName(jo.getString("subLocationName"));
//                project.setProjectName(jo.getString("projectName"));
//                project.setIsJoin(jo.getString("isJoin"));
//                project.setImage(jo.getString("image"));
//                listProject.add(project);
//                initializeAdapter();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
////        adapter.notifyDataSetChanged();
//    }

    public void requestLocation() {

        /*progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);*/
        presenter.getLocation(memberRef);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_search:
                Intent intent = new Intent(ProjectActivity.this, ProjectSearchActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_retry:
                startActivity(new Intent(this, LaunchActivity.class));
                break;


        }
    }

    public void showLocationResults(retrofit2.Response<List<LocationModel>> response) {
        //progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        cardViewSpinner.setVisibility(View.VISIBLE);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        listLocation = response.body();
        for (int i = 0; i < listLocation.size(); i++) {
            LocationModel locationModel = new LocationModel();
            locationModel.setLocationRef(response.body().get(i).getLocationRef());
            locationModel.setLocationName(response.body().get(i).getLocationName());
            LocationModelDao locationModelDao = daoSession.getLocationModelDao();
            locationModelDao.insertOrReplace(locationModel);
        }
//        locationAdapter.notifyDataSetChanged();
        spinnerLoc();
    }

    public void showLocationFailure(Throwable t) {
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        cardViewSpinner.setVisibility(View.VISIBLE);
        LocationModelDao locationModelDao = daoSession.getLocationModelDao();
        List<LocationModel> locationModels = locationModelDao.queryBuilder().list();
        Log.d("getLocationLocalDB", "" + locationModels.toString());
        int numData = locationModels.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                LocationModel locationModel = locationModels.get(i);
                listLocation.add(locationModel);
            }
            spinnerLoc();
        } else {
            finish();
        }

    }

    public void showListProjectResults(retrofit2.Response<List<ProjectModelNew>> response) {

        progressDialog.dismiss();
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        listProject = response.body();
        for (int i = 0; i < listProject.size(); i++) {
            ProjectModelNew projectModel = new ProjectModelNew();
            projectModel.setDbMasterRef(listProject.get(i).getDbMasterRef());
            projectModel.setProjectRef(listProject.get(i).getProjectRef());
            projectModel.setLocationRef(listProject.get(i).getLocationRef());
            projectModel.setLocationName(listProject.get(i).getLocationName());
            projectModel.setSublocationRef(listProject.get(i).getSublocationRef());
            projectModel.setSubLocationName(listProject.get(i).getSubLocationName());
            projectModel.setProjectName(listProject.get(i).getProjectName());
            projectModel.setIsJoin(listProject.get(i).getIsJoin());
            projectModel.setIsWaiting(listProject.get(i).getIsWaiting());
            String id = String.valueOf(i);
            projectModel.setIdProject(id);
            ProjectModelNewDao projectModel2Dao = daoSession.getProjectModelNewDao();
            projectModel2Dao.insertOrReplace(projectModel);
        }
        initializeAdapter();


    }

    public void showListProjectFailure(Throwable t) {
        progressDialog.dismiss();
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        ProjectModelNewDao projectModel2Dao = daoSession.getProjectModelNewDao();
        if (locationRef.equals("0")) {
            List<ProjectModelNew> projectModels = projectModel2Dao.queryBuilder().orderAsc(ProjectModelNewDao.Properties.IdProject)
                    .list();
            Log.d("getProjectLocalDB", "" + projectModels.toString());
            int numData = projectModels.size();
            if (numData > 0) {
                for (int i = 0; i < numData; i++) {
                    ProjectModelNew projectModel = projectModels.get(i);
                    listProject.add(projectModel);
                    initializeAdapter();

                }
            } else {
                finish();
            }

        } else {
            List<ProjectModelNew> projectModels = projectModel2Dao.queryBuilder()
                    .where(ProjectModelNewDao.Properties.LocationRef.eq(locationRef)).orderDesc(ProjectModelNewDao.Properties.DbMasterRef)
                    .list();
            Log.d("getProjectLocalDB", "" + projectModels.toString());
            int numData = projectModels.size();
            if (numData > 0) {
                for (int i = 0; i < numData; i++) {
                    ProjectModelNew projectModel = projectModels.get(i);
                    listProject.add(projectModel);
                    initializeAdapter();

                }
            } else {
                finish();
            }
        }
    }
}
