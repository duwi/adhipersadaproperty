package com.nataproperty.android.nataproperty.model.listing;

import java.util.List;

/**
 * Created by nata on 12/5/2016.
 */

public class ListingPropertyStatusModel {
    int status;
    String message;
    List<ListingListingTypeModel> listingType;
    List<ListingCategoryTypeModel> categoryType;
    List<ListingCertificateModel> certificate;
    List<ListingPriceTypeModel> priceType;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ListingListingTypeModel> getListingType() {
        return listingType;
    }

    public void setListingType(List<ListingListingTypeModel> listingType) {
        this.listingType = listingType;
    }

    public List<ListingCategoryTypeModel> getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(List<ListingCategoryTypeModel> categoryType) {
        this.categoryType = categoryType;
    }

    public List<ListingCertificateModel> getCertificate() {
        return certificate;
    }

    public void setCertificate(List<ListingCertificateModel> certificate) {
        this.certificate = certificate;
    }

    public List<ListingPriceTypeModel> getPriceType() {
        return priceType;
    }

    public void setPriceType(List<ListingPriceTypeModel> priceType) {
        this.priceType = priceType;
    }
}
