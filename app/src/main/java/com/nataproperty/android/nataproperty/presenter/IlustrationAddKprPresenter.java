package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterIlustrationAddKprInteractor;
import com.nataproperty.android.nataproperty.model.ilustration.StatusIlustrationAddKprModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationAddKPRActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationAddKprPresenter implements PresenterIlustrationAddKprInteractor {
    private IlustrationAddKPRActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public IlustrationAddKprPresenter(IlustrationAddKPRActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }







    @Override
    public void getKprPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<StatusIlustrationAddKprModel> call = service.getAPI().getIlustrationAddKpr(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);
        call.enqueue(new Callback<StatusIlustrationAddKprModel>() {
            @Override
            public void onResponse(Call<StatusIlustrationAddKprModel> call, Response<StatusIlustrationAddKprModel> response) {
                view.showKprPaymentResults(response);
            }

            @Override
            public void onFailure(Call<StatusIlustrationAddKprModel> call, Throwable t) {
                view.showKprPaymentFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
