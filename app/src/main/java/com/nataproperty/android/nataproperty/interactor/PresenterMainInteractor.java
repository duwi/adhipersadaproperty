package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMainInteractor {
    void sendGCMToken(String GCMToken,String memberRef);
    void requestTicket(String memberRef);
    void rxUnSubscribe();
    void requestAgencyInfo(String memberRef);
    void postRegisterAgency(String memberRef,String edtCompanyCode);
    void getCountChat(String memberRef);
}
