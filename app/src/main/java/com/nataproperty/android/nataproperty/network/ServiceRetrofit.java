package com.nataproperty.android.nataproperty.network;


import android.util.LruCache;

import com.nataproperty.android.nataproperty.model.StatusTokenGCMModel;
import com.nataproperty.android.nataproperty.model.before_login.VersionModel;
import com.nataproperty.android.nataproperty.model.ilustration.BlockMappingModel;
import com.nataproperty.android.nataproperty.model.ilustration.DiagramColor;
import com.nataproperty.android.nataproperty.model.ilustration.IlustrationModel;
import com.nataproperty.android.nataproperty.model.ilustration.MapingModelNew;
import com.nataproperty.android.nataproperty.model.ilustration.StatusIlustrationAddKprModel;
import com.nataproperty.android.nataproperty.model.kpr.CalculationModel;
import com.nataproperty.android.nataproperty.model.kpr.PaymentCCStatusModel;
import com.nataproperty.android.nataproperty.model.kpr.PaymentKprStatusModel;
import com.nataproperty.android.nataproperty.model.kpr.TemplateModel;
import com.nataproperty.android.nataproperty.model.listing.ChatHistoryModel;
import com.nataproperty.android.nataproperty.model.listing.IlustrationStatusModel;
import com.nataproperty.android.nataproperty.model.listing.LIstBlockDiagramModel;
import com.nataproperty.android.nataproperty.model.listing.ListCountChatModel;
import com.nataproperty.android.nataproperty.model.listing.ListTicketModel;
import com.nataproperty.android.nataproperty.model.listing.ListUnitMappingModel;
import com.nataproperty.android.nataproperty.model.listing.ListingAgencyInfoModel;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatusModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingAddPropertyUnitModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeListingPropertyUnitModel;
import com.nataproperty.android.nataproperty.model.listing.ResponeLookupListingPropertyModel;
import com.nataproperty.android.nataproperty.model.mybooking.BookingDetailStatusModel;
import com.nataproperty.android.nataproperty.model.mybooking.MyBookingModel;
import com.nataproperty.android.nataproperty.model.nup.MouthModel;
import com.nataproperty.android.nataproperty.model.nup.YearModel;
import com.nataproperty.android.nataproperty.model.profile.BaseData;
import com.nataproperty.android.nataproperty.model.profile.CityModel;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;
import com.nataproperty.android.nataproperty.model.profile.SubLocationModel;
import com.nataproperty.android.nataproperty.model.project.CategoryModel;
import com.nataproperty.android.nataproperty.model.project.ChatRoomModel;
import com.nataproperty.android.nataproperty.model.project.CommisionStatusModel;
import com.nataproperty.android.nataproperty.model.project.DetailStatusModel;
import com.nataproperty.android.nataproperty.model.project.LocationModel;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nata on 11/22/2016.
 */

public class ServiceRetrofit {
    //private static String baseUrl ="http://www.nataproperty.com/services/appsServices.asmx/";
//    private static String baseUrl ="http://192.168.10.186/services/AppsServices.asmx/";
   // private static String baseUrl ="http://192.168.10.192/services/AppsServices.asmx/";
    public static String baseUrl = "http://192.168.10.128/services/AppsServices.asmx/";
     //private static String baseUrl ="http://192.168.10.252/services/AppsServices.asmx/";
    //private static String baseUrl ="http://192.168.43.83/services/AppsServices.asmx/";
    private NetworkAPI networkAPI;
    private OkHttpClient okHttpClient;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public ServiceRetrofit(){
        this(baseUrl);
    }

    public ServiceRetrofit(String baseUrl){
        okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     * @return
     */
    public NetworkAPI getAPI(){
        return  networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     * @return
     */
    public OkHttpClient buildClient(){

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });

        return  builder.build();
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache(){
        apiObservables.evictAll();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param clazz
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache){

        Observable<?> preparedObservable = null;

        if(useCache)//this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if(preparedObservable!=null)
            return preparedObservable;



        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if(cacheObservable){
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }


        return preparedObservable;
    }


    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {


        @FormUrlEncoded
        @POST("getListTiketRsvp")
        Call<List<ListTicketModel>> getTicket(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("saveGCMTokenSvc")
        Call<StatusTokenGCMModel> postGCMToken(@Field("GCMToken") String GCMToken, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingInfoSvc")
        Call<ListingAgencyInfoModel> getAgencyInfo(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("registerAgencyByCode")
        Call<ListingAgencyInfoModel> postRegisterAgency(@Field("memberRef") String memberRef,@Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getListMonthSvc")
        Call<List<MouthModel>> getMonthList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getListYearSvc")
        Call<List<YearModel>> getYearsList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<PaymentCCStatusModel> getPaymentCC(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef,
                                                @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getBookingDetailSvc")
        Call<BookingDetailStatusModel> getBookingDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("bookingRef") String bookingRef);

        @FormUrlEncoded
        @POST("getAppVersionSvc")
        Call<VersionModel> getNewVersion(@Field("version") String version);

        @FormUrlEncoded
        @POST("getDiagramColorSvc")
        Call<DiagramColor> getDiagramColour(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListUnitMapingSvc")
        Call<List<MapingModelNew>> getUnitMapping(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef,
                                                  @Field("clusterRef") String clusterRef, @Field("blockName") String blockName);

        @FormUrlEncoded
        @POST("getProjectDetailSvc")
        Call<DetailStatusModel> getProjectDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getProjectCommisionSvc")
        Call<CommisionStatusModel> getCommision(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetLocationLookupSvc")
        Call<List<LocationModel>> getLocation(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListProjectSvc")
        Call<List<ProjectModelNew>> getListProject(@Field("memberRef") String memberRef,@Field("locationRef") String locationRef);

        @FormUrlEncoded
        @POST("getListUnitSvc")
        Call<List<ListUnitMappingModel>> getListUnitDiagram(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<LIstBlockDiagramModel>> getListBlockDiagram(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("cekMyAgencyChatHistorySvc")
        Call<ListCountChatModel> getCountChat(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListMyAgencyChatHistorySvc")
        Call<List<ChatHistoryModel>> getHistoryMessage(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingMemberInfoKeywordSvc")
        Call<BaseData> getContactMessage(@Field("agencyCompanyRef") String agencyCompanyRef,@Field("keyword") String keyword,@Field("pageNo") String pageNo);

        @FormUrlEncoded
        @POST("getListingMemberInfoAllSvc")
        Call<BaseData> getContactMessageAll(@Field("agencyCompanyRef") String agencyCompanyRef);

        @FormUrlEncoded
        @POST("getListMyAgencyChatInsertSvc")
        Call<List<ChatRoomModel>> postMessaging(@Field("memberRefSender") String memberRefSender,@Field("memberRefReceiver") String memberRefReceiver,@Field("chatMessage") String chatMessage);

        @FormUrlEncoded
        @POST("getClusterInfoSvc")
        Call<IlustrationStatusModel> getClusterInfo(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListPaymentIlustrationSvc")
        Call<List<IlustrationModel>> getIlustrationPayment(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef, @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<StatusIlustrationAddKprModel> getIlustrationAddKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getListCategorySvc")
        Call<List<CategoryModel>> getIlusCategory(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getIlustrationPaymentKPRSvc")
        Call<PaymentCCStatusModel> getIlusPaymentKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo, @Field("KPRYear") String KPRYear);

        @FormUrlEncoded
        @POST("getIlustrationPaymentTermSvc")
        Call<PaymentKprStatusModel> getIlusPaymentTerm(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<BlockMappingModel>> getListBlok(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef
                , @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListCalcUserProject")
        Call<List<CalculationModel>> getListCal(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListBookingSvc")
        Call<List<MyBookingModel>> getListBook(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListTemplateProjectSvc")
        Call<List<TemplateModel>> getListTemplate(@Field("project") String project,@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingPropertyKeywordSvc")
        Call<ListingPropertyStatus> getListingPropertyKey(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("psRef") String psRef,
                                                          @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo,
                                                          @Field("locationRef") String locationRef, @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getProvinceListSvc")
        Call<List<ProvinceModel>> getListProvinsi(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListSvc")
        Call<List<CityModel>> getListCity(@Field("countryCode") String countryCode,@Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListSvc")
        Call<List<SubLocationModel>> getSubLoc(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);

        @FormUrlEncoded
        @POST("getProvinceListListingSvc")
        Call<List<ProvinceModel>> getListProvinsiListListing(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListListingSvc")
        Call<List<CityModel>> getListCityListListing(@Field("countryCode") String countryCode,@Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListListingSvc")
        Call<List<SubLocationModel>> getSubLocListListing(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);


        @FormUrlEncoded
        @POST("getListingAddPropertySvc")
        Call<ListingPropertyStatusModel> getListProperty(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getListingPropertyFilterSvc")
        Call<ListingPropertyStatus> getListingSearch(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo
                , @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode, @Field("locationRef") String locationRef, @Field("listingTypeRef") String listingTypeRef
                , @Field("categoryType") String categoryType, @Field("memberRef") String memberRef);

        @GET("getListingAddPropertyUnitSvc")
        Call<ResponeListingAddPropertyUnitModel> getListingPropertyUnit();

        @GET("getLookupListingPropertySvc")
        Call<ResponeLookupListingPropertyModel> getLookupListingProperty();

//        @Headers( "Content-Type: application/json" )
        @FormUrlEncoded
        @POST("insertPropertyListingUnitSvc")
        Call<ResponeListingPropertyModel> saveInformasiUnit(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("getListingPropertyInfoUnitSvc")
        Call<ResponeListingPropertyUnitModel> getListingPropertyUnitInfo(@Field("listingRef") String listingRef);

        @FormUrlEncoded
        @POST("GetLocationLookupListingSvc")
        Call<List<LocationModel>> getLocationListing(@Field("agencyCompanyRef") String agencyCompanyRef);

        @GET("GetLocationLookupIsCobrokeSvc")
        Call<List<LocationModel>> getLocationListingCobroke();

        @FormUrlEncoded
        @POST("getListingPropertyMemberSvc")
        Call<ListingPropertyStatus> getListingMember(@Field("agencyCompanyRef") String agencyCompanyRef,@Field("memberRef") String memberRef
                ,@Field("pageNo") String pageNo,@Field("locationRef") String locationRef);

        @FormUrlEncoded
        @POST("getListingPropertyFavoriteSvc")
        Call<ListingPropertyStatus> getListingBookmark(@Field("agencyCompanyRef") String agencyCompanyRef,@Field("listingStatus") String listingStatus
                ,@Field("pageNo") String pageNo,@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingPropertyAllSvc")
        Call<ListingPropertyStatus> getListingAllCoBroke(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo
                , @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode, @Field("locationRef") String locationRef, @Field("listingTypeRef") String listingTypeRef
                , @Field("categoryType") String categoryType, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("updateIsCobroke")
        Call<IsCobrokeModel> updateIsCobroke(@Field("listingRef") String listingRef,@Field("memberRef") String memberRef,@Field("isCobroke") String isCobroke);
    }

}
