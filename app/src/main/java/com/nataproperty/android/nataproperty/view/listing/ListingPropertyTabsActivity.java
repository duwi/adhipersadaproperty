package com.nataproperty.android.nataproperty.view.listing;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

import java.util.ArrayList;
import java.util.List;


public class ListingPropertyTabsActivity extends AppCompatActivity {
    public static final String TAG = "ListingPropertyTabs";

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    String listingRef, imageCover, listingTitle, agencyCompanyRef,listingMemberRef;

    ImageView imageHeader;

    String propertyName, subPropertyName, propertyDesc, propertyCountry, propertyProvince, propertyCity, propertyAddress,
            propertyPostCode, launching, finishing, totalFloor, totalLift, totalUnit, subLocation, buildArea, landArea,
            developer, architecture, contractor, pasokanAir, pasokanListrik, clusterList, marketingAddress, marketingPhone,
            marketingOpenHours;

    double latitude, longitude;

    public static FragmentManager fragmentManager;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_property_tabs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Intent intent = getIntent();
        listingRef = intent.getStringExtra("listingRef");
        imageCover = intent.getStringExtra("imageCover");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        listingTitle = intent.getStringExtra("listingTitle");
        listingMemberRef  = intent.getStringExtra("listingMemberRef");

        Log.d(TAG, listingRef + "- " + imageCover + " -" + agencyCompanyRef + " - " + listingTitle);
        Log.d(TAG," listingMemberRef = " + listingMemberRef);

        title.setText(listingTitle);

        imageHeader = (ImageView) findViewById(R.id.img_header);
        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        Glide.with(this).load(imageCover).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageHeader);

        fragmentManager = getSupportFragmentManager();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ListingPropertyDescriptionFragment(), "Info");
        adapter.addFragment(new ListingPropertyGalleryFragment(), "Gallery");
        adapter.addFragment(new ListingPropertyMapsFragment(), "Peta");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
