package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListingBookmarkInteractor {
    void getListingMember(String agencyCompanyRef, String listingStatus, String pageNo, String memberRef);
    void rxUnSubscribe();

}
