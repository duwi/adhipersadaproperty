package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterAddGaleryInteractor {
    void postIsCobroke(String listingRef,String memberRef,String isCobroke);
    void rxUnSubscribe();

}
