package com.nataproperty.android.nataproperty.adapter.project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.project.ProductDetailModel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User on 5/11/2016.
 */
public class ProductDetailImageAdapter extends PagerAdapter {
    Context context;
    private List<ProductDetailModel> list;

    PhotoViewAttacher attacher;
    //int[] imageId = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image5};

    public ProductDetailImageAdapter(Context context, List<ProductDetailModel> list){
        this.context = context;
        this.list = list;
    }

    String productRef;

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_product_detail_image, container, false);
        final ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_product_detail_image);
        ImageView shared = (ImageView) viewItem.findViewById(R.id.btn_shared);
        //imageView.setImageResource(imageId[position]);
        ProductDetailModel image = list.get(position);
        //zoom
        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        Glide.with(context).load(WebService.getProductImageDetail()+image.getQuerystring()).into(imageView);

        productRef = list.get(position).getDbMasterProjectProductFileRef();

        shared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();

                String root = Environment.getExternalStorageDirectory().toString();
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();
               /*Random gen = new Random();
                int n = 10000;
                n = gen.nextInt(n);*/
                String fotoname = "productImage-"+productRef+".jpg";
                File file = new File (wallpaperDirectory, fotoname);
                if (file.exists ()) file.delete ();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                    //Toast.makeText(context, "Saved to your folder", Toast.LENGTH_SHORT ).show();
                   /* File filePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + filename.toString() + "." + extension.toString());*/
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + "productImage-"+productRef+".jpg";
                    File imageFileToShare = new File(imagePath);
                    Uri uri = Uri.fromFile(imageFileToShare);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    context.startActivity(Intent.createChooser(share, "Share Image!"));

                } catch (Exception e) {

                }
            }
        });

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
