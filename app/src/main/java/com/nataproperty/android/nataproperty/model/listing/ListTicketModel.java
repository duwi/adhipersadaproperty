package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by nata on 11/22/2016.
 */

public class ListTicketModel {
    String eventScheduleRef;
    String title;
    String eventScheduleDate;
    String countRsvp;

    public String getEventScheduleRef() {
        return eventScheduleRef;
    }

    public void setEventScheduleRef(String eventScheduleRef) {
        this.eventScheduleRef = eventScheduleRef;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEventScheduleDate() {
        return eventScheduleDate;
    }

    public void setEventScheduleDate(String eventScheduleDate) {
        this.eventScheduleDate = eventScheduleDate;
    }

    public String getCountRsvp() {
        return countRsvp;
    }

    public void setCountRsvp(String countRsvp) {
        this.countRsvp = countRsvp;
    }
}
