package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterIlustrationPaymentInteractor {
    void getPayment(String dbMasterRef,String projectRef,String clusterRef,String productRef,String unitRef,String termRef,String termNo);
    void getPaymentKpr(String dbMasterRef,String projectRef,String clusterRef,String productRef,String unitRef,String termRef,String termNo,String KPRYear);
    void rxUnSubscribe();

}
