package com.nataproperty.android.nataproperty.model.ilustration;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/24/2016.
 */
@Entity
public class DiagramColor {
    @Id
    long id;

    int status;
    String message;
    String color1;
    String color2;


    @Generated(hash = 1516435320)
    public DiagramColor(long id, int status, String message, String color1,
            String color2) {
        this.id = id;
        this.status = status;
        this.message = message;
        this.color1 = color1;
        this.color2 = color2;
    }

    @Generated(hash = 1801094893)
    public DiagramColor() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getColor1() {
        return color1;
    }

    public void setColor1(String color1) {
        this.color1 = color1;
    }

    public String getColor2() {
        return color2;
    }

    public void setColor2(String color2) {
        this.color2 = color2;
    }
}
