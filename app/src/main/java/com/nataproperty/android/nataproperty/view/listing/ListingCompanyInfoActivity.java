package com.nataproperty.android.nataproperty.view.listing;

import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListingCompanyInfoActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    SharedPreferences sharedPreferences;

    String memberRef,agencyCompanyRef,agencyRef,companyCode,companyName,companyPhone,companyHp,companyEmail,companyCountryName,companyProvinceName,
            companyCityName,companyPostCode,companyAddress,principleName,principleHP1,principleHP2,principleEmail1,principleEmail2,npwp,siup,tdp,
            domisili,sppkp,ptName,bankRef,bankName,bankAccNo,bankBranch,bankAccName,imageLogo;

    @Bind(R.id.txt_agency_code)
    TextView txtAgencyCode;
    @Bind(R.id.txt_agency_name)
    TextView txtAgencyName;
    @Bind(R.id.txt_agency_email)
    TextView txtAgencyEmail;
    @Bind(R.id.txt_agency_mobile)
    TextView txtAgencyMobile;
    @Bind(R.id.txt_agency_phone)
    TextView txtAgencyPhone;

    @Bind(R.id.txt_country)
    TextView txtCompanyCountry;
    @Bind(R.id.txt_province)
    TextView txtCompanyProvince;
    @Bind(R.id.txt_city)
    TextView txtCompanyCity;
    @Bind(R.id.txt_post_code)
    TextView txtCompanyPostCode;
    @Bind(R.id.txt_address)
    TextView txtCompanyAddress;

    @Bind(R.id.txt_principle_name)
    TextView txtPrincipleName;
    @Bind(R.id.txt_principle_email1)
    TextView txtPrincipleEmail1;
    @Bind(R.id.txt_principle_email2)
    TextView txtPrincipleEmail2;
    @Bind(R.id.txt_principle_hp1)
    TextView txtPrincipleHp1;
    @Bind(R.id.txt_principle_hp2)
    TextView txtPrincipleHp2;

    @Bind(R.id.txt_pt_name)
    TextView txtPtName;
    @Bind(R.id.txt_company_npwp)
    TextView txtNPWP;
    @Bind(R.id.txt_company_sppkp)
    TextView txtSPPKP;
    @Bind(R.id.txt_company_tdp)
    TextView txtTDP;
    @Bind(R.id.txt_company_siup)
    TextView txtSIUP;
    @Bind(R.id.txt_company_sk_domisili)
    TextView txtSkDomisili;

    @Bind(R.id.txt_account_bank)
    TextView txtAccountBank;
    @Bind(R.id.txt_account_name)
    TextView txtAccountName;
    @Bind(R.id.txt_account_nomor)
    TextView txtAccountNomor;
    @Bind(R.id.txt_bank_branch)
    TextView txtBankBranch;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_company_info);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Informasi Perusahaan");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        agencyCompanyRef = getIntent().getStringExtra("agencyCompanyRef");
        imageLogo = getIntent().getStringExtra("imageLogo");

        imageView = (ImageView) findViewById(R.id.image_projek);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        Double widthButton = size.x / 3.0;
        Double heightButton = widthButton / 1.0;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        Glide.with(this).load(imageLogo).centerCrop().into(imageView);

        requestCompanyInfo();
    }

    public void requestCompanyInfo() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingCompanyInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");

                    if (status==200){
                        /*Agency*/
                        companyCode = jsonObject.getJSONObject("data").getString("companyCode");
                        companyName = jsonObject.getJSONObject("data").getString("companyName");
                        companyEmail = jsonObject.getJSONObject("data").getString("companyEmail");
                        companyHp = jsonObject.getJSONObject("data").getString("companyHp");
                        companyPhone = jsonObject.getJSONObject("data").getString("companyPhone");

                        txtAgencyCode.setText(companyCode);

                        if (companyName.equals("")) {
                            txtAgencyName.setText("-");
                        } else {
                            txtAgencyName.setText(companyName);
                        }

                        if (companyEmail.equals("")) {
                            txtAgencyEmail.setText("-");
                        } else {
                            txtAgencyEmail.setText(companyEmail);
                        }

                        if (companyHp.equals("")) {
                            txtAgencyMobile.setText("-");
                        } else {
                            txtAgencyMobile.setText(companyHp);
                        }

                        if (companyPhone.equals("")) {
                            txtAgencyPhone.setText("-");
                        } else {
                            txtAgencyPhone.setText(companyPhone);
                        }

                        /*Address*/
                        companyCountryName = jsonObject.getJSONObject("data").getString("companyCountryName");
                        companyProvinceName = jsonObject.getJSONObject("data").getString("companyProvinceName");
                        companyCityName = jsonObject.getJSONObject("data").getString("companyCityName");
                        companyPostCode = jsonObject.getJSONObject("data").getString("companyPostCode");
                        companyAddress = jsonObject.getJSONObject("data").getString("companyAddress");

                        txtCompanyCountry.setText(companyCountryName);
                        txtCompanyProvince.setText(companyProvinceName);
                        txtCompanyCity.setText(companyCityName);

                        if (companyPostCode.equals("")) {
                            txtCompanyPostCode.setText("-");
                        } else {
                            txtCompanyPostCode.setText(companyPostCode);
                        }

                        if (companyAddress.equals("")) {
                            txtCompanyAddress.setText("-");
                        } else {
                            txtCompanyAddress.setText(companyAddress);
                        }

                        /*Principle*/
                        principleName = jsonObject.getJSONObject("data").getString("principleName");
                        principleHP1 = jsonObject.getJSONObject("data").getString("principleHP1");
                        principleHP2 = jsonObject.getJSONObject("data").getString("principleHP2");
                        principleEmail1 = jsonObject.getJSONObject("data").getString("principleEmail1");
                        principleEmail2 = jsonObject.getJSONObject("data").getString("principleEmail2");

                        if (principleName.equals("")) {
                            txtPrincipleName.setText("-");
                        } else {
                            txtPrincipleName.setText(principleName);
                        }

                        if (principleHP1.equals("")) {
                            txtPrincipleHp1.setText("-");
                        } else {
                            txtPrincipleHp1.setText(principleHP1);
                        }

                        if (principleHP2.equals("")) {
                            txtPrincipleHp2.setText("-");
                        } else {
                            txtPrincipleHp2.setText(principleHP2);
                        }

                        if (principleEmail1.equals("")) {
                            txtPrincipleEmail1.setText("-");
                        } else {
                            txtPrincipleEmail1.setText(principleEmail1);
                        }

                        if (principleEmail2.equals("")) {
                            txtPrincipleEmail2.setText("-");
                        } else {
                            txtPrincipleEmail2.setText(principleEmail2);
                        }

                        /*AgencyInfo*/
                        ptName = jsonObject.getJSONObject("data").getString("ptName");
                        npwp = jsonObject.getJSONObject("data").getString("npwp");
                        siup = jsonObject.getJSONObject("data").getString("siup");
                        sppkp = jsonObject.getJSONObject("data").getString("sppkp");
                        tdp = jsonObject.getJSONObject("data").getString("tdp");
                        domisili = jsonObject.getJSONObject("data").getString("domisili");

                        if (ptName.equals("")) {
                            txtPtName.setText("-");
                        } else {
                            txtPtName.setText(ptName);
                        }

                        if (npwp.equals("")) {
                            txtNPWP.setText("-");
                        } else {
                            txtNPWP.setText(npwp);
                        }

                        if (siup.equals("")) {
                            txtSIUP.setText("-");
                        } else {
                            txtSIUP.setText(siup);
                        }

                        if (sppkp.equals("")) {
                            txtSPPKP.setText("-");
                        } else {
                            txtSPPKP.setText(sppkp);
                        }

                        if (tdp.equals("")) {
                            txtTDP.setText("-");
                        } else {
                            txtTDP.setText(tdp);
                        }

                        if (domisili.equals("")) {
                            txtSkDomisili.setText("-");
                        } else {
                            txtSkDomisili.setText(domisili);
                        }

                        /*Bank*/
                        bankRef = jsonObject.getJSONObject("data").getString("bankRef");
                        bankName = jsonObject.getJSONObject("data").getString("bankName");
                        bankAccNo = jsonObject.getJSONObject("data").getString("bankAccNo");
                        bankAccName = jsonObject.getJSONObject("data").getString("bankAccName");
                        bankBranch = jsonObject.getJSONObject("data").getString("bankBranch");

                        if (bankRef.equals("0")){
                            txtAccountBank.setText("-");
                        } else {
                            txtAccountBank.setText(bankName);
                        }

                        if (bankAccName.equals("")){
                            txtAccountName.setText("-");
                        } else {
                            txtAccountName.setText(bankAccName);
                        }

                        if (bankAccNo.equals("")){
                            txtAccountNomor.setText("-");
                        }else {
                            txtAccountNomor.setText(bankAccNo);
                        }

                        if (bankBranch.equals("")){
                            txtBankBranch.setText("-");
                        }else {
                            txtBankBranch.setText(bankBranch);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListingCompanyInfoActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("agencyCompanyRef", agencyCompanyRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "companyInfo");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
