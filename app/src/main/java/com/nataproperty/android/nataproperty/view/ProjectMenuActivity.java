package com.nataproperty.android.nataproperty.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.CommisionDetailModel;
import com.nataproperty.android.nataproperty.model.project.CommisionDetailModelDao;
import com.nataproperty.android.nataproperty.model.project.CommisionStatusModel;
import com.nataproperty.android.nataproperty.model.project.DetailStatusModel;
import com.nataproperty.android.nataproperty.model.project.MenuProjectModel2;
import com.nataproperty.android.nataproperty.model.project.MenuProjectModel2Dao;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ProjectMenuPresenter;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.gallery.CategoryGalleryActivity;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationCategoryActivity;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationClusterActivity;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationProductActivity;
import com.nataproperty.android.nataproperty.view.nup.NupTermActivity;
import com.nataproperty.android.nataproperty.view.project.CategoryActivity;
import com.nataproperty.android.nataproperty.view.project.ClusterActivity;
import com.nataproperty.android.nataproperty.view.project.CommisionActivity;
import com.nataproperty.android.nataproperty.view.project.DownloadActivity;
import com.nataproperty.android.nataproperty.view.project.NewsProjectActivity;
import com.nataproperty.android.nataproperty.view.project.ProductActivity;
import com.nataproperty.android.nataproperty.view.project.ProjectDetailActivity;

import java.util.List;

/**
 * Created by User on 5/3/2016.
 */
public class ProjectMenuActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "ProjectMenuActivity";
    public static final String PREF_NAME = "pref";
    public static final String MEMBER_REF = "memberRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    //maps
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    //Nup
    public static final String NUP_AMT = "nupAmt";
    //Booking
    public static final String IS_NUP = "isNUP";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String PRODUCT_REF = "productRef";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String URL_VIDEO = "urlVideo";
    public static final String IMAGE_LOGO = "imageLogo";

    private ImageView imageView;
    private GridView mainMenu;
    Context context;

    private long dbMasterRef;
    private String projectRef, projectName, locationName, locationRef, sublocationName, sublocationRef;
    private String countCategory, countCluster, salesStatus, isBooking, isNUP, isShowAvailableUnit;
    private String categoryRef, clusterRef;
    private String projectDescription;
    private String nupAmt, linkDetail, bookingContact = "";
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    private double latitude, longitude;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ProjectMenuPresenter presenter;
    ProgressDialog progressDialog;

    static ProjectMenuActivity projectMenuActivity;

    String imageLogo, urlVideo, memberRef;

    ImageButton btnInformation, btnDownload, btnSearchUnit, btnNup, btnCommision, btnGallery, btnIlustration, btnNews;

    SharedPreferences sharedPreferences;
    private String commision, closingFee, description;
    private double commissionAmt;
    private boolean myListNup = false;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Integer width;
    Double result;
    Double widthButton, heightButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projek_menu);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ProjectMenuPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        projectRef = getIntent().getStringExtra(PROJECT_REF);
        dbMasterRef = getIntent().getLongExtra(DBMASTER_REF, 0);
        projectName = getIntent().getStringExtra(PROJECT_NAME);
        locationName = getIntent().getStringExtra(LOCATION_NAME);
        locationRef = getIntent().getStringExtra(LOCATION_REF);
        sublocationName = getIntent().getStringExtra(SUBLOCATION_NAME);
        sublocationRef = getIntent().getStringExtra(SUBLOCATION_REF);
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
        initWidget();


        Log.d(TAG, projectRef + " " + dbMasterRef + " " + projectName + " " + locationName + " " + locationRef
                + " " + sublocationName + " " + sublocationRef + " " + projectDescription + " " + countCluster + " " + countCategory);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        projectMenuActivity = this;
        Glide.with(this)
                .load(WebService.getProjectImage() + String.valueOf(dbMasterRef) +
                        "&pr=" + projectRef).into(imageView);
        Log.d(TAG, WebService.getProjectImage() + String.valueOf(dbMasterRef) + "&pr=" + projectRef);
        context = this;
        menuRow();

        requestProjectDetail();
        requestCommision();

        retry.setOnClickListener(this);
        btnInformation.setOnClickListener(this);
        btnSearchUnit.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnNup.setOnClickListener(this);
        btnCommision.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        Log.d(TAG, "" + salesStatus);
        btnNews.setOnClickListener(this);
    }

    private void menuRow() {
        /*Row1*/
        ViewGroup.LayoutParams paramsInfo = btnInformation.getLayoutParams();
        paramsInfo.height = heightButton.intValue();
        btnInformation.setLayoutParams(paramsInfo);
        btnInformation.requestLayout();

        /*Row2*/
        ViewGroup.LayoutParams paramsNup = btnNup.getLayoutParams();
        paramsNup.height = heightButton.intValue();
        btnNup.setLayoutParams(paramsNup);
        btnNup.requestLayout();

        ViewGroup.LayoutParams paramsIlustrasi = btnIlustration.getLayoutParams();
        paramsIlustrasi.height = heightButton.intValue();
        btnIlustration.setLayoutParams(paramsIlustrasi);
        btnIlustration.requestLayout();

        ViewGroup.LayoutParams paramsKomisi = btnCommision.getLayoutParams();
        paramsKomisi.height = heightButton.intValue();
        btnCommision.setLayoutParams(paramsKomisi);
        btnCommision.requestLayout();

        /*Row3*/
        ViewGroup.LayoutParams paramsGallery = btnGallery.getLayoutParams();
        paramsGallery.height = heightButton.intValue();
        btnGallery.setLayoutParams(paramsGallery);
        btnGallery.requestLayout();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(String.valueOf(projectName));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.image_projek);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        widthButton = size.x / 3.0;
        heightButton = widthButton / 1.0;
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        btnInformation = (ImageButton) findViewById(R.id.btn_project_information);
        btnSearchUnit = (ImageButton) findViewById(R.id.btn_search_unit);
        btnDownload = (ImageButton) findViewById(R.id.btn_download);
        btnNup = (ImageButton) findViewById(R.id.btn_nup);
        btnCommision = (ImageButton) findViewById(R.id.btn_commision);
        btnIlustration = (ImageButton) findViewById(R.id.btn_ilustration);
        btnGallery = (ImageButton) findViewById(R.id.btn_gallery);
        btnNews = (ImageButton) findViewById(R.id.btn_news);

    }


    private void requestProjectDetail() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getProjectDtl(String.valueOf(dbMasterRef), projectRef);
    }

    private void requestCommision() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getCommision(String.valueOf(dbMasterRef), projectRef, memberRef);

    }

    public static ProjectMenuActivity getInstance() {
        return projectMenuActivity;
    }

    private void bookingAvailable() {
        if (countCategory != null) {
            if (countCategory.equals("1")) {
                if (countCluster.equals("1")) {
                    Intent intentProduct = new Intent(ProjectMenuActivity.this, IlustrationProductActivity.class);
                    intentProduct.putExtra(PROJECT_REF, projectRef);
                    intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                    intentProduct.putExtra(CATEGORY_REF, categoryRef);
                    intentProduct.putExtra(CLUSTER_REF, clusterRef);

                    intentProduct.putExtra(PROJECT_NAME, projectName);
                    intentProduct.putExtra(IS_NUP, isNUP);
                    intentProduct.putExtra(IS_BOOKING, isBooking);
                    intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentProduct.putExtra(NUP_AMT, nupAmt);

                    intentProduct.putExtra(PRODUCT_REF, "");
                    intentProduct.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentProduct);

                } else {
                    Intent intentCluster = new Intent(ProjectMenuActivity.this, IlustrationClusterActivity.class);
                    intentCluster.putExtra(PROJECT_REF, projectRef);
                    intentCluster.putExtra(DBMASTER_REF, dbMasterRef);
                    intentCluster.putExtra(CATEGORY_REF, categoryRef);
                    intentCluster.putExtra(CLUSTER_REF, clusterRef);

                    intentCluster.putExtra(PROJECT_NAME, projectName);
                    intentCluster.putExtra(IS_NUP, isNUP);
                    intentCluster.putExtra(IS_BOOKING, isBooking);
                    intentCluster.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentCluster.putExtra(NUP_AMT, nupAmt);
                    intentCluster.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentCluster);
                }
            } else {
                Intent intentCategory = new Intent(ProjectMenuActivity.this, IlustrationCategoryActivity.class);
                intentCategory.putExtra(PROJECT_REF, projectRef);
                intentCategory.putExtra(DBMASTER_REF, dbMasterRef);

                intentCategory.putExtra(PROJECT_NAME, projectName);
                intentCategory.putExtra(IS_NUP, isNUP);
                intentCategory.putExtra(IS_BOOKING, isBooking);
                intentCategory.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentCategory.putExtra(NUP_AMT, nupAmt);
                intentCategory.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentCategory);

            }
        } else {

            //Toast.makeText(ProjectMenuActivity.this, "error connection", Toast.LENGTH_LONG).show();

        }
    }

    private void bookingNotAvailable() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        LayoutInflater inflater = ProjectMenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Booking Contact");
        txtspecialEnquiries.setText(bookingContact);

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProjectMenuActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_retry:
                startActivity(new Intent(ProjectMenuActivity.this, LaunchActivity.class));
                break;
            case R.id.btn_project_information:
                Intent intent = new Intent(ProjectMenuActivity.this, ProjectDetailActivity.class);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(LOCATION_NAME, locationName);
                intent.putExtra(LOCATION_REF, locationRef);
                intent.putExtra(SUBLOCATION_NAME, sublocationName);
                intent.putExtra(SUBLOCATION_REF, sublocationRef);
                intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                intent.putExtra(LINK_DETAIL, linkDetail);
                intent.putExtra(LATITUDE, latitude);
                intent.putExtra(LONGITUDE, longitude);
                intent.putExtra(URL_VIDEO, urlVideo);
                intent.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intent);
                break;
            case R.id.btn_search_unit:
                if (countCategory != null) {
                    if (countCategory.equals("1")) {
                        if (countCluster.equals("1")) {
                            Intent intentProduct = new Intent(ProjectMenuActivity.this, ProductActivity.class);
                            intentProduct.putExtra(PROJECT_REF, projectRef);
                            intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                            intentProduct.putExtra(CATEGORY_REF, categoryRef);
                            intentProduct.putExtra(CLUSTER_REF, clusterRef);
                            intentProduct.putExtra(PROJECT_NAME, projectName);

                            intentProduct.putExtra(LATITUDE, latitude);
                            intentProduct.putExtra(LONGITUDE, longitude);

                            intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                            intentProduct.putExtra(IS_NUP, isNUP);
                            intentProduct.putExtra(IS_BOOKING, isBooking);

                            intentProduct.putExtra(NUP_AMT, nupAmt);
                            intentProduct.putExtra(IMAGE_LOGO, imageLogo);
                            startActivity(intentProduct);

                        } else {
                            Intent intentCluster = new Intent(ProjectMenuActivity.this, ClusterActivity.class);
                            intentCluster.putExtra(PROJECT_REF, projectRef);
                            intentCluster.putExtra(DBMASTER_REF, dbMasterRef);
                            intentCluster.putExtra(CATEGORY_REF, categoryRef);
                            intentCluster.putExtra(PROJECT_NAME, projectName);

                            intentCluster.putExtra(LATITUDE, latitude);
                            intentCluster.putExtra(LONGITUDE, longitude);

                            intentCluster.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                            intentCluster.putExtra(IS_NUP, isNUP);
                            intentCluster.putExtra(IS_BOOKING, isBooking);
                            intentCluster.putExtra(NUP_AMT, nupAmt);
                            intentCluster.putExtra(IMAGE_LOGO, imageLogo);
                            startActivity(intentCluster);

                        }
                    } else {
                        Intent intentCategory = new Intent(ProjectMenuActivity.this, CategoryActivity.class);
                        intentCategory.putExtra(PROJECT_REF, projectRef);
                        intentCategory.putExtra(DBMASTER_REF, dbMasterRef);
                        intentCategory.putExtra(PROJECT_NAME, projectName);

                        intentCategory.putExtra(LATITUDE, latitude);
                        intentCategory.putExtra(LONGITUDE, longitude);

                        intentCategory.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                        intentCategory.putExtra(IS_NUP, isNUP);
                        intentCategory.putExtra(IS_BOOKING, isBooking);
                        intentCategory.putExtra(NUP_AMT, nupAmt);
                        intentCategory.putExtra(IMAGE_LOGO, imageLogo);

                        startActivity(intentCategory);

                    }
                }
                break;
            case R.id.btn_download:
                Intent intentDownload = new Intent(ProjectMenuActivity.this, DownloadActivity.class);
                intentDownload.putExtra(PROJECT_REF, projectRef);
                intentDownload.putExtra(DBMASTER_REF, dbMasterRef);
                intentDownload.putExtra(PROJECT_NAME, projectName);
                intentDownload.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentDownload);
                break;
            case R.id.btn_nup:
                Intent intentNup = new Intent(ProjectMenuActivity.this, NupTermActivity.class);
                intentNup.putExtra(PROJECT_REF, projectRef);
                intentNup.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentNup.putExtra(PROJECT_NAME, projectName);
                intentNup.putExtra(PROJECT_DESCRIPTION, projectDescription);
                intentNup.putExtra(NUP_AMT, nupAmt);
                intentNup.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentNup);
                break;
            case R.id.btn_commision:
                Intent intentCommision = new Intent(ProjectMenuActivity.this, CommisionActivity.class);
                intentCommision.putExtra(PROJECT_REF, projectRef);
                intentCommision.putExtra(DBMASTER_REF, dbMasterRef);
                intentCommision.putExtra(PROJECT_NAME, projectName);
                intentCommision.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentCommision);
                break;
            case R.id.btn_gallery:
                Intent intentGallery = new Intent(ProjectMenuActivity.this, CategoryGalleryActivity.class);
                intentGallery.putExtra(PROJECT_REF, projectRef);
                intentGallery.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentGallery.putExtra(PROJECT_NAME, projectName);
                intentGallery.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentGallery);
                break;
            case R.id.btn_news:
                Intent intentNews = new Intent(ProjectMenuActivity.this, NewsProjectActivity.class);
                intentNews.putExtra(PROJECT_REF, projectRef);
                intentNews.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentNews.putExtra(PROJECT_NAME, projectName);
                intentNews.putExtra(IMAGE_LOGO, imageLogo);
                startActivity(intentNews);
                break;

        }
    }

    public void showMenuResults(retrofit2.Response<DetailStatusModel> response) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        if (status == 200) {
            //Local DB
            MenuProjectModel2 projectMenuModel = new MenuProjectModel2();
            projectMenuModel.setDbMasterRef(response.body().getData().getDbMasterRef());
            projectMenuModel.setProjectDescription(response.body().getData().getProjectDescription());
            projectMenuModel.setCountCategory(response.body().getData().getCountCategory());
            projectMenuModel.setCountCluster(response.body().getData().getCountCluster());
            projectMenuModel.setCategoryRef(response.body().getData().getCategoryRef());
            projectMenuModel.setClusterRef(response.body().getData().getClusterRef());
            projectMenuModel.setSalesStatus(response.body().getData().getSalesStatus());
            projectMenuModel.setIsBooking(response.body().getData().getIsBooking());
            projectMenuModel.setIsNUP(response.body().getData().getIsNUP());
            projectMenuModel.setIsShowAvailableUnit(response.body().getData().getIsShowAvailableUnit());
            projectMenuModel.setLatitude(response.body().getData().getLatitude());
            projectMenuModel.setLongitude(response.body().getData().getLongitude());
            projectMenuModel.setNupAmt(response.body().getData().getNupAmt());
            projectMenuModel.setLinkProjectDetail(response.body().getData().getLinkProjectDetail());
            projectMenuModel.setUrlVideo(response.body().getData().getUrlVideo());
            projectMenuModel.setImageLogo(response.body().getData().getImageLogo());
            projectMenuModel.setBookingContact(response.body().getData().getBookingContact());
            MenuProjectModel2Dao menuProjectModelDao = daoSession.getMenuProjectModel2Dao();
            menuProjectModelDao.insertOrReplace(projectMenuModel);

            projectDescription = response.body().getData().getProjectDescription();
            //cari unit
            countCategory = response.body().getData().getCountCategory();
            countCluster = response.body().getData().getCountCluster();
            categoryRef = response.body().getData().getCategoryRef();
            clusterRef = response.body().getData().getClusterRef();
            //ilustration
            salesStatus = response.body().getData().getSalesStatus();
            isBooking = response.body().getData().getIsBooking();
            isNUP = response.body().getData().getIsNUP();
            isShowAvailableUnit = response.body().getData().getIsShowAvailableUnit();
            //maps
            latitude = response.body().getData().getLatitude();
            longitude = response.body().getData().getLongitude();
            //nup
            nupAmt = response.body().getData().getNupAmt();
            linkDetail = response.body().getData().getLinkProjectDetail();
            urlVideo = response.body().getData().getUrlVideo();
            imageLogo = response.body().getData().getImageLogo();
            bookingContact = response.body().getData().getBookingContact();

            Log.d(TAG, "imageLogo " + imageLogo);

            if (salesStatus.equals(WebService.salesStatusOpen)) {
                if (!isBooking.equals("0")) {
                    btnIlustration.setEnabled(true);
                    btnIlustration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bookingAvailable();
                        }
                    });
                } else {
                    btnIlustration.setEnabled(false);
                    btnIlustration.setBackgroundResource(R.drawable.ic_project_no_ilustrasi);
                    //Toast.makeText(ProjectMenuActivity.this, bookingContact, Toast.LENGTH_LONG).show();
                    /*btnIlustration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bookingNotAvailable();
                        }
                    });*/
                }

                if (!isNUP.equals("0")) {
                    btnNup.setEnabled(true);
                } else {
                    btnNup.setEnabled(false);
                    btnNup.setBackgroundResource(R.drawable.ic_project_no_nup);
                }

            } else {
                btnIlustration.setEnabled(false);
                btnNup.setEnabled(false);
                btnIlustration.setBackgroundResource(R.drawable.ic_project_no_ilustrasi);
                btnNup.setBackgroundResource(R.drawable.ic_project_no_nup);
            }

            // Glide.with(ProjectMenuActivity.this).load(imageLogo).into(imageView);

        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            finish();
            startActivity(getIntent());
        }

    }

    public void showMenuFailure(Throwable t) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
        btnInformation.setEnabled(false);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        MenuProjectModel2Dao menuProjectModelDao = daoSession.getMenuProjectModel2Dao();
        List<MenuProjectModel2> menuProjectModels = menuProjectModelDao.queryBuilder().where(MenuProjectModel2Dao.Properties.DbMasterRef.eq(dbMasterRef))
                .list();

        if (menuProjectModels.size() > 0) {
            //BaseApplication.getInstance().stopLoader();
            projectDescription = menuProjectModels.get(0).getProjectDescription();
            //cari unit
            countCategory = menuProjectModels.get(0).getCountCategory();
            countCluster = menuProjectModels.get(0).getCountCluster();
            categoryRef = menuProjectModels.get(0).getCategoryRef();
            clusterRef = menuProjectModels.get(0).getClusterRef();
            //ilustration
            salesStatus = menuProjectModels.get(0).getSalesStatus();
            isBooking = menuProjectModels.get(0).getIsBooking();
            isNUP = menuProjectModels.get(0).getIsNUP();
            isShowAvailableUnit = menuProjectModels.get(0).getIsShowAvailableUnit();
            //maps
            latitude = menuProjectModels.get(0).getLatitude();
            longitude = menuProjectModels.get(0).getLongitude();
            //nup
            nupAmt = menuProjectModels.get(0).getNupAmt();
            linkDetail = menuProjectModels.get(0).getLinkProjectDetail();
            urlVideo = menuProjectModels.get(0).getUrlVideo();
            imageLogo = menuProjectModels.get(0).getImageLogo();
            bookingContact = menuProjectModels.get(0).getBookingContact();

            Log.d(TAG, "imageLogo " + imageLogo);

            if (salesStatus.equals(WebService.salesStatusOpen)) {
                if (!isBooking.equals("0")) {
                    //btnIlustration.setEnabled(true);
                    btnIlustration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bookingAvailable();
                        }
                    });
                } else {
                    btnIlustration.setEnabled(false);
                    btnIlustration.setBackgroundResource(R.drawable.ic_project_no_ilustrasi);
                    //Toast.makeText(ProjectMenuActivity.this, bookingContact, Toast.LENGTH_LONG).show();
                    /*btnIlustration.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bookingNotAvailable();
                        }
                    });*/
                }

                if (!isNUP.equals("0")) {
                    btnNup.setEnabled(true);
                } else {
                    btnNup.setEnabled(false);
                    btnNup.setBackgroundResource(R.drawable.ic_project_no_nup);
                }

            } else {
                btnIlustration.setEnabled(false);
                btnNup.setEnabled(false);
                btnIlustration.setBackgroundResource(R.drawable.ic_project_no_ilustrasi);
                btnNup.setBackgroundResource(R.drawable.ic_project_no_nup);
            }

            //Glide.with(ProjectMenuActivity.this).load(imageLogo).into(imageView);

        } else {
            Toast.makeText(getApplicationContext(), "Maaf anda tidak terkoneksi dengan Jaringan silahkan klik tombol retry", Toast.LENGTH_LONG).show();
        }
    }

    public void showCommisionResults(retrofit2.Response<CommisionStatusModel> response) {
//        progressDialog.dismiss();
//        MainMenuActivity.OFF_LINE_MODE = false;
//        snackBarBuatan.setVisibility(View.GONE);
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        if (status == 200) {
            CommisionDetailModel commisionDetailModel = new CommisionDetailModel();
            commisionDetailModel.setId(dbMasterRef);
            commisionDetailModel.setCommision(response.body().getData().getCommision());
            commisionDetailModel.setDescription(response.body().getData().getDescription());
            commisionDetailModel.setCommissionAmt(response.body().getData().getCommissionAmt());
            CommisionDetailModelDao commisionDetailModelDao = daoSession.getCommisionDetailModelDao();
            commisionDetailModelDao.insertOrReplace(commisionDetailModel);


            commision = response.body().getData().getCommision();
            closingFee = response.body().getData().getClosingFee();
            description = response.body().getData().getDescription();
            commissionAmt = response.body().getData().getCommissionAmt();

            Log.d(TAG, commision + " " + commissionAmt);

            if (Double.parseDouble(commision) == 0) {
                if (commissionAmt == 0.0) {
                    btnCommision.setBackgroundResource(R.drawable.ic_project_no_commision);
                    btnCommision.setEnabled(false);
                } else {
                    btnCommision.setEnabled(true);
                }

            } else if (commissionAmt == 0.0) {
                if (Double.parseDouble(commision) == 0) {
                    btnCommision.setBackgroundResource(R.drawable.ic_project_no_commision);
                    btnCommision.setEnabled(false);
                } else {
                    btnCommision.setEnabled(true);
                }
            } else {
                btnCommision.setEnabled(true);
            }
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }


    }

    public void showCommisionFailure(Throwable t) {
//        progressDialog.dismiss();
//        MainMenuActivity.OFF_LINE_MODE = true;
//        snackBarBuatan.setVisibility(View.VISIBLE);
        btnInformation.setEnabled(false);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        CommisionDetailModelDao commisionDetailModelDao = daoSession.getCommisionDetailModelDao();
        List<CommisionDetailModel> commisionDetailModels = commisionDetailModelDao.queryBuilder().where(CommisionDetailModelDao.Properties.Id.eq(dbMasterRef))
                .list();
        if (commisionDetailModels.size() > 0) {
            commision = commisionDetailModels.get(0).getCommision();
            closingFee = commisionDetailModels.get(0).getClosingFee();
            description = commisionDetailModels.get(0).getDescription();
            commissionAmt = commisionDetailModels.get(0).getCommissionAmt();

            Log.d(TAG, commision + " " + commissionAmt);

            if (Double.parseDouble(commision) == 0) {
                if (commissionAmt == 0.0) {
                    btnCommision.setBackgroundResource(R.drawable.ic_project_no_commision);
                    btnCommision.setEnabled(false);
                } else {
                    btnCommision.setEnabled(true);
                }

            } else if (commissionAmt == 0.0) {
                if (Double.parseDouble(commision) == 0) {
                    btnCommision.setBackgroundResource(R.drawable.ic_project_no_commision);
                    btnCommision.setEnabled(false);
                } else {
                    btnCommision.setEnabled(true);
                }
            } else {
                btnCommision.setEnabled(true);
            }
        }


    }
}