package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class BathRTypeModel {
    String bathRType;
    String bathRTypeName;

    public String getBathRType() {
        return bathRType;
    }

    public void setBathRType(String bathRType) {
        this.bathRType = bathRType;
    }

    public String getBathRTypeName() {
        return bathRTypeName;
    }

    public void setBathRTypeName(String bathRTypeName) {
        this.bathRTypeName = bathRTypeName;
    }
}
