package com.nataproperty.android.nataproperty.adapter.project;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.project.ProductDetailModel;

import java.util.List;

/**
 * Created by User on 4/21/2016.
 */
public class ProjectDetailAdapter extends PagerAdapter {

    Context context;
    //int[] imageId = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image5};
    private List<ProductDetailModel> list;

    public ProjectDetailAdapter(Context context,List<ProductDetailModel> list){
        this.context = context;
        this.list = list;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View viewItem = inflater.inflate(R.layout.item_list_product_detail_image, container, false);

        ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_product_detail_image);
        String dbMasterRef = list.get(position).getDbMasterRef();
        //imageView.setImageResource(imageId[position]);
        /*Picasso.with(context).load(list.get(position))
                .placeholder(R.drawable.wait_image).into(imgDisplay);
*/
        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }

    private class ListProductDetailHolder {
        ImageView productDetailImg;
    }
}
