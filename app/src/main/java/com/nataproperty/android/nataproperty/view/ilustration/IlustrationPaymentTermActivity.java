package com.nataproperty.android.nataproperty.view.ilustration;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.ilustration.IlustrationPaymentTermAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyListView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.PaymentTermModel;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/13/2016.
 */
public class IlustrationPaymentTermActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String IS_BOOKING = "isBooking";

    private ArrayList<PaymentTermModel> listPayment = new ArrayList<PaymentTermModel>();
    private IlustrationPaymentTermAdapter adapter;

    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtPropertyName, txtCategoryType, txtCluster, txtProduct, txtUnitNo, txtArea, txtEstimate;
//    ListView listView;

    String dbMasterRef, projectRef, categoryRef, clusterRef, projectName, productRef, unitRef, isBooking;

    String propertys, category, product, unit, area, priceInc, netPrice, specialEnquiries,bookingContact="";

    Button btnSpecialEnquiries;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;
    MyListView listView;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_payment_term);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        isBooking = intent.getStringExtra(IS_BOOKING);
        initWidget();
        final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);
        Log.d("Cek payment", dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unitRef);
        requestPaymentTerm();

        Log.d("isBooking", " " + isBooking);


        adapter = new IlustrationPaymentTermAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String termRef = listPayment.get(position).getTermRef();
                String termNo = listPayment.get(position).getTermNo();
                String finType = listPayment.get(position).getFinType();
                String dpNo = listPayment.get(position).getDpNo();

                if (Integer.parseInt(finType) == 2) {
                    Intent intentKpr = new Intent(IlustrationPaymentTermActivity.this, IlustrationAddKPRActivity.class);
                    intentKpr.putExtra("dbMasterRef", dbMasterRef);
                    intentKpr.putExtra("projectRef", projectRef);
                    intentKpr.putExtra("categoryRef", categoryRef);
                    intentKpr.putExtra("clusterRef", clusterRef);
                    intentKpr.putExtra("productRef", productRef);
                    intentKpr.putExtra("unitRef", unitRef);
                    intentKpr.putExtra("termRef", termRef);
                    intentKpr.putExtra("termNo", termNo);
                    intentKpr.putExtra("projectName", projectName);

                    intentKpr.putExtra("propertys", propertys);
                    intentKpr.putExtra("category", category);
                    intentKpr.putExtra("product", product);
                    intentKpr.putExtra("unit", unit);
                    intentKpr.putExtra("area", area);
                    intentKpr.putExtra("priceInc", priceInc);

                    intentKpr.putExtra(IS_BOOKING, isBooking);
                    intentKpr.putExtra("finType", Integer.parseInt(finType));
                    intentKpr.putExtra("bookingContact", bookingContact);
                    startActivity(intentKpr);
                } else {
                    Intent intentSchadule = new Intent(IlustrationPaymentTermActivity.this, IlustrationPaymentActivity.class);
                    intentSchadule.putExtra("dbMasterRef", dbMasterRef);
                    intentSchadule.putExtra("projectRef", projectRef);
                    intentSchadule.putExtra("categoryRef", categoryRef);
                    intentSchadule.putExtra("clusterRef", clusterRef);
                    intentSchadule.putExtra("productRef", productRef);
                    intentSchadule.putExtra("unitRef", unitRef);
                    intentSchadule.putExtra("termRef", termRef);
                    intentSchadule.putExtra("termNo", termNo);
                    intentSchadule.putExtra("projectName", projectName);

                    intentSchadule.putExtra("propertys", propertys);
                    intentSchadule.putExtra("category", category);
                    intentSchadule.putExtra("product", product);
                    intentSchadule.putExtra("unit", unit);
                    intentSchadule.putExtra("area", area);
                    intentSchadule.putExtra("priceInc", priceInc);

                    intentSchadule.putExtra(IS_BOOKING, isBooking);
                    intentSchadule.putExtra("finType", Integer.parseInt(finType));
                    intentSchadule.putExtra("bookingContact", bookingContact);
                    startActivity(intentSchadule);
                }

            }
        });

        btnSpecialEnquiries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationPaymentTermActivity.this);
                LayoutInflater inflater = IlustrationPaymentTermActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Special Enquiries");
                txtspecialEnquiries.setText(specialEnquiries);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_payment_term));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        btnSpecialEnquiries = (Button) findViewById(R.id.btn_specialEnquiries);
        listView = (MyListView) findViewById(R.id.list_payment_term);
        btnSpecialEnquiries.setTypeface(font);

    }

    public void requestPaymentTerm() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getPaymentTerm(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("payment", response);

                    if (status == 200) {
                        propertys = jo.getJSONObject("data").getString("propertys");
                        category = jo.getJSONObject("data").getString("category");
                        product = jo.getJSONObject("data").getString("product");
                        unit = jo.getJSONObject("data").getString("unit");
                        area = jo.getJSONObject("data").getString("area");
                        priceInc = jo.getJSONObject("data").getString("priceInc");
                        specialEnquiries = jo.getJSONObject("data").getString("specialEnquiries");
                        bookingContact = jo.getJSONObject("data").getString("bookingContact");

                        Log.d("cek result", propertys + " " + category + " " + product + " " + unit + " " + area + " " + priceInc
                                + " " + bookingContact);
                        txtPropertyName.setText(propertys);
                        txtCategoryType.setText(category);
                        txtProduct.setText(product);
                        txtUnitNo.setText(unit);
                        txtArea.setText(Html.fromHtml(area));
                        txtEstimate.setText(priceInc);

                        JSONArray jsonArray = new JSONArray(jo.getJSONArray("dataList").toString());
                        generatePaymentIlustration(jsonArray);

                        if (!specialEnquiries.equals("")) {
                            btnSpecialEnquiries.setVisibility(View.VISIBLE);
                        }

                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(IlustrationPaymentTermActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(IlustrationPaymentTermActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "Term");

    }

    private void generatePaymentIlustration(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                PaymentTermModel payment = new PaymentTermModel();
                payment.setTermRef(joArray.getString("termRef"));
                payment.setTermNo(joArray.getString("termNo"));
                payment.setTermName(joArray.getString("termName"));
                payment.setDiscount(joArray.getString("discount"));
                payment.setDownPayment(joArray.getString("downPayment"));
                payment.setPriceIncTerm(joArray.getString("priceIncTerm"));
                payment.setNetPrice(joArray.getString("netPrice"));
                payment.setFinType(joArray.getString("finType"));
                payment.setDpNo(joArray.getString("dpNo"));
                listPayment.add(payment);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationPaymentTermActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
