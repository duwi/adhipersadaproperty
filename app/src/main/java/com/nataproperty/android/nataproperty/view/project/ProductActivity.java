package com.nataproperty.android.nataproperty.view.project;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.project.ProductAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyGridView;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.ProductModel;
import com.nataproperty.android.nataproperty.model.project.ProductModelDao;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.before_login.LaunchActivity;
import com.nataproperty.android.nataproperty.view.nup.NupTermActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/4/2016.
 */
public class ProductActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TITLE_PRODUCT = "titleProduct";

    public static final String BATHROOM = "bathroom";
    public static final String BEDROOM = "bedroom";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String IS_NUP = "isNUP";
    public static final String IS_BOOKING = "isBooking";
    public static final String NUP_AMT = "nupAmt";

    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";

    public static final String IMAGE_LOGO = "imageLogo";

    private List<ProductModel> listProduct = new ArrayList<ProductModel>();
    private ProductAdapter adapter;

    private MyGridView listView;

    ImageView imgLogo;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    TextView txtProjectName;
    Button btnNUP;

    long dbMasterRef;
    String projectRef, clusterRef, productRef, projectName, bathroom, bedroom, categoryRef, titleProduct;
    String newClusterRef;
    String isNUP, nupAmt, isBooking, imageLogo;
    String isShowAvailableUnit;

    private double latitude, longitude;
    Display display;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
//        if (MainMenuActivity.OFF_LINE_MODE) {
//            snackBarBuatan.setVisibility(View.VISIBLE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ProductActivity.this, LaunchActivity.class));
                    finish();
                }
            });
//        }
//        else {
//            snackBarBuatan.setVisibility(View.GONE);
//        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Product");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);

        Log.d("cek productActivity", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef);

        latitude = getIntent().getDoubleExtra(LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(LONGITUDE, 0);

        Log.d("cek productActivity", latitude + " " + longitude);

        isNUP = intent.getStringExtra(IS_NUP);
        isBooking = intent.getStringExtra(IS_BOOKING);
        nupAmt = getIntent().getStringExtra(NUP_AMT);

        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);

        imageLogo = intent.getStringExtra(IMAGE_LOGO);


        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);

        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        btnNUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNup = new Intent(ProductActivity.this, NupTermActivity.class);
                intentNup.putExtra(PROJECT_REF, projectRef);
                intentNup.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intentNup.putExtra(NUP_AMT, nupAmt);
                intentNup.putExtra("imageLogo", imageLogo);
                startActivity(intentNup);
            }
        });

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_product);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productRef = String.valueOf(listProduct.get(position).getProductRef());
                newClusterRef = listProduct.get(position).getClusterRef();
                bathroom = listProduct.get(position).getNumOfBathrooms();
                bedroom = listProduct.get(position).getNumOfBedrooms();
                titleProduct = listProduct.get(position).getTitleProduct();

                Intent intent = new Intent(ProductActivity.this, ProductDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CLUSTER_REF, newClusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(CATEGORY_REF, categoryRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(TITLE_PRODUCT, titleProduct);

                intent.putExtra(BATHROOM, bathroom);
                intent.putExtra(BEDROOM, bedroom);

                intent.putExtra(LATITUDE, latitude);
                intent.putExtra(LONGITUDE, longitude);

                intent.putExtra(IS_NUP, isNUP);
                intent.putExtra(IS_BOOKING, isBooking);
                intent.putExtra(NUP_AMT, nupAmt);

                intent.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);

                intent.putExtra(IMAGE_LOGO, imageLogo);
                Log.d("cek new clusterRef", newClusterRef);
                startActivity(intent);
            }
        });

        requestProduct();
    }

    public void requestProduct() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProduct(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MainMenuActivity.OFF_LINE_MODE=false;
                snackBarBuatan.setVisibility(View.GONE);
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListProduct(jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ProductModel productModel = gson.fromJson("" + data, ProductModel.class);
                        productModel.setDbMasterRef(data.optString("dbMasterRef"));
                        productModel.setProjectRef(data.optString("projectRef"));
                        productModel.setClusterRef(data.optString("clusterRef"));
                        productModel.setProductRef(data.optLong("productRef"));
                        productModel.setNumOfBathrooms(data.optString("numOfBathrooms"));
                        productModel.setNumOfBedrooms(data.optString("numOfBedrooms"));
                        productModel.setTitleProduct(data.optString("titleProduct"));
                        productModel.setProductDescription(data.optString("productDescription"));
                        productModel.setNumOfAdditionalrooms(data.optString("numOfAdditionalrooms"));
                        productModel.setNumOfAdditionalBathrooms(data.optString("numOfAdditionalBathrooms"));
                        productModel.setImage(data.getString("image"));
                        ProductModelDao productModelDao = daoSession.getProductModelDao();
                        productModelDao.insertOrReplace(productModel);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainMenuActivity.OFF_LINE_MODE=true;
                        snackBarBuatan.setVisibility(View.VISIBLE);
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        ProductModelDao productModelDao = daoSession.getProductModelDao();
                        List<ProductModel> productModels = productModelDao.queryBuilder()
                                .where(ProductModelDao.Properties.DbMasterRef.eq(dbMasterRef),
                                        ProductModelDao.Properties.ClusterRef.eq(clusterRef)).list();
                        int numData = productModels.size();
                        if (numData > 0) {
                            for (int i = 0; i < numData; i++) {
                                ProductModel productModel = productModels.get(i);
                                listProduct.add(productModel);

                            }

                            initListView();
                        } else {
                            finish();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("categoryRef", categoryRef);
                params.put("clusterRef", clusterRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "product");

    }

    private void generateListProduct(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProductModel product = new ProductModel();
                product.setDbMasterRef(jo.getString("dbMasterRef"));
                product.setProjectRef(jo.getString("projectRef"));
                product.setClusterRef(jo.getString("clusterRef"));
                product.setProductRef(jo.getLong("productRef"));
                product.setNumOfBathrooms(jo.getString("numOfBathrooms"));
                product.setNumOfBedrooms(jo.getString("numOfBedrooms"));
                product.setTitleProduct(jo.getString("titleProduct"));
                product.setProductDescription(jo.getString("productDescription"));
                product.setNumOfAdditionalrooms(jo.getString("numOfAdditionalrooms"));
                product.setNumOfAdditionalBathrooms(jo.getString("numOfAdditionalBathrooms"));
                product.setImage(jo.getString("image"));

                listProduct.add(product);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        initListView();
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new ProductAdapter(this, listProduct, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }
        return super.onOptionsItemSelected(item);

    }
}
