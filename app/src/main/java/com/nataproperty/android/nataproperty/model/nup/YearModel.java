package com.nataproperty.android.nataproperty.model.nup;

/**
 * Created by User on 5/22/2016.
 */
public class YearModel {
    String year;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
