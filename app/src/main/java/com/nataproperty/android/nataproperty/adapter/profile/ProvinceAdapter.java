package com.nataproperty.android.nataproperty.adapter.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.profile.ProvinceModel;

import java.util.List;

/**
 * Created by User on 4/17/2016.
 */
public class ProvinceAdapter extends BaseAdapter {
    private Context context;
    private List<ProvinceModel> list;
    private ListProvinceHolder holder;

    public ProvinceAdapter(Context context, List<ProvinceModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_province,null);
            holder = new ListProvinceHolder();
            holder.provinceName = (TextView) convertView.findViewById(R.id.proviceName);

            convertView.setTag(holder);
        }else{
            holder = (ListProvinceHolder) convertView.getTag();
        }
        ProvinceModel country = list.get(position);
        holder.provinceName.setText(country.getProvinceName());

        return convertView;
    }

    private class ListProvinceHolder {
        TextView provinceName;
    }
}
