package com.nataproperty.android.nataproperty.adapter.listing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyGalleryModel;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User on 5/11/2016.
 */
public class ListingPropertyDetailImageAdapter extends PagerAdapter {
    public static final String TAG = "NewsDetailImageAdapter";

    Context context;
    private List<ListingPropertyGalleryModel> list;

    PhotoViewAttacher attacher;

    public ListingPropertyDetailImageAdapter(Context context, List<ListingPropertyGalleryModel> list){
        this.context = context;
        this.list = list;
    }

    String projectName;
    String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_gallery_detail_image, container, false);
        final ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_gallery_detail_image);
        ImageView download = (ImageView) viewItem.findViewById(R.id.btn_shared);

        ListingPropertyGalleryModel image = list.get(position);
        projectName = image.getImgTitle();

        Glide.with(context).load(image.getImgLink()).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);

        //zoom
        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();

                String root = Environment.getExternalStorageDirectory().toString();
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();
               /*Random gen = new Random();
                int n = 10000;
                n = gen.nextInt(n);*/
                String fotoname = projectName+"-"+fileName+".jpg";
                File file = new File (wallpaperDirectory, fotoname);
                if (file.exists ()) file.delete ();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                    //Toast.makeText(context, "Saved to your folder", Toast.LENGTH_SHORT ).show();
                   /* File filePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + filename.toString() + "." + extension.toString());*/
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/nataproperty/" + projectName+"-"+fileName+".jpg";
                    File imageFileToShare = new File(imagePath);
                    Uri uri = Uri.fromFile(imageFileToShare);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    context.startActivity(Intent.createChooser(share, "Share Image!"));

                } catch (Exception e) {

                }

            }

        });

        Log.d(TAG,""+image.getImgLink());

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }

    
}
