package com.nataproperty.android.nataproperty.config;

/**
 * Created by Administrator on 3/31/2016.
 */
public class WebService {
    //SERVER NATAPROPERTY
//    private static final String URL= "http://www.nataproperty.com/services/appsServices.asmx/";
//    public static final String URL_IMAGE= "http://www.nataproperty.com/services/Assets/support/displayImage.aspx";
//    public static final String URL_DOWNLOAD= "http://www.nataproperty.com/services/Assets/support/displayAttachment.aspx";

    //local taufik
//    private static final String URL = "http://192.168.10.186/services/AppsServices.asmx/";
//    public static final String URL_IMAGE = "http://192.168.10.186/services/Assets/support/displayImage.aspx";
//    public static final String URL_DOWNLOAD = "http://192.168.10.186/services/Assets/support/displayAttachment.aspx";

    //local taufik
    private static final String URL = "http://192.168.10.128/services/AppsServices.asmx/";
    public static final String URL_IMAGE = "http://192.168.10.128/services/Assets/support/displayImage.aspx";
    public static final String URL_DOWNLOAD = "http://192.168.10.128/services/Assets/support/displayAttachment.aspx";

    //local taufik iphone
    /*private static final String URL = "http://192.168.10.252/services/AppsServices.asmx/";
    public static final String URL_IMAGE = "http://192.168.10.252/services/Assets/support/displayImage.aspx";
    public static final String URL_DOWNLOAD = "http://192.168.10.252/services/Assets/support/displayAttachment.aspx";*/

   /* private static final String URL = "http://192.168.43.83/services/AppsServices.asmx/";
    public static final String URL_IMAGE = "http://192.168.43.83/services/Assets/support/displayImage.aspx";
    public static final String URL_DOWNLOAD = "http://192.168.43.83/services/Assets/support/displayAttachment.aspx";*/


    /**
     * veritrans
     */
    //set environment
    public static final boolean VT_IsProduction = false;

    //set client key taufik
    public static final String CLIENT_KEY = "VT-client-PkleEH_bqIsnaNpS";

    //set client key server
    /*public static final String CLIENT_KEY = " VT-client-1o3GNZiWcbD9x6Ye";*/

    //Notification
    public static final String notificationDowloadProject = "1";
    public static final String notificationNews = "2";
    public static final String notificationNewProject = "3";
    public static final String notificationGeneral = "4";
    public static final String notificationUpdate = "5";
    public static final String notificationEventDetail = "8";
    public static final String notificationListingProperty = "9";

    public static final String notificationChat = "C";
    public static final String notificationApprovalProject = "AP";

    //updateApp
    public static final String linkAppStore = "https://play.google.com/store/apps/details?id=com.nataproperty.android.nataproperty";
    public static final String linkRegisterAgency = "http://www.nataproperty.com/agency";
    public static final String linkeditMaps = "http://www.nataproperty.com/preview-listing/";

    public static final String salesStatusPreparation = "1";
    public static final String salesStatusOpen = "2";

    //PaymentMethod
    public static final String updatePaymentTypeCreditCard = "1";
    public static final String updatePaymentTypeCimb = "2";
    public static final String updatePaymentTypeBCA = "3";
    public static final String updatePaymentTypeMandiri = "4";
    public static final String updatePaymentTypeBankTransfer = "7";

    //ListingProperty
    public static final String listingStatus = "1";

    //Verison
    private static String version = "getAppVersionSvc";

    //Project
    private static String project = "getListProjectSvc";
    private static String projectKeyword = "getListProjectKeywordSvc";
    private static String projectMenu = "getListProjectSvc";
    private static String projectDetail = "getProjectDetailSvc";
    private static String category = "getListCategorySvc";
    private static String cluster = "getListClusterSvc";
    private static String product = "getListProductSvc";
    private static String productDetail = "getProductDetailSvc";
    private static String productListImage = "getListProductImageSvc";
    private static String feature = "getListFeatureSvc";
    private static String commision = "getProjectCommisionSvc";
    private static String subscribe = "subscribeProjectSvc";
    private static String UnSubscribe = "unSubscribe";
    private static String subscribeProjectInhouse = "subscribeProjectInhouseSvc";
    private static String
            GetLocationLookup = "GetLocationLookupSvc";

    private static String projectImage = "?is=project&dr=";
    private static String categoryImage = "?is=category&dr=";
    private static String clusterImage = "?is=cluster&dr=";
    private static String productImage = "?is=product&dr=";
    private static String logoProject = "?is=projectlogo&dr=";

    private static String profileImage = "?is=profile&dt=5&mr=";
    private static String login = "cekLogin";
    private static String registerAgency = "registerAgency";
    private static String registerSalesAgent = "registerSalesAgent";
    private static String registerInhouse = "registerInhouse";
    private static String signInGoogle = "cekRegisterByGoogle";

    private static String companyCode = "getAgencyCompanyInfoByCode"; //companycode
    private static String agencyCompany = "getListAgencyCompanySvc"; //company
    private static String forgotPassword = "forgotPassword";
    private static String aboutMe = "updateAboutMeSvc";
    private static String ktp = "?is=ktp&dr=";
    private static String npwp = "?is=npwp&dr=";
    private static String deleteKtp = "deleteKTPSvc";
    private static String deleteNpwp = "deleteNPWPSvc";
    private static String contactUs = "contactUsSvc";
    private static String cekEmailRegisterSvc = "cekEmailRegisterSvc";

    private static String memberType = "getMemberType";
    //addr
    private static String country = "getCountryListSvc";
    private static String province = "getProvinceListSvc";
    private static String city = "getCityListSvc";
    private static String subLocation = "getSubLocationListSvc";

    private static String memberInfo = "getMemberInfoSvc";
    private static String others = "getOtherProfileSvc";

    private static String profileReuired = "updateRequiredProfileSvc";
    private static String idAddress = "updateIDAddressSvc";
    private static String changePassword = "changePasswordSvc";
    private static String upload = "updateImageProfileSvc";
    private static String updateImageKtpSvc = "updateImageKtpSvc";
    private static String updateImageNpwpSvc = "updateImageNpwpSvc";
    private static String updateOtherProfileSvc = "updateOtherProfileSvc";
    private static String saveFeedback = "saveFeedbackSvc";
    private static String updateAccontBank = "updateAccountBankSvc";

    //ilustration
    private static String ilustration = "getListPaymentIlustrationSvc";
    private static String clusterInfo = "getClusterInfoSvc";
    private static String paymentTerm = "getIlustrationPaymentTermSvc";
    private static String payment = "getIlustrationPaymentSvc";
    private static String paymentKPR = "getIlustrationPaymentKPRSvc";
    private static String blockMapping = "getListBlockSvc";
    private static String unitMapping = "getListUnitMapingSvc";
    private static String sendToFriend = "sendToFriend";

    private static String getDiagramColorSvc = "getDiagramColorSvc";

    private static String myListing = "getMyListingProjectSvc";
    private static String download = "getListDownloadSvc";

    //nup
    private static String termAndConditionNUP = "getTermAndConditionNUPSvc";
    private static String costumer = "searchCustomerSvc";
    private static String costumerRegister = "registerNewCustomerSvc";
    private static String saveOrderNup = "saveOrderNUPSvc";
    private static String bank = "getListBankSvc";
    private static String saveBankTranfer = "savePaymentBankTransferSvc";
    private static String getOrderInfo = "getOrderInfoSvc";
    private static String mounth = "getListMonthSvc";
    private static String year = "getListYearSvc";
    private static String veritrans = "veritransSvc";
    private static String accountBank = "getProjectAccountBank";
    private static String updatePaymentTypeOrder = "updatePaymentTypeOrderSvc";
    private static String updatePaymentTypeOrderVT = "updatePaymentTypeOrderVTSvc";
    private static String GetLookupSurveyNUP = "GetLookupSurveyNUPSvc";

    //booking
    private static String termAndConditionBooking = "getTermAndConditionBookingSvc";
    private static String saveOrderBooking = "saveOrderBookingSvc";
    private static String savePaymentBookingTranfer = "savePaymentBookingBankTransferSvc";
    private static String veritransBooking = "veritransBookingSvc";
    private static String unitDiagram = "getListUnitSvc";
    private static String veritransBookingVT = "veritransBookingVTSvc";

    //My Nup
    private static String getListNup = "getListTypeNUP";
    private static String listUnPaid = "getUnpaidNUPSvc";
    private static String listPaid = "getPaidNUPSvc";
    private static String getNeedAttentionNUP = "getNeedAttentionNUPSvc";
    private static String getWaitingVerification = "getWaitingVerificationSvc";
    private static String generateNUP = "generateNUPSvc";

    //News
    private static String listNews = "getListNewsSvc";
    private static String listNewsPaging = "getListNewsPagingSvc";
    private static String listNewsProject = "getListNewsProjectSvc";
    private static String newsDetail = "getNewsImageSliderSvc";

    //Event
    private static String getListEvent = "getListEventSvc";
    private static String getListEventSchadule = "getListEventScheduleSvc";

    //MyBooking
    private static String getListBooking = "getListBookingSvc";
    private static String getBookingPaymentInfoSvc = "getBookingPaymentInfoSvc";
    private static String getDetailBooking = "getBookingDetailSvc";
    private static String getListPaymentSchedule = "getListPaymentScheduleSvc";

    //GCM
    private static String saveGCMToken = "saveGCMTokenSvc";
    private static String getGCMMessageSvc = "getGCMMessageSvc";

    //RSVP
    private static String registerRsvp = "saveRsvpSvc";
    private static String getListTiketRsvp = "getListTiketRsvp";
    private static String getListTiketQRCode = "getListTiketQRCode";

    //GCM
    //private static String getListNotification = "getListNotificationSvc";
    private static String getListNotification = "getListNotificationSSTSvc";

    //calculate
    private static String getListCalcUserProject = "getListCalcUserProject";
    private static String saveNewCalcUserProject = "saveNewCalcUserProject";
    private static String getListTemplateProject = "getListTemplateProjectSvc";
    private static String savePaymentTermCalcUserProject = "savePaymentTermCalcUserProject";
    private static String getInfoPayment = "getInfoPaymentTermCalcUser";
    private static String getListCalcUserProjectIlustration = "getListCalcUserProjectIlustration";
    private static String getListCalcCluster = "getListCalcCluster";
    private static String getListCalcProduct = "getListCalcProduct";
    private static String getListCalcBlock = "getListCalcBlock";
    private static String getInfoCalcUserProject = "getInfoCalcUserProject";
    private static String deleteTemplateCalcUserProject = "deleteTemplateCalcUserProject";
    private static String deleteProjectCalcUserProject = "deleteProjectCalcUserProject";
    private static String sendToFriendCalcUserProject = "sendToFriendCalcUserProject";

    /*Gallery*/
    private static String getListGroupGallery = "getListGroupGallerySvc";
    private static String getListProjectGallery = "getListProjectGallerySvc";

    /*ListingProperty*/
    private static String getListingInfo = "getListingInfoSvc";
    private static String registerAgencyByCode = "registerAgencyByCode";
    private static String getListingCompanyInfo = "getListingCompanyInfoSvc";
    private static String getListingMemberInfo = "getListingMemberInfoSvc";
    private static String getListingMemberInfoSearch = "getListingMemberInfoFilterSvc";
    private static String getListingSearchMemberInfo = "getListingMemberInfoKeywordSvc";

    private static String getListListingProperty = "getListingPropertySvc";
    private static String getListSearchListingProperty = "getListingPropertyKeywordSvc";
    private static String getListingPropertyInfo = "getListingPropertyInfoSvc";
    private static String getListListingPropertyGallery = "getListListingPropertyGallerySvc";
    private static String getListingPropertyListFacility = "getListingPropertyListFacilitySvc";
    private static String getListingPropertyDownload = "getListingPropertyDownloadSvc";
    private static String sendToFriendListingProperty = "sendToFriendListingPropertySvc";
    private static String getListingAddProperty = "getListingAddPropertySvc";
    private static String getListingAddPropertyName = "getListingAddPropertyNameSvc";
    private static String insertPropertyListing = "insertPropertyListingSvc";
    private static String updatePropertyListing = "updatePropertyListingSvc";
    private static String getListingAddFacility = "getListingAddFacilitySvc";
    private static String insertFacilityPropertyListing = "insertFacilityPropertyListingSvc";
    private static String insertImagePropertyListing = "insertImagePropertyListingSvc";
    private static String deleteImageListingProperty = "deleteImageListingPropertySvc";
    private static String deleteListingProperty = "deleteListingPropertySvc";
    private static String updateStatusListingProperty = "updateStatusListingPropertySvc";
    private static String sendNotifToAgencyMember = "sendNotifToAgencyMemberSvc";
    private static String GetLocationLookupListing = "GetLocationLookupListingSvc";


    //VersionCode
    public static String getVersionCode() {
        return URL + version;
    }

    public static String getLogin() {
        return URL + login;
    }

    public static String getRegisterAgency() {
        return URL + registerAgency;
    }

    public static String getRegisterSalesAgent() {
        return URL + registerSalesAgent;
    }

    public static String getRegisterInhouse() {
        return URL + registerInhouse;
    }

    public static String signInGoogle() {
        return URL + signInGoogle;
    }

    public static String getCompanyCode() {
        return URL + companyCode;
    }

    public static String getAgencyCompany() {
        return URL + agencyCompany;
    }

    public static String getForgotPassword() {
        return URL + forgotPassword;
    }

    public static String getMemberType() {
        return URL + memberType;
    }

    public static String cekEmailRegisterSvc() {
        return URL + cekEmailRegisterSvc;
    }

    public static String getMemberInfo() {
        return URL + memberInfo;
    }

    public static String getProfile() {
        return URL_IMAGE + profileImage;
    }

    public static String getOtherProfile() {
        return URL + others;
    }

    public static String profileReuired() {
        return URL + profileReuired;
    }

    public static String idAddress() {
        return URL + idAddress;
    }

    public static String changePassword() {
        return URL + changePassword;
    }

    public static String getAboutMe() {
        return URL + aboutMe;
    }

    public static String getKtp() {
        return URL_IMAGE + ktp;
    }

    public static String getNpwp() {
        return URL_IMAGE + npwp;
    }

    public static String deleteKtp() {
        return URL + deleteKtp;
    }

    public static String deleteNpwp() {
        return URL + deleteNpwp;
    }

    public static String getContactUs() {
        return URL + contactUs;
    }

    public static String saveFeedback() {
        return URL + saveFeedback;
    }

    public static String updateAccontBank() {
        return URL + updateAccontBank;
    }

    public static String updateImageProfileSvc() {
        return URL + upload;
    }

    public static String updateImageKtpSvc() {
        return URL + updateImageKtpSvc;
    }

    public static String updateImageNpwpSvc() {
        return URL + updateImageNpwpSvc;
    }

    public static String updateOtherProfileSvc() {
        return URL + updateOtherProfileSvc;
    }

    public static String getCountry() {
        return URL + country;
    }

    public static String getProvince() {
        return URL + province;
    }

    public static String getCity() {
        return URL + city;
    }

    public static String getSubLocation() {
        return URL + subLocation;
    }

    public static String getProject() {
        return URL + project;
    }

    public static String getProjectKeyword() {
        return URL + projectKeyword;
    }

    public static String getProjectMenu() {
        return URL + projectMenu;
    }

    public static String getProjectDetail() {
        return URL + projectDetail;
    }

    public static String getCategory() {
        return URL + category;
    }

    public static String getCluster() {
        return URL + cluster;
    }

    public static String getProduct() {
        return URL + product;
    }

    public static String getProductDetail() {
        return URL + productDetail;
    }

    public static String getListProductImage() {
        return URL + productListImage;
    }

    public static String getFeatureList() {
        return URL + feature;
    }

    public static String GetLocationLookup() {
        return URL + GetLocationLookup;
    }

    //ilustration
    public static String getIlustration() {
        return URL + ilustration;
    }

    public static String getClusterInfoSvc() {
        return URL + clusterInfo;
    }

    public static String getPaymentTerm() {
        return URL + paymentTerm;
    }

    public static String getPayment() {
        return URL + payment;
    }

    public static String getPaymentKPR() {
        return URL + paymentKPR;
    }

    public static String getListBlockMapping() {
        return URL + blockMapping;
    }

    public static String getListUnitMapping() {
        return URL + unitMapping;
    }

    public static String getDiagramColorSvc() {
        return URL + getDiagramColorSvc;
    }


    public static String sendToFriend() {
        return URL + sendToFriend;
    }

    public static String getMyListing() {
        return URL + myListing;
    }

    public static String getListDownload() {
        return URL + download;
    }

    public static String getCommision() {
        return URL + commision;
    }

    public static String subscribeProject() {
        return URL + subscribe;
    }

    public static String subscribeProjectInhouse() {
        return URL + subscribeProjectInhouse;
    }

    public static String unSubscribeProject() {
        return URL + UnSubscribe;
    }

    //Nup
    public static String getTermAndCondition() {
        return URL + termAndConditionNUP;
    }

    public static String getCostumer() {
        return URL + costumer;
    }

    public static String getCostumerRegister() {
        return URL + costumerRegister;
    }

    public static String saveOrderNup() {
        return URL + saveOrderNup;
    }

    public static String getBank() {
        return URL + bank;
    }

    public static String getSaveBankTranfer() {
        return URL + saveBankTranfer;
    }

    public static String getOrderInfo() {
        return URL + getOrderInfo;
    }

    public static String getMounth() {
        return URL + mounth;
    }

    public static String getYear() {
        return URL + year;
    }

    public static String getVeritransNUP() {
        return URL + veritrans;
    }

    public static String getAccountBank() {
        return URL + accountBank;
    }

    public static String updatePaymentTypeOrder() {
        return URL + updatePaymentTypeOrder;
    }

    public static String updatePaymentTypeOrderVT() {
        return URL + updatePaymentTypeOrderVT;
    }

    public static String GetLookupSurveyNUP() {
        return URL + GetLookupSurveyNUP;
    }

    //Booking
    public static String getTermAndConditionBooking() {
        return URL + termAndConditionBooking;
    }

    public static String saveOrderBooking() {
        return URL + saveOrderBooking;
    }

    public static String getSaveBookingBankTranfer() {
        return URL + savePaymentBookingTranfer;
    }

    public static String getVeritransBooking() {
        return URL + veritransBooking;
    }

    public static String getListUnitDiagram() {
        return URL + unitDiagram;
    }

    public static String veritransBookingVT() {
        return URL + veritransBookingVT;
    }


    //myNup
    public static String getListNup() {
        return URL + getListNup;
    }

    public static String getListUnPaid() {
        return URL + listUnPaid;
    }

    public static String getListPaid() {
        return URL + listPaid;
    }

    public static String getNeedAttentionNUP() {
        return URL + getNeedAttentionNUP;
    }

    public static String getWaitingVerification() {
        return URL + getWaitingVerification;
    }

    public static String generateNUP() {
        return URL + generateNUP;
    }

    //News
    public static String getNews() {
        return URL + listNews;
    }

    public static String getNewsPaging() {
        return URL + listNewsPaging;
    }

    public static String getNewsProject() {
        return URL + listNewsProject;
    }

    public static String getImageSlider() {
        return URL + newsDetail;
    }

    //Event
    public static String getListEvent() {
        return URL + getListEvent;
    }

    public static String getListEventSchadule() {
        return URL + getListEventSchadule;
    }

    //myBooking
    public static String getListBooking() {
        return URL + getListBooking;
    }

    public static String getBookingPaymentInfo() {
        return URL + getBookingPaymentInfoSvc;
    }


    public static String getDetailBooking() {
        return URL + getDetailBooking;
    }

    public static String getListPaymentSchedule() {
        return URL + getListPaymentSchedule;
    }

    //GCM
    public static String getSaveGCMToken() {
        return URL + saveGCMToken;
    }

    public static String getGCMMessage() {
        return URL + getGCMMessageSvc;
    }

    //RSVP
    public static String registerRsvp() {
        return URL + registerRsvp;
    }

    public static String getListTiketRsvp() {
        return URL + getListTiketRsvp;
    }

    public static String getListTiketQRCode() {
        return URL + getListTiketQRCode;
    }

    //Notification
    public static String getListNotification() {
        return URL + getListNotification;
    }

    //calculate
    public static String getListCalcUserProject() {
        return URL + getListCalcUserProject;
    }

    public static String getListTemplateProject() {
        return URL + getListTemplateProject;
    }

    public static String saveNewCalcUserProject() {
        return URL + saveNewCalcUserProject;
    }

    public static String savePaymentTermCalcUserProject() {
        return URL + savePaymentTermCalcUserProject;
    }

    public static String getInfoPayment() {
        return URL + getInfoPayment;
    }

    public static String getListCalcUserProjectIlustration() {
        return URL + getListCalcUserProjectIlustration;
    }

    public static String getListCalcCluster() {
        return URL + getListCalcCluster;
    }

    public static String getListCalcProduct() {
        return URL + getListCalcProduct;
    }

    public static String getListCalcBlock() {
        return URL + getListCalcBlock;
    }

    public static String getInfoCalcUserProject() {
        return URL + getInfoCalcUserProject;
    }

    public static String deleteTemplateCalcUserProject() {
        return URL + deleteTemplateCalcUserProject;
    }

    public static String deleteProjectCalcUserProject() {
        return URL + deleteProjectCalcUserProject;
    }

    public static String sendToFriendCalcUserProject() {
        return URL + sendToFriendCalcUserProject;
    }


    //image
    public static String getProjectImage() {
        return URL_IMAGE + projectImage;
    }

    public static String getCategoryImage() {
        return URL_IMAGE + categoryImage;
    }

    public static String getClusterImage() {
        return URL_IMAGE + clusterImage;
    }

    public static String getProductImage() {
        return URL_IMAGE + productImage;
    }

    public static String getProductImageDetail() {
        return URL_IMAGE;
    }

    public static String getLogoProject() {
        return URL_IMAGE + logoProject;
    }

    //gallery
    public static String getListGroupGallery() {
        return URL + getListGroupGallery;
    }

    public static String getListProjectGallery() {
        return URL + getListProjectGallery;
    }

    //listing
    public static String getListListingProperty() {
        return URL + getListListingProperty;
    }

    public static String getListSearchListingProperty() {
        return URL + getListSearchListingProperty;
    }

    public static String getListingInfo() {
        return URL + getListingInfo;
    }

    public static String registerAgencyByCode() {
        return URL + registerAgencyByCode;
    }

    public static String getListingCompanyInfo() {
        return URL + getListingCompanyInfo;
    }

    public static String getListingMemberInfo() {
        return URL + getListingMemberInfo;
    }

    public static String getListingMemberInfoSearch() {
        return URL + getListingMemberInfoSearch;
    }

    public static String getListingSearchMemberInfo() {
        return URL + getListingSearchMemberInfo;
    }

    public static String getListingPropertyInfo() {
        return URL + getListingPropertyInfo;
    }

    public static String getListListingPropertyGallery() {
        return URL + getListListingPropertyGallery;
    }

    public static String getListingPropertyListFacility() {
        return URL + getListingPropertyListFacility;
    }

    public static String getListingPropertyDownload() {
        return URL + getListingPropertyDownload;
    }

    public static String sendToFriendListingProperty() {
        return URL + sendToFriendListingProperty;
    }

    public static String getListingAddProperty() {
        return URL + getListingAddProperty;
    }

    public static String getListingAddPropertyName() {
        return URL + getListingAddPropertyName;
    }

    public static String insertPropertyListing() {
        return URL + insertPropertyListing;
    }

    public static String updatePropertyListing() {
        return URL + updatePropertyListing;
    }

    public static String getListingAddFacility() {
        return URL + getListingAddFacility;
    }

    public static String insertFacilityPropertyListing() {
        return URL + insertFacilityPropertyListing;
    }

    public static String insertImagePropertyListing() {
        return URL + insertImagePropertyListing;
    }

    public static String deleteImageListingProperty() {
        return URL + deleteImageListingProperty;
    }

    public static String deleteListingProperty() {
        return URL + deleteListingProperty;
    }

    public static String updateStatusListingProperty() {
        return URL + updateStatusListingProperty;
    }

    public static String sendNotifToAgencyMember() {
        return URL + sendNotifToAgencyMember;
    }

    public static String GetLocationLookupListing() {
        return URL + GetLocationLookupListing;
    }
}
