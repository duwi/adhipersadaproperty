package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterSearchListingInteractor;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyStatus;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.listing.ListingSearchPropertyDtl;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class SearchListingPresenter implements PresenterSearchListingInteractor {
    private ListingSearchPropertyDtl view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public SearchListingPresenter(ListingSearchPropertyDtl view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getSearchListing(String agencyCompanyRef, String listingStatus, String pageNo, String provinceCode, String cityCode, String locationRef, String listingTypeRef, String categoryType,String memberRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingSearch(agencyCompanyRef,listingStatus,pageNo,provinceCode,cityCode,locationRef,listingTypeRef,categoryType, memberRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showSearchResults(response);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showSearchFailure(t);

            }


        });
    }

    @Override
    public void getListingCobroking(String agencyCompanyRef, String listingStatus, String pageNo, String provinceCode, String cityCode, String locationRef, String listingTypeRef, String categoryType, String memberRef) {

    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
