package com.nataproperty.android.nataproperty.view.nup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/18/2016.
 */
public class NupInformasiCustomerActivity extends AppCompatActivity {
    public static final String TAG = "NupInformasiCustomer";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";
    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String STATUS = "status";

    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String QTY = "qty";
    public static final String TOTAL = "total";

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtPriortyPrice, txtTotal;
    EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId, editQty;
    ImageView imgKtp, imgNpwp;

    Button btnUploadKtp, btnUploadNpwp, btnNext;

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address;
    private String dbMasterRef, projectRef, projectName, memberCustomerRef, nupAmt, projectDescription, status;
    private String type;
    private String ktpImg, npwpImg;
    String defaultQty = "1";
    double gTotal;

    Bitmap bitmapKtp, bitmapNpwp;

    Uri file;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_informasi_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_buy_nup));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        Log.d("Cek Member Costumer", memberCustomerRef);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        projectName = intent.getStringExtra(PROJECT_NAME);

        phone1 = intent.getStringExtra(PHONE);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);

        Log.d(TAG, "" + phone1 + " " + address + " " + ktpid);

        if (phone1 != null && address != null && ktpid != null) {
            editPhone = (EditText) findViewById(R.id.edit_phone);
            editAddress = (EditText) findViewById(R.id.edit_address);
            editKtpId = (EditText) findViewById(R.id.edit_ktp_id);

            editPhone.setText(phone1);
            editAddress.setText(address);
            editKtpId.setText(ktpid);
        }

        status = intent.getStringExtra(STATUS);

        Log.d(TAG, " " + dbMasterRef + "-" + projectRef + "-" + memberCustomerRef + "-" + nupAmt);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobile = (EditText) findViewById(R.id.edit_mobile);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        editAddress = (EditText) findViewById(R.id.edit_address);
        editKtpId = (EditText) findViewById(R.id.edit_ktp_id);

        editQty = (EditText) findViewById(R.id.edit_qty);

        txtPriortyPrice = (TextView) findViewById(R.id.txt_priority_pass);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnUploadKtp = (Button) findViewById(R.id.btn_upload_ktp);
        btnUploadKtp.setTypeface(font);
        btnUploadNpwp = (Button) findViewById(R.id.btn_upload_npwp);
        btnUploadNpwp.setTypeface(font);
        btnNext = (Button) findViewById(R.id.btn_next);

        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        txtPriortyPrice.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(nupAmt))));

        editQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                defaultQty = editQty.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("cek s", " " + s);
                generateTotal(s.toString());
            }
        });

        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                selectImage();
            }
        });

        imgNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                selectImage();
            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = editQty.getText().toString();
                String cekFullname = editFullname.getText().toString();
                String cekPhone = editPhone.getText().toString();
                String cekMobile = editMobile.getText().toString();
                String cekEmail = editEmail.getText().toString();
                String cekAddress = editAddress.getText().toString();
                String cekKtpId = editKtpId.getText().toString();

                if (!qty.isEmpty() && !cekFullname.isEmpty() && !cekPhone.isEmpty() && !cekMobile.isEmpty()
                        && !cekEmail.isEmpty() && !cekAddress.isEmpty() && !cekKtpId.isEmpty() && !ktpRef.equals("")
                        && !npwpRef.equals("")) {

                    Intent intent = new Intent(NupInformasiCustomerActivity.this, NupProductInfoActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                    intent.putExtra(PROJECT_NAME, projectName);

                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(KTP_REF, ktpRef);
                    intent.putExtra(NPWP_REF, npwpRef);

                    intent.putExtra(QTY, editQty.getText().toString());
                    intent.putExtra(TOTAL, txtTotal.getText().toString());
                    startActivity(intent);
                } else {
                    if (editQty.getText().toString().equals("")) {
                        editQty.setError("Qty tidak boleh kosong");
                    } else {
                        editQty.setError(null);
                    }

                    if (cekFullname.isEmpty()) {
                        editFullname.setError(getResources().getString(R.string.txt_no_fullname));
                    } else {
                        editFullname.setError(null);
                    }

                    if (cekPhone.isEmpty()) {
                        editPhone.setError(getResources().getString(R.string.txt_no_phone));
                    } else {
                        editPhone.setError(null);
                    }

                    if (cekMobile.isEmpty()) {
                        editMobile.setError(getResources().getString(R.string.txt_no_mobile));
                    } else {
                        editMobile.setError(null);
                    }

                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail.setError(getResources().getString(R.string.txt_no_email));
                    } else {
                        editEmail.setError(null);
                    }

                    if (cekAddress.isEmpty()) {
                        editAddress.setError(getResources().getString(R.string.txt_no_address));
                    } else {
                        editAddress.setError(null);
                    }

                    if (cekKtpId.isEmpty()) {
                        editKtpId.setError(getResources().getString(R.string.txt_no_ktp_id));
                    } else {
                        editKtpId.setError(null);
                    }

                    if (ktpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_ktp)), Toast.LENGTH_LONG).show();
                    } else if (npwpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_npwp)), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private void generateTotal(String s) {
        String qtyValue = s.toString();
        if (qtyValue.equals("")) {
            qtyValue = "0";
        }

        double qty = Double.parseDouble(qtyValue.toString());
        double price = Double.parseDouble(nupAmt);
        gTotal = qty * price;
        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        String totalPrice = String.valueOf(decimalFormat.format(gTotal));
        txtTotal.setText("IDR " + totalPrice);
    }

    @Override
    protected void onResume() {
        super.onResume();
        generateTotal(editQty.getText().toString());
        requestPsInfo(memberCustomerRef);
    }

    private void requestPsInfo(final String memberCustomerRef) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int statusGet = jo.getInt("status");
                    String message = jo.getString("message");

                    if (statusGet == 200) {
                        //reuired
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        phone1 = jo.getJSONObject("data").getString("phone1");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        email1 = jo.getJSONObject("data").getString("email1");
                        address = jo.getJSONObject("data").getString("idAddr");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        if (!fullname.isEmpty()) {
                            editFullname.setText(fullname);
                            editFullname.setEnabled(false);
                        } else {
                            editFullname.setEnabled(true);
                        }

                        if (!email1.isEmpty()) {
                            editEmail.setText(email1);
                            editEmail.setEnabled(false);
                        } else {
                            editEmail.setEnabled(true);
                        }

                        if (!ktpid.isEmpty()) {
                            editKtpId.setText(ktpid);
                            editKtpId.setEnabled(false);
                        } else {
                            editKtpId.setEnabled(true);
                        }

                        if (!mobile1.isEmpty()) {
                            editMobile.setText(mobile1);
                            editMobile.setEnabled(false);
                        } else {
                            editMobile.setEnabled(true);
                        }

                        if (!phone1.isEmpty()) {
                            editPhone.setText(phone1);
                            editPhone.setEnabled(false);
                        } else {
                            editPhone.setEnabled(true);
                        }

                        if (!address.isEmpty()) {
                            editAddress.setText(address);
                            editAddress.setEnabled(false);
                        } else {
                            editAddress.setEnabled(true);
                        }

                        if (ktpRef.equals("")) {
                            imgKtp.setEnabled(true);
                        } else {
                            imgKtp.setEnabled(false);
                        }

                        if (npwpRef.equals("")) {
                            imgNpwp.setEnabled(true);
                        } else {
                            imgNpwp.setEnabled(false);
                        }

                        Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgKtp);

                        Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgNpwp);

                        Log.d("KTP/NPWP", " " + WebService.getKtp() + ktpRef + " " + WebService.getNpwp() + npwpRef);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        btnNext.setEnabled(false);

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupInformasiCustomerActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupInformasiCustomerActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberCustomerRef);
                Log.d("TAG Member Ref", memberCustomerRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NupInformasiCustomerActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra("type", type);
                    startActivityForResult(intent, REQUEST_CAMERA);*/
                    if (ContextCompat.checkSelfPermission(NupInformasiCustomerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(NupInformasiCustomerActivity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(NupInformasiCustomerActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        file = Uri.fromFile(getOutputMediaFile());
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    /*Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image*//*");
                    intent.putExtra("type", type);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Foto"),
                            SELECT_FILE);*/
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    intent.putExtra("type", type);
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                //handleGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                    //Toast.makeText(EditProfileActivity.this, "18", Toast.LENGTH_SHORT).show();
                } else {
                    handleGalleryResult19(data);
                    //Toast.makeText(EditProfileActivity.this, "19", Toast.LENGTH_SHORT).show();
                }
            else if (requestCode == REQUEST_CAMERA)
                //onCaptureImageResult(data);
                if (type != null && type.equals("2")) {
                    Intent intent = new Intent(this, NupKtpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (ktpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else if (type != null && type.equals("3")) {
                    Intent intent = new Intent(this, NupNpwpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (npwpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void handleGalleryResult19(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getPath(selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("2")) {
                    Intent intent = new Intent(this, NupKtpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (ktpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else if (type != null && type.equals("3")) {
                    Intent intent = new Intent(this, NupNpwpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (npwpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("2")) {
                        Intent intent = new Intent(this, NupKtpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        if (ktpRef.equals("")) {
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(intent);
                    } else if (type != null && type.equals("3")) {
                        Intent intent = new Intent(this, NupNpwpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        if (npwpRef.equals("")) {
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(intent);
                    } else {
                        Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("2")) {
                    Intent intent = new Intent(this, NupKtpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (ktpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else if (type != null && type.equals("3")) {
                    Intent intent = new Intent(this, NupNpwpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    if (npwpRef.equals("")) {
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    //i.putExtra("Image", byteArray);
                    startActivity(intent);
                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("2")) {
                        Intent intent = new Intent(this, NupKtpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        if (ktpRef.equals("")) {
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(intent);
                    } else if (type != null && type.equals("3")) {
                        Intent intent = new Intent(this, NupNpwpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        if (npwpRef.equals("")) {
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(intent);
                    } else {
                        Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = { MediaStore.Images.Media.DATA };
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[] { id }, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private void onCaptureImageResult(Intent data) {
        Log.d("Type Camera", " " + type);

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (type != null && type.equals("2")) {
            Intent intent = new Intent(this, NupKtpActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_AMT, nupAmt);
            intent.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                intent.putExtra(PHONE, editPhone.getText().toString());
                intent.putExtra(ADDRESS, editAddress.getText().toString());
                intent.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            intent.putExtra("Image", byteArray);
            startActivity(intent);
        } else if (type != null && type.equals("3")) {
            Intent intent = new Intent(this, NupNpwpActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_AMT, nupAmt);
            intent.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                intent.putExtra(PHONE, editPhone.getText().toString());
                intent.putExtra(ADDRESS, editAddress.getText().toString());
                intent.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            intent.putExtra("Image", byteArray);
            startActivity(intent);
        } else {
            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Log.d("Type Galery", " " + type);

        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        if (type != null && type.equals("2")) {
            Intent intent = new Intent(this, NupKtpActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_AMT, nupAmt);
            intent.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                intent.putExtra(PHONE, editPhone.getText().toString());
                intent.putExtra(ADDRESS, editAddress.getText().toString());
                intent.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            intent.putExtra("Image", byteArray);
            startActivity(intent);
        } else if (type != null && type.equals("3")) {
            Intent intent = new Intent(this, NupNpwpActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_AMT, nupAmt);
            intent.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                intent.putExtra(PHONE, editPhone.getText().toString());
                intent.putExtra(ADDRESS, editAddress.getText().toString());
                intent.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            intent.putExtra("Image", byteArray);
            startActivity(intent);
        } else {
            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupInformasiCustomerActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
