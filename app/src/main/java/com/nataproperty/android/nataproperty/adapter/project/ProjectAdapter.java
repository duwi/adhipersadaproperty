package com.nataproperty.android.nataproperty.adapter.project;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.model.project.ProjectModel2;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.ProjectMenuActivity;
import com.nataproperty.android.nataproperty.view.project.ProjectActivity;
import com.nataproperty.android.nataproperty.view.project.ValidateInhouseCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 4/19/2016.
 */
public class ProjectAdapter extends BaseAdapter implements Filterable {

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    private Context context;
    private List<ProjectModel2> list;
    public List<ProjectModel2> orig;

    private ListProjectHolder holder;

    private Display display;

    String memberRef, dbMasterRef, projectRef;

    public ProjectAdapter(Context context, List<ProjectModel2> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_project, null);
            holder = new ListProjectHolder();
            holder.projectName = (TextView) convertView.findViewById(R.id.projectName);
            holder.locationName = (TextView) convertView.findViewById(R.id.locationName);
            holder.subLocationName = (TextView) convertView.findViewById(R.id.subLocationName);
            holder.projectImg = (ImageView) convertView.findViewById(R.id.imgProject);
            holder.btnJoined = (ImageView) convertView.findViewById(R.id.btn_joined);
            holder.btn_project = (ImageView) convertView.findViewById(R.id.btnProject);

            convertView.setTag(holder);
        } else {
            holder = (ListProjectHolder) convertView.getTag();
        }

        final ProjectModel2 project = list.get(position);
        holder.projectName.setText(project.getProjectName().toUpperCase());
        holder.locationName.setText(project.getLocationName());
        holder.subLocationName.setText(project.getSubLocationName());

        if (project.getIsJoin().equals("0")) {
            holder.btn_project.setVisibility(View.VISIBLE);
            holder.btnJoined.setVisibility(View.GONE);
        } else {
            holder.btnJoined.setVisibility(View.VISIBLE);
            holder.btn_project.setVisibility(View.GONE);
        }

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        holder.projectImg.setLayoutParams(params);
        holder.projectImg.requestLayout();
        Glide.with(context).load(project.getImage()).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.projectImg);

        holder.projectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProjectMenuActivity.class);
                intent.putExtra(PROJECT_REF, project.getProjectRef());
                intent.putExtra(DBMASTER_REF, project.getDbMasterRef());
                intent.putExtra(PROJECT_NAME, project.getProjectName());
                intent.putExtra(LOCATION_NAME, project.getLocationName());
                intent.putExtra(LOCATION_REF, project.getLocationRef());
                intent.putExtra(SUBLOCATION_NAME, project.getSubLocationName());
                intent.putExtra(SUBLOCATION_REF, project.getSublocationRef());
                context.startActivity(intent);
            }
        });

        holder.btn_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("position ref", memberRef + " " + list.get(position).getDbMasterRef() + " " +
                        list.get(position).getProjectRef());

                dbMasterRef = String.valueOf(project.getDbMasterRef());
                projectRef = project.getProjectRef();

                android.support.v7.app.AlertDialog.Builder alertDialogBuilder =
                        new android.support.v7.app.AlertDialog.Builder(context);
                alertDialogBuilder.setMessage(R.string.subscribe);
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        subscribeProject();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        return convertView;
    }

    private class ListProjectHolder {
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btn_project, btnJoined;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProjectModel2> results = new ArrayList<ProjectModel2>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ProjectModel2 g : orig) {
                            if (g.getProjectName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                            else if (g.getLocationName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                            else if (g.getSubLocationName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<ProjectModel2>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void subscribeProject() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.subscribeProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result detail project", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, ProjectActivity.class);
                        ((Activity) context).finish();
                        context.startActivity(intent);
                    } else if (status == 201) {
                        Intent intent = new Intent(context, ValidateInhouseCode.class);
                        intent.putExtra("memberRef", memberRef);
                        intent.putExtra("dbMasterRef", dbMasterRef);
                        intent.putExtra("projectRef", projectRef);
                        context.startActivity(intent);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef.toString());
                params.put("projectRef", projectRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "project");

    }

}
