package com.nataproperty.android.nataproperty.presenter;

import com.nataproperty.android.nataproperty.interactor.PresenterIlustrationCategoryInteractor;
import com.nataproperty.android.nataproperty.model.project.CategoryModel;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.view.ilustration.IlustrationCategoryActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationCategoryPresenter implements PresenterIlustrationCategoryInteractor {
    private IlustrationCategoryActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public IlustrationCategoryPresenter(IlustrationCategoryActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListCategory(String dbMasterRef, String projectRef) {
        Call<List<CategoryModel>> call = service.getAPI().getIlusCategory(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                view.showListCategoryResults(response);
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                view.showListCategoryFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
