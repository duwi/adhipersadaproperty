package com.nataproperty.android.nataproperty.model.nup;

/**
 * Created by User on 5/21/2016.
 */
public class AccountBankModel {
    String bankName,accName,accNo;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }
}
