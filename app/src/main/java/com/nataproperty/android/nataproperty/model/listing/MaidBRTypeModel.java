package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by Nata on 12/6/2016.
 */

public class MaidBRTypeModel {
    String maidBRType;
    String maidBRTypeName;

    public String getMaidBRType() {
        return maidBRType;
    }

    public void setMaidBRType(String maidBRType) {
        this.maidBRType = maidBRType;
    }

    public String getMaidBRTypeName() {
        return maidBRTypeName;
    }

    public void setMaidBRTypeName(String maidBRTypeName) {
        this.maidBRTypeName = maidBRTypeName;
    }
}
