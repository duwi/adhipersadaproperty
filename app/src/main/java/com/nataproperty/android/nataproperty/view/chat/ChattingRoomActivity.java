package com.nataproperty.android.nataproperty.view.chat;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.gcm.ConfigGCM;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.model.ilustration.DaoSession;
import com.nataproperty.android.nataproperty.model.project.ChatRoomModel;
import com.nataproperty.android.nataproperty.model.project.ChatRoomModelDao;
import com.nataproperty.android.nataproperty.network.ServiceRetrofit;
import com.nataproperty.android.nataproperty.presenter.ChattingRoomPresenter;
import com.nataproperty.android.nataproperty.view.project.card.RVChatRoomAdapter;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by nata on 11/17/2016.
 */

public class ChattingRoomActivity extends AppCompatActivity implements View.OnClickListener,EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener {


    public static final String PREF_NAME = "pref" ;
    private List<ChatRoomModel> chatRoomModels= new ArrayList<>();
    private List<ChatRoomModel> chatRoomModels1= new ArrayList<>();
    public static final String TAG = "ListingMainActivity" ;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
//    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
//    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private ChattingRoomPresenter presenter;
    ProgressDialog progressDialog;
    String memberRefSender,memberRefReceiver,messaging,name,memberName;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    RecyclerView rvChatRoom;
    EditText textChat;
    ImageView btnBack,btnSendChatting,photoProfile;
//    Display display;
    Context context;
    TextView textUsername;
    SharedPreferences sharedPreferences;
    public static boolean OFF_LINE_MODE = false;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    RVChatRoomAdapter adapter;
    Handler handler;
    Runnable r;
    Button btnEmoticon;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ChattingRoomPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRefSender = sharedPreferences.getString("isMemberRef",null );
        memberName = sharedPreferences.getString("isName",null );
//        name = sharedPreferences.getString("isName", null);
        context = this;
        Intent intent = getIntent();
        memberRefReceiver = intent.getStringExtra("memberRefReceiver");
        name = intent.getStringExtra("name");
        messaging="";
        initWidget();
        toolbarWidget();
        initializeAdapter();
        chatRoomIn();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigGCM.PUSH_NOTIFICATION)) {
                    // new push message is received
                    handlePushNotification(intent);
                }
            }
        };
//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                chatRoomIn();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);
//        delayedStart(3000);


//        chatRoomIn();
//        textUsername.setText(name);
        Glide.with(context).load(WebService.getProfile() + memberRefReceiver).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                .into(new BitmapImageViewTarget(photoProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                photoProfile.setImageDrawable(circularBitmapDrawable);
            }
        });

        btnBack.setOnClickListener(this);
        btnSendChatting.setOnClickListener(this);
        btnEmoticon.setOnClickListener(this);
        textChat.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    chatRoom();
                    return true;
                }
                return false;
            }
        });


    }

    private void handlePushNotification(Intent intent) {
//        Message message = (Message) intent.getSerializableExtra("message");
        String message = intent.getStringExtra("message");

        if (message != null) {
            chatRoomIn();
//            chatRoomModels1.add(message);
//            mAdapter.notifyDataSetChanged();
//            if (mAdapter.getItemCount() > 1) {
//                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
//            }
        }

    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // registering the receiver for new notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ConfigGCM.PUSH_NOTIFICATION));

//        NotificationUtils.clearNotifications();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        chatRoomModels.clear();
//        chatRoomIn();
//    }

    private void toolbarWidget() {

        title.setText(name);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }


    private void chatRoom() {
        chatRoomModels1.clear();
        presenter.getSendMsg(memberRefSender, memberRefReceiver, messaging);
    }
    private void chatRoomIn() {
        chatRoomModels1.clear();
        presenter.getSendMsg(memberRefSender, memberRefReceiver, "");
    }

    private void initializeAdapter() {
//        int count = chatRoomModels.size();



//        rvChatRoom.removeAllViews();

//        adapter.notifyDataSetChanged();

        adapter = new RVChatRoomAdapter(this, chatRoomModels1);
//        rvChatRoom.scrollToPosition(count);
        rvChatRoom.setAdapter(adapter);


    }

    private void initWidget() {
        btnEmoticon=(Button)findViewById(R.id.btn_emoticon);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.idtxt_user_name);
        photoProfile = (ImageView) toolbar.findViewById(R.id.image_user);
        btnBack = (ImageView) toolbar.findViewById(R.id.ic_menu_back);
//        textUsername = (TextView)findViewById(R.id.txt_user_name);
        rvChatRoom = (RecyclerView) findViewById(R.id.rv_myproject);
        textChat  = (EditText)findViewById(R.id.edit_chat);
        LinearLayoutManager llm = new LinearLayoutManager(this);
//        llm.setSmoothScrollbarEnabled(true);
//        llm.setStackFromEnd(false);
        rvChatRoom.setItemAnimator(new DefaultItemAnimator());
        rvChatRoom.setLayoutManager(llm);

//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mAdapter);

//        rvChatRoom.setItemAnimator(new DefaultItemAnimator());

//        btnBack = (ImageView) findViewById(R.id.ic_menu_back);
        btnSendChatting=(ImageView)findViewById(R.id.btn_send_chatting);
//        photoProfile = (ImageView) findViewById(R.id.image_user);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_emoticon:
                setEmojiconFragment(false);
                 break;
            case R.id.ic_menu_back:
                handler.removeCallbacksAndMessages(r);
                finish();
                break;
            case R.id.btn_send_chatting:
                if(OFF_LINE_MODE){
                    Toast.makeText(getApplicationContext(), "Silahkan cek koneksi anda", Toast.LENGTH_LONG).show();
                }else {
                    messaging = textChat.getText().toString();
                    chatRoom();
                    textChat.getText().clear();
                }
                break;

        }

    }

    public void showChatResults(Response<List<ChatRoomModel>> response) {
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        OFF_LINE_MODE = false;
        chatRoomModels = response.body();


        for (int i = 0; i < chatRoomModels.size(); i++) {
            ChatRoomModel roomModel = new ChatRoomModel();
            roomModel.setChatMessage(response.body().get(i).getChatMessage());
            roomModel.setMemberRefReceiver(response.body().get(i).getMemberRefReceiver());
            roomModel.setMemberRefSender(response.body().get(i).getMemberRefSender());
            roomModel.setStatus(response.body().get(i).getStatus());
            roomModel.setSendTime(response.body().get(i).getSendTime());
            roomModel.setReadMessage(response.body().get(i).getReadMessage());
            roomModel.setIdChat(i);
            ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
            chatRoomModelDao.insertOrReplace(roomModel);
            chatRoomModels1.add(roomModel);
            adapter.notifyDataSetChanged();
            if (adapter.getItemCount() > 1) {
                // scrolling to bottom of the recycler view
                rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
            }

//            rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, chatRoomModels.size() - 1);
//            chatRoomModels1.add(roomModel);
//            adapter.notifyDataSetChanged();
//            if (adapter.getItemCount() > 1) {
//                // scrolling to bottom of the recycler view
//                rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
//            }
        }


//        initializeAdapter();
    }

    public void showChatFailure(Throwable t) {
        OFF_LINE_MODE = true;
        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
        List<ChatRoomModel> chatRoomModelss = chatRoomModelDao.queryBuilder().where(ChatRoomModelDao.Properties.MemberRefSender.eq(memberName))
                .orderDesc(ChatRoomModelDao.Properties.IdChat)
                .list();
        Log.d("getProjectLocalDB", "" + chatRoomModelss.toString());
        int numData = chatRoomModelss.size();
        if (numData > 0) {
            for (int i = 0; i < numData; i++) {
                ChatRoomModel roomModel = chatRoomModelss.get(i);
                chatRoomModels.add(roomModel);
                adapter.notifyDataSetChanged();
                if (adapter.getItemCount() > 1) {
                    // scrolling to bottom of the recycler view
                    rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
                }
//                initializeAdapter();


            }
        }

    }
    /**
     * Set the Emoticons in Fragment.
     * @param useSystemDefault
     */
    private void setEmojiconFragment(boolean useSystemDefault) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    /**
     * It called, when click on icon of Emoticons.
     * @param emojicon
     */
    @Override
    public void onEmojiconClicked(Emojicon emojicon) {

        EmojiconsFragment.input(textChat, emojicon);
    }

    /**
     * It called, when backspace button of Emoticons pressed
     * @param view
     */
    @Override
    public void onEmojiconBackspaceClicked(View view) {

        EmojiconsFragment.backspace(textChat);
    }
}
