package com.nataproperty.android.nataproperty.model.listing;

/**
 * Created by User on 9/28/2016.
 */
public class ListingDownloadModel {
    String docRef,docFN,linkDownload,extension;

    public String getDocRef() {
        return docRef;
    }

    public void setDocRef(String docRef) {
        this.docRef = docRef;
    }

    public String getDocFN() {
        return docFN;
    }

    public void setDocFN(String docFN) {
        this.docFN = docFN;
    }

    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
