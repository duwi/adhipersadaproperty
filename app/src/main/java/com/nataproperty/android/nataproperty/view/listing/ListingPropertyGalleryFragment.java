package com.nataproperty.android.nataproperty.view.listing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.listing.ListingPropertyGalleryAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyGridView;
import com.nataproperty.android.nataproperty.model.listing.ListingPropertyGalleryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ListingPropertyGalleryFragment extends Fragment {
    public static final String TAG = "PropertyGalleryFragment";

    public ListingPropertyGalleryFragment() {
        // Required empty public constructor
    }

    String listingRef, agencyCompanyRef, imageLogo;

    private ArrayList<ListingPropertyGalleryModel> listGallery = new ArrayList<ListingPropertyGalleryModel>();
    private ListingPropertyGalleryAdapter adapter;
    private MyGridView listView;
    private LinearLayout linearLayoutNoData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        listingRef = intent.getStringExtra("listingRef");

        requestPropertyGallery(listingRef);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_listing_property_gallery, container, false);


        Display display = getActivity().getWindowManager().getDefaultDisplay();

        linearLayoutNoData = (LinearLayout) rootView.findViewById(R.id.linear_no_data);
        listView = (MyGridView) rootView.findViewById(R.id.list_property_gallery);
        adapter = new ListingPropertyGalleryAdapter(getActivity(), listGallery, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //requestPropertyInfo(propertyRef);
    }


    public void requestPropertyGallery(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListListingPropertyGallery(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()!=0){
                        generateGallery(jsonArray);
                        linearLayoutNoData.setVisibility(View.GONE);
                    } else {
                        linearLayoutNoData.setVisibility(View.VISIBLE);
                    }




                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(getContext(), getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "galleryProperty");

    }

    private void generateGallery(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingPropertyGalleryModel propertyGalleryModel = new ListingPropertyGalleryModel();
                propertyGalleryModel.setImgRef(jo.getString("imageRef"));
                propertyGalleryModel.setImgTitle(jo.getString("imageTitle"));
                propertyGalleryModel.setImgLink(jo.getString("imageFile"));
                propertyGalleryModel.setPropertyRef(listingRef);

                listGallery.add(propertyGalleryModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

}
