package com.nataproperty.android.nataproperty.model.listing;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nata on 11/28/2016.
 */
@Entity
public class LIstBlockDiagramModel {
    @Id
    long id;
    String blockName;
    String unitMaping;
    String productRefList;
    String unitStatusList;
    String unitRefList;
    String productNameList;

    @Generated(hash = 2145301268)
    public LIstBlockDiagramModel(long id, String blockName, String unitMaping,
            String productRefList, String unitStatusList, String unitRefList,
            String productNameList) {
        this.id = id;
        this.blockName = blockName;
        this.unitMaping = unitMaping;
        this.productRefList = productRefList;
        this.unitStatusList = unitStatusList;
        this.unitRefList = unitRefList;
        this.productNameList = productNameList;
    }

    @Generated(hash = 1857501235)
    public LIstBlockDiagramModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnitMaping() {
        return unitMaping;
    }

    public void setUnitMaping(String unitMaping) {
        this.unitMaping = unitMaping;
    }

    public String getProductRefList() {
        return productRefList;
    }

    public void setProductRefList(String productRefList) {
        this.productRefList = productRefList;
    }

    public String getUnitStatusList() {
        return unitStatusList;
    }

    public void setUnitStatusList(String unitStatusList) {
        this.unitStatusList = unitStatusList;
    }

    public String getUnitRefList() {
        return unitRefList;
    }

    public void setUnitRefList(String unitRefList) {
        this.unitRefList = unitRefList;
    }

    public String getProductNameList() {
        return productNameList;
    }

    public void setProductNameList(String productNameList) {
        this.productNameList = productNameList;
    }
}
