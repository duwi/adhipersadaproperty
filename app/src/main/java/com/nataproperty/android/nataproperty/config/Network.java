package com.nataproperty.android.nataproperty.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by User on 4/25/2016.
 */
public class Network {


    public static String PostHttp(String url, String parameter){
        String respone = "Failed";

        try {
            URL urlToRequest = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            HttpURLConnection.setFollowRedirects(true);

            urlConnection.setConnectTimeout(100000);//5second
            urlConnection.setReadTimeout(100000);
            urlConnection.setDoOutput(true);

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            urlConnection.setFixedLengthStreamingMode(parameter.getBytes().length);
            urlConnection.connect();

            PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
            out.print(parameter);
            out.close();
            //baca
            BufferedReader reader = null;
            out = null;

            try {
                if(urlConnection.getInputStream()!=null){
                    reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()),512);
                }
            }catch (IOException e){
                if (urlConnection.getErrorStream()!=null){
                    reader = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()),512);
                }
            }

            if (reader !=null){
                respone = toString(reader);
                reader.close();
            }

            return respone;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return respone;
    }

    private static String toString (BufferedReader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine())!=null){
            sb.append(line).append('\n');
        }
        return sb.toString();
    }

}
