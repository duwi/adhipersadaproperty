package com.nataproperty.android.nataproperty.model.ilustration;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 5/13/2016.
 */
@Entity
public class IlustrationModel {
    @Id
    long unitRef;
    String unitNo;
    String area;
    String priceInc;
    String salesDisc;
    String netPrice;
    String dbMasterRef;
    String projectRef;
    String categoryRef;
    String clusterRef;
    String productRef;


    @Generated(hash = 106306000)
    public IlustrationModel(long unitRef, String unitNo, String area,
            String priceInc, String salesDisc, String netPrice, String dbMasterRef,
            String projectRef, String categoryRef, String clusterRef,
            String productRef) {
        this.unitRef = unitRef;
        this.unitNo = unitNo;
        this.area = area;
        this.priceInc = priceInc;
        this.salesDisc = salesDisc;
        this.netPrice = netPrice;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.categoryRef = categoryRef;
        this.clusterRef = clusterRef;
        this.productRef = productRef;
    }

    @Generated(hash = 960776142)
    public IlustrationModel() {
    }


    public long getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(long unitRef) {
        this.unitRef = unitRef;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getSalesDisc() {
        return salesDisc;
    }

    public void setSalesDisc(String salesDisc) {
        this.salesDisc = salesDisc;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }
}
