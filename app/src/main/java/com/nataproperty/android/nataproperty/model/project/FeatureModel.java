package com.nataproperty.android.nataproperty.model.project;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 5/15/2016.
 */
@Entity
public class FeatureModel {
    @Id

    long  projectFeature;
    String dbMasterRef, projectRef,projectFeatureName;

    @Generated(hash = 1136050529)
    public FeatureModel(long projectFeature, String dbMasterRef, String projectRef,
            String projectFeatureName) {
        this.projectFeature = projectFeature;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.projectFeatureName = projectFeatureName;
    }

    @Generated(hash = 1944918990)
    public FeatureModel() {
    }

    public long getProjectFeature() {
        return projectFeature;
    }

    public void setProjectFeature(long projectFeature) {
        this.projectFeature = projectFeature;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectFeatureName() {
        return projectFeatureName;
    }

    public void setProjectFeatureName(String projectFeatureName) {
        this.projectFeatureName = projectFeatureName;
    }
}
