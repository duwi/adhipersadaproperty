package com.nataproperty.android.nataproperty.view.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.android.nataproperty.R;
import com.nataproperty.android.nataproperty.adapter.profile.SelectorAdapter;
import com.nataproperty.android.nataproperty.config.BaseApplication;
import com.nataproperty.android.nataproperty.config.WebService;
import com.nataproperty.android.nataproperty.helper.MyTextViewLatoReguler;
import com.nataproperty.android.nataproperty.utils.LoadingBar;
import com.nataproperty.android.nataproperty.view.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 4/25/2016.
 */
public class SectionProfileActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String KEY_MEMBERREF = "memberRef";

    public static String[] listSectionMenu = {"Profile", "About Me", "Address", "Bank", "Others"};
    private ArrayList<String> listMenu = new ArrayList<>();

    private ListView listSection;
    //private String[] listMenu;
    //private ArrayAdapter adapter;

    private SharedPreferences sharedPreferences;

    String memberRef, memberType;

    String fullname, ktpid, birthPlace, mobile1, mobile2, email1, email2, birthdate, npwp, ktpRef, npwpRef;
    String idCountryCode, idProvinceCode, idCityCode, idAddr, idPostCode, proviceSortNo, citySortNo, countrySortNo,
            proviceSortNoCorres, citySortNoCorres, countrySortNoCorres, corresPostCode, corresAddr,corresCountryCode,
            corresProvinceCode,corresCityCode;
    String aboutMe, quotes;
    String bloodType, fax, passportID, simid;
    String nationSortNo, typeSortNo, regionSortNo, sexSortNo, occupationSortNo, jobTitleSortNo, maritalStatusSortNo;
    String bankRef, bankBranch, accName, accNo;

    static SectionProfileActivity sectionProfileActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_profile));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);

        sectionProfileActivity = this;

        listMenu.add(0, "Profile");
        listMenu.add(1, "About Me");
        listMenu.add(2, "Address");
        listMenu.add(3, "Bank");
        listMenu.add(4, "Others");

        //hide bank if agent property
        if (memberType.startsWith("Property")) {
            listMenu.remove(3);
        }

        listSection = (ListView) findViewById(R.id.list_section_profile);

        //listSection.setAdapter(new SelectorAdapter(this, listSectionMenu));
        listSection.setAdapter(new SelectorAdapter(this, listMenu));
        listSection.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String listName = listMenu.get(position);

                if (listName.equals("Profile")) {
                    Intent intentEditProfile = new Intent(SectionProfileActivity.this, EditProfileActivity.class);
                    intentEditProfile.putExtra("memberRef", memberRef);
                    intentEditProfile.putExtra("fullname", fullname);
                    intentEditProfile.putExtra("ktpid", ktpid);
                    intentEditProfile.putExtra("birthPlace", birthPlace);
                    intentEditProfile.putExtra("mobile1", mobile1);
                    intentEditProfile.putExtra("mobile2", mobile2);
                    intentEditProfile.putExtra("email1", email1);
                    intentEditProfile.putExtra("email2", email2);
                    intentEditProfile.putExtra("birthdate", birthdate);
                    intentEditProfile.putExtra("npwp", npwp);
                    intentEditProfile.putExtra("ktpRef", ktpRef);
                    intentEditProfile.putExtra("npwpRef", npwpRef);
                    startActivity(intentEditProfile);
                } else if (listName.equals("About Me")) {
                    Intent intentAboutMe = new Intent(SectionProfileActivity.this, AboutMeActivity.class);
                    intentAboutMe.putExtra("memberRef", memberRef);
                    intentAboutMe.putExtra("aboutMe", aboutMe);
                    intentAboutMe.putExtra("quotes", quotes);
                    startActivity(intentAboutMe);
                } else if (listName.equals("Address")) {
                    Intent intentAddr = new Intent(SectionProfileActivity.this, Address2Activity.class);
                    intentAddr.putExtra("memberRef", memberRef);
                    intentAddr.putExtra("idCountryCode", idCountryCode);
                    intentAddr.putExtra("idProvinceCode", idProvinceCode);
                    intentAddr.putExtra("idCityCode", idCityCode);
                    intentAddr.putExtra("idPostCode", idPostCode);
                    intentAddr.putExtra("idAddr", idAddr);
                    intentAddr.putExtra("provinceSortNo", proviceSortNo);
                    intentAddr.putExtra("citySortNo", citySortNo);
                    intentAddr.putExtra("countrySortNo", countrySortNo);

                    intentAddr.putExtra("corresCountryCode", corresCountryCode);
                    intentAddr.putExtra("corresProvinceCode", corresProvinceCode);
                    intentAddr.putExtra("corresCityCode", corresCityCode);
                    intentAddr.putExtra("corresPostCode", corresPostCode);
                    intentAddr.putExtra("corresAddr", corresAddr);

                    intentAddr.putExtra("proviceSortNoCorres", proviceSortNoCorres);
                    intentAddr.putExtra("citySortNoCorres", citySortNoCorres);
                    intentAddr.putExtra("countrySortNoCorres", countrySortNoCorres);
                    intentAddr.putExtra("corresPostCode", corresPostCode);
                    intentAddr.putExtra("corresAddr", corresAddr);
                    startActivity(intentAddr);
                } else if (listName.equals("Bank")) {
                    Intent intentAccountBank = new Intent(SectionProfileActivity.this, AccountBankActivity.class);
                    intentAccountBank.putExtra("memberRef", memberRef);
                    intentAccountBank.putExtra("bankRef", bankRef);
                    intentAccountBank.putExtra("bankBranch", bankBranch);
                    intentAccountBank.putExtra("accName", accName);
                    intentAccountBank.putExtra("accNo", accNo);
                    startActivity(intentAccountBank);
                } else if (listName.equals("Others")) {
                    Intent intentOthers = new Intent(SectionProfileActivity.this, OthersEditActivity.class);
                    intentOthers.putExtra("memberRef", memberRef);
                    intentOthers.putExtra("bloodType", bloodType);
                    intentOthers.putExtra("fax", fax);
                    intentOthers.putExtra("passportID", passportID);
                    intentOthers.putExtra("simid", simid);

                    intentOthers.putExtra("nationSortNo", nationSortNo);
                    intentOthers.putExtra("typeSortNo", typeSortNo);
                    intentOthers.putExtra("regionSortNo", regionSortNo);
                    intentOthers.putExtra("sexSortNo", sexSortNo);
                    intentOthers.putExtra("occupationSortNo", occupationSortNo);
                    intentOthers.putExtra("jobTitleSortNo", jobTitleSortNo);
                    intentOthers.putExtra("maritalStatusSortNo", maritalStatusSortNo);
                    startActivity(intentOthers);
                } else {

                }
                /*switch (position) {
                    case 0:
                        Intent intentEditProfile = new Intent(SectionProfileActivity.this, EditProfileActivity.class);
                        intentEditProfile.putExtra("memberRef", memberRef);
                        intentEditProfile.putExtra("fullname", fullname);
                        intentEditProfile.putExtra("ktpid", ktpid);
                        intentEditProfile.putExtra("birthPlace", birthPlace);
                        intentEditProfile.putExtra("mobile1", mobile1);
                        intentEditProfile.putExtra("mobile2", mobile2);
                        intentEditProfile.putExtra("email1", email1);
                        intentEditProfile.putExtra("email2", email2);
                        intentEditProfile.putExtra("birthdate", birthdate);
                        intentEditProfile.putExtra("npwp", npwp);
                        intentEditProfile.putExtra("ktpRef", ktpRef);
                        intentEditProfile.putExtra("npwpRef", npwpRef);
                        startActivity(intentEditProfile);
                        break;

                    case 1:
                        Intent intentAboutMe = new Intent(SectionProfileActivity.this, AboutMeActivity.class);
                        intentAboutMe.putExtra("memberRef", memberRef);
                        intentAboutMe.putExtra("aboutMe", aboutMe);
                        intentAboutMe.putExtra("quotes", quotes);
                        startActivity(intentAboutMe);
                        break;

                    case 2:
                        Intent intentAddr = new Intent(SectionProfileActivity.this, AddressActivity.class);
                        intentAddr.putExtra("memberRef", memberRef);
                        intentAddr.putExtra("idCountryCode", idCountryCode);
                        intentAddr.putExtra("idProvinceCode", idProvinceCode);
                        intentAddr.putExtra("idCityCode", idCityCode);
                        intentAddr.putExtra("idPostCode", idPostCode);
                        intentAddr.putExtra("idAddr", idAddr);
                        intentAddr.putExtra("provinceSortNo", proviceSortNo);
                        intentAddr.putExtra("citySortNo", citySortNo);
                        intentAddr.putExtra("countrySortNo", countrySortNo);

                        intentAddr.putExtra("corresPostCode", corresPostCode);
                        intentAddr.putExtra("corresAddr", corresAddr);
                        intentAddr.putExtra("proviceSortNoCorres", proviceSortNoCorres);
                        intentAddr.putExtra("citySortNoCorres", citySortNoCorres);
                        intentAddr.putExtra("countrySortNoCorres", countrySortNoCorres);
                        intentAddr.putExtra("corresPostCode", corresPostCode);
                        intentAddr.putExtra("corresAddr", corresAddr);
                        startActivity(intentAddr);

                        Log.d("idAddr", idCountryCode + " " + fullname + " " + idProvinceCode + " " + idCityCode + " " + idPostCode + " " + idAddr);
                        Log.d("city", citySortNo);
                        break;

                    case 3:
                        Intent intentAccountBank = new Intent(SectionProfileActivity.this, AccountBankActivity.class);
                        intentAccountBank.putExtra("memberRef", memberRef);
                        intentAccountBank.putExtra("bankRef", bankRef);
                        intentAccountBank.putExtra("bankBranch", bankBranch);
                        intentAccountBank.putExtra("accName", accName);
                        intentAccountBank.putExtra("accNo", accNo);
                        startActivity(intentAccountBank);
                        break;

                    case 4:
                        Intent intentOthers = new Intent(SectionProfileActivity.this, OthersEditActivity.class);
                        intentOthers.putExtra("memberRef", memberRef);
                        intentOthers.putExtra("bloodType", bloodType);
                        intentOthers.putExtra("fax", fax);
                        intentOthers.putExtra("passportID", passportID);
                        intentOthers.putExtra("simid", simid);

                        intentOthers.putExtra("nationSortNo", nationSortNo);
                        intentOthers.putExtra("typeSortNo", typeSortNo);
                        intentOthers.putExtra("regionSortNo", regionSortNo);
                        intentOthers.putExtra("sexSortNo", sexSortNo);
                        intentOthers.putExtra("occupationSortNo", occupationSortNo);
                        intentOthers.putExtra("jobTitleSortNo", jobTitleSortNo);
                        intentOthers.putExtra("maritalStatusSortNo", maritalStatusSortNo);
                        startActivity(intentOthers);
                        break;

                }*/
            }
        });

        //requestPsInfo();
    }

    private void requestPsInfo() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        //reuired
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        birthPlace = jo.getJSONObject("data").getString("birthPlace");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        mobile2 = jo.getJSONObject("data").getString("hP2");
                        email1 = jo.getJSONObject("data").getString("email1");
                        email2 = jo.getJSONObject("data").getString("email2");
                        birthdate = jo.getJSONObject("data").getString("birthDate");
                        npwp = jo.getJSONObject("data").getString("npwp");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        //address
                        idCountryCode = jo.getJSONObject("data").getString("idCountryCode");
                        idProvinceCode = jo.getJSONObject("data").getString("idProvinceCode");
                        idCityCode = jo.getJSONObject("data").getString("idCityCode");
                        idPostCode = jo.getJSONObject("data").getString("idPostCode");
                        idAddr = jo.getJSONObject("data").getString("idAddr");

                        corresCountryCode  = jo.getJSONObject("data").getString("corresCountryCode");
                        corresProvinceCode  = jo.getJSONObject("data").getString("corresProvinceCode");
                        corresCityCode  = jo.getJSONObject("data").getString("corresCityCode");
                        corresAddr = jo.getJSONObject("data").getString("corresAddr");
                        corresPostCode = jo.getJSONObject("data").getString("corresPostCode");

                        proviceSortNo = jo.getJSONObject("data").getString("provinceSortNo");
                        citySortNo = jo.getJSONObject("data").getString("citySortNo");
                        countrySortNo = jo.getJSONObject("data").getString("countrySortNo");
                        proviceSortNoCorres = jo.getJSONObject("data").getString("corresProvinceSortNo");
                        citySortNoCorres = jo.getJSONObject("data").getString("corresCitySortNo");
                        countrySortNoCorres = jo.getJSONObject("data").getString("corresCountrySortNo");

                        //about me
                        aboutMe = jo.getJSONObject("data").getString("aboutMe");
                        quotes = jo.getJSONObject("data").getString("quotes");
                        //others
                        bloodType = jo.getJSONObject("data").getString("bloodType");
                        fax = jo.getJSONObject("data").getString("fax");
                        passportID = jo.getJSONObject("data").getString("passportID");
                        simid = jo.getJSONObject("data").getString("simid");
                        //otherSpinner
                        nationSortNo = jo.getJSONObject("data").getString("nationRef");
                        typeSortNo = jo.getJSONObject("data").getString("typeSortNo");
                        regionSortNo = jo.getJSONObject("data").getString("religionSortNo");
                        sexSortNo = jo.getJSONObject("data").getString("sexSortNo");
                        occupationSortNo = jo.getJSONObject("data").getString("occupationSortNo");
                        jobTitleSortNo = jo.getJSONObject("data").getString("jobSortNo");
                        maritalStatusSortNo = jo.getJSONObject("data").getString("maritalSortNo");

                        //accountBank
                        bankRef = jo.getJSONObject("data").getString("bankRef");
                        bankBranch = jo.getJSONObject("data").getString("bankBranch");
                        accName = jo.getJSONObject("data").getString("accName");
                        accNo = jo.getJSONObject("data").getString("accNo");

                        Log.d("LOG TAG PROFILE", ktpid + " " + birthPlace + " " + birthPlace + " " + mobile1 + " " + mobile2
                                + " " + email1 + " " + email2 + " " + birthdate);

                        Log.d("LOG TAG ADDRESS", idCountryCode + " " + idProvinceCode + " " + idCityCode + " " + idPostCode
                                + " " + idAddr + " " + proviceSortNo);

                        SharedPreferences sharedPreferences = SectionProfileActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isName", fullname);
                        editor.commit();

                        Log.d("Section profile", message);

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(getIntent());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        //Toast.makeText(SectionProfileActivity.this,"error connection",Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MEMBERREF, memberRef);
                Log.d("TAG Member Ref", memberRef);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    @Override
    protected void onResume() {

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);
        super.onResume();
        requestPsInfo();
    }

    public static SectionProfileActivity getInstance() {
        return sectionProfileActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                SharedPreferences sharedPreferences = SectionProfileActivity.this.
                        getSharedPreferences(PREF_NAME, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isName", fullname);
                editor.commit();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(SectionProfileActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                SharedPreferences sharedPreferencess = SectionProfileActivity.this.
                        getSharedPreferences(PREF_NAME, 0);
                SharedPreferences.Editor editorr = sharedPreferencess.edit();
                editorr.putString("isName", fullname);
                editorr.commit();
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
