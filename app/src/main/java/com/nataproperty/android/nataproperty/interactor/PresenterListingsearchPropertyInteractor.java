package com.nataproperty.android.nataproperty.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListingsearchPropertyInteractor {
    void getSubLocListing(String countryCode, String provinceCode, String cityCode);
    void getCityListing(String countryCode, String provinceCode);
    void getProvinsiListing(String countryCode);
    void getLookupListingProperty();
    void getListProperty(String agencyCompanyRef, String psRef, String listingStatus, String pageNo, String locationRef, String keyword);
    void rxUnSubscribe();

}
