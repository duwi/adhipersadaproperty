package com.nataproperty.android.nataproperty.model.nup;

/**
 * Created by User on 5/21/2016.
 */
public class BankModel {
    String bankRef,bankName;

    public String getBankRef() {
        return bankRef;
    }

    public void setBankRef(String bankRef) {
        this.bankRef = bankRef;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
